# What is This? {#mainpage}

This software is a (Re-)Implementation of the Attentional Vector Sum (AVS) model proposed by \cite Regier2001 as well as an implementation of the reversed AVS (rAVS) model proposed by \cite Kluth2017 and the two models rAVS-CoO and AVS-BB discussed in \cite Kluth2018b (to see properly rendered references and a better looking README, please see the documentation in the `doc` folder. You might have to create the documentation first, see below for instructions).
The functional extension to the AVS model proposed by \cite Carlson2006 as well as the functional extensions discussed by \cite Kluth2014 are implemented as well.
Finally, the work presented in \cite Kluth2018c was also done with this software.

For instructions on how to recompute published results, please see the corresponding commands in the text files `doc/recompute*` (you'll first need to build the program, see below).

All implemented models are cognitive models that try to specify the processes taking place when humans understand spatial language, such as _The dot is above the rectangle._

Two releases of this software are published as a data publication: \cite Kluth2016 and \cite Kluth2018.
If you are using this software, please cite it: either with the appropriate data publication and/or one of my publications (either \cite Kluth2014, \cite Kluth2017, \cite Kluth2018c or \cite Kluth2018b; [the fulltexts of all my publications (includes some conference papers) are available here](https://pub.uni-bielefeld.de/person/54885831)). For any questions, please do not hesitate to [open an issue](https://gitlab.com/tkluth/sl-model/issues) or [write me an e-mail](mailto:t.kluth@posteo.de).
My master thesis \cite Kluth2014thesis is based on a previous version of this software and my Ph.D. thesis \cite Kluth2018thesis is based on this version (and previous versions).

# How to use?

## Build

This program was successfully used under the following platforms:
- Linux: Ubuntu 12.10+, Fedora 19+
- Mac OS X: 10.7.5., 10.12

Maybe it will work on other platforms as well. Feel free to try, I'm happy to hear if it works (or to help how it could work).

### Dependencies

To compile you need:
- `cmake`
- The [CGAL-Library](https://www.cgal.org). Just install the corresponding CGAL-packages that should come with your distribution. (For Mac OS X all dependencies are available via [Homebrew](http://brew.sh/).)
- The [GNU Scientific Library](https://www.gnu.org/software/gsl). Again, installing the corresponding GSL-packages should be enough.
Note, however, that the version of GSL needs to be at least 2.2 (for `gsl_ran_multivariate_gaussian()`). In particular, ubuntu 16.04 only has GSL 2.1, so you need to compile it on your own (e.g., download GSL, `./autogen.sh; ./configure --prefix=$HOME/gsl; make; make install`). Afterwards you need to tell CMake where it finds the self-compiled GSL: `export CMAKE_PREFIX_PATH=$HOME/gsl`. Or run cmake with `cmake -DCMAKE_PREFIX_PATH=$HOME/gsl ..`
(The variables `GSL_ROOT_DIR=$HOME/gsl` or `CMAKE_LIBRARY_PATH=$HOME/gsl/lib` and `CMAKE_INCLUDE_PATH=$HOME/gsl/include` should work, too.)
- The source-code uses some C++11 routines. It should work with any `gcc >= 5`.
- [R](https://www.r-project.org/) with the package [RInside](https://cran.r-project.org/web/packages/RInside/) is used for the posterior estimation. The project won't compile without `RInside` installed!
- optional: Some computational intensive processes were parallelized with the help of [OpenMP](http://www.openmp.org). To profit from this parallelization, you need more than one CPU and the OpenMP library. If the OpenMP library is not installed, the program will still work but without support for parallelization. For me, parallelization with OpenMP did not work under Mac OS X, so this is not tested. It should work on Linux, though.

Fur running the program and using all features you need:
- For visualization you need to install [gnuplot](http://www.gnuplot.info). For full feature support you need the wxt and latex terminal. On Fedora, install the `gnuplot-wx` package.
- For simple hold-out bootstrapped confidence interval computation, you need the `R` package [boot](https://cran.r-project.org/web/packages/boot/).
- For the posterior estimation to work in runtime you need the R packages [crossmatch](https://cran.r-project.org/web/packages/crossmatch/) and [MASS](https://cran.r-project.org/web/packages/MASS/). For controlling the number of threads for the posterior estimation you need to install the package [RhpcBLASctl](https://cran.r-project.org/web/packages/RhpcBLASctl/).
- In addition, the `R` packages [ggplot2](https://cran.r-project.org/web/packages/ggplot2/), [ggmcmc](https://cran.r-project.org/web/packages/ggmcmc/), and [coda](https://cran.r-project.org/web/packages/coda/) are used to plot the output from the posterior estimation as well as the parameter distributions from a simple hold-out run.

To build the documentation, you need [doxygen](http://www.doxygen.org). If you also want a pdf-documentation, a LaTeX environment is needed.

### Compilation

Just do the following:

    mkdir build
    cd build
    cmake ..
    cmake ..
    make

I don't know why `cmake` needs to be invoked two times. When running just one time, the c++11-flags are not appended to the compiler-flags. If someone knows how to do this better, tell me!

#### Documentation

Documentation of all variables and functions can be found in the doc-folder. Depending on where you got these files from, you might have to create the documentation first. To create the documentation you need [doxygen](www.doxygen.org). Once you have installed doxygen it is as easy as this:

    cd doc
    doxygen doxygen.config
    xdg-open html/index.html

For a pdf-document add this step (assuming you have an working LaTeX environment, obviously, `make pdf` will fail otherwise):

    cd latex
    make pdf
    xdg-open refman.pdf

## Options/Usage \anchor options

Let's start with two examples:
The following command uses the wide rectangle from the first experiment conducted by \cite Regier2001 as a RO
and a LO at the position 315, -160 to compute a rating with the rAVS model (default model).
It will also show the vector used for the computation and the whole spatial template:

    ./sl-model --reference-object ../data/regiercarlson2001/exp1_wide_above.data_RO --located-object 315,-160 -v

The following command takes the empirical rating data for "above" from the first experiment conducted by \cite Regier2001 and estimates the best parameters for the AVS model (`--model 0`):

    ./sl-model --data ../data/regiercarlson2001/exp1_tall_above.data,../data/regiercarlson2001/exp1_wide_above.data --parameter-estimation --model 0 --output results.txt

The data-folder provides the ROs, LOs, and empirical ratings for \cite Regier2001, \cite Logan1996 (only estimated ROs and LOs as the exact object sizes were not reported), \cite Hayward1995 (again only estimated ROs and LOs) and the study reported in \cite Kluth2018b (in the folder `predictions`).
Furthermore, there are rating data for experiments that investigated the role of functionality of objects (\cite Carlson-Radvansky1999 and \cite Horberg2008; see also \cite Kluth2014).

Here is a summary of all available options (also available via `./sl-model --help`):

### I/O-options (see subfolders of data for example data files):

Command | Action
------- | ------
`--located-object, -l x,y` | A single, one-point LO position. This option cannot be used with `--data, -d`.
`--reference-object, -r object.file` | Pass a file containing a CGAL polygon. Will be used as RO. This option cannot be used with `--data, -d`.
`--functional, -f functional.file` | Pass a file containing a CGAL polygon. Will be used as functional part of the RO. This option cannot be used with `--data, -d`.
`--data, -d data.file` | Pass a file (or several files separated by a ,) containing rating results. These results will be used with the LO placements found in 'data.file_LOplacements' and the preposition found in 'data.file_prep'. The RO will be read from 'data.file_RO'. The rating scale will be read from 'data.file_scale'. The functional part of the reference object will be searched in 'data.file_functionalPart'. There exist four shortcuts to the following datasets: `-d regier2001` uses all rating data from \cite Regier2001, `-d kluth2018_above` uses all 'above' ratings from \cite Kluth2018b, `-d kluth2018_below` uses all below ratings from \cite Kluth2018b, `-d kluth2018_asym` uses all ratings from the asymmetrical ROs from \cite Kluth2018b and finally, `-d kluth2018_rect` uses all ratings from the rectangular ROs used in \cite Kluth2018b. These shortcuts only work if you start the program from a build directory and the data folder is at '..' (and no files were deleted). See example files in the data-folder or the documentation for Class Parser for the syntax of these files. The `-r`, `-l`, and `-f` flags are ignored.
`--model, -m one int from -3 to 13` |  Choose a model out of these: {BB = -3, PC = -2, PC-BB = -1, AVS = 0, fAVS = 1, moveFocus = 2, focusOnlyAtFunction = 3, attentionalSwitch = 4, rAVS-comb = 5, rAVS-prox = 6, rAVS-c-o-m = 7, rAVS-w-comb = 8, rAVS-multiple (experimental) = 9, rAVS-abs-CoO (experimental) = 10, rAVS-CoO = 11, AVS-BB = 12, AVSr (experimental) = 13}. Default is rAVS-w-comb (8), this is the rAVS model proposed in \cite Kluth2017. Models 1--4 take the functionality of objects into account, see \cite Kluth2014.
`--ordinal` | If this flag is set, the model will compute a probability distribution across all ratings (in contrast to a single floating point value). You need to provide empirical rating distributions and the individual ratings (see `datafile.data_dist` and `datafile.data_ratings` files in the data folder or the Parser documentation). This computation is based on an ordinal regression model and introduces new model parameters: sigmaOrd (SD of the latent normal distribution) and r-1 thresholds, where r is the number of possible ratings in the rating scale. For parameter estimation, the Kullback-Leibler divergence is used as difference function to compare model output (probability distribution over all ratings) with empirical distribution (relative frequencies over all ratings).
`--posterior[=noOfChains]` | If this flag is set, the posterior distribution will be estimated with the provided number of MCMC chains (default: 2). This works only in the 'ordinal' mode. The parameter estimation method is adapted to reflect the default Metropolis algorithm with the posterior distribution (i.e., prior distribution multiplied with likelihood function) as objective function. For priors, see `--informative-priors`. As likelihood function, the cross-match test between the individual ratings and model generated ratings (for each LO, as many ratings as there were participants are generated) is used (\cite Rosenbaum2005). Only even numbers of participants are allowed (for performance reasons). Use `--threads` to control the number of threads R is using (the R package 'crossmatch' is called; thread control might not work on every system).
`--informative-priors` | If this option is set, the prior distributions for the posterior estimation will be changed from the default 'uninformative' priors (uniform distribution over the range of each parameter) to 'informative' priors: Gaussian distributions with mean and SD which are either based on 101 simple hold-out iterations on the data from \cite Regier2001, (`--informative-priors=regier2001`) or on the  closest to empirical patterns as estimated via PSP, threshold t_e = 0.5 (`--informative-priors=PSP`). Parameter ranges are still in place, though, making these 'informative' priors improper (they do not sum to 1.0).
`--prior-only` | If this flag is set, only the prior distribution is estimated (i.e., the likelihood is ignored). Only considered with `--posterior`.
`--likelihood-only` | If this flag is set, only the likelihood is estimated (i.e., the prior distribution is ignored). Only considered with `--posterior`.
`--output, -o output.txt` | Specify the file, where the results should be stored. Depending on the computations, several files will be saved with names appended to the argument of this option. If you use subfolders, be sure that those exist. Otherwise, the program will fail to save results only after all computations are done; it does neither create subfolders nor does it check for their existence in the beginning! Defaults to `results.txt`.
`--reverse-y, -R` | If this flag is set, the y-axis will be grow from top to bottom (like in computer graphics). Otherwise the y-axis will grow from bottom to top.
`--scale min:max` | Set the minimum and maximum of the rating scale. Default is 0:9 (if the options `-r, -l, -f` are used) or read from the data files (`data.file_scale`).
`--print-ratings` | Print the ratings of all LOs to the command line.

### model parameter options:

(see also section on default model parameters below)

Command | Action
------- | ------
`--lambda double-value` | Set parameter \f$ \lambda \f$. Default depends on the used model, see documentation.
`--slope double-value` | Set parameter slope. Default depends on the used model, see documentation.
`--slope-per-radian` | If this flag is set, the provided slope value will be interpreted as 'change per radian' not per degree as in the literature. To convert from 'change per degree' to 'change per radian' multiply with \f$ \frac{180}{\pi} \f$. To convert back from 'change per radian' to 'change per degree', multiply with \f$ \frac{\pi}{180} \f$.
`--intercept double-value` | Set parameter intercept. Default depends on the used model, see documentation.
`--highgain double-value` | Set parameter highgain. Default depends on the used model, see documentation.
`--phi double-value` | Set parameter \f$ \phi \f$. Default depends on the used model, see documentation.
`--alpha double-value` | Set parameter \f$ \alpha \f$. Default depends on the used model, see documentation.
`--sigma-ord double-value` | Set parameter \f$ \sigma \f$ for the ordinal regression extension (\cite Kluth2018c). Only considered if `--ordinal` is set, too. Default: 2.72304.
`--thresholds comma-separated doubles` | Set threshold parameters for the ordinal regression extension (\cite Kluth2018c). The first and last thresholds are fixed, so you need to provide K-1-2 thresholds (where K is the number of ratings). Only considered if `--ordinal` is set, too. Default: 3.07496,3.95202,4.47031,5.10299,5.72637,6.80488.
`--lrgain` | Set the lrgain parameter (for the BB model from \cite Regier2001). Default depends on the used model, see documentation.
`--lrexp` | Set the lrexp parameter (for the BB model from \cite Regier2001). Default depends on the used model, see documentation.
`--parameter-estimation, -p` | Estimate parameters with the metropolis algorithm. RO(s), LO(s) and data must be given.
`--simple-hold-out[=iterations]` | Use the simple hold out (SHO) method to compare models (see \cite Schultheis2013). Optionally, you can provide the number of iterations.
`--compare-models int-value,int-value` | Sets the two models to be compared with landscaping, or PBCM (data informed and uniformed versions). Integer values are interpreted as model variations, see -m flag for a list.
`--landscaping[=integer]` | Produces a landscape of two models (need to be set via the flag `--compare-models`) with the given amount of data points (default: 100 data points). GOF (nRMSE) is used for fitting the generated data; see \cite Navarro2003 \cite Navarro2004 for details on this method. This method is the same as the data uninformed parametric bootstrap crossfitting method but adds noise to the generated ratings, at the moment this is hardcoded Gausssian noise with SD 0.3).
`--pbcm-uninformed[=integer]` | Applies the data uninformed version of the parametric bootstrap crossfitting method (PBCM) for two models (need to be set via the flag `--compare-models`) with the given amount of data points (default: 100 data points; see \cite Wagenmakers2004). This is basically the same as `--landscaping`, but without adding noise to the generated ratings.
`--pbcm-informed[=integer]` | Applies the data informed version of the parametric bootstrap crossfitting method (PBCM) for two models (need to be set via the flag `--compare-models`) with the given amount of data points (default: 100 data points; see \cite Wagenmakers2004).
`--mfa[=integer-value]` | Starts a grid search with the specified number of intervals (default: 50) for each parameter, i.e., model output for the whole parameter space are computed. Thereafter, this output used to compute the phi value for the Model Flexibility Analysis (MFA) proposed by \cite Veksler2015. Be careful, this needs a lot of memory (depending on the number of intervals, obviously)!
`--mfa-mean` | This option is only considered for the MFA Computation. If this flag is set, the mean of all ratings for LOs above/below the same RO is computed to reduce the dimensionality of the data space. If this flag is not set, all single LO ratings are considered.
`--threads integer-value` | This option controls the number of threads. This works only, if the program is compiled with OpenMP support. Default is 2.

### visualization options:

Command | Action
------- | ------
`--plot` | Plot results (the spatial template or, if ordinal is set, plot of empirical and estimated proportions/probabilities of rating distribution). If not set, you can get the plot by manually running the command `gnuplot plot.command`. Defaults to false if a model evaluation of any kind is started.
`--visualize-vectors, -v` | Visualize almost all weighted vectors.
`--visualize-vector-sum factor, -V factor` | If this is set to a value bigger than zero, the vector sum is visualized (i.e., the final direction). Length of the direction vector is scaled with this parameter and the height of the display (but the direction does not change, only the length of the vector).
`--visualize-attention, -a` | Visualize attentional distribution.
`--LO-Index, -i integer-value >= 0` | If more than one LO is given (via the `-d` option), this option specifies which LO to use for visualizing. All other LOs are also shown but the visible computation (attentional distribution, vector(s), spatial template) is only done with one LO. Default is 0 (i.e., the first LO).
`--grey` | Visualize in greyscale/disable color.
`-3` | Visualize the spatial template in 3D.
`--latex[=filename]` | Use gnuplots epslatex terminal. Beware of the = sign after the option! Output will be saved in `img/<filename>.tex` and `img/<filename>.eps`. Furthermore, the gnuplot commands and result files are saved as `<filename>_plot.command` and `<filename>_results.txt[.vectors]`. Thus, you should be able to get the same plot with `gnuplot <filename>_plot.command` again. Moreover, you may easily adopt the plot by editing this plot.command file. Default `<filename>` is 'figure'.
`--zoom, -z scalefactor` | The referenceObject and the functional-part are scaled with this factor. Default: 1.0.
`--width, -x lowestX:highestX` | Specify the width limits of the display.
`--height, -y lowestY:highestY` | Specify the height limits of the display.
`--quiet` | Suppresses all text output, only saving a simple text file with ratings (the one specified with the `--output` flag; default: results.txt) and not starting gnuplot or R. Useful if this program is called by another program (e.g., GNU octave).
`--help, -h` | Print this help message and exit.

### default model parameters

If you manually specify any model parameter, the not-specified model parameters are set to the default values for the AVS model.
If you don't specify any model parameters, the default parameter values depend on which model you choose, listed below.
All parameters not mentioned for a specific model are using the default value as listed for the AVS model.

- AVS model (`-m 0`):
	- \f$ \lambda = 0.512 \f$
	- \f$ slope = -0.007 \f$
	- \f$ intercept = 1.224 \f$
	- \f$ highgain = 0.002 \f$
	- \f$ \phi = 1.0 \f$
	- \f$ lrgain = 0.109 \f$
	- \f$ lrexp = 0.062 \f$
	- \f$ \alpha = 0.5 \f$
	- \f$ sigmaOrd = 2.72304 \f$
	- \f$ thresholds = 3.07496, 3.95202, 4.47031, 5.10299, 5.72637, 6.80488 \f$

- rAVS-comb model (`-m 5`):
	- \f$ slope = -0.40107 \f$

- rAVS-prox model (`-m 6`):
	- \f$ intercept = 1.16886 \f$
	- \f$ slope = -0.337504 \f$
	- \f$ highgain = 0.0042564 \f$

- rAVS-COM model (`-m 7`):
	- \f$ intercept = 0.9735 \f$
	- \f$ slope = -0.199308 \f$
	- \f$ highgain = 2.58177 \f$

- rAVS-w-comb model (`-m 8`):
	- \f$ \alpha = 0.284187 \f$
	- \f$ intercept = 0.97288 \f$
	- \f$ slope = -0.353709 \f$
	- \f$ highgain = 2.39325 \f$

- rAVS-CoO model (`-m 11`):
	- \f$ \alpha = 0.28417 \f$
	- \f$ intercept = 0.972863 \f$
	- \f$ slope = -0.353695 \f$
	- \f$ highgain = 5.32818 \f$

- AVS-BB model (`-m 12`):
	- see AVS model

- BB model (`-m -3`):
	- \f$ highgain = 0.373 \f$

- PC model (`-m -2`):
	- \f$ \alpha = 0.174 \f$
	- \f$ intercept = 0.929 \f$
	- \f$ slope = -0.006 \f$
	- \f$ highgain = 3.265 \f$

- PC-BB model (`-m -1`):
	- \f$ alpha = 0.115 \f$
	- \f$ intercept = 0.909 \f$
	- \f$ slope = -0.005 \f$
	- \f$ highgain = 6.114 \f$

- fAVS model (`-m 1`):
	- \f$ \phi = 0.6804 \f$

-  moveFocus model (`-m 2`):
	- \f$ \phi = 0.6804 \f$

-  focusOnlyAtFunction model (`-m 3`):
	- \f$ \phi = 0.6804 \f$

- attentionalSwitch model (`-m 4`):
	- \f$ \phi = 0.6804 \f$

- rAVS-multiple model (`-m 9`, experimental model just changing the direction of the vectors in the AVS model):
	- see AVS model

- rAVS-abs-CoO model (experimental rAVS model with absolute distance; neither fully developed nor tested, `-m 10`):
	- \f$ \alpha = 5.0 \f$
	- \f$ intercept = 0.980312 \f$
	- \f$ slope = -0.216995 \f$
	- \f$ highgain = 8.59439 \f$

- AVSr model (experimental model combining both directionalities of attention; neither fully developed nor tested, `-m 13`):
	- see AVS model

# Bugs?

I'm sure there are some. Let me know via the [bugtracker](https://gitlab.com/tkluth/sl-model/issues) or drop me a line at [t.kluth@posteo.de](mailto:t.kluth@posteo.de).

# Hosting

This project is hosted at [gitlab](https://gitlab.com/tkluth/sl-model). Two static versions are published as data publications (version 2.0 \cite Kluth2016, version 4.0 \cite Kluth2018, there is no version 3.0).

# License

Copyright 2014-2018 [Thomas Kluth](http://www.techfak.de/~tkluth)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (file COPYING). If not, see <http://www.gnu.org/licenses/>.

From October 2014 to September 2018, the development of this software was supported by the
[Excellence Cluster EXC 277 Cognitive Interaction Technology](http://www.cit-ec.de).
The Excellence Cluster EXC 277 is a grant of the Deutsche
Forschungsgemeinschaft (DFG) in the context of the German
Excellence Initiative.

## other code

### ranker.h

This program contains source-code that is licensed under the BSD-license (src/ranker.h). Here is the license for the file src/ranker.h repeated:
Copyright (c) 2006, Ken Wilder (formerly University of Chigago, now at Google)
All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
 * Neither the name of the The University of Chicago nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL KEN WILDER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

### fixbb

This software contains source code of a bash script to fix the bounding box of gnuplot generated eps files (in visualizer.cpp).
Source is Copyright Jeff Spirko / Petr Mikulik and licensed under the GPL, see http://www.gnuplot.info/scripts/files/fixbb
