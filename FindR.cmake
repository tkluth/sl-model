# finds and adds headers and libraries related to compile with R, RInside, and Rcpp
# based on https://github.com/vinecopulib/vinecopulib/commit/185f1c899a951212c630746eec95a5cff70b51bf#diff-b453ac8b7784990317967b88ddb2cfe9

find_program(R_EXECUTABLE R DOC "R executable.")
find_program(RSCRIPT_EXECUTABLE Rscript DOC "RScript executable.")

if(R_EXECUTABLE)
	# look for R's root folder
	execute_process(WORKING_DIRECTORY .
					COMMAND ${R_EXECUTABLE} RHOME
					OUTPUT_VARIABLE R_ROOT_DIR
					OUTPUT_STRIP_TRAILING_WHITESPACE)

	find_path(R_INCLUDE_DIR R.h
					HINTS ${R_ROOT_DIR}
					PATHS /usr/local/lib /usr/local/lib64 /usr/lib /usr/lib64 /usr/share
					PATH_SUFFIXES include R/include
					DOC "Path to file R.h")
	if(NOT R_INCLUDE_DIR)
			message(FATAL_ERROR
						"R.h file not found. Locations tried:\n"
						"/usr/local/lib /usr/local/lib64 /usr/lib /usr/lib64 /usr/share ${R_ROOT_DIR}"
			)
	endif()
	find_library(R_LIBRARY NAMES R HINTS "${R_ROOT_DIR}/lib")

	# look for (potentially) local RInside folder
	execute_process(WORKING_DIRECTORY .
					COMMAND ${RSCRIPT_EXECUTABLE} -e "library(RInside); cat(path.package()[1])"
					OUTPUT_VARIABLE RINSIDE_DIR
					OUTPUT_STRIP_TRAILING_WHITESPACE)

	SET(RINSIDE_INCLUDE_DIR "${RINSIDE_DIR}/include")
	find_library(RINSIDE_LIB_DIR NAMES RInside HINTS "${RINSIDE_DIR}/lib")

	if(NOT RINSIDE_INCLUDE_DIR)
		message(FATAL_ERROR "RInside.h file not found. Please install 'RInside'.")
	endif()

	# look for (potentially) local Rcpp folder
	execute_process(WORKING_DIRECTORY .
					COMMAND ${RSCRIPT_EXECUTABLE} -e "library(Rcpp); cat(path.package()[1])"
					OUTPUT_VARIABLE RCPP_DIR
					OUTPUT_STRIP_TRAILING_WHITESPACE)

	SET(RCPP_INCLUDE_DIR "${RCPP_DIR}/include")
	# RCPP_LIBRARY is not needed as one cannot link to the Rcpp library
	if(NOT RCPP_INCLUDE_DIR)
			message(FATAL_ERROR "Rcpp.h file not found. Please install 'Rcpp'.")
	endif()

	# combine paths
	SET(R_INCLUDE_DIRS ${R_INCLUDE_DIR} ${RINSIDE_INCLUDE_DIR} ${RCPP_INCLUDE_DIR})
	SET(R_LIBRARIES ${R_LIBRARY} ${RINSIDE_LIB_DIR})

else (R_EXECUTABLE)
	message(SEND_ERROR "Could not find R header file. Is R installed?")
	return()
endif(R_EXECUTABLE)
