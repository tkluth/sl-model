/*
 * locatedObject.h
 *
 * This file is part of sl-model:
 * https://www.gitlab.com/tkluth/sl-model
 *
 * Copyright(c) 2014-2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 *
 * From October 2014 to September 2018, the
 * development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 *
 */

#ifndef LOCATEDOBJECT_H
#define LOCATEDOBJECT_H

#include "polygonObject.h"

/**
 * This class represents a locatedObject as a CGAL-polygon.
 * This class inherits from \ref PolygonObject, which provides general methods for ROs as well as LOs.
 * At the moment, this class does not provide any more methods or variables than \ref PolygonObject.
 * This might change in the future.
 */

class LocatedObject: public PolygonObject {
public:

	/**
	 * empty constructor
	 * */
	 LocatedObject();

	/**
	 * constructs an object of type LocatedObject by constructing an object of its super-class \ref PolygonObject
	 * @param polygonCGAL a 2D CGAL-Polygon as the base for the LocatedObject
	 * @param flipYAxis if true, the y-values will be multiplied with -1, i.e., a higher y-coordinate means a lower point and vice versa
	 * @param magnifier a number with which the polygon should be scaled (to zoom or shrink)
	 * @param idx a (hopefully) unique index
		*/
	LocatedObject(Polygon polygonCGAL, bool flipYAxis, double magnifier, unsigned int idx);
};

#endif /* locatedObject_H */

