/*
 * parser.h
 *
 * This file is part of sl-model:
 * https://www.gitlab.com/tkluth/sl-model
 *
 * Copyright(c) 2014-2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 *
 * From October 2014 to September 2018, the
 * development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 *
 */

#ifndef PARSER_H
#define PARSER_H

#include <string>
#include <vector>

#include "locatedObject.h"
#include "utilities.h"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polygon_2.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel CGALKernel;
typedef CGAL::Point_2<CGALKernel> Point;
typedef CGAL::Polygon_2<CGALKernel> Polygon;

/**
 * The Parser-class parses located/reference objects and empirical ratings.
 * Files need to be in the following syntax. For examples see files in the data-folder.
 *
 * - `ratings.txt` contains (empirical) mean ratings, one rating per line:
 *
 *        0.6
 *        7.8
 *
 * - `ratings.txt_RO` contains the RO as a CGAL polygon, i.e., first the number of vertices, then each vertex with its coordinates:
 *
 *        4
 *        314 157
 *        326 157
 *        326 193
 *        314 193
 *
 * - `ratings.txt_LOplacements` contains corresponding LO positions as CGAL polygons `x y` coordinates. One LO per line.
 * The 1 at the start of the line denotes a 1-point polygon.
 * Thus, you can easily add n-point polygons as located objects by changing the 1 to another integer.
 * You have to supply more coordinates in that case:
 *
 *      1 12 15
 *      3 13 17 13 19 15 19
 *
 * - `ratings.txt_scale` the scale used for the study, i.e., minimum and maximum, in not-negative integer numbers (floating point numbers are interpreted as integers):
 *
 *        0
 *        9
 * - `ratings.txt_prep` denotes the preposition to be used. This file should contain one line with one of the two words `above` or `below` in it.
 *
 * - `rating.txt_ratings` contains individual ratings. If not existing, 0s are produced and the program prints a warning if the user wanted to use this information. One line denotes one LO and contains as many integers as there were participants. For example, with 3 participants and 4 LOs this might look like:
 *
 *        1 3 2
 *        3 5 2
 *        7 6 8
 *        6 5 9
 *
 * - `rating.txt_dist` contains rating distribution for every LO and the number of participants in the first line. Every other line denotes one LO and it has as many integers in it, as the rating range allows. Every line needs to sum to the number of participants. For instance, with a rating range from 1-4, 10 participants, and 5 LOs, this might look like
 *
 *        10
 *        7 2 1 0
 *        9 1 0 0
 *        2 2 3 3
 *        0 1 8 1
 *        0 0 0 10
 *
 * - `ratings.txt_functionalPart` (optional): functional part as a CGAL polygon (see above). If missing, no functional part is used.
 */
class Parser {
public:

	/**
	* Constructor of Class Parser.
	* @param magnifier the scale with which the objects should be multiplied. A scale of 2, for example, would result in double-sized objects.
	* @param flipY if true, every Y coordinate is multiplied with -1 to flip the Y-axis.
	* */
	Parser(double magnifier, bool flipY);

	/**
	* This method parses one or more polygons and returns a vector of CGAL-polygons.
	* @param filename the path to the file that contains the Polygons in the syntax described above.
	* @param manyPolygons if true, the file is parsed as it would contain several polygons with one polygon per line. If false, only one polygon gets parsed.
	* @returns a vector of 2D CGAL-polygons
	* */
	std::vector<Polygon> parseCGALPolygons(std::string filename, bool manyPolygons);

	/**
	* This method parses empirical ratings and returns them as a vector of doubles. The syntax of the file is described above.
	* @param filename the path to the file containing the rating in the syntax described above (one rating per line)
	* @returns a vector of mean ratings
	* */
	std::vector<double> parseEmpiricalMeanData(std::string filename);

	/**
	 * This method parses proportions of empirical ratings. The syntax of the file is described above.
	 * This method is called with proper parameters from main.cpp.
	 * @param filename the filename to read
	 * @param noOfRatings how many ratings (i.e., LOs) are in the data set?
	 * @returns the number of participants in the study and a vector that contains noOfRatings vectors that contain ratingRange numbers
	 * */
	std::pair<unsigned char, std::vector<std::vector<unsigned char> > > parseEmpiricalRatingDist(std::string filename, unsigned int noOfRatings);

	/**
	* This method parses files that contain individual ratings. The syntax of the file is described above.
	* This method is called with proper parameters from main.cpp.
	* @param filename the file to be parsed
	* @param noOfLOs how many LOs are in the data set? Only used if the file can't be opened, then the appropriate amount of 0s is produced
	* @returns a vector that contains for every LO a vector containing the individual ratings
	* */
	std::vector<std::vector<unsigned int> > parseEmpiricalIndividualData(std::string filename, size_t noOfLOs);

	/**
	* This method parses the rating scale (i.e, minimal and maximal rating) and return minimum and maximum as a pair of two doubles (first minimum, second maximum)
	* @param filename the path to the file that contains the scale: two lines; in the first is the minimum, in the second the maximum
	* @returns a pair of doubles, the first number is the minimum of the scale, the second number is the maximum of the scale
	* */
  std::pair<unsigned int, unsigned int> parseRatingScale(std::string filename);

	/**
	 * Parses a file that contains either "above" or "below" and returns the appropriate preposition enum.
	 * If the file cannot be opened of does not contain what's expected, this function throws a runtime error
	 * @param filename the file to read in
	 * @returns enum denoting the preposition
	 * */
	preposition parsePreposition(std::string filename);

private:

	/**
	* The scale RO(s)/LO(s) should be multiplied with (to zoom/shrink the display).
	* */
	double scale;

	/**
	* Whether the files contain Y-coordinates with a Y-axis from top to bottom (true) versus from bottom to top (false).
	* */
	bool revertY;
};

#endif /* PARSER_H */
