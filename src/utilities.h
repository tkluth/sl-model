/*
 * utilities.h
 *
 * This file is part of sl-model:
 * https://www.gitlab-com/tkluth/sl-model
 *
 * Copyright(c) 2014-2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 *
 * From October 2014 to September 2018, the
 * development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 *
 */

#ifndef UTILITIES_H
#define UTILITIES_H

#include "referenceObject.h"
#include "locatedObject.h"

/**
 * \anchor enumModels
 * This enum distinguishes the different model variations.
 * Numbers -3 to 0 are the models (including the AVS model) proposed by \cite Regier2001,
 * numbers 1-4 are functional model variations \cite Kluth2014,
 * numbers 5-9 are model variations with a reversed attentional shift \cite Kluth2017.
 * numbers 11 & 12 are model variations that consider the center-of-object instead of the center-of-mass \cite Kluth2016d, \cite Kluth2018b.
 * More details:
 * - -3: BB model \cite Regier2001
 * - -2: PC model \cite Regier2001
 * - -1: PC-BB model \cite Regier2001
 * - 0: AVS model \cite Regier2001
 * - 1: fAVS, functional extension to the AVS model proposed by \cite Carlson2006.
 * - 2: extension that moves the focus in the direction of the functional part \cite Kluth2014
 * - 3: extension that puts the focus always on the functional part \cite Kluth2014
 * - 4: extension that computes two vector sums: one, as if the RO had no functional parts and another with the focus on the functional part \cite Kluth2014
 * - 5: rAVS variation that averages center-of-mass and proximal orientation
 * - 6: rAVS variation that considers the proximal point of the RO as end point of the vector
 * - 7: rAVS variation that considers the center-of-mass of the RO as end point of the vector
 * - 8: rAVS variation that considers a weighted average of the two orientations \cite Kluth2018b
 * 	(this model variation performed best among all rAVS variations. If you have read rAVS somewhere, this is most probably the model you are looking for)
 * - 9: rAVS variation with multiple vectors pointing from the LO to every point on the RO. Mathematically the same as the AVS model, conceptually not a focusing but a spreading of attention.
 * - 10: rAVS variation that uses absolute instead of relative distance and the center-of-object instead of the center-of-mass for its computation. Not tested, do not use before checking source code!
 * - 11: rAVS variation that uses relative distance and the center-of-object instead of the center-of-mass for its computation \cite Kluth2016d \cite Kluth2018b
 * - 12: AVS variation that computes vectors for the whole bounding box of the object and thus relies on the center-of-object \cite Kluth2016d \cite Kluth2018b
 * */
enum AVSmethod {
	// Regier & Carlson (2001):
	BB, PC, PC_BB, originalAVS,
	// Kluth & Schultheis (2014):
	fAVS, moveFocus, focusOnlyAtFunction, attentionalSwitch,
	// Kluth, Burigo, Knoeferle (2017):
	rAVS_comb, rAVS_prox,	rAVS_COM, rAVS_w_comb, rAVS_multiple, rAVS_abs_middle,
	// Kluth, Burigo, Schultheis, & Knoeferle (2016, 2018):
	rAVS_CoO, AVS_BB,
	// not yet used model to implement both directionalities
	AVSr,
	// not an actual model but a placeholder for properly printing the model when crossfitting is applied
	CROSS,
	INVALID_MODEL
};

// inline to avoid multiple definitions
/**
 * This function converts an integer from -3 to 13 to \ref AVSmethod enum.
 * @param m int to convert
 * @returns appropriate enum
 * */
inline AVSmethod intToMethod(int m){
	AVSmethod method;

	switch (m) {
		case -3:
			method = BB;
			break;

		case -2:
			method = PC;
			break;

		case -1:
			method = PC_BB;
			break;

		case 0:
			method = originalAVS;
			break;

		case 1:
			method = fAVS;
			break;

		case 2:
			method = moveFocus;
			break;

		case 3:
			method = focusOnlyAtFunction;
			break;

		case 4:
			method = attentionalSwitch;
			break;

		case 5:
			method = rAVS_comb;
			break;

		case 6:
			method = rAVS_prox;
			break;

		case 7:
			method = rAVS_COM;
			break;

		case 8:
			method = rAVS_w_comb;
			break;

		case 9:
			method = rAVS_multiple;
			break;

		case 10:
			method = rAVS_abs_middle;
			break;

		case 11:
			method = rAVS_CoO;
			break;

		case 12:
			method = AVS_BB;
			break;

		case 13:
			method = AVSr;
			break;

		default:
			std::cout << "main():\t " << "Model " << optarg << " doesn't exist. Quitting." << std::endl;
			throw std::runtime_error("utilities.h:\tNon existing model / method. Quitting.");
			break;
	}

	return method;
}

// inline to avoid multiple definitions
/**
 * This function converts an \ref AVSmethod enum into a human readable std::string
 * @param method the method to be converted
 * @returns a human-readable std::string
 * */
inline std::string enumToString(AVSmethod method) {

	std::string methodString;

	switch (method) {

		case BB:
			methodString = "BB";
			break;

		case PC:
			methodString = "PC";
			break;

		case PC_BB:
			methodString = "PC-BB";
			break;

		case originalAVS:
			methodString = "AVS";
			break;

		case fAVS:
			methodString = "fAVS";
			break;

		case moveFocus:
			methodString = "moveFocus";
			break;

		case focusOnlyAtFunction:
			methodString = "focusOnlyAtFunction";
			break;

		case attentionalSwitch:
			methodString = "attentionalSwitch";
			break;

		case rAVS_comb:
			methodString = "rAVS-comb";
			break;

		case rAVS_prox:
			methodString = "rAVS-prox";
			break;

		case rAVS_COM:
			methodString = "rAVS-c-o-m";
			break;

		case rAVS_w_comb:
			methodString = "rAVS-w-comb";
			break;

		case rAVS_multiple:
			methodString = "rAVS-multiple";
			break;

		case rAVS_abs_middle:
			methodString = "rAVS-abs-middle";
			break;

		case rAVS_CoO:
			methodString = "rAVS-CoO";
			break;

		case AVS_BB:
			methodString = "AVS-BB";
			break;

		case AVSr:
			methodString = "AVSr";
			std::cerr << "Warning: This model (combining both directionalities) is a quick prototype only and not well tested!" << std::endl;
			break;

		case CROSS:
			methodString = "two different (crossfitting); see other output";
			break;

		case INVALID_MODEL:
			methodString = "invalid model";
			throw std::runtime_error("utilities.h:\tNon existing model / method.\nIf you're trying to crossfit models (landscaping/PBCM), you need to specify both models via '--compare-models [integer,integer]'.\nIf you want to compute the MFA or estimate the posterior distribution, you need to explicitly set the model via the -m option.\nQuitting.");
			break;

		default:
			methodString = "invalid model string";
			throw std::runtime_error("utilities.h:\tNo model / method selected.\nIf you're trying to crossfit models (landscaping/PBCM), you need to specify both models via '--compare-models [integer,integer]'.\nIf you want to compute the MFA or estimate the posterior distribution, you need to explicitly set the model via the -m option.\nQuitting.");
			break;
	}

	return methodString;
}

/** enum for prepositions */
enum preposition{ABOVE, BELOW, INVALID};

/**
 * This function converts a \ref preposition enum to a human-readable string
 * @param p the preposition to return
 * @returns a human-readable string
 * */
inline std::string prepToString(preposition p){
	if (p == ABOVE) {
		return "above";
	} else if (p == BELOW) {
		return "below";
	} else if (p == INVALID) {
		return "invalid preposition!";
	} else {
		return "error!";
	}
}

/**
 * Helper function to implement approximate equality for floating point numbers.
 * Based on https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
 * @param a number 1 to compare
 * @param b number 2 to compare
 * @param maxDiff if the two numbers are closer to each other than this precision threshold, they are considered equal
 * @param maxRelDiff same as before but taking care of relative magnitudes of the two numbers
 * @returns true, when a and b are approximately equal, false otherwise
 * */
bool inline almostEqual(double a, double b, double maxDiff = 1e-200, double maxRelDiff = std::numeric_limits<double>::epsilon()) {
	// Check if the numbers are really close
	// needed when comparing numbers near zero.
	double diff = fabs(a - b);
	if (diff <= maxDiff) {
		return true;
	}

	a = fabs(a);
	b = fabs(b);
	double largest = (b > a) ? b : a;

	if (diff <= largest * maxRelDiff) {
		return true;
	}
	return false;
}

/**
 * This struct is used to represent model parameters.
 * It consists of
 * - name: a std::string denoting the name of the parameter
 * - value: the current value of the model parameter
 * - RCfit: for BB, PC, PC-BB, AVS, & AVS-BB models: the best fit for fitting exp. 7 from \cite Regier2001 as reported in \cite Regier2001; for other models: the best fit for exp. 7 from \cite Regier2001 as fit with this program or other sensible good parameter values
 * - min, max: the lower and upper limit of the parameter
 * */
struct ModelParameter{
	std::string name;
	double value;
	double RCfit;
	double min;
	double max;
};

/**
 * This struct contains information about one LO.
 * More precisely, it consists of the RO, the LO, and the functional part of the RO (referenceObject, locatedObject, functionalPart)
 * and the empirical mean rating (meanRating).
 * Next, it holds the absolute and relative (divided by number of participants) number of ratings grouped by possible ratings (so, the vector has the size of the range of the rating scale and its sum matches the number of participants; absoluteNoOfRatings and proportions).
 * Furthermore, it stores a vector of individual ratings (the vector length here is the same as the number of participants).
 * Finally, the struct contains the number of participants and the \ref preposition that was rated.
 * The fields functionalPart, absoluteNoOfRatings, proportions, individualRatings, and noOfParticipants
 * are required for some model simulations but might not be available in the data. See \ref Parser for how to initialize the struct then.
 * */
struct RatingDataPoint {
	std::shared_ptr<ReferenceObject> referenceObject;
	LocatedObject locatedObject;
	std::string functionalPart = "";
	// for empirical rating distribution, absolute number of ratings per possible rating (length of vector)
	std::vector<unsigned char> absoluteNoOfRatings;
	// relative number of ratings (divided by noOfParticipants)
	std::vector<double> proportions;
	// individual ratings ordered by subject
	std::vector<unsigned int> individualRatings;
	double meanRating = std::numeric_limits<double>::quiet_NaN();
	unsigned char noOfParticipants = 0;
	preposition prep = INVALID;
};

#endif /* UTILITIES_H */
