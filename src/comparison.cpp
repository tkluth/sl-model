/*
 * comparison.cpp
 *
 * This file is part of sl-model:
 * https://www.gitlab.com/tkluth/sl-model
 *
 * Copyright(c) 2014-2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 *
 * From October 2014 to September 2018, the
 * development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 *
 */

/**
 * the higher this value, the more output
 * if set to 0 only important messages will be printed
 * this debug switch is placed in every file and only valid for one file!
 */
#define VERBOSITY 0

#include "comparison.h"

// OpenMP: parallelization of PBCM / landscaping
#ifndef NOOPENMP
#include <omp.h>
#endif
//measure time:
#include <chrono>

// GNU Scientific Library Random Numbers
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

Comparison::Comparison(Model modelDefault, std::pair<unsigned int, unsigned int> rScale, std::vector<RatingDataPoint> emp, unsigned int noROs, AVSmethod m1, AVSmethod m2, bool flipY, bool pRatings, int widthLow, int widthHigh, int heightLow, int heightHigh, unsigned int noThreads){

	// for parameter settings of the default model, see Model::setMethod() and main.cpp, around line 729
	this->defaultModel = modelDefault;

	this->ratingScale = rScale;

	this->nrOfROs = noROs;

	this->empirical = emp;

	this->method1 = m1;
	this->method2 = m2;

	this->reverseY = flipY;
	this->printRatings = pRatings;

	this->heightLimitLow = heightLow;
	this->widthLimitLow = widthLow;
	this->heightLimitHigh = heightHigh;
	this->widthLimitHigh = widthHigh;

	this->noOfThreads = noThreads;
}

void Comparison::landscaping(unsigned int noOfIterations){
	this->crossfitting(UNINFORMED, noOfIterations, true);
}

void Comparison::pbcmInformed(unsigned int noOfIterations){
	this->crossfitting(INFORMED, noOfIterations, false);
}

void Comparison::pbcmUninformed(unsigned int noOfIterations){
	this->crossfitting(UNINFORMED, noOfIterations, false);
}

void Comparison::simpleHoldOut(unsigned int noOfIterations) {

	if(this->empirical.size() < 4) {
		throw std::runtime_error("It's not useful to compute simple hold-out with less than 4 data points. Quitting.");
	}

	double amountOfTrainingData = 0.7;
	unsigned int splitIndex = (unsigned int) round(((double) this->empirical.size()) * amountOfTrainingData);

	std::vector<std::pair<std::string, std::vector<double> > > valuesToSave;

	std::vector<std::string> namesToSave;
	namesToSave.push_back("predictionError");
	namesToSave.push_back("GOF");
	namesToSave.push_back("slope");
	namesToSave.push_back("intercept");
	namesToSave.push_back("highgain");
	namesToSave.push_back("lambda");

	if (this->method2 == fAVS || this->method2 == moveFocus || this->method2 == focusOnlyAtFunction || this->method2 == attentionalSwitch) {
		namesToSave.push_back("phi");
	}

	if (this->method2 == rAVS_comb || this->method2 == rAVS_prox || this->method2 == rAVS_COM || this->method2 == rAVS_w_comb ||
		this->method2 == rAVS_multiple || this->method2 == rAVS_abs_middle || this->method2 == rAVS_CoO) {
		namesToSave.push_back("alpha");
	}

	if (this->method2 == BB || this->method2 == PC || this->method2 == PC_BB) {
		namesToSave.push_back("lrexp");
		namesToSave.push_back("lrgain");
	}

	if (this->defaultModel.getOrdinal()) {
		namesToSave.push_back("sigmaOrd");
		for (size_t t = 1; t < (this->ratingScale.second - this->ratingScale.first) - 1; ++t){
			std::stringstream tmp;
			tmp << "t" << t;
			namesToSave.push_back(tmp.str());
		}
	}

	for(size_t toSave = 0; toSave < namesToSave.size(); toSave++){
		std::pair<std::string, std::vector<double> > save;
		save.first = namesToSave[toSave];
		std::vector<double> tempVector;
		save.second = tempVector;
		save.second.reserve(noOfIterations);
		valuesToSave.push_back(save);
	}

	int counter = 0;

	gsl_rng* randomGenerator = gsl_rng_alloc(gsl_rng_ranlxs2);
	long seed = time(NULL) * getpid();
	gsl_rng_set(randomGenerator, (unsigned long) seed);

#ifndef NOOPENMP
	omp_set_num_threads(this->noOfThreads);
	std::cout << std::setprecision(0) << this->noOfThreads << " threads are spawned." << std::endl << std::flush;
	#pragma omp parallel
	{
#endif

	Model model = Model(this->empirical, this->nrOfROs, this->ratingScale, this->widthLimitLow, this->widthLimitHigh,
		this->heightLimitLow, this->heightLimitHigh, this->printRatings, this->method2,
		this->defaultModel.getOrdinal(), this->defaultModel.getPosterior(), this->defaultModel.getInformativePriors(), this->defaultModel.getPriorOnly(), this->defaultModel.getLikelihoodOnly(), this->defaultModel.getNoChangeOrdinal(), 1);
	// this is an size_t-array
	size_t dataToUseIdx[this->empirical.size()];

	for (size_t i = 0; i < this->empirical.size(); ++i) {
		dataToUseIdx[i] = i;
	}

	std::vector<RatingDataPoint> trainingData;
	std::vector<RatingDataPoint> testData;

	std::chrono::system_clock::time_point startTime = std::chrono::system_clock::now();
	time_t startTime_C = std::chrono::system_clock::to_time_t(startTime);

#ifndef NOOPENMP
	// nowait allows the first idle thread to compute a normal GOF afterwards
	#pragma omp for nowait
#endif
	for (size_t i = 0; i < noOfIterations; ++i) {

		// reset model parameters
		model.setMethod(this->method2);

		// compute SHO iteration

		#ifndef NOOPENMP
		// allow access to random number generator only once
			#pragma omp critical
			{
		#endif
		// using gsl random shuffler:
		gsl_ran_shuffle(randomGenerator, dataToUseIdx, this->empirical.size(), sizeof(size_t));
		#ifndef NOOPENMP
			}
		#endif

		trainingData.clear();
		testData.clear();

		#if VERBOSITY >= 1
		std::cout << "simpleHoldOut(): training with: ";
		#endif

		for (unsigned int j = 0; j < splitIndex; ++j) {
			#if VERBOSITY >= 1
			std::cout << dataToUseIdx[j] << ", ";
			#endif
			trainingData.push_back(this->empirical[dataToUseIdx[j]]);
		}

		double GOFiter = model.parameterEstimation(trainingData, false);

		#if VERBOSITY >= 1
		std::cout << std::endl << "simpleHoldOut(): testing with ";
		#endif

		for (unsigned int j = splitIndex; j < this->empirical.size(); j++) {
			#if VERBOSITY >= 1
			std::cout << dataToUseIdx[j] << ", ";
			#endif
			testData.push_back(this->empirical[dataToUseIdx[j]]);
		}

		#if VERBOSITY >= 1
		std::cout << std::endl;
		#endif

		double pe = model.computeResults(&testData, false);

		#ifndef NOOPENMP
			#pragma omp critical
			{
		#endif

		// save predictionError, GOFs on training data and parameter values
		for (size_t toSave = 0; toSave < valuesToSave.size(); toSave++) {
			if (valuesToSave[toSave].first == "GOF"){
				valuesToSave[toSave].second.push_back(GOFiter);
			} else if (valuesToSave[toSave].first == "predictionError"){
			 valuesToSave[toSave].second.push_back(pe);
			} else {
				valuesToSave[toSave].second.push_back(model.getParam(valuesToSave[toSave].first).value);
			}
		}

		counter++;
		double percentage = (100.0 * ((double) counter / (double) noOfIterations));
		std::chrono::system_clock::duration estimatedRemainingTime = std::chrono::duration_cast<std::chrono::system_clock::duration>(100 * ((std::chrono::system_clock::now() - startTime) / percentage));
		std::chrono::system_clock::duration additionalGOF = std::chrono::duration_cast<std::chrono::system_clock::duration>(1.3 * ((std::chrono::system_clock::now() - startTime) / counter));
		std::chrono::system_clock::time_point endTime = startTime + estimatedRemainingTime + additionalGOF;
		time_t endTime_C = std::chrono::system_clock::to_time_t(endTime);
		std::cout << "simpleHoldOut():\tstarted on " << std::put_time(localtime(&startTime_C), "%b %d, %T");
		std::cout << ", finished (incl. GOF) by (estimated): " << std::put_time(localtime(&endTime_C), "%b %d, %T") << ", progress: "  << std::setprecision(2) << percentage << " %\r" << std::endl << std::flush;
		#ifndef NOOPENMP
			} // critical
		#endif
	} // for

	#ifndef NOOPENMP
		#pragma omp single //nowait
		{
	#endif
	// compute only normal GOF in the end

	std::cout << "SHO(): computing GOF: " << std::endl;
	model.parameterEstimation(this->empirical, true);

	this->GOF = model.getResultsPE();

	#ifndef NOOPENMP
		} // single
	} // parallel
	#endif

	gsl_rng_free(randomGenerator);

	for (size_t toSave = 0; toSave < valuesToSave.size(); toSave++) {
		std::stringstream writeResults;

		std::cout << std::endl << "all " << valuesToSave[toSave].first << " values:" << std::endl;
		for (size_t i = 0; i < valuesToSave[toSave].second.size() - 1; i++) {
			std::cout << std::setprecision(5) << valuesToSave[toSave].second[i] << ", ";
			writeResults << valuesToSave[toSave].second[i] << ", ";
		}
		std::cout << valuesToSave[toSave].second[valuesToSave[toSave].second.size()-1] << std::endl;
		writeResults << valuesToSave[toSave].second[valuesToSave[toSave].second.size()-1];

		std::pair<std::string, std::string> resultPair;

		resultPair.first = namesToSave[toSave];
		resultPair.second = writeResults.str();

		this->resultsSHO.push_back(resultPair);
	}

	// means, medians and bootstrap confidence intervals are computed with R, see Visualizer::saveResultsSHO()
}

void Comparison::posteriorEstimation(std::vector<RatingDataPoint> data, unsigned int noOfChains, std::string basefilename) {
	// RInside only allows one R instance, so we can't parallelize the posterior estimation via openMP without implementing it first in C++
	// parallelizing via boost::thread is not faster (since calls to RInside are a critical, one-thread-only section)
	// running R with an optimized BLAS already uses more threads

	Model model = Model(this->empirical, this->nrOfROs, this->ratingScale, this->widthLimitLow, this->widthLimitHigh, this->heightLimitLow, this->heightLimitHigh, this->printRatings, this->method2, this->defaultModel.getOrdinal(), this->defaultModel.getPosterior(), this->defaultModel.getInformativePriors(), this->defaultModel.getPriorOnly(), this->defaultModel.getLikelihoodOnly(), this->defaultModel.getNoChangeOrdinal(), this->noOfThreads);

	for (unsigned int chain = 0; chain < noOfChains; ++chain) {
		model.setMethod(this->method2);

		std::cout << std::endl << "posteriorEstimation():\tstarting chain no. " << chain << " with model " << &model << " " << enumToString(this->method2) << std::endl;

		model.parameterEstimation(data, true);
		this->savePosterior(model, chain, basefilename);
	}
}

void Comparison::mfa(unsigned int numberOfIntervals, bool aggregatePerRO) {

	std::vector<std::vector<unsigned short> > allPredictedRatings;
	// use unsigned short instead of double/double to save some memory
	// due to the need of a lot of memory ressources
	// double/double to short:
	// 0.1234 * 10000 -> 1234
	// we can only represent 4 digits after the comma (short upper limit: 65535)
	// this, however, does not harm the MFA, since each dimension of the data space
	// has maximal 10 intervals -> only the first digit after the comma is considered (roughly)

	int counter = 0;

	// epsilon is needed to include upper limits of the parameter ranges
	// otherwise 5 <= 5 fails due to high machine precision
	double alphaStep = (std::abs(this->defaultModel.getParam("alpha").min - this->defaultModel.getParam("alpha").max) / (numberOfIntervals - 1))  - std::numeric_limits<double>::epsilon();
	double slopeStep = (std::abs(this->defaultModel.getParam("slope").min - this->defaultModel.getParam("slope").max) / (numberOfIntervals - 1)) - std::numeric_limits<double>::epsilon();
	double interceptStep = (std::abs(this->defaultModel.getParam("intercept").min - this->defaultModel.getParam("intercept").max) / (numberOfIntervals - 1)) - std::numeric_limits<double>::epsilon();
	double highgainStep = (std::abs(this->defaultModel.getParam("highgain").min - this->defaultModel.getParam("highgain").max) / (numberOfIntervals - 1)) - std::numeric_limits<double>::epsilon();

	if(!(almostEqual(this->defaultModel.getParam("alpha").min, this->defaultModel.getParam("lambda").min)) | !(almostEqual(this->defaultModel.getParam("alpha").max, this->defaultModel.getParam("lambda").max))) {
		throw std::runtime_error("mfa(): error: lambda and alpha need to have the same range. You can change this behavior by changing the code in Comparison::mfa().");
	}

	// pre-compute alpha intervals for parallelization:
	// openmp only works with for-loops with an int as variable
	double tempAlpha = this->defaultModel.getParam("alpha").min;
	std::vector<double> alphaIntervals((size_t) numberOfIntervals);

	for (size_t i = 0; i < numberOfIntervals; ++i){
		alphaIntervals[i] = tempAlpha;
		tempAlpha += alphaStep;
	}

	std::chrono::system_clock::time_point startTime = std::chrono::system_clock::now();
	time_t startTime_C = std::chrono::system_clock::to_time_t(startTime);

#ifndef NOOPENMP
	omp_set_num_threads(this->noOfThreads);
	std::cout << std::setprecision(0) << this->noOfThreads << " threads are spawned." << std::endl << std::flush;
	#pragma omp parallel
	{
#endif

	// always use a rating scale from 0.0 to 1.0, otherwise the code to compute the number of unique model predictions below (std::floor(rating * granularity), adapted from the R code by Veksler et al. (2015)) won't work
	Model model = Model(this->empirical, this->nrOfROs, std::pair<unsigned int, unsigned int>(0, 1), this->widthLimitLow, this->widthLimitHigh, this->heightLimitLow, this->heightLimitHigh, this->printRatings, this->method2, this->defaultModel.getOrdinal(), this->defaultModel.getPosterior(), this->defaultModel.getInformativePriors(), this->defaultModel.getPriorOnly(), this->defaultModel.getLikelihoodOnly(), this->defaultModel.getNoChangeOrdinal(), 1);

	std::cout << "mfa():\tStarting MFA for model " << model.getMethod() << std::endl;
	if(aggregatePerRO){
		std::cout << "mfa():\taggregating per RO " << std::endl;
	}

#ifndef NOOPENMP
	#pragma omp for
#endif
	for (unsigned int alphaIntervalIdx = 0; alphaIntervalIdx < numberOfIntervals; ++alphaIntervalIdx) {
		for (double slope = defaultModel.getParam("slope").min; slope <= defaultModel.getParam("slope").max; slope += slopeStep) {
			for (double intercept = defaultModel.getParam("intercept").min; intercept <= defaultModel.getParam("intercept").max; intercept += interceptStep) {
				for (double highgain = defaultModel.getParam("highgain").min; highgain <= defaultModel.getParam("highgain").max; highgain += highgainStep) {

					// phi, lrexp, lrgain are not changed, use default RC-fit values
					// alpha == lambda !!
					model.setMethod(this->method2, alphaIntervals[alphaIntervalIdx], slope, intercept, highgain, defaultModel.getParam("phi").RCfit, alphaIntervals[alphaIntervalIdx], defaultModel.getParam("lrgain").RCfit, defaultModel.getParam("lrexp").RCfit);

					std::vector<unsigned short> shortRatings;

					// use the whole output incl. information about preposition and RO to be able to use the mean ratings over a single RO
					std::vector<RatingDataPoint> generatedRatings = model.generateRatings(0);

					std::unordered_map<std::string, std::vector<double> > ratingsPerRO;

					for(size_t i = 0; i < generatedRatings.size(); ++i){
						// set everything higher than 1.0 to 1.0 and lower than 0.0 to 0.0
						// (this should not happen anyway...)
						if(generatedRatings[i].meanRating < 0.0){
							generatedRatings[i].meanRating = 0.0;
						} else if(generatedRatings[i].meanRating > 1.0){
							generatedRatings[i].meanRating = 1.0;
						}

						if(aggregatePerRO){
							std::stringstream configuration;
							configuration << "RO: " << *(generatedRatings[i].referenceObject->getCGALPolygon()); // << ", prep: " << generatedRatings[i].prep;
							ratingsPerRO[configuration.str()].push_back((double) generatedRatings[i].meanRating);
						}else{
							// multiply with 1000 to only consider digits after the comma
							shortRatings.push_back((unsigned short) std::floor(generatedRatings[i].meanRating * 10000));
						}
					}

					if(aggregatePerRO){
						for(std::unordered_map<std::string, std::vector<double> >::iterator iter = ratingsPerRO.begin(); iter != ratingsPerRO.end(); iter++){
							double mean = 0.0;
							for (size_t j = 0; j < iter->second.size(); ++j){
								mean = mean + iter->second[j];
							}
							mean = mean / ((double) iter->second.size());

							// multiply with 10000 to only consider digits after the comma
							shortRatings.push_back((unsigned short) std::floor(mean * 10000));
						}
					}

					#ifndef NOOPENMP
						#pragma omp critical
						{
						#endif
							allPredictedRatings.push_back(shortRatings);
							#ifndef NOOPENMP
							}
						#endif

				}
			}

				#ifndef NOOPENMP
					#pragma omp critical
					{
				#endif
					counter++;
					double percentage = (100.0 * ((double) counter / (double) (numberOfIntervals*numberOfIntervals)));
					std::chrono::system_clock::duration estimatedRemainingTime = std::chrono::duration_cast<std::chrono::system_clock::duration>(100 * ((std::chrono::system_clock::now() - startTime) / percentage));
					std::chrono::system_clock::time_point endTime = startTime + estimatedRemainingTime;
					time_t endTime_C = std::chrono::system_clock::to_time_t(endTime);
					std::cout << "MFA():\tstarted on " << std::put_time(localtime(&startTime_C), "%b %d, %T"); // welcome to the ugly world of C++: if you put both time informations in the same stream, they will print the same time ...
					std::cout << ", finished by (estimated): " << std::put_time(localtime(&endTime_C), "%b %d, %T") << ", progress: " << std::setprecision(2) << percentage << " %\r" << std::flush;
				#ifndef NOOPENMP
					}
				#endif
			}
		}

	#ifndef NOOPENMP
	}
	#endif

	std::cout << std::endl;
	std::cout << "mfa():\tcomputed all predictions.";

	std::stringstream writeResults;

	unsigned long int noOfPredictions = allPredictedRatings.size(); // corresponds to nrow(predictions) in the code from Veksler et al. (2015)
	size_t noOfMeasures = allPredictedRatings[0].size(); // corresponds to ncol(predictions) in the code from Veksler et al. (2015)
	writeResults << std::endl << "mfa():\tnumber of intervals " << numberOfIntervals << std::endl;
	writeResults << "mfa():\tnumber of predictions: " << noOfPredictions << ", (should be equal to numberOfIntervals^4 = " << std::pow(numberOfIntervals, 4) << ")" << std::endl;
	writeResults << "mfa():\tnumber of measures (dimension of the data space): " << noOfMeasures << std::endl;

	std::cout << std::endl << "mfa():\tstarting now to look which predictions fall in the same cell ..." << std::endl;

	std::vector<std::vector<unsigned short> > allPredictedRatings_VekslerGrid = allPredictedRatings;

	// granularity equals rating space intervals
	long double granularity = std::abs((int) (this->ratingScale.second - this->ratingScale.first)) + 1.0;

	for (size_t i = 0; i < allPredictedRatings.size(); i++){
		for (size_t j = 0; j < allPredictedRatings[i].size(); j++){
			// divide with 10000 to convert the digits after comma stored as shorts to floating point values again
			allPredictedRatings[i][j] = (unsigned short) std::floor(((double) allPredictedRatings[i][j]/10000.0) * granularity);
		}
	}

	// look only for unique values by using a set (only unique values allowed)
	std::set<std::vector<unsigned short> > uniquePredictions(allPredictedRatings.begin(), allPredictedRatings.end());
	long double totalCells = std::pow(granularity, noOfMeasures); // for grid proposed by Veksler et al. this equals noOfPredictions
	long double phi = ((long double) uniquePredictions.size() / totalCells);

	writeResults << "mfa():\tWe have " << uniquePredictions.size() << " unique predictions for a granularity of " << granularity << " in each dimension (totalCells = granularity ^ dimensions = " << totalCells << ")" << std::endl;
	writeResults << "mfa():\tThis makes Phi = " << phi << std::endl;
	writeResults << "mfa():\tWith Phi_max = " << std::pow(numberOfIntervals, 4) / std::pow(granularity, noOfMeasures) << std::endl;
	writeResults << "mfa():\twe obtain Phi_normalized = " << phi / (std::pow(numberOfIntervals, 4) / std::pow(granularity, noOfMeasures)) << std::endl;

	writeResults << std::endl;

	// as suggested by Veksler et al
	granularity = std::pow(((double) allPredictedRatings_VekslerGrid.size()), 1.0 / ((double)noOfMeasures) );

	for (size_t i = 0; i < allPredictedRatings_VekslerGrid.size(); i++){
		for (size_t j = 0; j < allPredictedRatings_VekslerGrid[i].size(); j++){
			// divide with 10000 to convert the digits after comma stored as shorts to floating point values again
			allPredictedRatings_VekslerGrid[i][j] = (unsigned short) std::floor(((double) allPredictedRatings_VekslerGrid[i][j]/10000.0) * granularity);
		}
	}

	// look only for unique values by using a set (only unique values allowed)
	std::set<std::vector<unsigned short> > uniquePredictions_VekslerGrid(allPredictedRatings_VekslerGrid.begin(), allPredictedRatings_VekslerGrid.end());
	long double totalCells_VekslerGrid = noOfPredictions;
	assert(std::abs(totalCells_VekslerGrid - std::pow(granularity, noOfMeasures)) - std::numeric_limits<long double>::epsilon());

	phi = ((long double) uniquePredictions_VekslerGrid.size() / totalCells_VekslerGrid);

	writeResults << "mfa():\tWe have " << uniquePredictions_VekslerGrid.size() << " unique predictions for a granularity of " << granularity << " in each dimension (totalCells = granularity ^ dimensions = " << totalCells_VekslerGrid << ")" << std::endl;
	writeResults << "mfa():\tThis makes Phi = " << phi << std::endl;
	writeResults << "mfa():\tWith Phi_max = " << std::pow(numberOfIntervals, 4) / std::pow(granularity, noOfMeasures) << std::endl;
	writeResults << "mfa():\twe obtain Phi_normalized = " << phi / (std::pow(numberOfIntervals, 4) / std::pow(granularity, noOfMeasures)) << std::endl;
	writeResults << std::endl;

	std::cout << writeResults.str();

	this->mfaString = writeResults.str();
}

// getter functions

std::string Comparison::getGOF() {
	return this->GOF;
}

std::vector<std::pair<std::string, std::string> > Comparison::getResultsSHO(){
	return this->resultsSHO;
}

DeltaGOFsHistograms Comparison::getDeltaGOFsHistograms(){
	return this->deltaGOFsHistograms;
}

Landscape Comparison::getLandscape(){
	return this->landscape;
}

std::string Comparison::getResultsMFA(){
	return this->mfaString;
}

// private functions


void Comparison::crossfitting(samplingMethod algorithm, unsigned int noOfIterations, bool addNoise){

	std::vector<double> deltaGOFsModel1Data(noOfIterations);
	std::vector<double> deltaGOFsModel2Data(noOfIterations);
	deltaGOFsModel1Data.reserve(noOfIterations);
	deltaGOFsModel2Data.reserve(noOfIterations);

	std::vector<std::vector<double> > data1 (noOfIterations);
	std::vector<std::vector<double> > data2 (noOfIterations);
	data1.reserve(noOfIterations);
	data2.reserve(noOfIterations);

	std::vector<double> GOF1_data1 (noOfIterations);
	std::vector<double> GOF2_data1 (noOfIterations);
	std::vector<double> GOF1_data2 (noOfIterations);
	std::vector<double> GOF2_data2 (noOfIterations);
	GOF1_data1.reserve(noOfIterations);
	GOF2_data1.reserve(noOfIterations);
	GOF1_data2.reserve(noOfIterations);
	GOF2_data2.reserve(noOfIterations);

	int counter = 0;

	// create one global random number generator
	gsl_rng* randomGenerator = gsl_rng_alloc(gsl_rng_mt19937);
	long seed = time(NULL) * getpid();
	gsl_rng_set(randomGenerator, (unsigned long) seed);
	std::cout << "seeded random number generator with " << seed << std::endl;

	std::ofstream debugFile;
	std::stringstream buffer;
	debugFile.open("crossfitting-debug.txt");

	std::chrono::system_clock::time_point startTime = std::chrono::system_clock::now();
	time_t startTime_C = std::chrono::system_clock::to_time_t(startTime);

#ifndef NOOPENMP
	omp_set_num_threads(this->noOfThreads);
	std::cout << std::setprecision(0) << this->noOfThreads << " threads are spawned." << std::endl << std::flush;

	#pragma omp parallel
	{
#endif
	Model model1(this->empirical, this->nrOfROs, this->ratingScale, this->widthLimitLow, this->widthLimitHigh, this->heightLimitLow, this->heightLimitHigh, this->printRatings, this->method1, this->defaultModel.getOrdinal(), this->defaultModel.getPosterior(), this->defaultModel.getInformativePriors(), this->defaultModel.getPriorOnly(), this->defaultModel.getLikelihoodOnly(), this->defaultModel.getNoChangeOrdinal(), 1);

	Model model2(this->empirical, this->nrOfROs, this->ratingScale,  this->widthLimitLow, this->widthLimitHigh, this->heightLimitLow, this->heightLimitHigh, this->printRatings, this->method2, this->defaultModel.getOrdinal(), this->defaultModel.getPosterior(), this->defaultModel.getInformativePriors(), this->defaultModel.getPriorOnly(), this->defaultModel.getLikelihoodOnly(), this->defaultModel.getNoChangeOrdinal(), 1);

#ifndef NOOPENMP
	#pragma omp critical
	{
#endif
	this->landscape.model1 = model1;
	this->landscape.model2 = model2;
	this->deltaGOFsHistograms.model1 = model1;
	this->deltaGOFsHistograms.model2 = model2;
#ifndef NOOPENMP
	}
#endif

	std::cout << "crossfitting():\tstarting crossfitting (" << noOfIterations << " iterations) for models " << model1.getMethod() << " and " << model2.getMethod() << std::endl;

#ifndef NOOPENMP
	#pragma omp for
#endif
	for (unsigned int i = 0; i < noOfIterations; ++i) {

		if (algorithm == INFORMED) {
			std::cout << "crossfitting():\tbootstrapping data -> x* (#" << (i + 1) << "/" << noOfIterations << ")."<< std::endl;

			std::vector<RatingDataPoint> bootstrappedData = bootstrapData(this->empirical);

			model1.setEmpiricalData(bootstrappedData, this->nrOfROs, this->ratingScale);
			model2.setEmpiricalData(bootstrappedData, this->nrOfROs, this->ratingScale);

			std::cout << "crossfitting():\tfitting models to bootstrapped data -> Theta^* (#" << (i+1) << "/" << noOfIterations << ")." << std::endl;

			//fit models to bootstrapped data
			model1.parameterEstimation(bootstrappedData, false);
			model2.parameterEstimation(bootstrappedData, false);
		} else if (algorithm == UNINFORMED) {

			#ifndef NOOPENMP
			// allow access to random number generator only once
				#pragma omp critical
				{
			#endif

		#if VERBOSITY >= 1
			std::cout << std::endl << "crossfitting():\tsampling random parameters for both models (#" << (i+1) << "/" << noOfIterations << ")." << std::endl;
		#endif

			double lambdaM1 = std::numeric_limits<double>::quiet_NaN();
			double interceptM1 = std::numeric_limits<double>::quiet_NaN();
			double slopeM1 = std::numeric_limits<double>::quiet_NaN();
			double highgainM1 = std::numeric_limits<double>::quiet_NaN();
			double alphaM1 = std::numeric_limits<double>::quiet_NaN();
			double lrgainM1 = std::numeric_limits<double>::quiet_NaN();
			double lrexpM1 = std::numeric_limits<double>::quiet_NaN();
			double phiM1 = std::numeric_limits<double>::quiet_NaN();

			double lambdaM2 = std::numeric_limits<double>::quiet_NaN();
			double interceptM2 = std::numeric_limits<double>::quiet_NaN();
			double slopeM2 = std::numeric_limits<double>::quiet_NaN();
			double highgainM2 = std::numeric_limits<double>::quiet_NaN();
			double alphaM2 = std::numeric_limits<double>::quiet_NaN();
			double lrgainM2 = std::numeric_limits<double>::quiet_NaN();
			double lrexpM2 = std::numeric_limits<double>::quiet_NaN();
			double phiM2 = std::numeric_limits<double>::quiet_NaN();

		do {
			lambdaM1 = gsl_rng_uniform(randomGenerator) * (this->defaultModel.getParam("lambda").max - this->defaultModel.getParam("lambda").min) + this->defaultModel.getParam("lambda").min;
			interceptM1 = gsl_rng_uniform(randomGenerator)* (this->defaultModel.getParam("intercept").max - this->defaultModel.getParam("intercept").min) + this->defaultModel.getParam("intercept").min;
			slopeM1 = gsl_rng_uniform(randomGenerator) * (this->defaultModel.getParam("slope").max - this->defaultModel.getParam("slope").min) + this->defaultModel.getParam("slope").min;
			highgainM1 = gsl_rng_uniform(randomGenerator) * (this->defaultModel.getParam("highgain").max - this->defaultModel.getParam("highgain").min) + this->defaultModel.getParam("highgain").min;
			alphaM1 = gsl_rng_uniform(randomGenerator) * (this->defaultModel.getParam("alpha").max - this->defaultModel.getParam("alpha").min) + this->defaultModel.getParam("alpha").min;
			lrgainM1 = gsl_rng_uniform(randomGenerator) * (this->defaultModel.getParam("lrgain").max - this->defaultModel.getParam("lrgain").min) + this->defaultModel.getParam("lrgain").min;
			lrexpM1 = gsl_rng_uniform(randomGenerator) * (this->defaultModel.getParam("lrexp").max - this->defaultModel.getParam("lrexp").min) + this->defaultModel.getParam("lrexp").min;
			phiM1 = gsl_rng_uniform(randomGenerator) * (this->defaultModel.getParam("phi").max - this->defaultModel.getParam("phi").min) + this->defaultModel.getParam("phi").min;
		} while( // don't allow param values outside the ranges
			lambdaM1 < this->defaultModel.getParam("lambda").min && lambdaM1 > this->defaultModel.getParam("lambda").max &&
			interceptM1 < this->defaultModel.getParam("intercept").min && interceptM1 > this->defaultModel.getParam("intercept").max &&
			slopeM1 < this->defaultModel.getParam("slope").min && slopeM1 > this->defaultModel.getParam("slope").max &&
			highgainM1 < this->defaultModel.getParam("highgain").min && highgainM1 > this->defaultModel.getParam("highgain").max &&
			alphaM1 < this->defaultModel.getParam("alpha").min && alphaM1 > this->defaultModel.getParam("alpha").max &&
			lrgainM1 < this->defaultModel.getParam("lrgain").min && lrgainM1 > this->defaultModel.getParam("lrgain").max &&
			lrexpM1 < this->defaultModel.getParam("lrexp").min && lrexpM1 > this->defaultModel.getParam("lrexp").max &&
			phiM1 < this->defaultModel.getParam("phi").min && phiM1 > this->defaultModel.getParam("phi").max
		);

			model1.setMethod(this->method1, lambdaM1, slopeM1, interceptM1, highgainM1, phiM1, alphaM1, lrgainM1, lrexpM1);

			buffer << "iteration " << (i + 1) << " sampled parameters for " << model1.getMethod() << ": --lambda " << lambdaM1 << " --slope " << slopeM1 << " --intercept " << interceptM1 << " --highgain " << highgainM1 << " --phi " << phiM1 << " --alpha " << alphaM1 << " --lrgain " << lrgainM1 << " --lrexp " << lrexpM1 << std::endl;

			do {
				lambdaM2 = gsl_rng_uniform(randomGenerator) * (this->defaultModel.getParam("lambda").max - this->defaultModel.getParam("lambda").min) + this->defaultModel.getParam("lambda").min;
				interceptM2 = gsl_rng_uniform(randomGenerator)* (this->defaultModel.getParam("intercept").max - this->defaultModel.getParam("intercept").min) + this->defaultModel.getParam("intercept").min;
				slopeM2 = gsl_rng_uniform(randomGenerator) * (this->defaultModel.getParam("slope").max - this->defaultModel.getParam("slope").min) + this->defaultModel.getParam("slope").min;
				highgainM2 = gsl_rng_uniform(randomGenerator) * (this->defaultModel.getParam("highgain").max - this->defaultModel.getParam("highgain").min) + this->defaultModel.getParam("highgain").min;
				alphaM2 = gsl_rng_uniform(randomGenerator) * (this->defaultModel.getParam("alpha").max - this->defaultModel.getParam("alpha").min) + this->defaultModel.getParam("alpha").min;
				lrgainM2 = gsl_rng_uniform(randomGenerator) * (this->defaultModel.getParam("lrgain").max - this->defaultModel.getParam("lrgain").min) + this->defaultModel.getParam("lrgain").min;
				lrexpM2 = gsl_rng_uniform(randomGenerator) * (this->defaultModel.getParam("lrexp").max - this->defaultModel.getParam("lrexp").min) + this->defaultModel.getParam("lrexp").min;
				phiM2 = gsl_rng_uniform(randomGenerator) * (this->defaultModel.getParam("phi").max - this->defaultModel.getParam("phi").min) + this->defaultModel.getParam("phi").min;
			} while( // don't allow param values outside the ranges
				lambdaM2 < this->defaultModel.getParam("lambda").min && lambdaM2 > this->defaultModel.getParam("lambda").max &&
				interceptM2 < this->defaultModel.getParam("intercept").min && interceptM2 > this->defaultModel.getParam("intercept").max &&
				slopeM2 < this->defaultModel.getParam("slope").min && slopeM2 > this->defaultModel.getParam("slope").max &&
				highgainM2 < this->defaultModel.getParam("highgain").min && highgainM2 > this->defaultModel.getParam("highgain").max &&
				alphaM2 < this->defaultModel.getParam("alpha").min && alphaM2 > this->defaultModel.getParam("alpha").max &&
				lrgainM2 < this->defaultModel.getParam("lrgain").min && lrgainM2 > this->defaultModel.getParam("lrgain").max &&
				lrexpM2 < this->defaultModel.getParam("lrexp").min && lrexpM2 > this->defaultModel.getParam("lrexp").max &&
				phiM2 < this->defaultModel.getParam("phi").min && phiM2 > this->defaultModel.getParam("phi").max
			);

			model2.setMethod(this->method2, lambdaM2, slopeM2, interceptM2, highgainM2, phiM2, alphaM2, lrgainM2, lrexpM2);

			buffer << "iteration " << (i + 1) << " sampled parameters for " << model2.getMethod() << ": --lambda " << lambdaM2 << " --slope " << slopeM2 << " --intercept " << interceptM2 << " --highgain " << highgainM2 << " --phi " << phiM2 << " --alpha " << alphaM2 << " --lrgain " << lrgainM2 << " --lrexp " << lrexpM2 << std::endl;

			#ifndef NOOPENMP
				}
				// end critical section
			#endif

		}

	#if VERBOSITY >= 1
		std::cout << "crossfitting():\tgenerating data (#" <<  (i + 1) << "/" << noOfIterations << ")."<< std::endl;
	#endif

		// generate data with the fitted models -> parametric bootstrapping
		std::vector<RatingDataPoint> generatedRatingsModel1 = model1.generateRatings();
		std::vector<RatingDataPoint> generatedRatingsModel2 = model2.generateRatings();
		assert(generatedRatingsModel1.size() == generatedRatingsModel2.size());

		if (addNoise) {
			// add gaussian noise (SD = 0.3 = SE mean for empirical data from Kluth et al., 2018) to the ratings (only for landscaping, as said in Navarro et al. 2004)
			// critical section, thus avoid more than one write-access to the random number generator
			#ifndef NOOPENMP
				#pragma omp critical
				{
			#endif
				buffer << "iteration " << (i + 1) << ", generated ratings + noise" << std::endl;
				for (size_t r = 0; r < generatedRatingsModel1.size(); r++) {
					buffer << model1.getMethod() << "-data-" << r << ": " << generatedRatingsModel1[r].meanRating << std::endl;
					buffer << model2.getMethod() << "-data-" << r << ": " << generatedRatingsModel2[r].meanRating << std::endl;

					generatedRatingsModel1[r].meanRating = generatedRatingsModel1[r].meanRating + gsl_ran_gaussian(randomGenerator, 0.3);

					// do not allow ratings outside of the rating scale:
					if (generatedRatingsModel1[r].meanRating < this->ratingScale.first) {
						generatedRatingsModel1[r].meanRating = this->ratingScale.first;
					} else if (generatedRatingsModel1[r].meanRating > this->ratingScale.second) {
						generatedRatingsModel1[r].meanRating = this->ratingScale.second;
					}

					generatedRatingsModel2[r].meanRating = generatedRatingsModel2[r].meanRating + gsl_ran_gaussian(randomGenerator, 0.3);

					if (generatedRatingsModel2[r].meanRating < this->ratingScale.first) {
						generatedRatingsModel2[r].meanRating = this->ratingScale.first;
					} else if (generatedRatingsModel2[r].meanRating > this->ratingScale.second) {
						generatedRatingsModel2[r].meanRating = this->ratingScale.second;
					}

					buffer << model1.getMethod() << "-noisy-data-" << r << ": " << generatedRatingsModel1[r].meanRating << std::endl;
					buffer << model2.getMethod() << "-noisy-data-" << r << ": " << generatedRatingsModel2[r].meanRating << std::endl;
					buffer << std::endl;
			}
			#ifndef NOOPENMP
				}
			#endif
		}

		// save the generated (potentially noisy data)
		for (size_t r = 0; r < generatedRatingsModel1.size(); r++) {
			data1[i].push_back(generatedRatingsModel1[r].meanRating);
			data2[i].push_back(generatedRatingsModel2[r].meanRating);
		}

		// fit both models to both generated data sets:
	#if VERBOSITY >= 1
		std::cout << "crossfitting():\tfitting models to generated data (#" << (i+1) << "/" << noOfIterations << ")."<< std::endl;
	#endif

		// model 1 generated data first
		model1.setEmpiricalData(generatedRatingsModel1, this->nrOfROs, this->ratingScale);
		model2.setEmpiricalData(generatedRatingsModel1, this->nrOfROs, this->ratingScale);

		// reset model to default parameter values
		model1.setMethod(this->method1);
		model2.setMethod(this->method2);

	#if VERBOSITY >= 1
		std::cout << "crossfitting():\t" << model1.getMethod() << " has generated random ratings. " << model1.getMethod() << " is fitting these data now (#" << (i+1) << "/" << noOfIterations << ")." << std::endl;
	#endif

		double model1GOF_model1Data = model1.parameterEstimation(generatedRatingsModel1, false);

	#if VERBOSITY >= 1
		std::cout << "crossfitting():\t" << model1.getMethod() << " has generated random ratings. " << model2.getMethod() << " is fitting these data now (#" << (i+1) << "/" << noOfIterations << ")." << std::endl;
	#endif

		double model2GOF_model1Data = model2.parameterEstimation(generatedRatingsModel1, false);

		deltaGOFsModel1Data[i] = model1GOF_model1Data - model2GOF_model1Data;
		GOF1_data1[i] = model1GOF_model1Data;
		GOF2_data1[i] = model2GOF_model1Data;

		// model 2 generated data
		model1.setEmpiricalData(generatedRatingsModel2, this->nrOfROs, ratingScale);
		model2.setEmpiricalData(generatedRatingsModel2, this->nrOfROs, ratingScale);

		// reset model to default parameter values
		model1.setMethod(this->method1);
		model2.setMethod(this->method2);

		#if VERBOSITY >= 1
		std::cout << "crossfitting():\t" << model2.getMethod() << " has generated random ratings. " << model1.getMethod() << " is fitting these data now (#" << (i+1) << "/" << noOfIterations << ")."<< std::endl;
		#endif

		double model1GOF_model2Data = model1.parameterEstimation(generatedRatingsModel2, false);

		#if VERBOSITY >= 1
		std::cout << "crossfitting():\t" << model2.getMethod() << " has generated random ratings. " << model2.getMethod() << " is fitting these data now (#" << (i+1) << "/" << noOfIterations << ")."<< std::endl;
		#endif

		double model2GOF_model2Data = model2.parameterEstimation(generatedRatingsModel2, false);

		deltaGOFsModel2Data[i] = model1GOF_model2Data - model2GOF_model2Data;
		GOF1_data2[i] = model1GOF_model2Data;
		GOF2_data2[i] = model2GOF_model2Data;

		#ifndef NOOPENMP
			#pragma omp critical
			{
		#endif
			++counter;
			double percentage = (100.0 * ((double) counter / (double) noOfIterations));
			std::chrono::system_clock::duration estimatedRemainingTime = std::chrono::duration_cast<std::chrono::system_clock::duration>(100 * ((std::chrono::system_clock::now() - startTime) / percentage));
			std::chrono::system_clock::time_point endTime = startTime + estimatedRemainingTime;
			time_t endTime_C = std::chrono::system_clock::to_time_t(endTime);
			std::cout << "crossfitting():\tstarted on " << std::put_time(localtime(&startTime_C), "%b %d, %T"); // welcome to the ugly world of C++: if you put both time informations in the same stream, they will print the same time ...
			std::cout << ", finished by (estimated): " << std::put_time(localtime(&endTime_C), "%b %d, %T") << ", progress: "  << std::setprecision(2) << percentage << " %\r" << std::endl;
			buffer << "iteration " << (i + 1) << ", " << model1.getMethod() << "-data, " << model1.getMethod() << "-fit: " << model1GOF_model1Data << "; " << model2.getMethod() << "-fit: " << model2GOF_model1Data << std::endl;
			buffer << "iteration " << (i + 1) << ", " << model2.getMethod() << "-data, " << model1.getMethod() << "-fit: " << model1GOF_model2Data << "; " << model2.getMethod() << "-fit: " << model2GOF_model2Data << std::endl;
		#ifndef NOOPENMP
			}
		#endif
	}
	#ifndef NOOPENMP
	}
	#endif

	std::cout << std::endl;

	debugFile << buffer.str();
	debugFile.close();

	gsl_rng_free(randomGenerator);

	this->deltaGOFs = std::pair<std::vector<double>, std::vector<double> > (deltaGOFsModel1Data, deltaGOFsModel2Data);
	computeDeltaGOFsHistograms();

	this->landscape.model1Data = data1;
	this->landscape.model2Data = data2;

	this->landscape.model1GOF_model1Data = GOF1_data1;
	this->landscape.model2GOF_model1Data = GOF2_data1;
	this->landscape.model1GOF_model2Data = GOF1_data2;
	this->landscape.model2GOF_model2Data = GOF2_data2;

	return;
}

std::vector<RatingDataPoint> Comparison::bootstrapData(std::vector<RatingDataPoint> emp) {
	//random generator from GNU Scientific Library
	gsl_rng* randomGenerator = gsl_rng_alloc(gsl_rng_mt19937);
	long seed = time(NULL) * getpid();
	gsl_rng_set(randomGenerator, (unsigned long) seed);

	std::vector<RatingDataPoint> bootstrapSample (emp.size());

	for(size_t i = 0; i < emp.size(); i++){

		size_t randomIdx = gsl_rng_uniform_int(randomGenerator, emp.size());
		#if VERBOSITY > 1
		std::cout << "bootstrapData():\tSampled datapoint #" << randomIdx << std::endl;
		#endif

		bootstrapSample[i] = emp[randomIdx];
	}

	gsl_rng_free(randomGenerator);

	return bootstrapSample;
}

void Comparison::computeDeltaGOFsHistograms() {

	assert(deltaGOFs.first.size() == deltaGOFs.second.size());

	double model1Min = this->deltaGOFs.first[0];
	double model1Max = this->deltaGOFs.first[0];
	double model2Min = this->deltaGOFs.second[0];
	double model2Max = this->deltaGOFs.second[0];

	for (size_t i = 0; i < deltaGOFs.first.size(); i++){
		if(deltaGOFs.first[i] < model1Min){
			model1Min = deltaGOFs.first[i];
		}
		if(deltaGOFs.first[i] > model1Max){
			model1Max = deltaGOFs.first[i];
		}

		if(deltaGOFs.second[i] < model2Min){
			model2Min = deltaGOFs.second[i];
		}
		if(deltaGOFs.second[i] > model2Max){
			model2Max = deltaGOFs.second[i];
		}
	}

	// include min and max into the histogram

	unsigned int nbins = 50;

	double model1BinWidth = std::abs(model1Min - model1Max) / nbins;
	double model2BinWidth = std::abs(model2Min - model2Max) / nbins;

	// avoid GSL error if minimum and maximum of histogram are equal ( -> binsize 0)
	// should only happen with a very small number of iterations
	if(std::abs(model1BinWidth) < std::numeric_limits<double>::epsilon()){
		model1BinWidth = 2*std::numeric_limits<double>::epsilon();
	}
	if(std::abs(model2BinWidth) < std::numeric_limits<double>::epsilon()){
		model2BinWidth = 2*std::numeric_limits<double>::epsilon();
	}

	model1Min = model1Min - model1BinWidth;
	model1Max = model1Max + model1BinWidth;
	model2Min = model2Min - model2BinWidth;
	model2Max = model2Max + model2BinWidth;

	gsl_histogram* hist1 = gsl_histogram_alloc(nbins);
	gsl_histogram_set_ranges_uniform(hist1, model1Min, model1Max);

	gsl_histogram* hist2 = gsl_histogram_alloc(nbins);
	gsl_histogram_set_ranges_uniform(hist2, model2Min, model2Max);

	for (size_t i = 0; i < deltaGOFs.first.size(); i++){
		gsl_histogram_increment(hist1, deltaGOFs.first[i]);
		gsl_histogram_increment(hist2, deltaGOFs.second[i]);
	}

	std::stringstream buffer1, buffer2;

	for (size_t i = 0; i < gsl_histogram_bins(hist1); i++){

		double lower = 0.0;
		double upper = 0.0;

		gsl_histogram_get_range(hist1, i, &lower, &upper);
		buffer1 << ((lower+upper)/2.0) << " " << gsl_histogram_get(hist1, i) << std::endl;

		gsl_histogram_get_range(hist2, i, &lower, &upper);
		buffer2 << ((lower+upper)/2.0) << " " << gsl_histogram_get(hist2, i) << std::endl;
	}

	gsl_histogram_free(hist1);
	gsl_histogram_free(hist2);

	std::string hist1String = buffer1.str();
	std::string hist2String = buffer2.str();

	this->deltaGOFsHistograms.histogram_model1Data = hist1String;
	this->deltaGOFsHistograms.histogram_model2Data = hist2String;
}

void Comparison::savePosterior(Model m, unsigned int chain, std::string basefilename) {

	std::vector<std::map<std::string, float> > posteriorValues = m.getVisitedParams();

	std::stringstream filename;
	filename << basefilename << "_posterior" << chain;
	std::ofstream distributionFile;
	distributionFile.open(filename.str());

	// file header
	distributionFile << "step";

	for (std::map<std::string, float>::iterator param = posteriorValues[0].begin(); param != posteriorValues[0].end(); ++param) {
		distributionFile << "," << param->first;
	}

	distributionFile << std::endl;

	for (size_t step = 0; step < posteriorValues.size(); ++step) {

		distributionFile << step;

		for (std::map<std::string, float>::iterator param = posteriorValues[step].begin(); param != posteriorValues[step].end(); ++param) {
			distributionFile << "," << param->second;
		}

		distributionFile << std::endl;
	}

	distributionFile.close();

}
