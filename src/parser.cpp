/*
 * parser.cpp
 *
 * This file is part of sl-model:
 * https://www.gitlab.com/tkluth/sl-model
 *
 * Copyright(c) 2014-2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 *
 * From October 2014 to September 2018, the
 * development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 *
 */

/**
 * the higher this value, the more output
 * if set to 0 only important messages will be printed
 * this debug switch should be placed in every cpp file and only valid for one file!
 */
#define VERBOSITY 0

#include "parser.h"

#include <utility>
#include <iostream>
#include <fstream>

Parser::Parser(double magnifier, bool flipY) {
	this->scale = magnifier;
	this->revertY = flipY;
}

std::vector<Polygon> Parser::parseCGALPolygons(std::string filename, bool manyPolygons) {

#if VERBOSITY >= 1
	std::cout << "Parser:\t reading CGAL Polygon(s) from " << filename << std::endl;
#endif

	std::ifstream fs(filename.c_str());

	if (!fs) {
		std::cout << "Trying to read " << filename << std::endl;
		throw std::runtime_error("Couldn't read LO/RO file. Quitting.");
	}

	std::vector<Polygon> cgalPolygons;

	if (manyPolygons) {
		if (fs.is_open()) {
			std::string line;
			while (getline(fs, line)) {
				std::istringstream iss(line);
				Polygon p;
				if (iss >> p) {
					cgalPolygons.push_back(p);
#if VERBOSITY >= 2
					std::cout << "Parser:\t just read " << p << std::endl;
#endif
				}
			}
		}

		fs.close();
	} else { // only one polygon in the file, don't read it line by line
		Polygon p;
		fs >> p;
		cgalPolygons.push_back(p);
#if VERBOSITY >= 2
		std::cout << "Parser:\t just read " << p << std::endl;
#endif
	}

	return cgalPolygons;
}

std::vector<double> Parser::parseEmpiricalMeanData(std::string filename) {
	std::ifstream fs(filename.c_str());
	if (!fs) {
		std::cout << "Trying to read " << filename << std::endl;
		throw std::runtime_error("Couldn't read empirical data file. Quitting.");
	}

#if VERBOSITY >= 1
	std::cout << "Parser:\t Reading empirical data from " << filename << std::endl;
#endif

	std::vector<double> empiricalData;

	if (fs.is_open()) {
		std::string line;
		while (getline(fs, line)) {
			std::istringstream iss(line);
			double res;
			if (iss >> res) {
				empiricalData.push_back(res);
#if VERBOSITY >= 2
				std::cout << "Parser:\t just read " << res << std::endl;
#endif
			}
		}
	}

	fs.close();

	return empiricalData;
}

std::pair<unsigned char, std::vector<std::vector<unsigned char> > > Parser::parseEmpiricalRatingDist(std::string filename, unsigned int noOfRatings) {

	unsigned char noOfParticipants = 0;
	std::vector<std::vector<unsigned char> > empiricalData;

	std::ifstream fs(filename.c_str());
	if (!fs) {
		#if VERBOSITY >= 1
		std::cout << "Tried to read " << filename << std::endl;
		std::cout << "assuming no information about rating distribution is available, returning 0s" << std::endl;
		#endif
		for (size_t i = 0; i < noOfRatings; i++) {
			std::vector<unsigned char> tmp;
			empiricalData.push_back(tmp);
			empiricalData[i].push_back(0.0);
		}

		return std::pair<unsigned char, std::vector<std::vector<unsigned char> > >(noOfParticipants, empiricalData);
	}

	#if VERBOSITY >= 1
		std::cout << "Parser:\t Reading empirical rating distribution from " << filename << std::endl;
	#endif

	bool firstline = true;

	if (fs.is_open()) {
		std::string line;
		while (getline(fs, line)) {
			std::istringstream iss(line);
			std::vector<unsigned char> numbers;
			// using unsigned char here does not work to read in full numbers
			unsigned int res;
			while (iss >> res) {
				if(firstline) {
					noOfParticipants = (unsigned char) res;
				} else {
					numbers.push_back((unsigned char)res);
				}
			#if VERBOSITY >= 1
				std::cout << "Parser:\t just read " << res << ", in firstline?: " << firstline << std::endl;
			#endif
			}
			if(! firstline){
				empiricalData.push_back(numbers);
			}
			firstline = false;
		}
	}

	fs.close();

	return std::pair<unsigned char, std::vector<std::vector<unsigned char> > >(noOfParticipants, empiricalData);
}

std::vector<std::vector<unsigned int> > Parser::parseEmpiricalIndividualData(std::string filename, size_t noOfLOs) {

	std::vector<std::vector<unsigned int> > empiricalData;

	std::ifstream fs(filename.c_str());
	if (!fs) {
		#if VERBOSITY >= 1
		std::cout << "Tried to read " << filename << std::endl;
		std::cout << "assuming no information about individual ratings is available, returning 0s" << std::endl;
		#endif

		for (size_t s = 0; s < noOfLOs; ++s) {
			std::vector<unsigned int> tmp;
			empiricalData.push_back(tmp);
			empiricalData[s].push_back(0);
		}

		return empiricalData;
	}

	#if VERBOSITY >= 1
		std::cout << "Parser:\t Reading empirical individual ratings from " << filename << std::endl;
	#endif

	if (fs.is_open()) {
		std::string line;
		while (getline(fs, line)) {
			#if VERBOSITY >= 1
			std::cout << "Parser:\t next LO " << std::endl;
			#endif
			std::istringstream iss(line);
			std::vector<unsigned int> numbers;
			unsigned int res;
			while (iss >> res) {
				numbers.push_back(res);
				#if VERBOSITY >= 1
				std::cout << "Parser:\t just read next subject's rating: " << res << std::endl;
				#endif
			}
			empiricalData.push_back(numbers);
		}
	}

	fs.close();

	return empiricalData;
}

std::pair<unsigned int, unsigned int> Parser::parseRatingScale(std::string filename) {
	std::ifstream fs(filename.c_str());
	if (!fs) {
		std::cout << "Trying to read " << filename << " ...\n Couldn't read rating scale file. Please specify a valid rating scale file. Quitting." << std::endl;
		throw std::runtime_error("Couldn't read rating scale file. Please specify a valid rating scale file. Quitting.");
	}

#if VERBOSITY >= 1
	std::cout << "Parser:\t Reading rating scale from " << filename << std::endl;
#endif

	std::pair<unsigned int, unsigned int> ratingScale;

	if (fs.is_open()) {
		std::string line;
		if (getline(fs, line)) {
			std::istringstream iss(line);
			unsigned int min;
			if (iss >> min) {
				ratingScale.first = min;
#if VERBOSITY >= 2
				std::cout << "Parser-scale:\t just read min " << min << std::endl;
#endif
			}
		} else {
			std::cout << "Trying to read min from " << filename << std::endl;
			throw std::runtime_error("Couldn't read rating scale file. Quitting.");
		}

		if(getline(fs, line)) {
			std::istringstream iss(line);
			unsigned int max;
			if (iss >> max) {
				ratingScale.second = max;
#if VERBOSITY >= 2
				std::cout << "Parser-scale:\t just read max " << max << std::endl;
#endif
			}
		} else {
			std::cout << "Trying to read max from " << filename << std::endl;
			throw std::runtime_error("Couldn't read rating scale file. Quitting.");
		}
	}

	fs.close();

	return ratingScale;
}

preposition Parser::parsePreposition(std::string filename) {
	std::ifstream fs(filename.c_str());
	if (!fs) {
		std::cout << "Trying to read " << filename << "...\n Couldn't read the preposition file. Please specify a valid preposition file. Quitting." << std::endl;
		throw std::runtime_error("Couldn't read the preposition file. Please specify a valid preposition file. Quitting.");
	}

#if VERBOSITY >= 1
	std::cout << "Parser:\t Reading preposition from " << filename << std::endl;
#endif

	//default to INVALID
	preposition prep = INVALID;

	if (fs.is_open()) {
		std::string line;
		if(getline(fs, line)) {
			std::istringstream iss(line);
			std::string tmp;
			if (iss >> tmp) {
				if(tmp == "above") {
					prep = ABOVE;
				} else if (tmp == "below") {
					prep= BELOW;
				} else {
					std::cout << "Preposition must be either 'above' or 'below'." << filename << " contains '" << tmp << "'. Quitting." << std::endl;
					throw std::runtime_error("Wrong type of preposition specified.");
				}
#if VERBOSITY >= 1
				std::cout << "Parser-preposition:\t just read preposition: " << prepToString(prep) << std::endl;
#endif
			}
		} else {
			std::cout << "Trying to read preposition from " << filename << std::endl;
			throw std::runtime_error("Couldn't read the preposition file. Quitting.");
		}
	}

	fs.close();

	return prep;
}

