/*
 * locatedObject.cpp
 *
 * This file is part of sl-model:
 * https://www.gitlab.com/tkluth/sl-model
 *
 * Copyright(c) 2014-2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 *
 * From October 2014 to September 2018, the
 * development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 *
 */

/**
 * the higher this value, the more output
 * if set to 0 only important messages will be printed
 * this debug switch is placed in every file and only valid for one file!
 */
#define VERBOSITY 0

#include "locatedObject.h"

LocatedObject::LocatedObject(){
	std::stringstream s;
	s << "1" << std::endl << "1 1" << std::endl;
	std::istringstream iss(s.str());
	Polygon tmp;
	iss >> tmp;
	LocatedObject(tmp, false, 1.0, 0);
}

LocatedObject::LocatedObject(Polygon polygonCGAL, bool flipYAxis, double magnifier, unsigned int idx) :
	// call the super constructor
	PolygonObject::PolygonObject(polygonCGAL, flipYAxis, magnifier, idx) {
	// otherwise empty constructor
}
