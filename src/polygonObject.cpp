/*
 * polygonObject.cpp
 *
 * This file is part of sl-model:
 * https://www.gitlab.com/tkluth/sl-model
 *
 * Copyright(c) 2014-2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 *
 * From October 2014 to September 2018, the
 * development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 *
 */

/**
 * the higher this value, the more output
 * if set to 0 only important messages will be printed
 * this debug switch is placed in every file and only valid for one file!
 */
#define VERBOSITY 0

#include "polygonObject.h"
#include <algorithm> // for std::binary_search

PolygonObject::PolygonObject() {
	std::stringstream s;
	s << "1" << std::endl << "1 1" << std::endl;
	std::istringstream iss(s.str());
	Polygon tmp;
	iss >> tmp;
	PolygonObject(tmp, false, 1.0, 0);
}

PolygonObject::PolygonObject(Polygon polyCGAL, bool revertedYAxis, double magnifier, unsigned int idx) {

	this->scale = magnifier;
	this->cgalPolygon = polyCGAL;
	this->index = idx;

	if (!this->cgalPolygon.is_simple()) {
		std::cout << "The following polygon is not simple: " << this->cgalPolygon << std::endl;
		throw std::runtime_error("Polygon must be simple.");
	}

	for (Polygon::Vertex_const_iterator it = this->cgalPolygon.vertices_begin(); it != this->cgalPolygon.vertices_end(); it++) {
		if (revertedYAxis) {
			this->cgalPolygon.set(it, Point(it->x() * scale, (-1.0 * it->y() * scale)));
		} else {
			this->cgalPolygon.set(it, Point(it->x() * scale, (it->y() * scale)));
		}
	}

	this->revertedY = revertedYAxis;

	if (this->cgalPolygon.is_counterclockwise_oriented()) {
		#if VERBOSITY >= 1
		std::cout << "PolygonObject():\t Polygon must be clockwise oriented. Turning polygons orientation..." << std::endl;
		#endif
		this->cgalPolygon.reverse_orientation();
	}

	calculateTop();
	calculateBottom();

	calculatePolygonPoints();

	this->height = std::abs(this->hightop - this->lowBottom);
	this->width = std::abs(this->cgalPolygon.right_vertex()->x() - this->cgalPolygon.left_vertex()->x());

	calculateCenterOfMass();
	calculateCenterOfObject();

#if VERBOSITY >= 1
	std::cout << "PolygonObject():\t" << "Polygon Object: " << cgalPolygon << ", width * height: " << width << " * " << height << std::endl;
#endif
}

void PolygonObject::calculateTop() {
	// calculate lowtop and hightop
	this->hightop = this->cgalPolygon.top_vertex()->y();
	this->rightTop = *(this->cgalPolygon.right_vertex());
	this->leftTop = *(this->cgalPolygon.left_vertex());
	std::vector<Point> leftPoints;

	for (Polygon::Vertex_const_iterator it = this->cgalPolygon.vertices_begin(); it != this->cgalPolygon.vertices_end(); it++) {
		if (it->x() <= leftTop.x()) {
			leftPoints.push_back(*it);
		}
	}

	for (size_t p = 0; p < leftPoints.size(); p++) {
		if (leftPoints[p].y() > leftTop.y()) {
			leftTop = leftPoints[p];
		}
	}

	this->lowtop = (leftTop.y() < rightTop.y()) ? leftTop.y() : rightTop.y();

	Polygon::Vertex_const_circulator topLineStart = this->cgalPolygon.vertices_circulator();

#if VERBOSITY >= 1
	std::cout << "calculateTop():\t" << "hightop: " << hightop << " lowtop: " << lowtop << std::endl;
	std::cout << "calculateTop():\t" << "leftTop: " << leftTop << " rightTop: " << rightTop << std::endl;
	std::cout << "calculateTop():\t" << "vertices_circulator(): " << *topLineStart << std::endl;
#endif

	while (*topLineStart != this->leftTop) {
		topLineStart++;
		#if VERBOSITY >= 1
		std::cout << "circulating vertices; next point: " << *topLineStart << std::endl;
		#endif
	}

#if VERBOSITY >= 1
	std::cout << "calculateTop():\t" << "topLineStart, should be left top point " << *topLineStart << std::endl;
#endif

	Polygon::Vertex_const_circulator nextPoint = topLineStart;

	// leftTop to next point (clockwise) -> first segment

	this->topEdges.clear();

	while (*nextPoint != this->rightTop) {
		nextPoint = topLineStart + 1;
		Segment edge = Segment(*topLineStart, *nextPoint);
		topLineStart++;
#if VERBOSITY >= 1
		std::cout << "calculateTop():\t" << "New edge for top-edges: " << edge << std::endl;
#endif
		if (nextPoint->y() < lowtop) {
			lowtop = nextPoint->y();
#if VERBOSITY >= 1
			std::cout << "found smaller lowtop: " << lowtop << std::endl;
#endif
		}
		this->topEdges.push_back(edge);
	}
}

void PolygonObject::calculatePolygonPoints() {
	CGAL::Bbox_2 polygonObjectBox = this->cgalPolygon.bbox();

	for (double x = polygonObjectBox.xmin(); x <= polygonObjectBox.xmax(); x++) {
		for (double y = polygonObjectBox.ymin(); y <= polygonObjectBox.ymax(); y++) {
			Point point(x, y);

			this->polygonObjectBBPoints.push_back(point);
			if (isInsideCGAL(&point)) {
				this->polygonObjectPoints.push_back(point);
			}
		}
	}
	if (this->polygonObjectPoints.size() == 0) {
#if VERBOSITY >= 1
		std::cout
				<< "calculatePolygonpoints:\t Warning: Found no points inside the polygon. It seems to be too small. Try -z 2 (or a bigger value). Added the left-top edge point as only point."
				<< std::endl;
#endif
		this->polygonObjectPoints.push_back(this->getLeftTop());
	}
}

bool PolygonObject::isInsideCGAL(Point* p) {
	return (this->cgalPolygon.has_on_bounded_side(*p) || this->cgalPolygon.has_on_boundary(*p));
}

bool PolygonObject::isInside(Point* p) {

	assert(this->polygonObjectPoints.size() > 0);

	if (std::binary_search(this->polygonObjectPoints.begin(), this->polygonObjectPoints.end(), *p)) {
		return true;
	} else {
		return false;
	}
}
void PolygonObject::calculateCenterOfObject() {

	this->centerOfObject = Point(round((this->width / 2.0) + this->cgalPolygon.left_vertex()->x()), round((this->height / 2.0) + this->cgalPolygon.bottom_vertex()->y()));

#if VERBOSITY >= 1
	std::cout << "calculateCenterOfObject():\t computed center-of-object: " << this->centerOfObject << std::endl;
#endif
}

void PolygonObject::calculateCenterOfMass() {

	assert(this->polygonObjectPoints.size() > 0);

	double meanX = 0.0;
	double meanY = 0.0;

	for(size_t p = 0; p < this->polygonObjectPoints.size(); p++){
		meanX += this->polygonObjectPoints[p].x();
		meanY += this->polygonObjectPoints[p].y();
	}

	// discretize center-of-mass
	meanX = round(meanX / (double) this->polygonObjectPoints.size());
	meanY = round(meanY / (double) this->polygonObjectPoints.size());

	this->centerOfMass = Point(meanX, meanY);

#if VERBOSITY >= 1
	std::cout << "calculateCenterOfMass():\t computed center-of-mass: " << this->centerOfMass << ", # of points: " << this->polygonObjectPoints.size() << std::endl;
#endif
}

Point* PolygonObject::getCenterOfObject() {
	return &this->centerOfObject;
}

Point* PolygonObject::getCenterOfMass() {
	return &this->centerOfMass;
}

double PolygonObject::getScale() {
	return this->scale;
}

bool PolygonObject::isYReverted() {
	return this->revertedY;
}

const Polygon* PolygonObject::getCGALPolygon() const {
	return &(this->cgalPolygon);
}

double PolygonObject::getLowtop() {
	return this->lowtop;
}

double PolygonObject::getHightop() {
	return this->hightop;
}

Point PolygonObject::getLeftTop() {
	return this->leftTop;
}

Point PolygonObject::getRightTop() {
	return this->rightTop;
}

std::vector<Segment> PolygonObject::getTopEdges() {
	return this->topEdges;
}

std::vector<Point>* PolygonObject::getPoints() {
	return &(this->polygonObjectPoints);
}

std::vector<Point>* PolygonObject::getBBPoints() {
	return &(this->polygonObjectBBPoints);
}

double PolygonObject::getHeight() {
	assert(this->height > 0);
	return this->height;
}

double PolygonObject::getWidth() {
	assert(this->width > 0);
	return this->width;
}

void PolygonObject::calculateBottom() {
	// calculate lowBottom and highBottom
	this->highBottom = this->cgalPolygon.bottom_vertex()->y();
	this->rightBottom = *(this->cgalPolygon.right_vertex());
	this->leftBottom = *(this->cgalPolygon.left_vertex());

	std::vector<Point> rightPoints;

	for (Polygon::Vertex_iterator it = this->cgalPolygon.vertices_begin(); it != this->cgalPolygon.vertices_end(); it++) {
		if (it->x() >= rightBottom.x()) {
			rightPoints.push_back(*it);
		}
	}

	for (size_t p = 0; p < rightPoints.size(); p++) {
		if (rightPoints[p].y() < rightBottom.y()) {
			rightBottom = rightPoints[p];
		}
	}

	this->lowBottom = (leftBottom.y() < rightBottom.y()) ? leftBottom.y() : rightBottom.y();

#if VERBOSITY >= 1
	std::cout << "calculateBottom():\t" << "highBottom: " << highBottom << ", lowBottom: " << lowBottom << " , leftBottom: " << this->leftBottom << std::endl;
#endif

	Polygon::Vertex_const_circulator bottomLineStart = this->cgalPolygon.vertices_circulator();

	this->bottomEdges.clear();

	while (*bottomLineStart != this->rightBottom) {
		bottomLineStart++;
	}

	Polygon::Vertex_const_circulator nextPoint = bottomLineStart;

	while (*nextPoint != this->leftBottom) {
		if (bottomLineStart->y() < lowBottom) {
			this->lowBottom = bottomLineStart->y();
#if VERBOSITY >= 1
			std::cout << "calculateBottom():\tfound a new lowBottom: " << this->lowBottom << std::endl;
#endif
		}
		if (bottomLineStart->y() > highBottom) {
			this->highBottom = bottomLineStart->y();
#if VERBOSITY >= 1
			std::cout << "calculateBottom():\tfound a new highBottom: " << this->highBottom << std::endl;
#endif
		}
		nextPoint = bottomLineStart + 1;
		Segment edge = Segment(*bottomLineStart, *nextPoint);
		bottomLineStart++;
		this->bottomEdges.push_back(edge);

#if VERBOSITY >= 1
		std::cout << "calculateBottom():\t" << "edge for LO-bottom: " << edge << std::endl;
#endif
	}

	if (bottomEdges.size() == 0) {
		//single-point LO, add this single point to the bottom edges std::list
		Segment singleEdge = Segment(this->leftBottom, this->leftBottom);
		bottomEdges.push_back(singleEdge);
#if VERBOSITY >= 1
		std::cout << "calculateBottom():\t" << "single-point LO, adding this point to bottom-edges: " << singleEdge << std::endl;
#endif
	}
}

double PolygonObject::getLowBottom() {
	return this->lowBottom;
}

double PolygonObject::getHighBottom() {
	return this->highBottom;
}

Point PolygonObject::getLeftBottom() {
	return this->leftBottom;
}

Point PolygonObject::getRightBottom() {
	return this->rightBottom;
}

std::vector<Segment> PolygonObject::getBottomEdges() {
	return this->bottomEdges;
}

unsigned int PolygonObject::getIndex(){
	return this->index;
}
