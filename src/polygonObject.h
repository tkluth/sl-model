/*
 * polygonObject.h
 *
 * This file is part of sl-model
 * https://www.gitlab.com/tkluth/sl-model
 *
 * Copyright(c) 2014-2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 *
 * From October 2014 to September 2018, the
 * development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 *
 */

#ifndef POLYGONOBJECT_H
#define POLYGONOBJECT_H
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Boolean_set_operations_2.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/centroid.h>

#include <iostream>
#include <vector>
#include <set>
#include <unordered_map>

typedef CGAL::Exact_predicates_inexact_constructions_kernel CGALKernel;
typedef CGAL::Point_2<CGALKernel> Point;
typedef CGAL::Segment_2<CGALKernel> Segment;
typedef CGALKernel::Vector_2 Vector;
typedef CGAL::Polygon_2<CGALKernel> Polygon;
typedef Polygon::Vertex_circulator Vertex_circulator;

/**
 * This class represents a polygon that can be either a RO or a LO. This class contains methods used for ROs as well as LOs.
 * Basically, this class is a wrapper for a CGAL 2D polygon and just adds some methods.
 * The classes \ref LocatedObject and \ref ReferenceObject are both inheriting from PolygonObject.
 */
class PolygonObject {
	public:

		/**
		 * empty constructor
		 * */
		 PolygonObject();

		/**
		 * constructs a PolygonObject object
		 * @param polyCGAL a CGAL 2D polygon
		 * @param revertedYAxis if true, the Y-coordinates are treated as upside down (the higher the point, the lower the Y-coordinate)
		 * @param magnifier a factor with which the polygon was multiplied in order to zoom/shrink
		 * @param idx a (hopefully) unique index
		 */
		PolygonObject(Polygon polyCGAL, bool revertedYAxis, double magnifier, unsigned int idx);

		/**
		 * Uses std::binary_search on \ref polygonObjectPoints, i.e., better runtime than CGAL-methods (\ref isInsideCGAL())
		 * @param p the 2d CGAL Point to be checked
		 * @returns true, if p is inside the polygon and false if it is outside.
		 * */
		bool isInside(Point* p);

		// getter/setter

		/**
		 * @returns the scale with which the polygons were parsed.
		 * */
		double getScale();

		/**
		 * @returns whether the Y-Axis was inverted during parsing.
		 * */
		bool isYReverted();

		/**
		 * @returns the CGAL-Polygon that is the base for this polygon representation (i.e., the variable \ref cgalPolygon).
		 * */
		const Polygon* getCGALPolygon() const;

		/**
		 * @returns all points inside the polygon as computed by \ref calculatePolygonPoints() (i.e., the variable \ref polygonObjectPoints).
		 * */
		std::vector<Point>* getPoints();

		/**
		 * @returns all points inside the bounding box (BB) of the polygon as computed by \ref calculatePolygonPoints() (i.e., the variable \ref polygonObjectBBPoints).
		 * */
		std::vector<Point>* getBBPoints();

		/**
		 * @returns the y-coordinate of the highest point of the polygon.
		 * */
		double getHightop();

		/**
		 * @returns the y-coordinate of the lowest point on top of the polygon.
		 * */
		double getLowtop();

		/**
		 * @returns the leftmost point on top of the polygon.
		 * */
		Point getLeftTop();

		/**
		 * @returns the rightmost point on top of the polygon.
		 * */
		Point getRightTop();

		/**
		 * @returns all edges that make up together the top of the polygon.
		 * */
		std::vector<Segment> getTopEdges();

		/**
		 * @returns the y-coordinate of the highest point on the bottom of the polygon.
		 * */
		double getHighBottom();

		/**
		 * @returns the y-coordinate of the lowest point on the bottom of the polygon.
		 * */
		double getLowBottom();

		/**
		 * @returns the leftmost point on the bottom of the polygon.
		 * */
		Point getLeftBottom();

		/**
		 * @returns the rightmost point on the bottom of the polygon.
		 * */
		Point getRightBottom();

		/**
		 * @returns the edges that make up the bottom of the polygon.
		 * */
		std::vector<Segment> getBottomEdges();

		/**
		 * @returns the center-of-object (i.e., \ref centerOfObject) of the polygon as computed with \ref calculateCenterOfObject().
		 * */
		Point* getCenterOfObject();

		/**
		 * @returns the center-of-mass (i.e., \ref centerOfMass) of the polygon as computed with \ref calculateCenterOfMass().
		 * */
		Point* getCenterOfMass();

		/**
		 * @returns the height of the polygon (highest point - lowest point)
		 * */
		double getHeight();

		/**
		 * @returns the width of the polygon (rightmost point - leftmost point)
		 * */
		double getWidth();

		/**
		 * returns the index of the Polygon
		 * */
		 unsigned int getIndex();

	protected:

		// variables

		/** y-coordinate of the highest point on top of the object */
		double hightop;

		/** y-coordinate of the lowest point on top of the object */
		double lowtop;

		/** 2D-CGAL point; the leftmost point on top of the object */
		Point leftTop;

		/** 2D-CGAL point; the rightmost point on top of the object */
		Point rightTop;

		/** the edges that together make up the top of the object */
		std::vector<Segment> topEdges;

		/** the y-coordinate of the highest point at the bottom of the object */
		double highBottom;

		/** the y-coordinate of the lowest point at the bottom of the object */
		double lowBottom;

		/** the leftmost point at the bottom of the object */
		Point leftBottom;

		/** the rightmost point at the bottom of the object */
		Point rightBottom;

		/** the edges that together make up the bottom of the object */
		std::vector<Segment> bottomEdges;

		/** the height of the object: \f$h = |hightop - lowbottom|\f$ */
		double height;

		/** the width of the object: \f$w = | rightVertex_x - leftVertex_x |\f$ */
		double width;

		/** the scaling factor that the user has applied */
		double scale;

		/** if true, all y-coordinates were multiplied with -1 */
		bool revertedY;

		/** the CGAL-polygon that underlies this wrapper class */
		Polygon cgalPolygon;

		/** a vector of all points that are inside the polygon. Basically, this is a caching variable to speed-up computation. */
		std::vector<Point> polygonObjectPoints;

		/** a vector of all points that are inside the bounding box (BB) of the polygon. */
		std::vector<Point> polygonObjectBBPoints;

		/** the center of this object, computed in \ref calculateCenterOfObject() */
		Point centerOfObject;

		/** the center-of-mass of this object, computed by averaging all points, see \ref calculateCenterOfMass() */
		Point centerOfMass;

		/** the index of the polygon */
		unsigned int index;

		// methods:

		/** calculates the vertices that are on top of the polygon
		 * (i.e., fills the variables \ref hightop, \ref lowtop, \ref leftTop, \ref rightTop, \ref topEdges).
		 * The top is defined as all points where no other point exists inside the polygon with the same x-coordinate but a higher y-coordinate
		 * */
		void calculateTop();

		/** calculates the vertices that are on the bottom of the polygon
		 * (i.e., fills the variables \ref lowBottom, \ref highBottom \ref leftBottom, \ref rightBottom \ref bottomEdges).
		 * The bottom is defined as all points where no other point exists inside the polygon with the same x-coordinate but a lower y-coordinate
		 * */
		void calculateBottom();

		/**
		 * calculates all points inside the polygon and stores them in \ref polygonObjectPoints.
		 * */
		void calculatePolygonPoints();

		/**
		 * computes the center of the object regardless of its mass. This formula is used to compute it:
		 * \f$centerOfObject = Point(round(\frac{width}{2} + leftVertex_x), round(\frac{height}{2} + bottomVertex_x)\f$
		 * */
		void calculateCenterOfObject();

		/** computes the center of mass of the polygon by averaging the coordinates of all points inside the polygon
		 * Therefore, \ref calculatePolygonPoints() needs to be called first.
		 * The center-of-mass is stored in \ref centerOfMass.
		 * */
		void calculateCenterOfMass();

		/**
		 * Uses the CGAL methods, i.e., no caching or optimization.
		 * @param p the Point to be checked
		 * @returns true if p is inside the polygon and false if it is outside.
		 * */
		bool isInsideCGAL(Point* p);

};

#endif /* POLYGONOBJECT_H */
