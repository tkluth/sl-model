/*
 * referenceObject.cpp
 *
 * This file is part of sl-model:
 * https://www.gitlab.com/tkluth/sl-model
 *
 * Copyright(c) 2014-2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 *
 * From October 2014 to September 2018, the
 * development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 *
 */

/**
 * the higher this value, the more output
 * if set to 0 only important messages will be printed
 * this debug switch is placed in every file and only valid for one file!
 */
#define VERBOSITY 0

#include "referenceObject.h"
// for std::binary_search
#include <algorithm>

ReferenceObject::ReferenceObject(){
	std::stringstream s;
	s << "1" << std::endl << "1 1" << std::endl;
	std::istringstream iss(s.str());
	Polygon tmp;
	iss >> tmp;
	ReferenceObject(tmp, std::vector<std::string>(), std::vector<Polygon>(), false, 1.0, 0);
}

ReferenceObject::ReferenceObject(Polygon polyCGAL, std::vector<std::string> functionalFileNames, std::vector<Polygon> functionalCGALPolygons, bool revertedYAxis, double magnifier, unsigned int idx) :
	PolygonObject::PolygonObject(polyCGAL, revertedYAxis, magnifier, idx) {

	this->hasFunctionalPart = false;

	for (size_t f = 0; f < functionalFileNames.size(); ++f) {
		// set all functional polygons
		// if the files of the functional part do not exist, they should not be in here
#if VERBOSITY > 1
		std::cout << "adding functional part from file " << functionalFileNames[f] << " to RO." << std::endl;
#endif
		this->functionalParts[functionalFileNames[f]] = functionalCGALPolygons[f];
		this->hasFunctionalPart = true;
		this->setFunctionalPolygon(functionalFileNames[f]);
	}

	//the functional part to start with is not defined
	//~ this->currentFunctionalFileName = "";
}

void ReferenceObject::setFunctionalPolygon(std::string functionalFileName) {

	if (this->hasFunctionalPart == false) {
		// RO has no functional part, skipping this method
		return;
	}

	#if VERBOSITY >=2
	std::cout << "setFunctionalPolygon():\tcurrent functional file name:" <<  this->currentFunctionalFileName << " functional file name to set: "<< functionalFileName << std::endl;
	#endif

	if (! this->insideFunctional[functionalFileName].empty()) {
		//features of the functional part already computed?
		if (this->currentFunctionalFileName == functionalFileName) {
			// functional part is already set - no need to do anything
			return;
		}
		// functional part was already set up, just change it to be the actual...
		this->currentFunctionalFileName = functionalFileName;
		// and compute top-values
#if VERBOSITY >= 2
		// WONTFIX before someone needs to work with functionality and needs greater performance) cache top of function for different functional parts -> maybe, if someone is working with functionality again
		std::cout << "RO.setFuncPoly()():\t" << "functional polygon reset, newly computing top of function: " << cgalPolygon << std::endl;
#endif
		calculateTopFunction();
		calculateBottomFunction();
	} else {
		// add the functional part. this should happen only once for every functional part
		this->currentFunctionalFileName = functionalFileName;

		if (functionalFileName.size() > 0) {

			#if VERBOSITY >= 1
			std::cout << "setFunctionalPolygon():\tNew functional part: " << this->functionalParts[functionalFileName] << ", scale: " << scale << ", filename: " << functionalFileName << std::endl;
			#endif

			//apply reverted y-axis and scale
			for (Polygon::Vertex_const_iterator it = functionalParts[functionalFileName].vertices_begin(); it != functionalParts[functionalFileName].vertices_end(); it++) {
				if (this->revertedY) {
					this->functionalParts[functionalFileName].set(it, Point(it->x() * scale, (-1.0 * it->y() * scale)));
				} else {
					this->functionalParts[functionalFileName].set(it, Point(it->x() * scale, (it->y() * scale)));
				}

				if (! (this->cgalPolygon.has_on_bounded_side(*it) || this->cgalPolygon.has_on_boundary(*it))) {
					std::cerr << "Warning: Functional Part is outside Landmark! This is probably not what something that you want." << std::endl;
				}

				if (this->functionalParts[functionalFileName].is_counterclockwise_oriented()) {
					#if VERBOSITY >= 1
					std::cout << "setFunctionalPolygon():\t Polygon must be clockwise oriented. Turning polygons orientation..." << std::endl;
					#endif
					this->functionalParts[functionalFileName].reverse_orientation();
				}
			}

			#if VERBOSITY >= 1
			std::cout << "setFunctionalPolygon():\tScaled (and maybe reversed) the functional part: " << this->functionalParts[functionalFileName] << " " << scale << std::endl;
			#endif

			if (!this->functionalParts[functionalFileName].is_simple()) {
				std::cout << this->functionalParts[functionalFileName] << " is not simple." << std::endl;
				throw std::runtime_error("Functional Part not simple. Quitting.");
			}

			this->insideFunctional[functionalFileName] = std::vector<Point>();
			calculateTopFunction();
			calculateBottomFunction();
			calculateInnerFunctionPoints();
		} else {
			this->insideFunctional[functionalFileName] = std::vector<Point>();
			this->functionalParts[functionalFileName] = Polygon();
			calculateTopFunction();
			calculateBottomFunction();
		}
	}

}

void ReferenceObject::calculateTopFunction() {
	//calculate lowtop and hightop of the current functional part
	this->hightopFunction = this->functionalParts[this->currentFunctionalFileName].top_vertex()->y();
	this->rightTopFunction = *(this->functionalParts[this->currentFunctionalFileName].right_vertex());
	this->leftTopFunction = *(this->functionalParts[this->currentFunctionalFileName].left_vertex());
	std::vector<Point> leftPoints;

	for (Polygon::Vertex_iterator it = this->functionalParts[this->currentFunctionalFileName].vertices_begin();
			it != this->functionalParts[this->currentFunctionalFileName].vertices_end(); it++) {
		if (it->x() <= this->leftTopFunction.x()) {
			leftPoints.push_back(*it);
		}
	}

	for (size_t p = 0; p < leftPoints.size(); p++) {
		if (leftPoints[p].y() > leftTopFunction.y()) {
			this->leftTopFunction = leftPoints[p];
		}
	}

	// TODO (WONTFIX before someone needs to work with functionality): this does not work for functional parts where the left and right points are lower than some points in the middle (same for FunctionTop) -> look for lowest / highest y in the std::vector<Segment> created a few lines below
	this->lowtopFunction = (this->leftTopFunction.y() < this->rightTopFunction.y()) ? this->leftTopFunction.y() : this->rightTopFunction.y();
#if VERBOSITY >= 2
	std::cout << "calculateTopFunction():\t" << "hightopfunction: " << this->hightopFunction << " lowtopfunction: " << this->lowtopFunction << std::endl;
#endif
	Polygon::Vertex_const_iterator topLineStart = this->functionalParts[this->currentFunctionalFileName].vertices_begin();

	while (*topLineStart != this->leftTopFunction) {
		topLineStart++;
	}

	Polygon::Vertex_const_iterator nextPoint = topLineStart;

	// leftTop to next point (clockwise) -> first segment
	this->topEdgesFunction.clear();

	while (*nextPoint != this->rightTopFunction) {
		nextPoint = topLineStart + 1;
		Segment edge = Segment(*topLineStart, *nextPoint);
		topLineStart++;
#if VERBOSITY >= 2
		std::cout << "RO.calculateFuncTop():\t" << "New edge for top-func-edges on the RO: " << edge << std::endl;
#endif
		this->topEdgesFunction.push_back(edge);
	}
}

void ReferenceObject::calculateInnerFunctionPoints() {
	for (size_t i = 0; i < this->polygonObjectPoints.size(); i++) {
		if (this->functionalParts[this->currentFunctionalFileName].has_on_bounded_side(this->polygonObjectPoints[i])
				|| this->functionalParts[this->currentFunctionalFileName].has_on_boundary(this->polygonObjectPoints[i])) {
			this->insideFunctional[this->currentFunctionalFileName].push_back(this->polygonObjectPoints[i]);
		}
	}
}

void ReferenceObject::calculateBottomFunction() {

	std::cout << "calculateBottomFunction():\tWarning: This function (calculating the bottom of a functional part) is not tested. Use at your own risk." << std::endl;

	//calculate lowbottom and highbottom of the current functional part
	this->highBottomFunction = this->functionalParts[this->currentFunctionalFileName].top_vertex()->y();
	this->lowBottomFunction = this->functionalParts[this->currentFunctionalFileName].bottom_vertex()->y();
	this->rightBottomFunction = *(this->functionalParts[this->currentFunctionalFileName].right_vertex());
	this->leftBottomFunction = *(this->functionalParts[this->currentFunctionalFileName].left_vertex());
	std::vector<Point> leftPoints;

	for (Polygon::Vertex_iterator it = this->functionalParts[this->currentFunctionalFileName].vertices_begin();
			it != this->functionalParts[this->currentFunctionalFileName].vertices_end(); it++) {
		if (it->x() <= this->leftBottomFunction.x()) {
			leftPoints.push_back(*it);
		}
	}

	for (size_t p = 0; p < leftPoints.size(); p++) {
		if (leftPoints[p].y() > leftBottomFunction.y()) {
			this->leftBottomFunction = leftPoints[p];
		}
	}

	// TODO (WONTFIX before someone needs to work with functionality): this does not work for functional parts where the left and right points are lower than some points in the middle (same for FunctionTop) -> look for lowest / highest y in the std::vector<Segment> created a few lines below
	//~ this->highBottomFunction = (this->leftBottomFunction.y() > this->rightBottomFunction.y()) ? this->leftBottomFunction.y() : this->rightBottomFunction.y();
#if VERBOSITY >= 2
	std::cout << "calculateBottomFunction():\t" << "highBottomfunction: " << this->highBottomFunction << " lowBottomfunction: " << this->lowBottomFunction << std::endl;
#endif

	Polygon::Vertex_const_iterator bottomLineStart = this->functionalParts[this->currentFunctionalFileName].vertices_begin();

	while (*bottomLineStart != this->rightBottomFunction) {
		bottomLineStart++;
	}

	Polygon::Vertex_const_iterator nextPoint = bottomLineStart;

	// leftTop to next point (clockwise) -> first segment
	this->bottomEdgesFunction.clear();

	while (*nextPoint != this->leftBottomFunction) {
		nextPoint = bottomLineStart + 1;
		Segment edge = Segment(*bottomLineStart, *nextPoint);
		bottomLineStart++;
#if VERBOSITY >= 2
		std::cout << "RO.calculateFuncBottom():\t" << "New edge for bottom-func-edges on the RO: " << edge << std::endl;
#endif
		this->bottomEdgesFunction.push_back(edge);
	}
}

bool ReferenceObject::isInsideFunction(Point* p) {
	if (this->functionalParts[this->currentFunctionalFileName].is_empty()) {
		return false;
	}

	if (std::binary_search(insideFunctional[this->currentFunctionalFileName].begin(), insideFunctional[this->currentFunctionalFileName].end(), *p)) {
		return true;
	} else {
		return false;
	}
}

Polygon ReferenceObject::getFunctionalPolygon() {
	return this->functionalParts[this->currentFunctionalFileName];
}

double ReferenceObject::getLowtopFunction() {
	return this->lowtopFunction;
}

double ReferenceObject::getHightopFunction() {
	return this->hightopFunction;
}

Point ReferenceObject::getLeftTopFunction() {
	return this->leftTopFunction;
}

Point ReferenceObject::getRightTopFunction() {
	return this->rightTopFunction;
}

std::vector<Segment> ReferenceObject::getTopEdgesFunction() {
	return this->topEdgesFunction;
}

double ReferenceObject::getLowBottomFunction() {
	return this->lowBottomFunction;
}

double ReferenceObject::getHighBottomFunction() {
	return this->highBottomFunction;
}

Point ReferenceObject::getLeftBottomFunction() {
	return this->leftBottomFunction;
}

Point ReferenceObject::getRightBottomFunction() {
	return this->rightBottomFunction;
}

std::vector<Segment> ReferenceObject::getBottomEdgesFunction() {
	return this->bottomEdgesFunction;
}
