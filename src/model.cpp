/*
 * model.cpp
 *
 * This file is part of sl-model:
 * https://www.gitlab.com/tkluth/sl-model
 *
 * Copyright(c) 2014-2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 *
 * From October 2014 to September 2018, the
 * development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 *
 */

/**
 * the higher this value, the more output
 * if set to 0 only important messages will be printed
 * this debug switch is placed in every file and only valid for one file!
 */
#define VERBOSITY 0
#include "model.h"

#pragma GCC diagnostic push
// turn off some warnings for ranker.h (not my code and no ressources to fix the warnings)
#pragma GCC diagnostic ignored "-Wfloat-equal"
#pragma GCC diagnostic ignored "-Wconversion"
#if defined(__clang__) || __GNUC__ >= 7
#pragma GCC diagnostic ignored "-Wimplicit-fallthrough"
#endif
#include "ranker.h"
#pragma GCC diagnostic pop

#include <assert.h>

#include <CGAL/intersections.h>

// GNU Scientific Library
// Random Numbers
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_sort_vector.h>
//~ #include <gsl/gsl_permutation.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_errno.h>

#include <chrono>

// empty constructor
Model::Model() {
	// parameter values in Model() constructor have default values in model.h
	Model(std::vector<RatingDataPoint>(), 0, std::pair<unsigned int, unsigned int>(0, 0), 0, 0, 0, 0, false, INVALID_MODEL, false, false, "uninf", false, false, false, 2);
}

// constructor

Model::Model(std::vector<RatingDataPoint> empData, unsigned int noROs, std::pair<unsigned int, unsigned int> ratingScale,
	int widthLimitLow, int widthLimitHigh, int heightLimitLow, int heightLimitHigh, bool printRs,
	AVSmethod m, bool ord, bool posteriorEstimation, std::string infPriors, bool pOnly, bool lOnly, bool noChangeOrd, unsigned int threads,
	double vLambda, double vSlope, double vIntercept, double vHighgain, double vPhi, double vAlpha, double vLrgain, double vLrexp, double vSigmaOrd, std::vector<double> vThresholds) {

	this->setEmpiricalData(empData, noROs, ratingScale);
	this->generatedRatings = std::vector<RatingDataPoint>(this->empiricalData.size());

	this->ordinal = ord;
	this->posterior = posteriorEstimation;
	this->informativePriors = infPriors;
	this->priorOnly = pOnly;
	this->likelihoodOnly = lOnly;
	this->noChangeOrdinal = noChangeOrd;
	this->noOfThreads = threads;

	if (this->priorOnly && this->likelihoodOnly) {
		this->error("You set --prior-only and --likelihood-only at the same time. This does not make sense.", "Model()", this->empiricalData[0], true);
	}

	if (this->likelihoodOnly && this->informativePriors != "uninf") {
		std::cout << "WARNING: informative priors are not used because you want to only estimate the likelihood" << std::endl;
	}

	if (this->posterior) {
		if (!this->ordinal) {
			this->error("Posterior estimation does only work in ordinal mode.", "Model()", this->empiricalData[0], true);
		} else if (this->empiricalData[0].noOfParticipants == 0) {
			this->error("Posterior estimation needs empirical rating distributions.", "Model()", this->empiricalData[0], true);
		} else {
			this->tempChange = 0.3;
		}
	} else {
		this->tempChange = 0.999;
	}

	// random generator from GNU Scientific Library
	std::shared_ptr<gsl_rng> r(gsl_rng_alloc(gsl_rng_ranlxs0), gsl_rng_free);
	this->randomGenerator = r;
	long seed = time(NULL) * getpid();
	gsl_rng_set(this->randomGenerator.get(), (unsigned long) seed);

	this->setMethod(m, vLambda, vSlope, vIntercept, vHighgain, vPhi, vAlpha, vLrgain, vLrexp, vSigmaOrd, vThresholds);
	this->setLimits(widthLimitLow, widthLimitHigh, heightLimitLow, heightLimitHigh, printRs);
}

//model evaluation methods:

void printMatrix(gsl_matrix* m, bool diag) {
	std::cout << std::scientific << std::setprecision(2);

	for (size_t i = 0; i < m->size1; ++i) {
		for (size_t j = 0; j < m->size2; ++j) {
			if (diag) {
				if (i != j) {
					continue;
				}
			}
			std::cout << gsl_matrix_get(m, i, j) << ", ";
		}
		if(!diag) {
			std::cout << std::endl;
		}
	}

	std::cout << std::fixed << std::endl;
}

void Model::initializeTemperatureMatrix(gsl_matrix* tm) {
	for (size_t paramIdx = 0; paramIdx < this->paramList.size(); ++paramIdx) {
		ModelParameter* param = this->paramList[paramIdx];
		double temperature = std::numeric_limits<double>::quiet_NaN();
		if (this->posterior &&
					param->name.substr(0, 1) == "t") {
						// temperature for the thresholds
						temperature = 0.1;
		}	else if (this->posterior) {
			// hand-tune parameters:
			if (param->name == "intercept") {
				temperature = 0.06;
			} else if (param->name == "slope") {
				temperature = 0.127;
			} else if (param->name == "highgain") {
				temperature = 1.0;
			} else if (param->name == "lambda") {
				temperature = 0.5;
			} else if (param->name == "alpha") {
				temperature = 0.5;
			} else if (param->name == "sigmaOrd") {
				temperature = 0.4;
			} else {
				std::cout << "parameter " << param->name << "'s temperature not hand-tuned, using default temperature" << std::endl;
				temperature = std::abs((param->max - param->min) / 10.0); // * 0.35;
			}
		} else {
			temperature = std::abs((param->max - param->min) / 20.0);
			if (param->name == "sigmaOrd" || param->name.substr(0, 1) == "t") {
				// the ordinal parameters have + / - infinity as boundaries, so the above computation won't work
				temperature = 0.5;
			}
		}
		// multivariate sigmas are stored in the diagonal of the matrix (rowIdx == colIdx)
		gsl_matrix_set(tm, paramIdx, paramIdx, temperature);
	}
}

double Model::parameterEstimation(std::vector<RatingDataPoint> data, bool print) {

	if (this->ordinal && (data[0].noOfParticipants == 0)) {
		this->error("You wanted ordinal output but you did not provide the absolute number of empirical ratings via ratings.data_dist (see Parser.cpp documentation). Can't calculate error function, disabling ordinal output.", "parameterEstimation()", data[0], false);
		this->ordinal = false;
		if (this->posterior) {
			this->error("posterior estimation not possible without distributions of ratings. Quitting", "parameterEstimation", data[0], true);
		}
	}

	unsigned int iterations;
	int coolingPeriod;

	if (posterior) {
		iterations = 625;
		coolingPeriod = 200;
	} else {
		iterations = 350;
		coolingPeriod = 300;
	}

	std::map<std::string, double> oldParamValues;
	std::map<std::string, double> bestParamValues;
	this->visitedParams.reserve(iterations * (unsigned int) coolingPeriod);

	// initialize all matrix cells with zero (calloc instead of alloc)
	// temperatureMatrix aka A_i on page 5106 in Garthwaite, Fan, Sisson (2016)
	gsl_matrix* temperatureMatrix = gsl_matrix_calloc(this->paramList.size(), this->paramList.size());
	gsl_matrix_set_identity(temperatureMatrix);
	initializeTemperatureMatrix(temperatureMatrix);

	for (size_t paramIdx = 0; paramIdx < this->paramList.size(); ++paramIdx) {
		ModelParameter* param = this->paramList[paramIdx];
		oldParamValues[param->name] = param->value;
		bestParamValues[param->name] = param->value;
	}

	#if VERBOSITY >= 1
	std::cout << "parameterEstimation():\tstarting parameter estimation with model " << getMethod() << " (address: " << this << ")" << std::endl;
	#endif
	#if VERBOSITY >= 2
		std::cout << " and start parameters: " << std::endl;
		std::cout << "parameterEstimation():\tlambda " << std::setprecision(3) << this->lambda.value << std::endl;
		std::cout << "parameterEstimation():\tintercept " << this->intercept.value << std::endl;
		std::cout << "parameterEstimation():\tslope " << this->slope.value << std::endl;
		std::cout << "parameterEstimation():\thighgain " << this->highgain.value << std::endl;
		std::cout << "parameterEstimation():\talpha "<< this->alpha.value << std::setprecision(0);
		std::cout << std::endl << "tempChange aka sigma: " << std::setprecision(3) << this->tempChange;
	#endif

	// first fit value
	double valueNew = computeResults(&data, false);
	double valueOld = valueNew;
	double best = valueNew;

	std::chrono::system_clock::time_point startTime = std::chrono::system_clock::now();
	time_t startTime_C = std::chrono::system_clock::to_time_t(startTime);

	unsigned int accepted = 0;
	unsigned int rejected = 0;
	// the actual amount of iterations, if MCMC is restarted, this is reset to 0
	unsigned int iterTrue = 0;
	// the amount of iterations for the Robbins-Monro process,
	// if MCMC is restarted, this is set to startAdapting
	unsigned int iterRM = 0;
	double acceptanceRate = std::numeric_limits<double>::quiet_NaN();
	std::vector<double> acceptanceRates;
	// the optimal acceptance probability for the multivariate case
	double pstar = 0.234;
	double acceptanceProb = std::numeric_limits<double>::quiet_NaN();
	unsigned int startAdapting = (unsigned int) round(5 / (pstar * (1 - pstar)));
	// aka sigma from the adaptive MCMC algorithm
	// tempChange is set in setMethod()
	double tempChangeStart = this->tempChange; // used to check for too large changes of sigma
	if (!posterior) {
		this->tempChange = 0.99;
	}
	// values that don't change for updateSigma()
	double alphaRM = gsl_cdf_ugaussian_Qinv(pstar / 2.0);
	double c = (1.0 - 1.0 / (double) this->paramList.size()) *
						sqrt(2.0 * M_PI) * exp(std::pow(alphaRM, 2.0) / 2.0) / (2.0 * alphaRM) +
						1.0 / ((double) this->paramList.size() * pstar * (1.0 - pstar));

	gsl_matrix* thetaMean = gsl_matrix_alloc(1, this->paramList.size());
	gsl_matrix* visitedParamsMatrix = gsl_matrix_alloc( (size_t) iterations * (unsigned int) coolingPeriod, this->paramList.size());
	gsl_matrix* covMatrix = gsl_matrix_alloc(this->paramList.size(), this->paramList.size());
	gsl_vector* gslOldParams = gsl_vector_alloc(this->paramList.size());
	gsl_vector* gslNewParams = gsl_vector_alloc(this->paramList.size());

	this->outOfBounds = 0;

	for (unsigned int swap = 0; swap < iterations; ++swap) {
		for (int jump = 0; jump < coolingPeriod; ++jump) {

			// save old parameters values in oldParamValues and gslOldParams
			for (size_t paramIdx = 0; paramIdx < this->paramList.size(); ++paramIdx){
				ModelParameter* param = this->paramList[paramIdx];
				oldParamValues[param->name] = param->value;
				gsl_vector_set(gslOldParams, paramIdx, param->value);
				assert(param->min < param->max);
			}

			gsl_matrix_memcpy(covMatrix, temperatureMatrix);
			if (posterior) {
				gsl_matrix_scale(covMatrix, pow(this->tempChange, 2.0));
			}

			// don't crash when the cov matrix is not positive definite
			gsl_error_handler_t* gsl_original_error_handler = gsl_set_error_handler_off();

			if (gsl_linalg_cholesky_decomp1(covMatrix)) {
				std::cout << "this matrix is not positive definite: " << std::endl;
				printMatrix(covMatrix, false);
				std::cout << "tempchange: " << tempChange << std::endl;
			}

			// turn error handler back on
			gsl_set_error_handler(gsl_original_error_handler);

			// sample new parameters
			gsl_ran_multivariate_gaussian(this->randomGenerator.get(), gslOldParams, covMatrix, gslNewParams);

			// crash when any parameter value is NaN
			bool anynans = false;

			for (size_t paramIdx = 0; paramIdx < this->paramList.size(); ++paramIdx){
				double paramvalue = gsl_vector_get(gslNewParams, paramIdx);
				this->paramList[paramIdx]->value = paramvalue;
				if (std::isnan(paramvalue)) {
					std::cout << std::scientific << std::setprecision(4) << "param: " << this->paramList[paramIdx]->name << ", old value: " << oldParamValues[this->paramList[paramIdx]->name] << ", new value: " << this->paramList[paramIdx]->value << ", abs(diff): " << std::abs(oldParamValues[this->paramList[paramIdx]->name] - this->paramList[paramIdx]->value) << ", oldParams.value: " << gsl_vector_get(gslOldParams, paramIdx) << std::endl;
					printMatrix(covMatrix, false);
					anynans = true;
				}
			}

			if (anynans) {
				this->error("Some newly sampled parameter values are NaNs. Quitting.", "parameterEstimation", data[0], true);
			}

			// function value with new parameters
			valueNew = computeResults(&data, false);

			// metropolis aka posterior distribution estimation
			if (this->posterior) {

				// careful: better here means higher number (higher posterior value)
				if (valueNew > best) {
					// the best result found so far
					for (size_t paramIdx = 0; paramIdx < this->paramList.size(); ++paramIdx){
						ModelParameter* param = this->paramList[paramIdx];
						bestParamValues[param->name] = param->value;
						best = valueNew;
					}
				}

				// if valueNew > valueOld (better result), acceptanceProb == 1.0
				// if valueNew < valueOld (worse result), acceptanceProb gets smaller the farther away valueNew is from valueOld
				acceptanceProb = std::min(1.0, valueNew / valueOld);
				double randomAccept = gsl_rng_uniform(this->randomGenerator.get());

				// that is, reject, if random number is >= acceptanceProb
				// random number is in [0, 1)
				if (randomAccept >= acceptanceProb) {
					// reject new values, i.e., restore previous parameter values
					++rejected;
					for (size_t paramIdx = 0; paramIdx < this->paramList.size(); ++paramIdx){
						this->paramList[paramIdx]->value = oldParamValues[this->paramList[paramIdx]->name];
					}
					// we are not doing the proposed move, restore the previous
					// posterior value for comparison in the next iteration
					valueNew = valueOld;
				} else {
					// accepting values is done implicitly by not re-storing the old values
					++accepted;
				}
			} else {
				// parameter fitting aka simulated annealing

				if (valueNew > valueOld) {
					// worse result (lower is better, minimizing error), but:
					// accept new parameters with some probability to prevent stucking in a valley
					// done individually for each parameter in the next loop (temperatures are defined for every parameter)

					double randomAccept = gsl_rng_uniform(this->randomGenerator.get());
					bool accept = true;

					for (size_t paramIdx = 0; paramIdx < this->paramList.size(); ++paramIdx) {
						if ((exp(-(valueNew - valueOld) / gsl_matrix_get(temperatureMatrix, paramIdx, paramIdx))) < randomAccept) {
							// don't accept new values -> if one parameter is not accepted do not accept any parameter
							accept = false;
							break; // no need to check for any more parameters
						}
					}

					if (!accept) {
						// reject, restore previous values
						for (size_t paramIdx = 0; paramIdx < this->paramList.size(); ++paramIdx) {
							this->paramList[paramIdx]->value = oldParamValues[this->paramList[paramIdx]->name];
						}
						valueNew = valueOld;
					}

				} else {
					// else: better result, accept new parameter values
					// done implicitly, they are already stored in member variables

					if (valueNew < best) {
						// the best result found so far
						for (size_t paramIdx = 0; paramIdx < this->paramList.size(); ++paramIdx) {
							bestParamValues[this->paramList[paramIdx]->name] = this->paramList[paramIdx]->value;
							best = valueNew;
						}
					}
				} // end simulated annealing acceptance
			} // end acceptance computation (both posterior and simulated annealing

			// in either case: add parameter values (old or new) and value (error or likelihood) to
			// the std::vector of visited parameter values:
			std::map<std::string, float> visited;
			visited["value"] = (float) valueNew;
			for (size_t paramIdx = 0; paramIdx < this->paramList.size(); ++paramIdx) {
				visited[this->paramList[paramIdx]->name] = (float) this->paramList[paramIdx]->value;
				gsl_matrix_set(visitedParamsMatrix, iterTrue, paramIdx, this->paramList[paramIdx]->value);
			}
			this->visitedParams.push_back(visited);

			if (posterior) {
				// adaptive change of covariance matrix aka temperature matrix aka A_i
				// page 5106 Garthwaite et al. (2016)
				if (iterRM > 100) {
					if (iterRM == 101) {
						gsl_matrix_const_view filledElementsOfVisitedParams = gsl_matrix_const_submatrix(visitedParamsMatrix, 0, 0, iterTrue, this->paramList.size());

						// cov(visitedParamsMatrix) is saved in temperatureMatrix
						cov(&(filledElementsOfVisitedParams.matrix), temperatureMatrix);

						// colMeans are saved in thetaMean
						colMeans(&(filledElementsOfVisitedParams.matrix), thetaMean);
					} else {
						gsl_matrix_const_view currentParamsView = gsl_matrix_const_submatrix(visitedParamsMatrix, iterTrue, 0, 1, this->paramList.size());
						const gsl_matrix* currentParams = &currentParamsView.matrix;
						updateCov(thetaMean, temperatureMatrix, currentParams, iterRM);
					}
				}

				if (iterRM > startAdapting) {
					// for posterior estimation
					// adapt temperature (resp. scalar used to multiply temperatureMatrix)
					// depending on acceptance rate

					this->tempChange = updateSigma(acceptanceProb, pstar, iterRM, c);

					if (this->tempChange > (tempChangeStart * 3.0) || this->tempChange < (tempChangeStart / 3.0)) {
						std::cout << std::endl << std::setprecision(6) << "restarting MCMC, change during sigma adaption was too big (sigma at [re]start: " << tempChangeStart << " , sigma current: " << this->tempChange << ")" << std::endl;
						std::cout << "acceptance rate " << acceptanceRate << ", iterTrue: " << iterTrue << ", outoufbounds: " << this->outOfBounds << ", value: " << std::scientific << valueNew << ", best: " << best << std::fixed << std::endl;
						tempChangeStart = this->tempChange; // new start value (otherwise we would be restarting all over)
						if (((double) this->outOfBounds / (double) iterTrue) >= 0.95 && this->tempChange > 2.0) {
							std::cout << "sampled almost only parameters out of bounds and temp change is greater than 2.0, using default tempChange again and start parameters known to be in the boundaries" << std::endl;
							this->tempChange = 0.1;
							tempChangeStart = 0.1;

							for (size_t paramIdx = 0; paramIdx < this->paramList.size(); ++paramIdx){
								this->paramList[paramIdx]->value = this->paramList[paramIdx]->RCfit;
							}
						}

						iterRM = 0; // not equal to startAdapting because we also delete the yet visited parameters and the temperature matrix;
						iterTrue = 0;
						accepted = 0;
						rejected = 0;
						this->outOfBounds = 0;
						swap = 0;
						jump = -1;
						this->visitedParams.clear();
						// clean up memory for good measure (this swaps an empty vector, the first part, with the full vector, the argument)
						std::vector<std::map<std::string, float> >().swap(this->visitedParams);
						// but reserve it again ( too avoid too large automatic reserves)
						this->visitedParams.reserve(iterations * (unsigned int) coolingPeriod);

						// re-initialize gsl vectors and matrices
						// free all gsl "objects"
						gsl_matrix_free(temperatureMatrix);
						gsl_matrix_free(visitedParamsMatrix);
						gsl_matrix_free(thetaMean);
						gsl_matrix_free(covMatrix);
						gsl_vector_free(gslOldParams);
						gsl_vector_free(gslNewParams);

						// and initialize them again
						thetaMean = gsl_matrix_alloc(1, this->paramList.size());
						visitedParamsMatrix = gsl_matrix_alloc( (size_t) iterations * (unsigned int) coolingPeriod, this->paramList.size());
						covMatrix = gsl_matrix_alloc(this->paramList.size(), this->paramList.size());
						gslOldParams = gsl_vector_alloc(this->paramList.size());
						gslNewParams = gsl_vector_alloc(this->paramList.size());

						temperatureMatrix = gsl_matrix_calloc(this->paramList.size(), this->paramList.size());
						gsl_matrix_set_identity(temperatureMatrix);
						initializeTemperatureMatrix(temperatureMatrix);

						continue;
					} // end restart MCMC if needed
				} // end update sigma
			} // end adapting

			// save function value for the next evaluation step
			valueOld = valueNew;

			acceptanceRate = (double) accepted / ((double) iterTrue + 1.0);
			acceptanceRates.push_back(acceptanceRate);

			++iterTrue;
			++iterRM;
		} // end cooling period loop

		// change the temperature, only for parameter estimation / simulated annealing
		// for parameter estimation this is always cooling down
		// to sample parameters nearer to the current parameters
		if (!posterior) {
			for (size_t paramIdx = 0; paramIdx < this->paramList.size(); ++paramIdx) {
				// don't change threshold temperatures as the algorithm doesn't converge otherwise
				if (this->paramList[paramIdx]->name.substr(0, 1) != "t") {
					gsl_matrix_scale(temperatureMatrix, this->tempChange);
				}
			}
		}

		if (print) {
			double percentage = (100.0 * ((double) swap / (double) (iterations)));
			std::chrono::system_clock::duration estimatedRemainingTime = std::chrono::duration_cast<std::chrono::system_clock::duration>(100 * ((std::chrono::system_clock::now() - startTime) / percentage));
			std::chrono::system_clock::time_point endTime = startTime + estimatedRemainingTime;
			time_t endTime_C = std::chrono::system_clock::to_time_t(endTime);
			std::cout << "start: " << std::put_time(localtime(&startTime_C), "%b %d, %T");
			// welcome to the ugly world of C++: if you put both time informations in the same stream, they will print the same time ...
			std::cout << ", ETA: " << std::put_time(localtime(&endTime_C), "%b %d, %T") << ", " << std::setprecision(2) << percentage;
			if (this->posterior) {
				std::cout << " %, acc. rate: " << std::setprecision(4) << acceptanceRate << ", tmpChng: " << this->tempChange << ", oob-ratio: " << ((double) this->outOfBounds / (double) iterTrue) << ", mean sec p lh: " << allSeconds / iterLH << ", mean LH SE: " << meanSE / iterLH << ", curr post.: " << std::scientific << valueOld << ", max post.: " << best << std::fixed << "\r" << std::flush;
			} else {
				std::cout << "%, current error: " << std::setprecision(6) << valueOld << ", best error: " << best << ", outOfBounds-ratio: " << ((double) this->outOfBounds / (double) iterTrue) << "\r" << std::flush;
			}
		} // end print
	} // end iterations loop

	if (print) {
		if (this->posterior) {
			std::cout << std::endl << "posterior estimation: 100 % done" << std::endl;
			std::cout << "mean acceptance rate: " << std::accumulate(acceptanceRates.begin(), acceptanceRates.end(), (double) 0.0) / (double) acceptanceRates.size() << std::endl;

			if (this->priorOnly) {
				std::cout << "WARNING: Estimated prior distribution only!" << std::endl;
			}

			if (this->likelihoodOnly) {
				std::cout << "WARNING: Estimated likelihood only!" << std::endl;
			}

		} else {
			std::cout << std::endl << "parameter estimation: 100 % done" << std::endl;
		}

		auto end = std::chrono::system_clock::now();
		auto time_diff = end - startTime;
		std::cout << "time needed: " << std::setprecision(2) << std::chrono::duration<double>(time_diff).count() << " seconds. (That are " << std::chrono::duration<double, std::ratio<60> >(time_diff).count() << " minutes or " << std::chrono::duration<double, std::ratio<3600> >(time_diff).count() << " hours or " << std::chrono::duration<double, std::ratio<86400> >(time_diff).count() << " days.)" << std::endl;
	}

	// free all gsl "objects"
	gsl_matrix_free(temperatureMatrix);
	gsl_matrix_free(visitedParamsMatrix);
	gsl_matrix_free(thetaMean);
	gsl_matrix_free(covMatrix);
	gsl_vector_free(gslOldParams);
	gsl_vector_free(gslNewParams);

	// print correlation with best parameters:
	for (size_t paramIdx = 0; paramIdx < this->paramList.size(); ++paramIdx) {
		ModelParameter* param = this->paramList[paramIdx];
		param->value = bestParamValues[param->name];
	}

	computeResults(&data, print);

	if (print) {
		std::cout << "best parameters found: " << std::endl;

		std::stringstream writeBestParams;
		writeBestParams << std::setprecision(6) << "# ";

		for (size_t paramIdx = 0; paramIdx < this->paramList.size(); ++paramIdx) {
			// thresholds are handled differently below
			if (this->paramList[paramIdx]->name.substr(0, 1) != "t") {
				writeBestParams << " --" << this->paramList[paramIdx]->name << " " << bestParamValues[this->paramList[paramIdx]->name];
			}
		}

		if (this->ordinal) {
			writeBestParams << " --thresholds ";
			for (size_t t = 1; t < this->thresholds.size() - 1; ++t) {
				writeBestParams << this->thresholds[t].value;
				if (t < this->thresholds.size() - 2) {
					writeBestParams << ",";
				}
			}
		}

		if (this->posterior) {
			writeBestParams << std::endl << "# max posterior: " << best << std::endl;
		} else if(this->ordinal){
			writeBestParams << std::endl << "# Kullback-Leibler: " << best << std::endl;
		} else {
			writeBestParams << std::endl << "#  nRMSE: " << best << std::endl;
		}

		std::cout << writeBestParams.str() << std::endl;

		for (size_t paramIdx = 0; paramIdx < this->paramList.size(); ++paramIdx) {
			writeBestParams << "# this->" <<  this->paramList[paramIdx]->name << ".value = " << bestParamValues[ this->paramList[paramIdx]->name] << ";" << std::endl;
		}
		if (this->posterior) {
			writeBestParams << "# max posterior: " << best << std::endl;
		} else if(this->ordinal){
			writeBestParams << "# lowest Kullback-Leibler divergence: " << best << std::endl;
		} else {
			writeBestParams << "# best nRMSE: " << best << std::endl;
		}

		this->resultsPE = this->resultsPE + writeBestParams.str();
	}

	return best;
}

void Model::colMeans(const gsl_matrix* m, gsl_matrix* meansMatrix) {

	for (size_t i = 0; i < m->size2; ++i) {
		gsl_vector_const_view column = gsl_matrix_const_column(m, i);
		double mean = 0.0;
		for (size_t item = 0; item < column.vector.size; ++item) {
			mean = mean + gsl_vector_get(&column.vector, item);
		}
		mean = mean / (double) column.vector.size;
		gsl_matrix_set(meansMatrix, 0, i, mean);
	}
}

void Model::cov(const gsl_matrix* m, gsl_matrix* c) {
	// this code is a modified version of sample code from the gsl mailing list
	// https://www.mail-archive.com/help-gsl@gnu.org/msg03071.html
	for (size_t i = 0; i < m->size2; ++i) {
		gsl_vector_const_view a = gsl_matrix_const_column(m, i);
		for (size_t j = 0; j < m->size2; ++j) {
			gsl_vector_const_view b = gsl_matrix_const_column(m, j);
			double v = gsl_stats_covariance(a.vector.data, a.vector.stride,
			b.vector.data, b.vector.stride, a.vector.size);
			gsl_matrix_set(c, i, j, v);
		}
	}
}

void Model::updateCov(gsl_matrix* thetaMean, gsl_matrix* temperatureMatrix, const gsl_matrix* currentParams, unsigned int iteration) {
	// comments refer to the R code from Yanan Fan, available from http://web.maths.unsw.edu.au/~yanan/RM.html
	double epsilon = std::pow(this->tempChange, 2.0) / (double) iteration;
	//~ double epsilon = 1.0 / (double) iteration;

	gsl_matrix* thetaMeanMul = gsl_matrix_calloc(this->paramList.size(), this->paramList.size());
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, thetaMean, thetaMean, 0.0, thetaMeanMul); // thetaMean %*% t(thetaMean)

	// thetaM2 = ((thetaM * i) + theta) / (i + 1)
	gsl_matrix* thetaNewMean = gsl_matrix_alloc(1, this->paramList.size());
	gsl_matrix_memcpy(thetaNewMean, thetaMean); // thetaM2 = thetaM
	gsl_matrix_scale(thetaNewMean, (double) iteration); // * i
	gsl_matrix_add(thetaNewMean, currentParams); // + currentParams (aka theta)
	gsl_matrix_scale(thetaNewMean, (1.0 / ((double) iteration + 1.0))); // divide with (iteration + 1)

	gsl_matrix* thetaNewMeanMul = gsl_matrix_calloc(this->paramList.size(), this->paramList.size());
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, thetaNewMean, thetaNewMean, 0.0, thetaNewMeanMul); // thetaM2 %*% t(thetaM2)

	gsl_matrix* currentParamsMul = gsl_matrix_calloc(this->paramList.size(), this->paramList.size());
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, currentParams, currentParams, 0.0, currentParamsMul); // currentParams %*% t(currentParams)

	// sigMat = (i - 1) / i * sigMat +
	// thetaM %*% t(thetaM) -
	// (i + 1) / i * thetaM2 %*% t(thetaM2) +
	// 1 / i * theta %*% t(theta) +
	// epsilon * diag(d)
	gsl_matrix_scale(temperatureMatrix, (((double) iteration - 1.0) / ((double) iteration))); // (i - 1) / i * sigMat
	gsl_matrix_add(temperatureMatrix, thetaMeanMul); // + thetaMean %*% t(thetaMean)

	gsl_matrix_scale(thetaNewMeanMul, (((double) iteration + 1.0) / ((double) iteration))); //  (i + 1) / i * thetaM2 %*% t(thetaM2)
	gsl_matrix_sub(temperatureMatrix, thetaNewMeanMul); // -

	gsl_matrix_scale(currentParamsMul, (1.0 / ((double) iteration))); // 1 / i * currentParams %*% t(currentParams)
	gsl_matrix_add(temperatureMatrix, currentParamsMul); // +

	gsl_matrix* epsilonMulMatrix = gsl_matrix_alloc(this->paramList.size(), this->paramList.size());
	gsl_matrix_set_identity(epsilonMulMatrix);
	gsl_matrix_scale(epsilonMulMatrix, epsilon); // epsilon * diag(d) (diag is the identity matrix)
	gsl_matrix_add(temperatureMatrix, epsilonMulMatrix); // +

	gsl_matrix_memcpy(thetaMean, thetaNewMean); // thetaM = thetaM2

	gsl_matrix_free(thetaMeanMul);
	gsl_matrix_free(thetaNewMean);
	gsl_matrix_free(thetaNewMeanMul);
	gsl_matrix_free(currentParamsMul);
	gsl_matrix_free(epsilonMulMatrix);

	// no return; the corresponding gsl_matrices are already changed at their memory locations
}

double Model::updateSigma(double acceptanceProbability, double pstar, unsigned int iteration, double c) {
	double theta = log(this->tempChange) + c * (acceptanceProbability - pstar) / std::max(200.0, (((double) iteration) / ((double) this->paramList.size())));
	// Garthwaite propose max(200, ...
	return exp(theta);
}

double Model::computePrior() {

	double prior = 1.0;

	// maximum entropy principle, Lee & Vanpaemel (2016), (Robert 2007, p111)
	if (this->informativePriors == "uninf") {
		// assuming no information is available -> uniform priors
		prior = prior * gsl_ran_flat_pdf(this->lambda.value, this->lambda.min, this->lambda.max);
		prior = prior * gsl_ran_flat_pdf(this->slope.value, this->slope.min, this->slope.max);
		prior = prior * gsl_ran_flat_pdf(this->intercept.value, this->intercept.min, this->intercept.max);
		prior = prior * gsl_ran_flat_pdf(this->highgain.value, this->highgain.min, this->highgain.max);

		if (this->method == rAVS_comb || this->method == rAVS_prox || this->method == rAVS_COM || this->method == rAVS_w_comb ||
			this->method == rAVS_multiple || this->method == rAVS_abs_middle || this->method == rAVS_CoO) {
			prior = prior * gsl_ran_flat_pdf(this->alpha.value, this->alpha.min, this->alpha.max);
		}
	} else if (this->informativePriors == "regier2001") {
		// Gaussian priors using the mean and SD from the SHO runs on the Regier & Carlson (2001) data:
		if (this->method == originalAVS) {
			// -0.2893475 = (-0.005050067) / pi * 180
			// 0.03474047 = 0.0006063355 / pi * 180
			prior = prior * gsl_ran_gaussian_pdf(this->lambda.value - 0.4045786, 0.1608102);
			prior = prior * gsl_ran_gaussian_pdf(this->slope.value - (-0.2893475), 0.03474047);
			prior = prior * gsl_ran_gaussian_pdf(this->intercept.value - 0.9933684, 0.030016);
			prior = prior * gsl_ran_gaussian_pdf(this->highgain.value - 0.1069702, 0.04949245);
		} else if (this->method == AVS_BB) {
			// -0.3012155 = (-0.005257203) / pi * 180
			// 0.0005261392 / pi * 180
			prior = prior * gsl_ran_gaussian_pdf(this->lambda.value - 0.5922284, 0.1854569);
			prior = prior * gsl_ran_gaussian_pdf(this->slope.value - (-0.3012155), 0.03014556);
			prior = prior * gsl_ran_gaussian_pdf(this->intercept.value - 1.010473, 0.03281687);
			prior = prior * gsl_ran_gaussian_pdf(this->highgain.value - 0.09039761, 0.04228747);
		} else if (this->method == rAVS_CoO) {
			// -0.2618814 =  (-0.004570692) / pi * 180
			// 0.02483112 = 0.0004333848 / pi * 180
			prior = prior * gsl_ran_gaussian_pdf(this->alpha.value - 0.4118462, 0.1481754);
			prior = prior * gsl_ran_gaussian_pdf(this->lambda.value - 2.416035, 1.510699);
			prior = prior * gsl_ran_gaussian_pdf(this->slope.value - (-0.2618814), 0.02483112);
			prior = prior * gsl_ran_gaussian_pdf(this->intercept.value - 0.9911035, 0.03068665);
			prior = prior * gsl_ran_gaussian_pdf(this->highgain.value - 0.1180988, 0.09939308);
		} else if (this->method == rAVS_w_comb) {
			// -0.2578131 = (-0.004499687 / M_PI * 180.0)
			// 0.02138072 = (0.000373164 / M_PI * 180.0)
			prior = prior * gsl_ran_gaussian_pdf(this->alpha.value - 0.3183467, 0.1158073);
			prior = prior * gsl_ran_gaussian_pdf(this->lambda.value - 2.614152, 1.466082);
			prior = prior * gsl_ran_gaussian_pdf(this->slope.value - (-0.2578131), 0.02138072);
			prior = prior * gsl_ran_gaussian_pdf(this->intercept.value - 0.9810393, 0.02487723);
			prior = prior * gsl_ran_gaussian_pdf(this->highgain.value - 0.3868885, 1.341426);
		} else {
			this->error("informative priors not (yet) available for this model", "computePrior()", this->empiricalData[0], true);
		}
	} else if (this->informativePriors == "PSP") {
		if (this->method == originalAVS) {
			prior = prior * gsl_ran_gaussian_pdf(this->lambda.value - 1.0642888, 0.11559);
			prior = prior * gsl_ran_gaussian_pdf(this->slope.value - (-1.2270), 0.034003);
			prior = prior * gsl_ran_gaussian_pdf(this->intercept.value - 0.8716050, 0.10738);
			prior = prior * gsl_ran_gaussian_pdf(this->highgain.value - 5.8440037, 2.7008);
		} else if (this->method == AVS_BB) {
			prior = prior * gsl_ran_gaussian_pdf(this->lambda.value - 1.56352, 1.1420);
			prior = prior * gsl_ran_gaussian_pdf(this->slope.value - (-0.67430), 0.32032);
			prior = prior * gsl_ran_gaussian_pdf(this->intercept.value - 0.88862, 0.12751);
			prior = prior * gsl_ran_gaussian_pdf(this->highgain.value - 4.71486, 2.9213);
		} else if (this->method == rAVS_CoO) {
			prior = prior * gsl_ran_gaussian_pdf(this->alpha.value - 2.63454, 1.2286);
			prior = prior * gsl_ran_flat_pdf(this->lambda.value, this->lambda.min, this->lambda.max);
			prior = prior * gsl_ran_gaussian_pdf(this->slope.value - (-0.80577), 0.28550);
			prior = prior * gsl_ran_gaussian_pdf(this->intercept.value - 0.96709, 0.15590);
			prior = prior * gsl_ran_gaussian_pdf(this->highgain.value - 5.13077, 2.8536);
		} else if (this->method == rAVS_w_comb) {
			prior = prior * gsl_ran_gaussian_pdf(this->alpha.value - 2.12763, 1.5775);
			prior = prior * gsl_ran_flat_pdf(this->lambda.value, this->lambda.min, this->lambda.max);
			prior = prior * gsl_ran_gaussian_pdf(this->slope.value - (-0.17395), 0.14068);
			prior = prior * gsl_ran_gaussian_pdf(this->intercept.value - 1.05622, 0.17040);
			prior = prior * gsl_ran_gaussian_pdf(this->highgain.value - 4.84716, 2.8670);
		} else {
			this->error("informative priors not (yet) available for this model", "computePrior()", this->empiricalData[0], true);
		}
	}

	return prior;
}

arma::vec Model::mahalanobis(const arma::mat* x, const arma::rowvec* center, const arma::mat* icov) {
	// https://stackoverflow.com/questions/17617618/dmvnorm-mvn-density-rcpparmadillo-implementation-slower-than-r-package-includi
	arma::uword n = x->n_rows;
	arma::mat x_cen;
	x_cen.copy_size(*x);

	for (arma::uword i = 0; i < n; ++i) {
		x_cen.row(i) = x->row(i) - *center;
	}

	return arma::sum((x_cen * (*icov)) % x_cen, 1);
}

double Model::crossmatch(std::vector<RatingDataPoint>* modelResults, std::vector<RatingDataPoint>* empData) {

	assert(modelResults->size() == empData->size());
	// otherwise we do not have individual ratings available ...
	assert(((*empData)[0].individualRatings.size() == (*empData)[0].noOfParticipants));

	// create table to hand over to R
	// table is supposed to look like this:
	// type  LO1 LO2 LO3 LO4 LO5 ...
	// emp   8   8   5   2   2   ...
	// emp   7   8   4   3   2   ...
	// emp   9   8   7   1   1   ...
	// model 7   8   5   2   1   ...
	// model 7   8   5   2   1   ...
	// model 7   8   5   2   1   ...
	//
	// the not-changing part of the table (first column, first half of rows)
	// is initialized in the beginning of parameterEstimation()
	// twice as many rows as subjects (every row is either a subject or a model sample)
	// every column is one LO; +1 for group column (subject or model sample)

	const unsigned int noOfRows = (unsigned int) this->dataMatrixForCrossmatch[0].size();
	const unsigned int noOfPartRows = (*empData)[0].noOfParticipants;
	const unsigned int noOfCols = (unsigned int) this->dataMatrixForCrossmatch.size();

	arma::mat armaRanks((arma::uword) noOfRows, (arma::uword) noOfCols);
	// this vector is often overwritten below
	std::vector<double> colRank(noOfRows);

	for (size_t columnIdx = 0; columnIdx < noOfCols; ++columnIdx) {
		for (size_t rowIdx = noOfPartRows; rowIdx < noOfRows; ++rowIdx) {
			// attention: rows and columns are flipped, columns come first
			this->dataMatrixForCrossmatch[columnIdx][rowIdx] = (*modelResults)[columnIdx].individualRatings[rowIdx - (*empData)[0].noOfParticipants];
		}

		// compute rank for just filled column (see ranker.h)
		rank(this->dataMatrixForCrossmatch[columnIdx], colRank, "average");
		for (size_t r = 0; r < noOfRows; ++r) {
			armaRanks(r, columnIdx) = colRank[r];
		}
	}

	arma::mat covMatrix = arma::cov(armaRanks);
	arma::vec tmpVec = sqrt(arma::as_scalar(arma::var(arma::linspace(1, noOfRows, noOfRows))) / arma::diagvec(covMatrix));
	arma::mat tmpMat = arma::diagmat(tmpVec) * covMatrix * arma::diagmat(tmpVec);
	arma::mat invCov = arma::pinv(tmpMat);

	arma::mat distances((arma::uword) noOfRows, (arma::uword) noOfRows);

	for (size_t rowIdx = 0; rowIdx < noOfRows; ++rowIdx) {
		arma::rowvec row = armaRanks.row(rowIdx);
		// row == col because a distance matrix is symmetric (this saves calling trans() on the mahalanobis result
		distances.col(rowIdx) = this->mahalanobis(&armaRanks, &row, &invCov);
	}

	Rcpp::NumericMatrix distancesForR = Rcpp::NumericMatrix(noOfRows, noOfRows);

	for (size_t rowIdx1 = 0; rowIdx1 < noOfRows; ++rowIdx1) {
		for (size_t rowIdx2 = 0; rowIdx2 < noOfRows; ++rowIdx2) {
			distancesForR(rowIdx1, rowIdx2) = distances(rowIdx1, rowIdx2);
		}
	}

	RInside::instance()["distances"] = distancesForR;
	// groups are set in setMethod() only once
	Rcpp::NumericVector p = RInside::instance().parseEval("crossmatchtest_a1(groups, distances)");
	#if VERBOSITY >= 1
	std::cout << "crossmatch result: " << std::setprecision(6) << p[0] << std::endl;
	#endif

	// crossmatch data() returns only the p value
	assert(p.size() == 1);

	return p[0];
}

std::vector<RatingDataPoint> Model::generateRatings(unsigned int noOfIndividuals, unsigned int noOfSamples) {

	this->computedRatings = std::vector<double>(this->empiricalData.size());

	// seeting the seed of the random generator here is harmful as it results in generated ratings way too similar
	// (a random generator with the same seed, which is the case if called within the same second and setting the seed here, returns the same random numbers)

	for (size_t idx = 0; idx < this->empiricalData.size(); ++idx) {

		if (this->ordinal) {
			std::vector<double> ratingProbs = rateProbs(&(this->empiricalData[idx]), 0, false, 0.0);

			gsl_ran_discrete_t* ratingSampler = gsl_ran_discrete_preproc((this->maxRating - this->minRating + 1), &ratingProbs[0]);

			#if VERBOSITY > 1
			if (this->printRatings) {
				std::cout << "probabilities: " << std::setprecision(3) << std::endl;
				for (size_t i = 0; i < ratingProbs.size(); ++i) {
					 std::cout << ratingProbs[i] << ", ";
				}
				std::cout << std::endl;
			}
			#endif

			this->generatedRatings[idx].individualRatings = std::vector<unsigned int>(noOfIndividuals);
			this->generatedRatings[idx].absoluteNoOfRatings = std::vector<unsigned char>(this->maxRating - this->minRating + 1);
			for (size_t i = 0; i < noOfIndividuals; ++i) {
				unsigned int sampleMean = 0;
				for (size_t s = 0; s < noOfSamples; ++s) {
					unsigned int randomRating = (unsigned int) gsl_ran_discrete(this->randomGenerator.get(), ratingSampler) + this->minRating;
					sampleMean = sampleMean + randomRating;

					#if VERBOSITY >= 1
						if (this->printRatings) {
							std::cout << ", " << randomRating;
						}
					#endif
				}

				//~ std::cout << "double mean: " << std::setprecision(6) << ((double) sampleMean / (double) noOfSamples) << std::endl;

				sampleMean = (unsigned int) round((double) sampleMean / (double) noOfSamples);

				//~ std::cout << ", int mean: " << sampleMean << std::endl;

				this->generatedRatings[idx].individualRatings[i] = sampleMean;
				++this->generatedRatings[idx].absoluteNoOfRatings[sampleMean - 1];
			}

			gsl_ran_discrete_free(ratingSampler);

			// think about adding the sampled ratings to this->computedRatings
			// it's more complicated because this goes to PSP
			// WONTFIX until someone needs PSP for individual ratings
			this->computedRatings[idx] = std::numeric_limits<double>::quiet_NaN();
		} else {
			this->generatedRatings[idx].meanRating = rate(&(this->empiricalData[idx]), 0, false, 0.0);
			// store ratings in member variable computedRatings, too
			// for PSP computation; accessed via visualizer.visualizeOctaveVector()
			// only used when flag --quiet is set
			// !but! not computed in this method but only in computeResults()
			// this line is accordingly not really relevant but also shouldn't harm
			// mainly for PSP computation
			this->computedRatings[idx] = generatedRatings[idx].meanRating;
		}

		this->generatedRatings[idx].prep = this->empiricalData[idx].prep;
		this->generatedRatings[idx].referenceObject = this->empiricalData[idx].referenceObject;
		this->generatedRatings[idx].locatedObject = this->empiricalData[idx].locatedObject;
		this->generatedRatings[idx].functionalPart = this->empiricalData[idx].functionalPart;

		#if VERBOSITY >= 0
		if (this->printRatings) {
			#if VERBOSITY >= 0
				std::cout << "RO: " << *(this->empiricalData[idx].referenceObject->getCGALPolygon()) << ", LO: " << *(this->empiricalData[idx].locatedObject.getCGALPolygon()) << " rating: " << std::endl;
			#endif
			if (ordinal) {
				for (size_t r = 0; r < this->generatedRatings[idx].individualRatings.size(); ++r) {
					std::cout << this->generatedRatings[idx].individualRatings[r] << ", ";
				}
				std::cout << std::endl;
			} else {
				std::cout << this->generatedRatings[idx].meanRating << ", ";
			}
		}
		#endif
	}

	#if VERBOSITY >= 1
	if(this->printRatings){
		std::cout << std::endl;
	}
	#endif

	return this->generatedRatings;
}

double Model::computeResults(std::vector<RatingDataPoint>* data, bool printCorrelation) {

	double value = std::numeric_limits<double>::quiet_NaN();

	if (!areParamsInBoundaries(false)) {
		// return a value that leads to rejecting this parameter set
		if (!this->posterior) {
			// the higher the error, the worse
			value = std::numeric_limits<double>::infinity();
		} else {
			// the lower the posterior, the worse
			value = 0.0;
		}
		++this->outOfBounds;
		return value;
	}

	std::vector<double> myResults(data->size());
	std::vector<std::vector<double> > myProbResults;

	if (this->posterior) {
		double prior = this->computePrior();

		if (this->priorOnly) {
			return prior;
		}

		if (this->likelihoodOnly) {
			// prior is ignored anyway in this condition but we need to make sure that the likelihood is computed
			prior = 1.0;
		}

		if (prior > 0.0) {

			double meanP = 0.0;

			#if VERBOSITY >= 0
			std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
			#endif

			unsigned int noOfSamples = 10;
			unsigned int noOfBlocks = 20;
			unsigned int noOfCrossmatchRunsPBlock = 4;
			std::vector<double> probs;

			//~ std::cout << "# crossmatches:";
			for (unsigned int block = 0; block < noOfBlocks; ++block) {
				double meanCMs = 0.0;
				for (unsigned int c = 0; c < noOfCrossmatchRunsPBlock; ++c) {
					// writes the generated ratings to this->generatedRatings
					this->generateRatings((*data)[0].noOfParticipants, noOfSamples);
					meanCMs = meanCMs + this->crossmatch(&this->generatedRatings, data);
				}
				meanCMs = meanCMs / (double) noOfCrossmatchRunsPBlock;

				//~ std::cout << "block" << block << ", mean # CMs: " << round(meanCMs / 2.0) * 2.0 << ", p: " << this->crossmatchTable[((int) round(meanCMs / 2.0) * 2)] << std::endl;
				probs.push_back(this->crossmatchTable[((int) round(meanCMs / 2.0) * 2)]);
				//~ meanP = meanP + probs[block];
				meanP = meanP + this->crossmatchTable[((int) round(meanCMs / 2.0) * 2)];
			}
			meanP = meanP / (double) noOfBlocks;

			#if VERBOSITY >= 0
			double sd = 0.0;
			for (size_t i = 0; i < probs.size(); ++i) {
				sd = sd + pow((probs[i] - meanP), 2);
			}
			sd = sqrt(sd / ((double) probs.size() - 1));
			double se = sd / sqrt((double) probs.size());
			// (uncorrected sample) standard deviation divided by sqrt(N) yields the standard error
			meanSE = meanSE + se;

			std::chrono::system_clock::time_point end = std::chrono::system_clock::now();
			auto time_diff = end - start;
			allSeconds = allSeconds +  std::chrono::duration<double>(time_diff).count();
			++iterLH;

			//~ std::cout << std::setprecision(6) << "meanP: " << meanP << ", standard deviation: " << sd << ", standard error: " << se << ", mean SE: " << meanSE / iter << ", seconds: " << std::chrono::duration<double>(time_diff).count() << ", mean seconds: " << allSeconds / (double) iter << std::endl;
			#endif

			if (this->likelihoodOnly) {
				return meanP;
			}

			value = prior * meanP;

		} else {
			// prior == 0.0 no need to compute likelihood
			value = 0.0;
		}
	} else {
		// non-posterior mode
		for (size_t i = 0; i < data->size(); ++i) { // for every LO
			std::string funcFile = (*data)[i].functionalPart;
			(*data)[i].referenceObject->setFunctionalPolygon(funcFile);

			if (this->ordinal) { // probability distribution over the whole rating scale for every LO
				std::vector<double> resultProbs = this->rateProbs(&((*data)[i]), funcFileToFocusIdx[funcFile], false, 0.0);
				myProbResults.push_back(resultProbs);

				// de-validate myResults
				myResults[i] = std::numeric_limits<double>::quiet_NaN();
			} else { // mean ratings
				double result = this->rate(&((*data)[i]), funcFileToFocusIdx[funcFile], false, 0.0);

				if(std::isnan(result)){
					this->error("There was a NaN rating. This should never happen. If you encounter this error, please contact tkluth@cit-ec.uni-bielefeld.de. Quitting.", "computeResults()", (*data)[i], true);
				}

				myResults[i] = result;

				#if VERBOSITY >= 2
					if (result < this->minRating || result > this->maxRating) {
						std::cout << "computeResults():\t Warning: Rating is higher or lower than allowed! Delta low, high: " << (result - this->minRating) << ", " << (result - this->maxRating) << std::endl;
					}
					std::cout << "computeResults():\t" << "LO: " << std::setprecision(0) << *((*data)[i].locatedObject.getCGALPolygon()) << ", RO: " << *((*data)[i].referenceObject->getCGALPolygon()) << std::endl;
					std::cout << "computeResults():\t" << "model: " << enumToString(this->method) << ", result: " << std::setprecision(4) << result << std::setprecision(0) << std::endl;
					std::cout << "computeResults():\t" << "functional: " << funcFile << std::endl;
					std::cout << "computeResults():\t" << "emp. data: " << std::setprecision(4) << (*data)[i].meanRating << std::setprecision(0) << ", RO hightop: " << (*data)[i].referenceObject->getHightop() << std::endl;
					std::cout << "-----------------------------------------------------" << std::endl;
				#endif
			}
		}

		this->computedRatings = myResults;

	 if (this->ordinal) {
			value = this->compareDistResults(&myProbResults, data, printCorrelation);
		} else {
			value = this->compareResults(&myResults, data, printCorrelation);
		}
	}

	return value;
}

double Model::compareResults(std::vector<double>* myResults, std::vector<RatingDataPoint>* theirResults, bool printCorrelation) {

	assert(myResults->size() == theirResults->size());

	double nRMSE = 0.0;
	double sumDifference = 0.0;

	std::stringstream modelData;
	std::stringstream empData;

	for (size_t i = 0; i < myResults->size() - 1; ++i) {
		double mse = std::pow(((*myResults)[i] - (*theirResults)[i].meanRating), 2);
		sumDifference += mse;
		if(printCorrelation && this->printRatings) {
			modelData << (*myResults)[i] << ", ";
			empData << (*theirResults)[i].meanRating << ", ";
		}
	}

	// last element
	size_t idxLastElement = myResults->size() - 1;
	sumDifference += std::pow(((*myResults)[idxLastElement] - (*theirResults)[idxLastElement].meanRating), 2);

	if(printCorrelation && this->printRatings) {
		modelData << (*myResults)[idxLastElement];
		empData << (*theirResults)[idxLastElement].meanRating;
		std::cout << "compareResults():\tRatings:" << std::endl;
		std::cout << modelData.str();
		std::cout << std::endl << "compareResults():\tEmpirical Data:" << std::endl;
		std::cout << empData.str() << std::endl;
		std::cout << "# copy-paste ready for R to compute a linear model between empirical data and model data: " << std::endl;
		std::cout << "modelData = c(" << modelData.str() << ")" << std::endl;
		std::cout << "empData = c(" << empData.str() << ")" << std::endl;
		std::cout << "summary(lm(modelData ~ empData))" << std::endl;
		std::cout << "coef(lm(modelData ~ empData))" << std::endl;
	}

	nRMSE = sqrt(sumDifference / ((double) myResults->size())) / ((double) (this->maxRating - this->minRating));

	if (printCorrelation) {
	#if VERBOSITY >= 1
		std::cout << std::endl << "compareResults():\tDifferences:\t";

		for(size_t i = 0; i < myResults->size()-1; i = i + 2){
			std::cout << (*myResults)[i] - (*myResults)[i+1] << ",\t";
		}
	#endif
		double correlation = computeCorrelation(myResults, theirResults);
		std::cout << std::endl << "compareResults():\t R^2: " << std::setprecision(4) << std::pow(correlation, 2) << ", nRMSE: " << nRMSE << std::endl;
	}

	return nRMSE;
}

double Model::compareDistResults(std::vector<std::vector<double> >* modelResults, std::vector<RatingDataPoint>* empiricalResults, bool print) {

	assert(modelResults->size() == empiricalResults->size());

	// tested a bit against minimizing rmse
	// KL divergence seems to provide tighter fits to the data
	double kullbackLeibler = 0.0;

	std::stringstream modelData;
	std::stringstream empData;

	size_t ratingRange = this->maxRating - this->minRating + 1;

	for (size_t i = 0; i < modelResults->size(); ++i) {
		for (size_t r = 0; r < ratingRange; ++r) {

			modelData << std::setprecision(4) << (*modelResults)[i][r] << ", ";
			empData << std::setprecision(4) << (*empiricalResults)[i].proportions[r] << ", ";

			// Kullback-Leibler divergence:
			assert((*empiricalResults)[i].proportions[r] >= 0.0);
			assert((*modelResults)[i][r] >= 0.0);
			if ((*empiricalResults)[i].proportions[r] > 0.0) {
				kullbackLeibler += (*empiricalResults)[i].proportions[r] * log((*empiricalResults)[i].proportions[r] / (*modelResults)[i][r]);
			} // else: kullbackLeibler += 0 (because first term is 0 and 0 multiplied with anything is 0)
			#ifndef NDEBUG
			if (std::isnan(kullbackLeibler)) {
				std::cout << "nan!: " << (*empiricalResults)[i].proportions[r] << ", " << (*modelResults)[i][r] << std::endl;
			}
			#endif
		}
		//~ std::cout << kullbackLeibler << std::endl;
		modelData << std::endl;
		empData << std::endl;
	}

	// normalize
	kullbackLeibler = kullbackLeibler / ((double) modelResults->size());

	if (print && this->printRatings) {
		std::cout << "compareDistResults():\tmodel output:" << std::endl;
		std::cout << modelData.str();
		std::cout << std::endl << "compareDistResults():\tempirical data:" << std::endl;
		std::cout << empData.str();
		std::cout << "compareDistResults():\t" << "Kullback-Leibler divergence for comparing proportions of ratings: " << kullbackLeibler << std::endl << std::endl;
	}

	return kullbackLeibler;
}


double Model::computeCorrelation(std::vector<double>* vector1, std::vector<RatingDataPoint>* vector2) {
	assert(vector1->size() == vector2->size());

	double mean1 = 0.0;
	double mean2 = 0.0;

	for (size_t i = 0; i < vector1->size(); ++i) {
		mean1 += (*vector1)[i];
		mean2 += (*vector2)[i].meanRating;
	}

	mean1 = mean1 / ((double) vector1->size());
	mean2 = mean2 / ((double) vector2->size());
	double correlationNumerator = 0.0;
	double correlationDenominator1 = 0.0;
	double correlationDenominator2 = 0.0;

	for (size_t i = 0; i < vector1->size(); i++) {
		correlationNumerator += ((*vector1)[i] - mean1) * ((*vector2)[i].meanRating - mean2);
		correlationDenominator1 += std::pow(((*vector1)[i] - mean1), 2);
		correlationDenominator2 += std::pow(((*vector2)[i].meanRating - mean2), 2);
	}

	#if VERBOSITY >= 2
		std::cout << std::endl << "correlation():\t computing: " << correlationNumerator << " / (sqrt[" << correlationDenominator1 << " * " << correlationDenominator2 << "])" << std::endl;
	#endif

	return correlationNumerator / sqrt(correlationDenominator1 * correlationDenominator2);
}

//core AVS methods:

double Model::centerBB(int x, std::shared_ptr<ReferenceObject> ro){
	return std::pow(sig(x - ro->getCGALPolygon()->left_vertex()->x(), this->lrgain.value), this->lrexp.value) * std::pow(sig(ro->getCGALPolygon()->right_vertex()->x() - x, this->lrgain.value), this->lrexp.value);
}

double Model::centerOfMassOrientation(LocatedObject* lo, std::shared_ptr<ReferenceObject> ro){
	Vector centerOfMass_LO (*(ro->getCenterOfMass()), *(lo->getCenterOfMass()));

	// upright.x == 0 -> don't do this unnecessary computation: direction.dx() * this->upright.dx()
	double angle = acos((centerOfMass_LO.y() * this->canonicalOrientation.dy()) / sqrt(centerOfMass_LO.squared_length()));

	// do not convert radian to degree anymore; g() now takes radians instead of degrees
	return angle; // * 180.0 / M_PI;
}

double Model::proximalOrientation(LocatedObject* lo, std::shared_ptr<ReferenceObject> ro){
	Vector prox_LO (computeProximalPoint(lo, ro), *(lo->getCenterOfMass()));

	// upright.x == 0 -> don't do this unnecessary computation: direction.dx() * this->upright.dx()
	double angle = acos((prox_LO.y() * this->canonicalOrientation.dy()) / sqrt(prox_LO.squared_length()));

	// do not convert radian to degree anymore; g() now takes radians instead of degrees
	return angle; // * 180.0 / M_PI;
}

double Model::rate(RatingDataPoint* input, unsigned int focusIndex, bool visualizeAllVectors, double longVectorScaling) {

	assert (input->locatedObject.getCGALPolygon() != NULL);
	assert (input->referenceObject->getCGALPolygon() != NULL);
	assert (input->locatedObject.getPoints()->size() != 0);
	assert (input->referenceObject->getPoints()->size() != 0);

	if (method == AVSr) {
		this->method = AVS_BB;
		double rating_AVS_BB = this->rate(input, focusIndex, visualizeAllVectors, longVectorScaling);

		this->method = rAVS_CoO;
		double rating_rAVS_CoO = this->rate(input, focusIndex, visualizeAllVectors, longVectorScaling);

		this->method = AVSr;
		double mean = (rating_AVS_BB + rating_rAVS_CoO) / 2.0;
		// or:
		// double mean = (this->phi.value * rating_AVS_BB) + ((1.0 - this->phi.value) * rating_rAVS_CoO);

		#if VERBOSITY >= 1
		if(this->beVerbose){
			std::cout << std::setprecision(4) << "AVS: " << rating_AVS_BB << ", rAVS: " << rating_rAVS_CoO << ", mean: " << mean << " " << std::setprecision(0) << std::endl;
		}
		#endif

		return mean;
	}

	if (input->prep == ABOVE) {
		if(method == rAVS_COM || method == rAVS_comb || method == rAVS_prox || method == rAVS_w_comb || method == rAVS_abs_middle || this->method == rAVS_CoO || this->method == rAVS_multiple){
			this->canonicalOrientation = this->downward;
		} else {
		this-> canonicalOrientation = this->upward;
		}
	} else if(input->prep == BELOW){
		if(method == rAVS_COM || method == rAVS_comb || method == rAVS_prox || method == rAVS_w_comb || method == rAVS_abs_middle || this->method == rAVS_CoO || this->method == rAVS_multiple){
			this->canonicalOrientation = this->upward;
		} else {
		this-> canonicalOrientation = this->downward;
		}
	}

	Point attentionalFocus;

	#if VERBOSITY >= 2
	if(this->beVerbose){
		std::cout << "rate():\tpreposition '" << prepToString(input->prep) << "' in use. The canonical orientation is now: " << this->canonicalOrientation << " (model: " << enumToString(this->method) << ")" << std::endl;
		std::cout << "rate():\tRating LO: " << *(input->locatedObject.getCGALPolygon()) << std::endl;
		std::cout << "rate():\twith RO: " << *(input->referenceObject->getCGALPolygon()) << std::endl;
		}
	#endif

	if (this->method == BB) {
		return heightFunction(input->locatedObject.getCenterOfMass()->y(), input->referenceObject, &(input->prep)) * centerBB((int) input->locatedObject.getCenterOfMass()->x(), input->referenceObject) * (this->maxRating - this->minRating) + this->minRating;
	}

	if (this->method == PC) {
		double rating = this->alpha.value * f(centerOfMassOrientation(&(input->locatedObject), input->referenceObject)) + (1 - this->alpha.value) * f(proximalOrientation(&(input->locatedObject), input->referenceObject));
		#if VERBOSITY >= 0
		if(this->beVerbose){
			std::cout << "rate():\t PC model. proximal orientation: " << proximalOrientation(&(input->locatedObject), input->referenceObject) << ", c-o-m orientation: " << centerOfMassOrientation(&(input->locatedObject), input->referenceObject) << std::endl;
		}
		#endif
		return rating * (this->maxRating - this->minRating) + this->minRating;
	}

	if (this->method == PC_BB) {
		double rating = this->alpha.value * g(centerOfMassOrientation(&(input->locatedObject), input->referenceObject)) + (1-this->alpha.value) * g(proximalOrientation(&(input->locatedObject), input->referenceObject));
		rating = rating * heightFunction(input->locatedObject.getCenterOfMass()->y(), input->referenceObject, &(input->prep));
		return rating * (this->maxRating - this->minRating) + this->minRating;
	}

	if (this->method == focusOnlyAtFunction || this->method == attentionalSwitch) {
		attentionalFocus = getFocusAtFunction(&(input->locatedObject), input->referenceObject, focusIndex);
	} else {
		if (this->method == rAVS_comb || this->method == rAVS_COM || this->method == rAVS_prox || this->method == rAVS_w_comb || this->method == rAVS_abs_middle || this->method == rAVS_CoO) {
			attentionalFocus = getFocusOnLocatedObject(&(input->locatedObject), input->referenceObject, focusIndex);
		} else { //AVS, BB, PC(_BB), AVS_BB
			attentionalFocus = getFocus(&(input->locatedObject), input->referenceObject, &(input->prep), focusIndex);
		}
	}

	Point vectorDestination;

	if (this->method == rAVS_comb || this->method == rAVS_prox) {
		vectorDestination = this->getFocus(&(input->locatedObject), input->referenceObject, &(input->prep), focusIndex);
	} else if (this->method == rAVS_COM || this->method == rAVS_w_comb) {
		vectorDestination = *(input->referenceObject->getCenterOfMass());
	} else {
		vectorDestination = *(input->locatedObject.getCenterOfMass());
	}

	if (this->method == rAVS_w_comb || this->method == rAVS_abs_middle || this->method == rAVS_CoO) {
		// computeProximalPoint() is different from getFocus()
		// getFocus() -> letting fall a perpendicular, always on the top of the RO -> attentional focus F from AVS
		// computeProximalPoint() -> closest point on the RO, not necessary on top of the RO -> "true" proximal point
		Point proximalPoint = this->computeProximalPoint(&(input->locatedObject), input->referenceObject);

		if (this->method == rAVS_w_comb) {
			double distanceX = std::abs(attentionalFocus.x() - proximalPoint.x());
			double distanceY = std::abs(attentionalFocus.y() - proximalPoint.y());

			double relativeDistance = (distanceX / (input->referenceObject->getWidth())) + (distanceY / (input->referenceObject->getHeight()));

			Vector centerToF = Vector(*input->referenceObject->getCenterOfMass(), getFocus(&(input->locatedObject), input->referenceObject, &(input->prep), focusIndex));
			double weight = (-this->alpha.value * relativeDistance) + 1.0;

			if (weight < 0.0) {
				weight = 0.0;
			}

			Vector loToWeightedDest = weight * centerToF;
			vectorDestination = Point(input->referenceObject->getCenterOfMass()->x() + loToWeightedDest.x(), input->referenceObject->getCenterOfMass()->y() + loToWeightedDest.y());

			#if VERBOSITY >= 2
			if (beVerbose) {
				std::cout << "rate():\t LO is close to the RO, weighted prox destination stronger. RO: " << *(input->referenceObject->getCGALPolygon()) << std::endl;
				std::cout << "rate():\t height of the RO: " << input->referenceObject->getHeight() << std::endl;
				std::cout << "rate():\t width of the RO: " << input->referenceObject->getWidth() << std::endl;
				std::cout << "rate():\t relative distance: " << relativeDistance << std::endl;
				std::cout << "rate():\t c-o-m: " << *(input->referenceObject->getCenterOfMass()) << std::endl;
				std::cout << "rate():\t Vector c-o-m to F: " << centerToF << std::endl;
				std::cout << "rate():\t weight: " << weight << std::endl;
				std::cout << "rate():\t Vector c-o-m to prox, weighted: " << loToWeightedDest << std::endl;
				std::cout << "rate():\t New vector destination is: " << vectorDestination << std::endl;
			}
		#endif
		}

		if (this->method == rAVS_CoO) {
			// rAVS with relative distance + center-of-object
			Point focus_AVS = getFocus(&(input->locatedObject), input->referenceObject, &(input->prep), focusIndex);

			double distanceX = std::abs(attentionalFocus.x() - proximalPoint.x());
			double distanceY = std::abs(attentionalFocus.y() - proximalPoint.y());
			double relativeDistance = (distanceX / (input->referenceObject->getWidth())) + (distanceY / (input->referenceObject->getHeight()));

			double weight = (-this->alpha.value * relativeDistance) + 1.0;

			if (weight < 0.0) {
				weight = 0.0;
			}

			Vector centerToF = Vector(*input->referenceObject->getCenterOfObject(), focus_AVS);
			Vector loToWeightedDest = weight * centerToF;

			vectorDestination = Point(input->referenceObject->getCenterOfObject()->x() + loToWeightedDest.x(), input->referenceObject->getCenterOfObject()->y() + loToWeightedDest.y());

			// if a LO is lower than the center-of-object while still above the top of the object (special case, see exp. 5, Regier & Carlson)
			// then the vectorDestination is the proximal point aka focus as in the AVS on the RO
			if(attentionalFocus == *input->referenceObject->getCenterOfObject()){
				#if VERBOSITY > 1
				std::cout << "rate():\trAVS-CoO, special treatment of an LO that is below the CoO but above the top of the RO..." << std::endl;
				std::cout << "LO: " << input->locatedObject.getCenterOfObject()->x() << ", " << input->locatedObject.getCenterOfObject()->y() << std::endl;
				#endif
				vectorDestination = focus_AVS;
			}

			#if VERBOSITY >= 2
			if(beVerbose){
				std::cout << "rate():\t LO is close to the RO, weighted prox destination stronger." << std::endl;
				std::cout << "rate():\t LO: " << *(input->locatedObject.getCGALPolygon()) << std::endl;
				std::cout << "rate():\t RO: " << *(input->referenceObject->getCGALPolygon()) << std::endl;
				std::cout << "rate():\t relative distance: " << relativeDistance << std::endl;
				std::cout << "rate():\t CoM: " << *(input->referenceObject->getCenterOfMass()) << std::endl;
				std::cout << "rate():\t MoO: " << *(input->referenceObject->getCenterOfObject()) << std::endl;
				//~ std::cout << "rate():\t centerOnTop: " << centerTop << std::endl;
				//~ std::cout << "rate():\t averaged CoM/MoO: " << averageMiddleCenter << std::endl;
				std::cout << "rate():\t Vector CoO to F: " << centerToF << std::endl;
				std::cout << "rate():\t weight: " << weight << std::endl;
				std::cout << "rate():\t Vector CoO to prox, weighted: " << loToWeightedDest << std::endl;
				std::cout << "rate():\t New vector destination is: " << vectorDestination << std::endl;
			}
			#endif
		}

		if (this->method == rAVS_abs_middle) {

			std::cout << "Warning: This model is experimental and not tested!" << std::endl;

			// rAVS with absolute distance + middle-of-object
			Point focus_AVS = getFocus(&(input->locatedObject), input->referenceObject, &(input->prep), focusIndex);
			double absoluteDistance = sqrt(squared_distance(attentionalFocus, proximalPoint));

			//~ double distanceX = std::abs(attentionalFocus.x() - proximalPoint.x());
			//~ double distanceY = std::abs(attentionalFocus.y() - proximalPoint.y());
			//~ double relativeDistance = (distanceX / (input->referenceObjectgetWidth())) + (distanceY / (input->referenceObject->getHeight()));

			//~ double weight = 1.0 / exp(this->alpha.value * absoluteDistance);
			//double weight = (-this->alpha.value * absoluteDistance) + 1.0;
			//~ double weight = (-this->alpha.value * relativeDistance) + 1.0;
			double weight = 1 - (absoluteDistance / (absoluteDistance + this->alpha.value));

			if (weight < 0.0) {
				weight = 0.0;
			}
			 //~ else if(weight > 1.0) {
				//~ weight = 1.0;
			//~ }

			//~ Point averageMiddleCenter = Point(((input->referenceObject->getCenterOfObject()->x() * this->phi.value) + (input->referenceObject->getCenterOfMass()->x() * (1-this->phi.value))),
					//~ ((input->referenceObject->getCenterOfObject()->y() * this->phi.value) + (input->referenceObject->getCenterOfMass()->y() * (1-this->phi.value))));
			//~ Vector middleToProx = Vector(averageMiddleCenter, focus_AVS);
			//~ Vector loToWeightedDest = weight * middleToProx;
			//~ vectorDestination = Point(averageMiddleCenter.x() + loToWeightedDest.x(), averageMiddleCenter.y() + loToWeightedDest.y());

			//~ Vector proxToMiddle = Vector(focus_AVS, *input->referenceObject->getCenterOfObject());
			//~ Vector loToWeightedDest = weight * proxToMiddle;
			//~ vectorDestination = Point(focus_AVS.x() + loToWeightedDest.x(), focus_AVS.y() + loToWeightedDest.y());

			Vector middleToProx = Vector(*input->referenceObject->getCenterOfObject(), focus_AVS);
			Vector loToWeightedDest = weight * middleToProx;
			vectorDestination = Point(input->referenceObject->getCenterOfObject()->x() + loToWeightedDest.x(), input->referenceObject->getCenterOfObject()->y() + loToWeightedDest.y());

			// if a LO is lower than the middle-of-object while still above the top of the object (special case, see exp. 5, Regier & Carlson)
			// then the vectorDestination is the proximal point aka focus as in the AVS on the RO
			//~ if(attentionalFocus.y() <= input->referenceObject->getCenterOfObject()->y() && attentionalFocus.y() > focus_AVS.y()){
			if(attentionalFocus == *input->referenceObject->getCenterOfObject()){
				#if VERBOSITY >= 2
				std::cout << "rate():\trAVS-abs-middle, special treatment of an LO that is below the MoO but above the top of the RO..." << std::endl;
				std::cout << "LO: " << input->locatedObject.getCenterOfObject()->x() << ", " << input->locatedObject.getCenterOfObject()->y() << std::endl;
				#endif
				vectorDestination = focus_AVS;
			}

			#if VERBOSITY >= 2
			if(beVerbose){
				std::cout << "rate():\t LO is close to the RO, weighted prox destination stronger." << std::endl;
				std::cout << "rate():\t LO: " << *(input->locatedObject.getCGALPolygon()) << std::endl;
				std::cout << "rate():\t RO: " << *(input->referenceObject->getCGALPolygon()) << std::endl;
				std::cout << "rate():\t absolute distance: " << absoluteDistance << std::endl;
				std::cout << "rate():\t CoM: " << *(input->referenceObject->getCenterOfMass()) << std::endl;
				std::cout << "rate():\t MoO: " << *(input->referenceObject->getCenterOfObject()) << std::endl;
				//~ std::cout << "rate():\t averaged CoM/MoO: " << averageMiddleCenter << std::endl;
				std::cout << "rate():\t Vector MoO to prox: " << middleToProx << std::endl;
				std::cout << "rate():\t weight: " << weight << std::endl;
				std::cout << "rate():\t Vector MoO to prox, weighted: " << loToWeightedDest << std::endl;
				std::cout << "rate():\t New vector destination is: " << vectorDestination << std::endl;
			}
			#endif
		}

	}

	double sigma = sqrt(squared_distance(attentionalFocus, vectorDestination));

	Vector direction;
	if (this->method == rAVS_comb || this->method == rAVS_prox || this->method == rAVS_COM || this->method == rAVS_w_comb || this->method == rAVS_abs_middle || this->method == rAVS_CoO) {
		direction = vectorSumLocatedObject(attentionalFocus, &vectorDestination, &(input->locatedObject), sigma, visualizeAllVectors, longVectorScaling);
	} else {
		if (this->method == rAVS_multiple) {
			direction = vectorSumReversed(attentionalFocus, &vectorDestination, sigma, visualizeAllVectors, longVectorScaling, input->referenceObject);
		} else {
			direction = vectorSum(attentionalFocus, &vectorDestination, sigma, visualizeAllVectors, longVectorScaling, input->referenceObject);
		}
	}

	if (direction.squared_length() < std::numeric_limits<double>::min() || std::isnan(direction.x()) || std::isnan(direction.y())) {
		if (this->method != rAVS_abs_middle) {
			std::stringstream errorstream;
			errorstream << "Warning! Computed direction is of length zero or NaN: direction length: " << std::setprecision(8) << direction.squared_length() << ", angle (in degrees): " << (acos((direction.y() * this->canonicalOrientation.dy()) / sqrt(direction.squared_length())) * 180.0 / M_PI) << ", isnan(direction.x)?: " << std::isnan(direction.x()) << ", isnan(direction.y)?: " << std::isnan(direction.y()) << ". Returning minimal rating.";
			this->error(errorstream.str(), "rate()", *input, false);
			return this->minRating;
		}
	}

	#if VERBOSITY >= 2
	if (this->beVerbose) {
		std::cout << "rate():\tc-o-m: " << *(input->referenceObject->getCenterOfMass()) << std::endl;
		std::cout << "rate():\tvector destination: " << vectorDestination << ", direction: " << std::setprecision(3) << direction << std::setprecision(0) << ", direction length: " <<  sqrt(direction.squared_length()) << std::endl;
	}
	#endif

	double angle;

	if (this->method == rAVS_comb) {
		// combine proximal with center-of-mass orientation

		// upright.x == 0 -> don't do this unnecessary computation: direction.dx() * this->upright.dx()
		angle = acos((direction.y() * this->canonicalOrientation.dy()) / sqrt(direction.squared_length()));

		// compute result with vectorDestination=RO.getCenterOfMass(), then combine both angles
		Point vectorDestinationCOM = *(input->referenceObject->getCenterOfMass());
		double sigmaCOM = sqrt(squared_distance(attentionalFocus, vectorDestinationCOM));
		Vector directionCOM = vectorSumLocatedObject(attentionalFocus, &vectorDestinationCOM, &(input->locatedObject), sigmaCOM, visualizeAllVectors, longVectorScaling);
		assert(!(almostEqual(directionCOM.squared_length(), 0)));
		//upright.x == 0 -> don't do this unnecessary computation: direction.dx() * this->upright.dx()
		double angleCOM = acos((directionCOM.y() * this->canonicalOrientation.dy()) / sqrt(directionCOM.squared_length()));

	#if VERBOSITY >= 2
		if (this->beVerbose) {
			std::cout << "rate():\trAVS-comb method." << std::endl;
			std::cout << "rate():\tvector destination proximal point: " << vectorDestination << ", direction: " << direction << std::endl;
			std::cout << "rate():\tvector destination center-of-mass: " << vectorDestinationCOM << ", direction: " << directionCOM << std::endl;
			std::cout << "rate():\tangleProx : " << angle << ", angleCOM : " << angleCOM << ", mean : " << ((angle + angleCOM) / 2.0) << std::endl;
			std::cout << "rate():\tangleProx°: " << angle * 180.0 / M_PI << ", angleCOM°: " << angleCOM * 180.0 / M_PI << ", mean°: " << ((angle + angleCOM) / 2.0) * 180.0 / M_PI << std::endl;
		}
	#endif
		// take the mean between both directions
		angle = (angle + angleCOM) / 2.0;

		// flip back to absolute difference from this->canonicalOrientations again
		if (angle < 0.0) {
			angle = -angle;
		}

	} else {
		// if (this->method == rAVS_prox || this->method == rAVS_COM || this->method == rAVS_w_comb || this->method == rAVS_multiple) {
		double acosarg = (direction.y() * this->canonicalOrientation.dy()) / sqrt(direction.squared_length());

		if (acosarg > 1.0 || acosarg < -1.0) {
			if (acosarg < -1.0) {
				std::cout << "rate():\tfloating point precision error. Rounding acos argument from " << acosarg << " to -1" << std::endl;
				acosarg = -1.0;
			} else if (acosarg > 1.0) {
				std::cout << "rate():\tfloating point precision error. Rounding acos argument from " << acosarg << " to 1.0" << std::endl;
				acosarg = 1.0;
			}
		}

		angle = acos(acosarg);

#if VERBOSITY >= 2
	std::cout << "rate():\t(" << direction.y() << " * " << this->canonicalOrientation.dy() << ") / " << sqrt(direction.squared_length()) << " = " << (direction.y() * this->canonicalOrientation.dy()) / sqrt(direction.squared_length()) << std::endl;
	std::cout << "rate():\tacos((" << direction.y() << " * " << this->canonicalOrientation.dy() << ") / " << sqrt(direction.squared_length()) << ") = " << acos((direction.y() * this->canonicalOrientation.dy()) / sqrt(direction.squared_length())) << std::endl;
#endif

		// TODO handle the special case when the LO is at the same point as the CoO for rAVS-abs-middle. Should be figured out for rAVS-w-comb as well (when LO == CoM).
		if ((std::isnan(direction.x()) || std::isnan(direction.y())) && (this->method == rAVS_abs_middle || this->method == rAVS_CoO) && attentionalFocus == vectorDestination) {
			//~ #if VERBOSITY > 1
			std::cout << "rate:\trAVS-abs-middle: LO is the same as the center of the object. Assuming zero deviation." << std::endl;
			//~ #endif
			angle = 0;
		}
	}

	if (this->method == attentionalSwitch) {
		Point attentionalFocusGeometry;
		attentionalFocusGeometry = getFocus(&(input->locatedObject), input->referenceObject, &(input->prep), focusIndex);
		double sigmaGeometry = sqrt(squared_distance(attentionalFocusGeometry, vectorDestination));
		Vector directionGeometry = vectorSum(attentionalFocusGeometry, &vectorDestination, sigmaGeometry, visualizeAllVectors, longVectorScaling, input->referenceObject);
		assert(!(almostEqual(directionGeometry.squared_length(), 0)));
		// upright.x == 0 -> don't do this unnecessary computation: direction.dx() * this->upright.dx()
		double angleGeometry = acos((directionGeometry.y() * this->canonicalOrientation.dy()) / sqrt(directionGeometry.squared_length()));
		// cross-product to find out, if direction is to the left or to the right
		double orientationGeometry = this->canonicalOrientation.dy() * directionGeometry.x();
		double orientationFunction = this->canonicalOrientation.dy() * direction.x();

		if (orientationGeometry > 0) {
			// direction to the left, flip sign
			angleGeometry = -angleGeometry;
		}

		if (orientationFunction > 0) {
			// direction to the left, flip sign
			angle = -angle;
		}

		//take the mean between both directions, SC2014 version:
		//~ angle = (angle + angleGeometry) / 2.0;

		// account for functional strengthness by weighting functional part
		double funcWeight = this->phi.value / 2.0;
		angle = (funcWeight * angle + angleGeometry) / (funcWeight + 1.0);

		// flip back to absolute difference from this->canonicalOrientations again
		if (angle < 0) {
			angle = -angle;
		}
	}

	// do not convert radian to degree anymore; g() now takes radians instead of degrees
	//~ angle = angle * 180.0 / M_PI;

	double result;
	if (this->method == rAVS_comb || this->method == rAVS_prox || this->method == rAVS_COM || this->method == rAVS_w_comb || this->method == rAVS_abs_middle || this->method == rAVS_CoO) {
		// the AVS alignment function + height component:
		result = g(angle) * this->heightFunction(attentionalFocus.y(), input->referenceObject, &(input->prep));
		// without height component, but the alignment function used by the BB model
	} else {
		result = g(angle) * this->heightFunction(vectorDestination.y(), input->referenceObject, &(input->prep));
	}

	// check whether the models did produce something inside the range of the allowed rating scale:
	double rating = result * (this->maxRating - this->minRating) + this->minRating;

	if (rating < this->minRating) {
		rating = this->minRating;
	} else if (rating > this->maxRating) {
		rating = this->maxRating;
	}

	#if VERBOSITY >= 2
	if (this->beVerbose) {
		#if VERBOSITY >= 2
		if (this->method == rAVS_comb || this->method == rAVS_prox || this->method == rAVS_COM || this->method == rAVS_w_comb || this->method == rAVS_abs_middle || this->method == rAVS_CoO) {
			std::cout << "rate():\tvector destination: " << vectorDestination << ", angle: " << angle << ", heightLO: " << this->heightFunction(attentionalFocus.y(), input->referenceObject, &(input->prep)) << std::endl;
		} else {
			std::cout << "rate():\tvector destination: " << vectorDestination << ", angle: " << angle << ", height: " << this->heightFunction(vectorDestination.y(), input->referenceObject, &(input->prep)) << std::endl;
		}
		std::cout << "rate():\tvector destination: " << vectorDestination << ", g(angle): " << g(angle) << std::endl;
		std::cout << "rate():\tvector destination: " << vectorDestination << ", rating (unchecked for out-of-range problems): " << result * (this->maxRating - this->minRating) + this->minRating << std::endl;
		#endif
		std::cout << "rate():\tvector destination: " << vectorDestination << ", final rating: " << std::setprecision(4) << rating << std::endl;
	}
	#endif

	return rating;
}

std::vector<double> Model::rateProbs(RatingDataPoint* input, unsigned int focusIndex, bool visualizeAllVectors, double longVectorScaling) {
	assert(this->ordinal);

	double mu = rate(input, focusIndex, visualizeAllVectors, longVectorScaling);

	assert(this->ratingsProbs.size() == (this->thresholds.size() + 1));

	// gsl's default mu == 0; mu and sigmaOrd are set in the argument of the function according to Kruschke (2015) pages 674 following
	this->ratingsProbs[0] = gsl_cdf_gaussian_P(((this->thresholds[0].value - mu) / this->sigmaOrd.value), 1.0); // - 0
	size_t noOfThresholds = this->thresholds.size();
	for (size_t t = 1; t < noOfThresholds; ++t) {
		this->ratingsProbs[t] = gsl_cdf_gaussian_P(((this->thresholds[t].value - mu) / this->sigmaOrd.value), 1.0) -
														gsl_cdf_gaussian_P(((this->thresholds[t - 1].value - mu) / this->sigmaOrd.value), 1.0);
	}
	this->ratingsProbs[this->ratingsProbs.size() - 1] = 1 - gsl_cdf_gaussian_P(((this->thresholds[this->thresholds.size() - 1].value - mu) / this->sigmaOrd.value), 1.0);

	#ifndef NDEBUG
	double probSum = 0.0;
	for (size_t t = 0; t < noOfThresholds + 1; ++t) {
		probSum = probSum + this->ratingsProbs[t];
	}
	assert(almostEqual(probSum, 1.0));
	#endif

	return this->ratingsProbs;
}

Point Model::getFocusAtFunction(LocatedObject* lo, std::shared_ptr<ReferenceObject> ro, unsigned int focusIndex) {

	Point* fromWhere = lo->getCenterOfMass();
	assert((((int) fromWhere->x()) - this->xOrigin) >= 0);
	unsigned int loIndex = (unsigned int) (((int) fromWhere->x()) - this->xOrigin);

	if (! std::isnan(this->fociFunction[this->method][ro->getIndex()][focusIndex][loIndex].x())
			&& ! std::isnan(this->fociFunction[this->method][ro->getIndex()][focusIndex][loIndex].y())) {
	#if VERBOSITY >= 2
		std::cout << "getFocusAtFunction():\t known focus: " << this->fociFunction[this->method][ro->getIndex()][focusIndex][loIndex] << ", from-where?: " << *fromWhere << ", focusIndex:" << focusIndex << std::endl;
	#endif
		return this->fociFunction[this->method][ro->getIndex()][focusIndex][loIndex];
	}

	Point attentionalFocus;

	// no vertical aligned point found, using next vertex (left or right)
	if (fromWhere->x() < ro->getLeftTopFunction().x()) {
		attentionalFocus = ro->getLeftTopFunction();
	#if VERBOSITY >= 1
		std::cout << "getFocusAtFunction():\t computed focus: " << attentionalFocus << ", left edge, from-where?: " << *fromWhere << ", roIndex " << ro->getIndex() << std::endl;
	#endif
		this->fociFunction[this->method][ro->getIndex()][focusIndex][loIndex] = attentionalFocus;
		return attentionalFocus;
	} else if (fromWhere->x() > ro->getRightTopFunction().x()) {
		attentionalFocus = ro->getRightTopFunction();
	#if VERBOSITY >= 1
		std::cout << "getFocusAtFunction():\t computed focus: " << attentionalFocus << ", right edge, from-where?: " << *fromWhere << ", roIndex " << ro->getIndex() << std::endl;
	#endif
		this->fociFunction[this->method][ro->getIndex()][focusIndex][loIndex] = attentionalFocus;
		return attentionalFocus;
	}

	Segment lot;

	if (fromWhere->y() <= ro->getLowtopFunction()) {
		// point below top of the referenceObject -> lot in direction to the top
		lot = Segment(*fromWhere, Point(fromWhere->x(), this->height));
	} else {
		lot = Segment(*fromWhere, Point(fromWhere->x(), this->yOrigin));
	}

	std::list<Polygon_with_holes> intersection;
	bool foundIntersection = false;

	for (size_t e = 0; e != ro->getTopEdgesFunction().size(); e++) {
		CGAL::Object obj = CGAL::intersection(lot, ro->getTopEdgesFunction()[e]);

		if (const Point* point = CGAL::object_cast<Point>(&obj)) {
			// one intersection
			if (!foundIntersection) {
				attentionalFocus = *point;
				foundIntersection = true;
			}
		} else if (const Segment* segment = CGAL::object_cast<Segment>(&obj)) {
			// intersection with an edge, choose highest point
			if (!foundIntersection) {
				attentionalFocus = (*segment).max();
				foundIntersection = true;
			}
		}
	}
	#if VERBOSITY >= 1
	std::cout << "getFocusAtFunction():\t computed focus: " << attentionalFocus << ", from-where?: " << *fromWhere << ", roIndex " << ro->getIndex() << std::endl;
	#endif
	this->fociFunction[this->method][ro->getIndex()][focusIndex][loIndex] = attentionalFocus;
	return attentionalFocus;
}

Point Model::getFocus(LocatedObject* lo, std::shared_ptr<ReferenceObject> ro, preposition* prep, unsigned int focusIndex) {

	if(*prep == ABOVE){
		return getFocusAbove(lo, ro, focusIndex);
	} else if (*prep == BELOW){
		return getFocusBelow(lo, ro, focusIndex);
	} else {
		this->error("Prepositions other than 'above' and 'below' are not implemented yet.", "getFocus()", this->empiricalData[0], true);
		return Point();
	}
}

Point Model::getFocusAbove(LocatedObject* lo, std::shared_ptr<ReferenceObject> ro, unsigned int focusIndex) {

	Point* fromWhere = lo->getCenterOfMass();
	assert((((int) fromWhere->x()) - this->xOrigin) >= 0);
	unsigned int loIndex = (unsigned int) (((int) fromWhere->x()) - this->xOrigin);

	if (! std::isnan(this->fociAbove[this->method][ro->getIndex()][focusIndex][loIndex].x())
			&& ! std::isnan(this->fociAbove[this->method][ro->getIndex()][focusIndex][loIndex].y()) ) {
	#if VERBOSITY >= 2
		if (this->beVerbose) {
			std::cout << "getFocusAbove():\t known focus: " << this->fociAbove[this->method][ro->getIndex()][focusIndex][loIndex] << " from-where?: " << *fromWhere << ", model: " << enumToString(this->method) << std::endl;
		}
	#endif
		return this->fociAbove[this->method][ro->getIndex()][focusIndex][loIndex];
	}

	Point attentionalFocus;

	// no vertical aligned point found, using next vertex (left or right)
	if (fromWhere->x() < ro->getLeftTop().x()) {
		//~ if (this->method == moveFocus) {
		//SC2014 version:
		//~ double x = (this->referenceObject->getLeftTop().x() + this->referenceObject->getLeftTopFunction().x()) / 2.0;
		//~ double y = (this->referenceObject->getLeftTop().y() + this->referenceObject->getLeftTopFunction().y()) / 2.0;
		//~ attentionalFocus = Point(x, y);
		//~ } else {
		attentionalFocus = ro->getLeftTop();
		//~ }

	#if VERBOSITY >= 1
		if(this->beVerbose) {
			std::cout << "getFocus():\t computed focus: " << attentionalFocus << ", left edge, from-where?: " << *fromWhere << " focusIndex: " << focusIndex << ", roIndex " << ro->getIndex() << ", model: " << enumToString(this->method) << std::endl;
		}
	#endif
		this->fociAbove[this->method][ro->getIndex()][focusIndex][loIndex] = attentionalFocus;
		if (this->method != moveFocus) {
			return attentionalFocus;
		}
	} else if (fromWhere->x() > ro->getRightTop().x()) {
		//~ if (this->method == moveFocus) {
		//SC2014 version:
		//~ double x = (this->referenceObject->getRightTop().x() + this->referenceObject->getRightTopFunction().x()) / 2.0;
		//~ double y = (this->referenceObject->getRightTop().y() + this->referenceObject->getRightTopFunction().y()) / 2.0;
		//~ attentionalFocus = Point(x, y);
		//~ } else {
		attentionalFocus = ro->getRightTop();
		//~ }

	#if VERBOSITY >= 1
		if(this->beVerbose) {
			std::cout << "getFocus():\t computed focus: " << attentionalFocus << ", right edge, from-where?: " << *fromWhere << " focusIndex: " << focusIndex << ", roIndex " << ro->getIndex() << ", model: " << enumToString(this->method) << std::endl;
		}
	#endif
		this->fociAbove[this->method][ro->getIndex()][focusIndex][loIndex] = attentionalFocus;
		if (this->method != moveFocus) {
			return attentionalFocus;
		}
	}

	Segment lot;

	if (fromWhere->y() <= ro->getLowtop()) {
		//point below top of the referenceObject -> lot in direction to the top
		lot = Segment(*fromWhere, Point(fromWhere->x(), this->height));
	} else {
		lot = Segment(*fromWhere, Point(fromWhere->x(), this->yOrigin));
	}

	std::list<Polygon_with_holes> intersection;
	bool foundIntersection = false;

	for (size_t e = 0; e != ro->getTopEdges().size(); e++) {
		CGAL::Object obj = CGAL::intersection(lot, ro->getTopEdges()[e]);

		if (const Point* point = CGAL::object_cast<Point>(&obj)) {
			// one intersection
			if (!foundIntersection) {
				attentionalFocus = *point;
				foundIntersection = true;
			}
		} else if (const Segment* segment = CGAL::object_cast<Segment>(&obj)) {
			// intersection with an edge, choose highest point
			if (!foundIntersection) {
				attentionalFocus = (*segment).max();
				foundIntersection = true;
			}
		}
	}

	if (this->method == moveFocus && !ro->isInsideFunction(&attentionalFocus)) {
		if (attentionalFocus.x() <= ro->getLeftTopFunction().x()) {
			// left from functional part
			// SC2014 version:
			//~ double x = (attentionalFocus.x() + this->referenceObject->getLeftTopFunction().x()) / 2.0;
			//~ double y = (attentionalFocus.y() + this->referenceObject->getLeftTopFunction().y()) / 2.0;
			//~ attentionalFocus = Point(x, y);

			// account for functional strengthness:
			// for phi = 2.0 the focus moves complete to the functional part
			// for phi = 0 the focus stays the same as in AVS
			Vector movementToFunction = (this->phi.value / 2.0) * (attentionalFocus - ro->getLeftTopFunction());
			attentionalFocus = attentionalFocus - movementToFunction;
		} else {
			// right from functional part
			// SC2014 version:
			//~ double x = this->referenceObject->getRightTopFunction().x() + ((attentionalFocus.x() - this->referenceObject->getRightTopFunction().x()) / 2.0);
			//~ double y = this->referenceObject->getRightTopFunction().y() + ((attentionalFocus.y() - this->referenceObject->getRightTopFunction().y()) / 2.0);
			//~ attentionalFocus = Point(x, y);

			// account for functional strengthness:
			// for phi = 2.0 the focus moves complete to the functional part
			// for phi = 0 the focus stays the same as in AVS
			Vector movementToFunction = (this->phi.value / 2.0) * (attentionalFocus - ro->getRightTopFunction());
			attentionalFocus = attentionalFocus - movementToFunction;
		}
	}
	#if VERBOSITY >= 1
	if(this->beVerbose) {
		std::cout << "getFocusAbove():\t computed focus: " << attentionalFocus << ", from-where?: " << *fromWhere << ", focusIndex: " << focusIndex << ", roIndex " << ro->getIndex() << ", model: " << enumToString(this->method) << std::endl;
	}
	#endif
	this->fociAbove[this->method][ro->getIndex()][focusIndex][loIndex] = attentionalFocus;
	return attentionalFocus;
}

Point Model::getFocusBelow(LocatedObject* lo, std::shared_ptr<ReferenceObject> ro, unsigned int focusIndex) {

	Point* fromWhere = lo->getCenterOfMass();
	assert((((int) fromWhere->x()) - this->xOrigin) >= 0);
	unsigned int loIndex = (unsigned int) (((int) fromWhere->x()) - this->xOrigin);

	if (! std::isnan(this->fociBelow[this->method][ro->getIndex()][focusIndex][loIndex].x())
			&& ! std::isnan(this->fociBelow[this->method][ro->getIndex()][focusIndex][loIndex].y())) {
	#if VERBOSITY >= 2
		if(this->beVerbose) {
			std::cout << "getFocusBelow():\t known focus: " << this->fociBelow[this->method][ro->getIndex()][focusIndex][loIndex] << " from-where?: " << *fromWhere << ", model: " << enumToString(this->method) << std::endl;
		}
	#endif
		return this->fociBelow[this->method][ro->getIndex()][focusIndex][loIndex];
	}

	Point attentionalFocus;

	// LO is not directly above / below the RO: using next vertex (left or right)
	if (fromWhere->x() < ro->getLeftBottom().x()) {
		attentionalFocus = ro->getLeftBottom();

	#if VERBOSITY >= 1
		if(this->beVerbose) {
			std::cout << "getFocusBelow():\t computed focus: " << attentionalFocus << ", left edge, from-where?: " << *fromWhere << " focusIndex: " << focusIndex << ", roIndex " << ro->getIndex() << ", model: " << enumToString(this->method) << std::endl;
		}
	#endif
		this->fociBelow[this->method][ro->getIndex()][focusIndex][loIndex] = attentionalFocus;

		if (this->method != moveFocus) {
			return attentionalFocus;
		}
	} else if (fromWhere->x() > ro->getRightBottom().x()) {
		attentionalFocus = ro->getRightBottom();

	#if VERBOSITY >= 1
		if(this->beVerbose) {
			std::cout << "getFocusBelow():\t computed focus: " << attentionalFocus << ", right edge, from-where?: " << *fromWhere << " focusIndex: " << focusIndex << ", roIndex " << ro->getIndex() << ", model: " << enumToString(this->method) << std::endl;
		}
	#endif
		this->fociBelow[this->method][ro->getIndex()][focusIndex][loIndex] = attentionalFocus;

		if (this->method != moveFocus) {
			return attentionalFocus;
		}
	}

	Segment lot;

	if (fromWhere->y() >= ro->getHighBottom()) {
		// point above bottom of the referenceObject -> lot in direction to the bottom
		lot = Segment(*fromWhere, Point(fromWhere->x(), this->yOrigin));
	} else {
		lot = Segment(*fromWhere, Point(fromWhere->x(), this->height));
	}

	std::list<Polygon_with_holes> intersection;
	bool foundIntersection = false;

	for (size_t e = 0; (e != ro->getBottomEdges().size()) && !foundIntersection; ++e) {
		CGAL::Object obj = CGAL::intersection(lot, ro->getBottomEdges()[e]);

		if (const Point* point = CGAL::object_cast<Point>(&obj)) {
			// one intersection
			if (!foundIntersection) {
				attentionalFocus = *point;
				foundIntersection = true;
			}
		} else if (const Segment* segment = CGAL::object_cast<Segment>(&obj)) {
			// intersection with an edge, choose highest point
			if (!foundIntersection) {
				attentionalFocus = (*segment).max();
				foundIntersection = true;
			}
		}
	}

	if (this->method == moveFocus && !ro->isInsideFunction(&attentionalFocus)) {
		if (attentionalFocus.x() <= ro->getLeftBottomFunction().x()) {
			// left from functional part

			// account for functional strengthness:
			// for phi = 2.0 the focus moves complete to the functional part
			// for phi = 0 the focus stays the same as in AVS
			Vector movementToFunction = (this->phi.value / 2.0) * (attentionalFocus - ro->getLeftBottomFunction());
			attentionalFocus = attentionalFocus - movementToFunction;
		} else {
			// right from functional part

			// account for functional strengthness:
			// for phi = 2.0 the focus moves complete to the functional part
			// for phi = 0 the focus stays the same as in AVS
			Vector movementToFunction = (this->phi.value / 2.0) * (attentionalFocus - ro->getRightBottomFunction());
			attentionalFocus = attentionalFocus - movementToFunction;
		}
	}
	#if VERBOSITY >= 1
	if(this->beVerbose) {
		std::cout << "getFocusBelow():\t computed focus: " << attentionalFocus << ", from-where?: " << *fromWhere << ", focusIndex: " << focusIndex << ", roIndex " << ro->getIndex() << ", model: " << enumToString(this->method) << std::endl;
	}
	#endif

	this->fociBelow[this->method][ro->getIndex()][focusIndex][loIndex] = attentionalFocus;
	return attentionalFocus;
}

Point Model::computeProximalPoint(LocatedObject* lo, std::shared_ptr<ReferenceObject> ro) {

	unsigned int indexOfLo = lo->getIndex();

	if (! std::isnan(this->proximalPoints[indexOfLo].x())
			&& ! std::isnan(this->proximalPoints[indexOfLo].y())) {
	#if VERBOSITY >= 2
		if(this->beVerbose) {
			std::cout << "computeProx():\t known proximal point: " << this->proximalPoints[indexOfLo] << ", *lo->getCenterOfMass(): " << *lo->getCenterOfMass() << ", roIndex " << ro->getIndex() << ", model: " << enumToString(this->method) << std::endl;
		}
	#endif
		return this->proximalPoints[indexOfLo];
	}

	double smallestDistance = squared_distance(*lo->getCenterOfMass(), *ro->getCenterOfMass());
	Point vectorDestProximal = *ro->getCenterOfMass();

	for (size_t rop = 0; rop < ro->getPoints()->size(); ++rop) {
	#if VERBOSITY >= 3
		if(this->beVerbose) {
			std::cout << "computeProx():\t" << *lo->getCenterOfMass() << " - " << (*ro->getPoints())[rop] << " distance: " << squared_distance(*lo->getCenterOfMass(), (*ro->getPoints())[rop]) << ", roIndex " << ro->getIndex() << std::endl;
		}
	#endif
		if (squared_distance(*lo->getCenterOfMass(), (*ro->getPoints())[rop]) < smallestDistance) {
			vectorDestProximal = (*ro->getPoints())[rop];
			smallestDistance = squared_distance(*lo->getCenterOfMass(), (*ro->getPoints())[rop]);
		}
	}
	#if VERBOSITY >= 3
	if (this->beVerbose) {
		std::cout << "computeProx():\t lo: " << *lo->getCGALPolygon() << ", points in the RO: " << ro->getPoints()->size() << ", proximal point on RO: " << vectorDestProximal << ", roIndex " << ro->getIndex() << std::endl;
	}
	#endif

	this->proximalPoints[indexOfLo] = vectorDestProximal;

	return vectorDestProximal;
}

Point Model::getFocusOnLocatedObject(LocatedObject* lo, std::shared_ptr<ReferenceObject> ro, unsigned int focusIndex) {
	unsigned int indexOfLo = lo->getIndex();

	#if VERBOSITY >= 2
	if(this->beVerbose) {
		std::cout << "loIndex: " << indexOfLo << ", roIndex " << ro->getIndex() << std::endl;
	}
	#endif
	if (! std::isnan(this->fociLO[this->method][ro->getIndex()][focusIndex][indexOfLo].x())
			&& ! std::isnan(this->fociLO[this->method][ro->getIndex()][focusIndex][indexOfLo].y())) {
	#if VERBOSITY >= 2
		if(this->beVerbose) {
			std::cout << "getFocusOnLO():\t known focus: " << this->fociLO[this->method][ro->getIndex()][focusIndex][indexOfLo] << ", *lo->getCenterOfMass(): " << *lo->getCenterOfMass() << ", roIndex " << ro->getIndex() << ", model: " << enumToString(this->method) << std::endl;
		}
	#endif
		return this->fociLO[this->method][ro->getIndex()][focusIndex][indexOfLo];
	}

	if (this->method == fAVS || this->method == moveFocus || this->method == focusOnlyAtFunction || this->method == attentionalSwitch) {
		std::cout << "getFocusonLO():\t Warning: None of the functional extensions are yet designed to work with a reversed attentional shift. The following results might be weird. Please use the originalAVS method (-m 0)" << std::endl;
	}

	Point attentionalFocus;

	if (lo->getCGALPolygon()->size() == 1) {
		attentionalFocus = *(lo->getCenterOfMass());
		this->fociLO[this->method][ro->getIndex()][focusIndex][indexOfLo] = attentionalFocus;
	#if VERBOSITY >= 2
		if (this->beVerbose) {
			std::cout << "getFocusOnLO():\t single point LO, focus=LO: " << this->fociLO[this->method][ro->getIndex()][focusIndex][indexOfLo] << ", *lo->getCenterOfMass(): " << *lo->getCenterOfMass() << ", roIndex " << ro->getIndex()  << ", model: " << enumToString(this->method) << std::endl;
		}
	#endif
		return this->fociLO[this->method][ro->getIndex()][focusIndex][indexOfLo];
	}

	Point* simplifiedLandmark = ro->getCenterOfMass();

	// no vertical aligned point found, using next vertex (left or right)
	if (simplifiedLandmark->x() < lo->getLeftBottom().x()) {
		attentionalFocus = lo->getLeftBottom();
	#if VERBOSITY >= 1
		if (this->beVerbose) {
			std::cout << "getFocusonLO():\t computed focus: " << attentionalFocus << ", left edge, lo-centroid: " << *lo->getCenterOfMass() << " focusIndex: " << ", roIndex " << ro->getIndex() << focusIndex  << ", model: " << enumToString(this->method) << std::endl;
		}
	#endif
		this->fociLO[this->method][ro->getIndex()][focusIndex][indexOfLo] = attentionalFocus;
		if (this->method != moveFocus) {
			return attentionalFocus;
		}
	} else if (simplifiedLandmark->x() > lo->getRightBottom().x()) {
		attentionalFocus = lo->getRightBottom();
	#if VERBOSITY >= 1
		if (this->beVerbose) {
			std::cout << "getFocusOnLO():\t computed focus: " << attentionalFocus << ", right edge, lo->getCenterOfMass(): " << *lo->getCenterOfMass() << " focusIndex: " << focusIndex << ", roIndex " << ro->getIndex()  << ", model: " << enumToString(this->method) << std::endl;
		}
	#endif

		this->fociLO[this->method][ro->getIndex()][focusIndex][indexOfLo] = attentionalFocus;
		if (this->method != moveFocus) {
			return attentionalFocus;
		}
	}

	Segment lot;

	if (simplifiedLandmark->y() <= lo->getLowBottom()) {
		lot = Segment(*simplifiedLandmark, Point(simplifiedLandmark->x(), this->height));
	} else {
		// point above bottom of the locatedObject -> lot in direction to the bottom
		lot = Segment(*simplifiedLandmark, Point(simplifiedLandmark->x(), this->yOrigin));
	}

	#if VERBOSITY >= 1
	if (this->beVerbose) {
		std::cout << "getFocusOnLO():\t lot: " << lot << ", roIndex " << ro->getIndex() <<  std::endl;
	}
	#endif

	std::list<Polygon_with_holes> intersection;
	bool foundIntersection = false;

	for (size_t e = 0; e != lo->getBottomEdges().size(); e++) {
		CGAL::Object obj = CGAL::intersection(lot, lo->getBottomEdges()[e]);

		if (const Point* point = CGAL::object_cast<Point>(&obj)) {
			// one intersection
			if (!foundIntersection) {
				attentionalFocus = *point;
				foundIntersection = true;
			}
		} else if (const Segment* segment = CGAL::object_cast<Segment>(&obj)) {
			// intersection with an edge, choose highest point
			if (!foundIntersection) {
				attentionalFocus = (*segment).max();
				foundIntersection = true;
			}
		}
	}

	#if VERBOSITY >= 1
	if (this->beVerbose) {
		std::cout << "getFocusOnLO():\t computed focus: " << attentionalFocus << ", lo->getCenterOfMass(): " << *lo->getCenterOfMass() << " focusIndex: " << focusIndex << ", roIndex " << ro->getIndex() << ", model: " << enumToString(this->method) << std::endl;
	}
	#endif

	this->fociLO[this->method][ro->getIndex()][focusIndex][indexOfLo] = attentionalFocus;
	return attentionalFocus;
}

double Model::g(double x) {
	// x must be in radian, not degree
	assert(x >= 0.0 && x <= 2.0 * M_PI);

	return this->slope.value * x + this->intercept.value;
}

// alignment function of PC model: linear + sigmoid
double Model::f(double x){
	return g(x) * sig(90 - x, this->highgain.value);
}

double Model::heightFunction(double y, std::shared_ptr<ReferenceObject> ro, preposition* p) {
	if(*p == ABOVE){
		#if VERBOSITY >= 2
		std::cout << "heightFunction():\thightop: " << ro->getHightop() << ", lowTop: " << ro->getLowtop() << ", y: " << y << ", highgain: " << this->highgain.value << std::endl;
		#endif
		return ((sig(y - ro->getHightop(), this->highgain.value) + sig(y - ro->getLowtop(), 1.0)) / 2.0);
	} else if(*p == BELOW){
		#if VERBOSITY >= 2
		std::cout << "heightFunction():\tRO:" << *(ro->getCGALPolygon()) << ", lowBottom: " << ro->getLowBottom() << ", highBottom: " << ro->getHighBottom() << ", y: " << y << ", highgain: " << this->highgain.value << ", computed height-value: " << ((sig(ro->getLowBottom() - y, this->highgain.value) + sig(ro->getHighBottom() - y, 1.0)) / 2.0) << std::endl;
		#endif
		return ((sig(ro->getLowBottom() - y, this->highgain.value) + sig(ro->getHighBottom() - y, 1.0)) / 2.0);
	} else {
		this->error("No preposition set. This should not happen. Quitting.", "heightFunction()", this->empiricalData[0], true);
		return std::numeric_limits<double>::quiet_NaN();
	}
}

// not used yet; idea for future development
//~ double Model::heightFunctionLocatedObject(double y) {
//~ return 1 - ((sig(y - this->locatedObjects[0].getHighBottom(), this->highgain.value) + sig(y - this->locatedObjects[0].getLowBottom(), 1.0)) / 2.0);
//~ }

double Model::sig(double x, double gain) {
	return (1.0 / (1.0 + exp(gain * (-x))));
}

Vector Model::vectorSum(Point attentionalFocus, Point* vectorDestination, double sigma, bool visualizeAll, double longVectorScaling, std::shared_ptr<ReferenceObject> ro) {

	std::vector<Point> referenceObjectPoints;

	if (this->method == originalAVS) {
		referenceObjectPoints = *(ro->getPoints());
	} else if (this->method == AVS_BB){
		referenceObjectPoints = *(ro->getBBPoints());
	}

	double granularity = 1.0;

	Vector sumVec(CGAL::NULL_VECTOR);

	unsigned int i = 0;

	for (double float_index = 0; float_index < ((float) referenceObjectPoints.size()); float_index = float_index + granularity) {

		i = (unsigned int) ceil(float_index);

		double attention = exp(-sqrt(squared_distance(attentionalFocus, referenceObjectPoints[i])) / (this->lambda.value * sigma));

		if ((this->method == fAVS) && ro->isInsideFunction(&referenceObjectPoints[i])) {
			attention = attention * (1 + this->phi.value);
		}

		//~ if ((this->method == AVS_BB) && !ro->isInside(&referenceObjectPoints[i])) {
			// give a different importance to points inside the BB but outside the polygon
			// this would be an enhancement for the AVS-BB model, see footnote on page 4 in Kluth, Burigo, Schultheis, and Knoeferle (2016, KogWis)
			//~ attention = attention * this->phi.value;
		//~ }

		if (visualizeAll) {
			std::pair<Point, Vector> arrow = std::pair<Point, Vector>(referenceObjectPoints[i], attention * Vector(referenceObjectPoints[i], *vectorDestination));
			this->vectorsToVisualize.push_back(arrow);
		}

		sumVec = sumVec + (attention * Vector(referenceObjectPoints[i], *vectorDestination));

	}

	if (longVectorScaling > 0.0) {
		double vecLength = sqrt(sumVec.squared_length());
		std::pair<Point, Vector> vectorSumToVisualize = std::pair<Point, Vector>(*(ro->getCenterOfMass()), (sumVec / vecLength) * (longVectorScaling * std::abs(this->xOrigin - this->height)));
		this->vectorsToVisualize.push_back(vectorSumToVisualize);
	}

	return sumVec;
}

Vector Model::vectorSumReversed(Point attentionalFocus, Point* vectorDestination, double sigma, bool visualizeAll, double longVectorScaling, std::shared_ptr<ReferenceObject> ro) {

	double attention = 0.0;
	Vector sumVec(CGAL::NULL_VECTOR);
	std::vector<Point> referenceObjectPoints = *(ro->getPoints());

	for (size_t i = 0; i < referenceObjectPoints.size(); i++) {
		attention = exp(-sqrt(squared_distance(attentionalFocus, referenceObjectPoints[i])) / (this->lambda.value * sigma));

		if (visualizeAll) {
			std::pair<Point, Vector> arrow = std::pair<Point, Vector>(*vectorDestination, attention * Vector(*vectorDestination, referenceObjectPoints[i]));
			this->vectorsToVisualize.push_back(arrow);
		}

		sumVec = sumVec + (attention * Vector(*vectorDestination, referenceObjectPoints[i]));
	}

	if (longVectorScaling > 0.0) {
		double vecLength = sqrt(sumVec.squared_length());
		std::pair<Point, Vector> vectorSumToVisualize = std::pair<Point, Vector>(*vectorDestination, (sumVec / vecLength) * (longVectorScaling * 0.5 * std::abs(this->xOrigin - this->height)));
		this->vectorsToVisualize.push_back(vectorSumToVisualize);
	}

	return sumVec;
}

Vector Model::vectorSumLocatedObject(Point attentionalFocus, Point* vectorDestination, LocatedObject* lo, double sigma, bool visualizeAll, double longVectorScaling) {

#if VERBOSITY >= 2
	std::cout << "vectorSumLocatedObject():\tcomputing vector sum on the located object (" << *(lo->getCGALPolygon()) << "), attentional focus: " << attentionalFocus << ", vector destination: " << *vectorDestination << std::endl;
#endif

	double attention = 0.0;
	Vector sumVec(CGAL::NULL_VECTOR);
	std::vector<Point> locatedObjectPoints = *(lo->getPoints());
	for (size_t i = 0; i < locatedObjectPoints.size(); i++) {
		attention = exp(-sqrt(squared_distance(attentionalFocus, locatedObjectPoints[i])) / (this->lambda.value * sigma));

		if (visualizeAll) {
			std::pair<Point, Vector> arrow = std::pair<Point, Vector>(locatedObjectPoints[i], attention * Vector(locatedObjectPoints[i], *vectorDestination));
			this->vectorsToVisualize.push_back(arrow);
		}

		sumVec = sumVec + (attention * Vector(locatedObjectPoints[i], *vectorDestination));
		#if VERBOSITY >= 2
		std::cout << "vectorSumLocatedObject():\tLO-point #" << i << ": " << locatedObjectPoints[i] << ", attention: " << attention << ", un-weighted vector: " << Vector(locatedObjectPoints[i], *vectorDestination) << std::endl;
		#endif
	}

	if (longVectorScaling > 0) {
		double vecLength = sqrt(sumVec.squared_length());
		std::pair<Point, Vector> vectorSumToVisualize = std::pair<Point, Vector>(*(empiricalData[0].locatedObject.getCenterOfMass()), (sumVec / vecLength) * (longVectorScaling * 0.5 * std::abs(this->xOrigin - this->height)));
		this->vectorsToVisualize.push_back(vectorSumToVisualize);
	}

	return sumVec;
}

//visualization methods:

std::string Model::printInformation(bool print = true) {

	std::stringstream info;

	info << "printInformation(): \t" << "width of the 'display': " << this->xOrigin << " - " << this->width << std::endl;
	info << "printInformation(): \t" << "height of the 'display': " << this->yOrigin << " - " << this->height << std::endl;

	info << "printInformation(): \t" << "model in use: " << this->getMethod() << std::endl;
	if(this->ordinal){
		info << "printInformation(): \t" << "ordinal regression mode" << std::endl;
	}

	info << "printInformation(): \t" << "parameters in use: " << std::endl;
	info << "printInformation(): \t" << "lambda: " << std::setprecision(6) << this->lambda.value << std::endl;
	info << "printInformation(): \t" << "slope: " << this->slope.value << " (change per radian; in change per degree: " << this->slope.value * (M_PI / 180.0) << ")" <<std::endl;
	info << "printInformation(): \t" << "intercept: " << this->intercept.value << std::endl;
	info << "printInformation(): \t" << "highgain: " << this->highgain.value << std::endl;
	info << "printInformation(): \t" << "phi: " << this->phi.value << std::endl;
	info << "printInformation(): \t" << "alpha: " << this->alpha.value << std::endl;
	if (ordinal) {
		info << "printInformation(): \t" << "sigmaOrd: "  << this->sigmaOrd.value << std::endl;
		for (size_t t = 0; t < this->thresholds.size(); ++t) {
			std::stringstream tmp;
			tmp << "t" << t;
			info << "printInformation(): \t" << "t" << t << ": " << this->thresholds[t].value << std::endl;
		}
	}
	info << "printInformation(): \t" << "# LOs: " << this->empiricalData.size() << std::endl;
	info << "printInformation(): \t" << "# ROs: " << this->nrOfROs << std::endl;

	if (print) {
		std::cout << info.str();
	}

	return info.str();
}

void Model::visualize(bool visualizeVectors, double longVectorScaling, bool visualizeAttention, unsigned int LOtoVisualize) {

	// only one LO gets considered for visualization:
	if(LOtoVisualize >= this->empiricalData.size()){
		std::cout << "visualize(): \t" << "The given LO-index is too high." << std::endl;
		std::cout << "visualize(): \t" << "There are only " << this->empiricalData.size() << " LOs, but you wanted to visualize LO #" << LOtoVisualize << "." << std::endl;
		std::cout << "visualize(): \t" << "The first LO is used for visualization." << std::endl;
		LOtoVisualize = 0;
	}

	LocatedObject* lo = &(this->empiricalData[LOtoVisualize].locatedObject);

	this->printInformation();

	Point focus;

	// when visualizing focus only one functional part should be there - so always use the first one, i.e. focusIndex==0
	if (this->method == focusOnlyAtFunction || this->method == attentionalSwitch) {
		focus = getFocusAtFunction(lo, this->empiricalData[LOtoVisualize].referenceObject, 0);
	} else {
		// methods: original (AVS), moveFocus(handled in getFocus), all rAVS models
		if (this->method == rAVS_comb || this->method == rAVS_prox || this->method == rAVS_COM || this->method == rAVS_w_comb) {
			// focus has no importance for rAVS (as long as attention at the focus is > 0)
			focus = getFocusOnLocatedObject(lo, this->empiricalData[LOtoVisualize].referenceObject, 0);
		} else {
			focus = getFocus(lo, this->empiricalData[LOtoVisualize].referenceObject, &this->empiricalData[LOtoVisualize].prep, 0);
		}
	}

	// discretize focus for visualization:
	Point discreteFocus = Point((int) round(focus.x()), (int) round(focus.y()));

	this->focusToVisualize = discreteFocus;

	// point where one or many vectors are pointing to
	Point vectorDestinationToVisualize;
	if (this->method == rAVS_COM || this->method == rAVS_w_comb) {
		vectorDestinationToVisualize = *(this->empiricalData[LOtoVisualize].referenceObject->getCenterOfMass());
	} else if (this->method == rAVS_comb || this->method == rAVS_prox) {
		vectorDestinationToVisualize = this->computeProximalPoint(lo, this->empiricalData[LOtoVisualize].referenceObject);
	} else {
		// all non-rAVS models
		vectorDestinationToVisualize = *(lo->getCenterOfMass());
	}

	if(this->method == rAVS_w_comb){

		// compute new vector destination according to the rAVS-w-comb model
		Point proximalPoint = this->computeProximalPoint(lo, this->empiricalData[LOtoVisualize].referenceObject);
		double distanceX = std::abs(focus.x() - proximalPoint.x());
		double distanceY = std::abs(focus.y() - proximalPoint.y());

		Vector centerToProx(*this->empiricalData[LOtoVisualize].referenceObject->getCenterOfMass(), getFocus(lo, this->empiricalData[LOtoVisualize].referenceObject, &this->empiricalData[LOtoVisualize].prep, 0));

		double relativeDistance = (distanceX / (this->empiricalData[LOtoVisualize].referenceObject->getWidth())) + (distanceY / (this->empiricalData[LOtoVisualize].referenceObject->getHeight()));

		double weight = (- this->alpha.value * relativeDistance) + 1.0;
		if (weight < 0.0) {
			weight = 0.0;
		}

		Vector loToWeightedDest = weight * centerToProx;
		vectorDestinationToVisualize = Point(this->empiricalData[LOtoVisualize].referenceObject->getCenterOfMass()->x() + loToWeightedDest.x(), this->empiricalData[LOtoVisualize].referenceObject->getCenterOfMass()->y() + loToWeightedDest.y());

		#if VERBOSITY >= 2
			std::cout << "visualize():\tmodel rAVS-w-comb in use:" << std::endl;
			std::cout << "visualize():\trAVS-w-comb:\trel. distance: " << relativeDistance << std::endl;
			std::cout << "visualize():\trAVS-w-comb:\tweight of prox. point: " << weight << std::endl;
			std::cout << "visualize():\trAVS-w-comb:\tcenter-of-mass: " << *(this->empiricalData[LOtoVisualize].referenceObject->getCenterOfMass()) << std::endl;
			std::cout << "visualize():\trAVS-w-comb:\tproximal point: " << proximalPoint << std::endl;
			std::cout << "visualize():\trAVS-w-comb:\tnew vector destination: " << vectorDestinationToVisualize << std::endl;
		#endif
	}

	// needed for visualization of attentional width
	double sigma = sqrt(squared_distance(focus, vectorDestinationToVisualize));

	std::cout << "visualize():\t" << std::setprecision(0) << "focus: " << focus << std::endl;

	if (this->ordinal) {
		std::vector<double> probs = this->rateProbs(&(this->empiricalData[LOtoVisualize]), 0, visualizeVectors, longVectorScaling);
		assert(probs.size() == (this->thresholds.size() + 1));
		for (size_t r = 0; r < probs.size(); ++r) {
			std::cout << "visualize():\trate with probabilities for " << prepToString(this->empiricalData[LOtoVisualize].prep) << " value at " << std::setprecision(0) << *(lo->getCenterOfMass()) << ", prob. of rating " << r+1 << std::setprecision(4) << ": " << probs[r] << std::endl;
			if(r < this->thresholds.size()){ // one less threshold than rating
				std::cout << "visualize():\t" << "threshold " << r << ": " << this->thresholds[r].value << std::endl;
			}
		}
	} else {
		std::cout << "visualize():\t" << prepToString(this->empiricalData[LOtoVisualize].prep) << " value at " << *(lo->getCenterOfMass()) << ": " << std::setprecision(4) << this->rate(&(this->empiricalData[LOtoVisualize]), 0, visualizeVectors, longVectorScaling) << std::setprecision(0) << std::endl;
	}

	// suppress message output for the spatial template computation
	this->beVerbose = false;

	// if empirical data given: compute ratings for all given LOs
	if (this->empiricalData.size() > 0) {
		computeResults(&this->empiricalData, true);
	}

	if (visualizeAttention) {
		if (this->method == rAVS_comb || this->method == rAVS_COM || this->method == rAVS_prox || this->method == rAVS_w_comb) {
			std::cout << "visualize():\t Info: attentional distribution does not matter for all rAVS variations" << std::endl;
		}
		for (int y = this->yOrigin; y < this->height; y++) {
			for (int x = this->xOrigin; x < this->width; x++) {
				Point p = Point(x, y);
				double value = 0.0;

				value = exp(-sqrt(squared_distance(focus, p)) / (this->lambda.value * sigma));

				if (this->method == fAVS && this->empiricalData[LOtoVisualize].referenceObject->isInsideFunction(&p)) {
					value = exp(-sqrt(squared_distance(focus, p)) / (this->lambda.value * sigma)) * (1 + this->phi.value);
				}

				assert((x - this->xOrigin) >= 0);
				assert((y - this->yOrigin) >= 0);
				this->display[(unsigned int) (x - this->xOrigin)][(unsigned int) (y - this->yOrigin)] = value;
			}
		}
	} else if (this->ordinal) {

		std::vector<std::vector<double> > aggregatedProbabilities;

		for (size_t idx = 0; idx < this->empiricalData.size(); ++idx) {
			aggregatedProbabilities.push_back(rateProbs(&(this->empiricalData[idx]), 0, false, 0.0));
		}

		std::vector<double> empProps(this->maxRating - this->minRating + 1);
		std::vector<double> modelProbabilities(this->maxRating - this->minRating + 1);

		for (size_t p = 0; p < this->empiricalData.size(); ++p) {
			for (size_t r = 0; r < this->empiricalData[p].absoluteNoOfRatings.size(); ++r) {
				empProps[r] = empProps[r] + this->empiricalData[p].absoluteNoOfRatings[r];
				modelProbabilities[r] = modelProbabilities[r] + aggregatedProbabilities[p][r];
			}
		}

		for (size_t i = 0; i < modelProbabilities.size(); ++i) {
			// normalize by dividing with no of data points / LOs and no of participants (the latter only for empirical data)
			empProps[i] = empProps[i] / ((double) this->empiricalData.size() * this->empiricalData[0].noOfParticipants);
			modelProbabilities[i] = modelProbabilities[i] / ((double) this->empiricalData.size());
		}

		this->ratingDist = std::pair<std::vector<double>, std::vector<double> >(empProps, modelProbabilities);

		// compute Kullback-Leibler divergence for whole dataset
		double kullbackLeibler = 0.0;
		for (size_t p = 0; p < this->empiricalData.size(); ++p) {
			for (size_t r = 0; r < this->empiricalData[p].absoluteNoOfRatings.size(); ++r) {
				if (this->empiricalData[p].proportions[r] > 0.0) { // else: kullbackLeibler += 0 (because first term is 0 and 0 multiplied with anything is 0)
					kullbackLeibler += this->empiricalData[p].proportions[r] * log(this->empiricalData[p].proportions[r] / aggregatedProbabilities[p][r]);
				}
			}
		}
		// normalize
		kullbackLeibler = kullbackLeibler / ((double) this->empiricalData.size());
		std::cout << "visualize():\tKullback-Leibler divergence for whole dataset: " << kullbackLeibler << std::endl;

		this->likelihoodOnly = true;
		this->posterior = true;
		for (size_t l = 0; l < 5; ++l) {
			double likelihood = computeResults(&this->empiricalData, true);
			std::cout << "visualize():\tLikelihood for current parameter setting: " << std::scientific << std::setprecision(3) << likelihood << std::endl;
		}

	} else {
		// compute spatial template

		// re-initialize caching arrays due to new locatedObjects
		// cannot use initializeArrays(), because this method only operates on the empiricalData (and not the fake LOs we are creating)

		std::cout << "visualize():\tcomputing spatial template, please be patient" << std::endl;

		// TODO FIXME computing spatial template takes way too long (and too much memory). improve!
		// this seems to be the faulty loop
		unsigned int noOfVirtualLOs = (unsigned int) ((unsigned int) (std::abs(this->yOrigin - this->height) * std::abs(this->xOrigin - this->width)) + this->empiricalData.size());
		for (unsigned int mi = 0; mi < this->models.size(); ++mi) {
			for (unsigned int fi = 0; fi < this->amountOfFoci; ++fi) {
				for (unsigned int i = 0; i < noOfVirtualLOs; ++i) {
					this->proximalPoints[i] = Point(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());
					this->fociLO[this->models[mi]][0][fi][i] = Point(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());
					for (int x = this->xOrigin; x < this->width; ++x) {
						assert((x - this->xOrigin) >= 0);
						unsigned int xcoord = (unsigned int) (x - this->xOrigin);
						this->fociAbove[this->models[mi]][0][fi][xcoord] = Point(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());
						this->fociBelow[this->models[mi]][0][fi][xcoord] = Point(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());
						this->fociFunction[this->models[mi]][0][fi][xcoord] = Point(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());
					}
				}
			}
		}

		unsigned int counter = this->empiricalData[empiricalData.size()-1].locatedObject.getIndex();

		double nrOfFakeLOs = (std::abs(this->yOrigin-this->height) * std::abs(this->xOrigin-this->width)) - ((double) this->empiricalData[LOtoVisualize].referenceObject->getPoints()->size());

		for (int y = this->yOrigin; y < this->height; ++y) {
			for (int x = this->xOrigin; x < this->width; ++x) {
				Point point[] = { Point(x, y) };
				// sigma = distance (focus - vectorDest); must not be 0; excluding those points for the spatial template
				// also, do not compute values for points inside the RO
				if(!(almostEqual(discreteFocus.x(), point->x()) && almostEqual(discreteFocus.y(), point->y())) && !(this->empiricalData[LOtoVisualize].referenceObject->isInside(point))){
					counter++;
					std::cout << "visualize():\t computing spatial template: " << ((int) (((double) counter) / ((double) (nrOfFakeLOs)) * 100.0)) << " % done" << '\r' << std::flush;
					// visualize vector(sum) only for the first LO -> happened already above
					RatingDataPoint fake;
					fake.locatedObject = LocatedObject(Polygon(point, point + 1), false, 1, counter);
					fake.prep = this->empiricalData[0].prep;
					fake.referenceObject = this->empiricalData[0].referenceObject;
					fake.meanRating = 0.0;
					fake.functionalPart = "";
					this->display[(size_t) (x - this->xOrigin)][(size_t) (y - this->yOrigin)] = rate(&(fake), 0, false, 0.0);
				}
			}
		}
	}

	std::cout << std::endl;

	this->beVerbose = true;

	// the following: visualization for the attentionalSwitch method (for ROs with functional parts)

	if (this->method == attentionalSwitch) {

		Vector direction = vectorSum(focus, &vectorDestinationToVisualize, sigma, false, false, 0);
		assert(!(almostEqual(direction.squared_length(), 0.0)));

		double angle = acos((direction.y() * this->canonicalOrientation.dy()) / sqrt(direction.squared_length()));

		Point focusGeometry = getFocus(lo, this->empiricalData[LOtoVisualize].referenceObject, &this->empiricalData[LOtoVisualize].prep, 0);
		assert(((int) focusGeometry.x()) >= this->xOrigin);
		assert(((int) focusGeometry.y()) >= this->yOrigin);
		this->display[(size_t) (((int) focusGeometry.x()) - this->xOrigin)][(size_t) (((int) focusGeometry.y()) - this->yOrigin)] = 2.0;
		sigma = sqrt(squared_distance(focusGeometry, vectorDestinationToVisualize));
		Vector directionGeometry = vectorSum(focusGeometry, &vectorDestinationToVisualize, sigma, visualizeVectors, longVectorScaling, 0);
		assert(!(almostEqual(directionGeometry.squared_length(), 0.0)));
		// upright.x == 0 -> don't do this unneccesary computation: direction.dx() * this->upright.dx()
		double angleGeometry = acos((directionGeometry.y() * this->canonicalOrientation.dy()) / sqrt(directionGeometry.squared_length()));

		// cross-prdouct to find out, if direction is to the left or to the right
		double orientationGeometry = this->canonicalOrientation.dy() * directionGeometry.x();
		double orientationFunction = this->canonicalOrientation.dy() * direction.x();

		if (orientationGeometry > 0) {
			// direction to the left, flip sign
			angleGeometry = -angleGeometry;
		}

		if (orientationFunction > 0) {
			// direction to the left, flip sign
			angle = -angle;
		}
		#if VERBOSITY >= 2
		std::cout << "visualize(): \t" << "geometryangle: " << angleGeometry << " functionangle: " << angle << std::endl;
		#endif

		// take the weighted mean between both directions
		double funcWeight = this->phi.value / 2.0;
		angle = (funcWeight * angle + angleGeometry) / (funcWeight + 1.0);

		#if VERBOSITY >= 2
		std::cout << "visualize(): \t" << "resulting angle: " << angle << std::endl;
		#endif

		double normalizeFactor = ((double) this->empiricalData[LOtoVisualize].referenceObject->getPoints()->size()) / 2.0;

		Point middleFunctionTop = Point((this->empiricalData[LOtoVisualize].referenceObject->getLeftTopFunction().x() + this->empiricalData[LOtoVisualize].referenceObject->getRightTopFunction().x()) / 2.0, this->empiricalData[LOtoVisualize].referenceObject->getLowtopFunction());
		std::pair<Point, Vector> arrow = std::pair<Point, Vector>(middleFunctionTop, direction / normalizeFactor);
		this->vectorsToVisualize.push_back(arrow);

		Point middleTop = Point((this->empiricalData[LOtoVisualize].referenceObject->getLeftTop().x() + this->empiricalData[LOtoVisualize].referenceObject->getRightTop().x()) / 2.0, this->empiricalData[LOtoVisualize].referenceObject->getLowtop());
		std::pair<Point, Vector> arrowGeometry = std::pair<Point, Vector>(middleTop, directionGeometry / (normalizeFactor));
		this->vectorsToVisualize.push_back(arrowGeometry);

		Point focusInMiddle = Point((focus.x() + focusGeometry.x()) / 2.0, focusGeometry.y());
		double r = sqrt(direction.squared_length() + directionGeometry.squared_length());
		Vector overallDirection = Vector(r * cos(M_PI / 2 + angle), r * sin(M_PI / 2 + angle));
		std::pair<Point, Vector> arrowCombined = std::pair<Point, Vector>(focusInMiddle, overallDirection / (2.0 * normalizeFactor));
		this->vectorsToVisualize.push_back(arrowCombined);
	}
}

void Model::computeOnlyRatings(){
	if (this->empiricalData.size() > 0) {
		computeResults(&this->empiricalData, false);
	}
}

// model initialization and getter / setter:

void Model::setMethod(AVSmethod m, double vLambda, double vSlope, double vIntercept, double vHighgain, double vPhi, double vAlpha, double vLrgain, double vLrexp, double vSigmaOrd, std::vector<double> vThresholds) {
	if (this->empiricalData.size() > 0 ) {
		enumToString(m); // check if method is a proper model and fail if not (enumToString() throws a runtime error then)
	}

	this->method = m;

	this->lambda.name = "lambda";
	this->lambda.value = vLambda;
	this->lambda.RCfit = 0.512;
	this->lambda.min = 0.001;
	this->lambda.max = 5.0;

	this->intercept.name = "intercept";
	this->intercept.value = vIntercept;
	this->intercept.RCfit = 1.224;
	this->intercept.min = 0.7;
	this->intercept.max = 1.3;

	this->slope.name = "slope";
	this->slope.value = vSlope;
	this->slope.RCfit = -0.007 * (180.0 / M_PI); // convert fit in slope _per degree_ (1 / degree -> -0.007) to fit in slope _per radian_ (1 / (degree * (pi / 180)) = slope * (180 / pi) )
	this->slope.min = -1.0 / (45.0 * (M_PI / 180.0)); // same here, allow minimum slope of 45 degrees;
	this->slope.max = 0.0;

	this->highgain.name = "highgain";
	this->highgain.value = vHighgain;
	this->highgain.RCfit = 0.002;
	this->highgain.min = 0.0;
	this->highgain.max = 10.0;

	this->alpha.name = "alpha";
	this->alpha.value = vAlpha;
	this->alpha.RCfit = 0.322; // parameter for the GOF reported in the COSLI paper
	this->alpha.min = 0.001;
	this->alpha.max = 5.0;

	this->phi.name = "phi";
	this->phi.value = vPhi;
	this->phi.RCfit = 0.6804; // my master thesis, p. 30
	this->phi.min = 0.0;
	this->phi.max = 2.0;

	this->lrgain.name = "lrgain";
	this->lrgain.value = vLrgain;
	this->lrgain.RCfit = 0.065; // BB model
	this->lrgain.min = 0.0;
	this->lrgain.max = 5.0;

	this->lrexp.name = "lrexp";
	this->lrexp.value = vLrexp;
	this->lrexp.RCfit = 0.22; // BB model
	this->lrexp.min = 0.0;
	this->lrexp.max = 5.0;

	// RCfits are all set above with AVS as default model
	// this changes some of them depending on the model used
	switch (method) {

		case BB:
			this->highgain.RCfit = 0.373;
			break;

		case PC:
			this->alpha.RCfit = 0.174;
			this->intercept.RCfit = 0.929;
			this->slope.RCfit = -0.006 * (180.0 / M_PI);
			this->highgain.RCfit = 3.265;
			break;

		case PC_BB:
			this->alpha.RCfit = 0.115;
			this->intercept.RCfit = 0.909;
			this->slope.RCfit = -0.005 * (180.0 / M_PI);
			this->highgain.RCfit = 6.114;
			break;

		case originalAVS:
			// everything set above
			break;

		case fAVS:
			// everything set above
			break;

		case moveFocus:
			// everything set above
			break;

		case focusOnlyAtFunction:
			// everything set above
			break;

		case attentionalSwitch:
			// everything set above
			break;

		case rAVS_comb:
			this->slope.RCfit = -0.40107;
			break;

		case rAVS_prox:
			this->intercept.RCfit = 1.16886;
			this->slope.RCfit = -0.337504;
			this->highgain.RCfit = 0.0042564;
			break;

		case rAVS_COM:
			this->intercept.RCfit = 0.9735;
			this->slope.RCfit = -0.199308;
			this->highgain.RCfit = 2.58177;
			break;

		case rAVS_w_comb:
			this->alpha.RCfit = 0.284187;
			this->intercept.RCfit = 0.97288;
			this->slope.RCfit = -0.353709;
			this->highgain.RCfit = 2.39325;
			break;

		case rAVS_multiple:
			// everything set above
			break;

		case rAVS_abs_middle:
			// not fully developed yet
			this->alpha.RCfit = 5.0;
			this->intercept.RCfit = 0.980312;
			this->slope.RCfit = -0.216995;
			this->highgain.RCfit = 8.59439;
			break;

		case rAVS_CoO:
			this->alpha.RCfit = 0.28417;
			this->intercept.RCfit = 0.972863;
			this->slope.RCfit = -0.353695;
			this->highgain.RCfit = 5.32818;
			break;

		case AVS_BB:
			// everything set above
			break;

		case AVSr:
			// not yet fully developed
			break;

		case CROSS:
			break;

		case INVALID_MODEL:
			break;

		default:
			break;
	}

	if (this->ordinal) {
		this->sigmaOrd.name = "sigmaOrd";
		this->sigmaOrd.value = vSigmaOrd;
		this->sigmaOrd.RCfit = 2.72304; // this comes from fitting data from Kluth et al. (2018) with rAVS-CoO in ordinal mode
		this->sigmaOrd.min = 1.0; // should not be negative ...
		this->sigmaOrd.max = 5.0;
		assert(this->maxRating > 0);
		unsigned int noOfThresholds = this->maxRating - this->minRating;

		this->ratingsProbs = std::vector<double>(noOfThresholds + 1);
		this->thresholds = std::vector<ModelParameter>(noOfThresholds);

		// the first and the last threshold are fixed to
		// (kind of arbitrary but sensemaking in the domain) values,
		// see Kruschke (2015, 675) or Liddell & Kruschke (2015, p. 22)
		this->thresholds[0].name = "t0";
		std::stringstream lastThresholdName;
		lastThresholdName << "t" << noOfThresholds;
		this->thresholds[noOfThresholds - 1].name = lastThresholdName.str();
		this->thresholds[0].value = this->minRating + 0.5;
		this->thresholds[0].RCfit = this->thresholds[0].value;
		this->thresholds[noOfThresholds - 1].value = (this->maxRating - this->minRating + 1) - 0.5;
		this->thresholds[noOfThresholds - 1].RCfit = this->thresholds[noOfThresholds - 1].value;

		// don't allow changing the fixed thresholds
		this->thresholds[0].min = this->thresholds[0].value;
		this->thresholds[0].max = this->thresholds[0].value;
		this->thresholds[noOfThresholds - 1].min = this->thresholds[noOfThresholds - 1].value;
		this->thresholds[noOfThresholds - 1].max = this->thresholds[noOfThresholds - 1].value;

		// the intermediate thresholds
		assert(vThresholds.size() == this->thresholds.size() - 2);
		for (unsigned int t = 1; t < noOfThresholds - 1; ++t) {
			std::stringstream tmpname;
			tmpname << "t" << t;
			this->thresholds[t].name = tmpname.str();
			this->thresholds[t].value = vThresholds[t - 1];
			this->thresholds[t].RCfit = this->thresholds[t].value;
			// no thresholds can be lower or higher than the fixed thresholds
			this->thresholds[t].min = this->thresholds[0].value;
			this->thresholds[t].max = this->thresholds[noOfThresholds - 1].value;
			this->ratingsProbs[t] = std::numeric_limits<double>::quiet_NaN();
		}
		// initialize the missing rating probabilities to NaN
		this->ratingsProbs[0] = std::numeric_limits<double>::quiet_NaN();
		this->ratingsProbs[noOfThresholds - 1] = std::numeric_limits<double>::quiet_NaN();
		this->ratingsProbs[noOfThresholds] = std::numeric_limits<double>::quiet_NaN();

		// initialize the not-changing part of the matrix for the cross-matchtest
		const unsigned int noOfRows = this->empiricalData[0].noOfParticipants + this->empiricalData[0].noOfParticipants;
		const unsigned int noOfPartRows = this->empiricalData[0].noOfParticipants; // noOfRows / 2;
		const unsigned int noOfCols = (unsigned int) this->empiricalData.size();

		Rcpp::NumericVector groups = Rcpp::NumericVector(noOfRows);
		// attention: the indices for this matrix are flipped, columns come first
		this->dataMatrixForCrossmatch = std::vector<std::vector<double> >(noOfCols, std::vector<double>(noOfRows));

		for (size_t rowIdx = 0; rowIdx < noOfPartRows; ++rowIdx) {
			for (size_t columnIdx = 0; columnIdx < noOfCols; ++columnIdx) {
				// attention: rows and columns are flipped, columns come first
				this->dataMatrixForCrossmatch[columnIdx][rowIdx] = this->empiricalData[columnIdx].individualRatings[rowIdx];
			}
			groups(rowIdx) = 0; // group "0" -> empirical data
		}

		for (size_t rowIdx = noOfPartRows; rowIdx < noOfRows; ++rowIdx) {
			groups(rowIdx) = 1; // group "1" -> model samples
		}

		RInside::instance()["groups"] = groups;

		std::stringstream cmdist;
		cmdist << "cmdist = crossmatchdist(" << noOfRows << ", " << (unsigned int) this->empiricalData[0].noOfParticipants << ")";
		RInside::instance().parseEval(cmdist.str());

		Rcpp::NumericVector possibleCrossmatches = RInside::instance().parseEval("cmdist[2,]");
		Rcpp::NumericVector crossmatchProbs = RInside::instance().parseEval("cmdist[5,]");
		assert(possibleCrossmatches.size() == crossmatchProbs.size());

		for (R_xlen_t c = 0; c < possibleCrossmatches.size(); ++c) {
			this->crossmatchTable[(int) possibleCrossmatches[c]] = crossmatchProbs[c];
		}

	} else {
		// de-validate parameters for ordinal regression
		this->sigmaOrd.value = std::numeric_limits<double>::quiet_NaN();
		this->ratingsProbs = std::vector<double>(1);
		this->thresholds = std::vector<ModelParameter>(1);
		this->ratingsProbs[0] = std::numeric_limits<double>::quiet_NaN();
		this->thresholds[0].value = std::numeric_limits<double>::quiet_NaN();
	}

	this->paramList.clear();

	this->paramList.push_back(&(this->lambda));
	this->paramList.push_back(&(this->intercept));
	this->paramList.push_back(&(this->slope));
	this->paramList.push_back(&(this->highgain));

	if (this->method == fAVS || this->method == moveFocus || this->method == focusOnlyAtFunction || this->method == attentionalSwitch) {
		this->paramList.push_back(&(this->phi));
	}

	if (this->method == rAVS_comb || this->method == rAVS_prox || this->method == rAVS_COM || this->method == rAVS_w_comb ||
		this->method == rAVS_multiple || this->method == rAVS_abs_middle || this->method == rAVS_CoO) {
		this->paramList.push_back(&(this->alpha));
	}

	if (this->method == BB || this->method == PC || this->method == PC_BB) {
		this->paramList.push_back(&(this->lrgain));
		this->paramList.push_back(&(this->lrexp));
	}

	if (this->ordinal && ! noChangeOrdinal) {
		this->paramList.push_back(&(this->sigmaOrd));
		// first and last thresholds are fixed, don't push to param list
		for (size_t t = 1; t < this->thresholds.size() - 1; ++t) {
			this->paramList.push_back(&(this->thresholds[t]));
		}
	}

	this->visitedParams.clear();
	// clean up memory for good measure (this swaps an empty vector, the first part, with the full vector, the argument)
	std::vector<std::map<std::string, float> >().swap(this->visitedParams);

	areParamsInBoundaries(true);
}

bool Model::areParamsInBoundaries(bool throwException) {

	for (size_t paramIdx = 0; paramIdx < this->paramList.size(); ++paramIdx) {
		ModelParameter* param = this->paramList[paramIdx];

		if (param->value < param->min || param->value > param->max) {
			if (!throwException) {
				return false;
			} else {
				std::cout << param->name << " out of bounds (" << param->value << ", should be between " << param->min << " and " << param->max << ")" << std::endl;
				this->error("param out of bounds ", "areParamsInBoundaries", this->empiricalData[0], true);
			}
		} else if (this->ordinal && param->name.substr(0, 1) == "t") {
			// don't allow overlapping thresholds (in addition to their global min / max which is the first / last threshold)
			bool negProb = false;
			size_t t = (size_t) std::stoi(param->name.substr(1, 1)); // get number of threshold
			if (t != 0) {
				negProb = this->thresholds[t].value < this->thresholds[t - 1].value;
			} else {
				negProb = this->thresholds[t].value > this->thresholds[t + 1].value;
			}
			if (negProb) {
				if (!throwException) {
					return false;
				} else {
					std::cout << param->name << " out of bounds (" << param->value << ")" << std::endl;
					this->error("param out of bounds ", "areParamsInBoundaries", this->empiricalData[0], true);
				}
			}
		}
	}

	return true;
}

/**
 * LO(s) and RO must be set first!
 */
void Model::setLimits(int widthLimitLow, int widthLimitHigh, int heightLimitLow, int heightLimitHigh, bool printRs) {

	if (this->empiricalData.size() == 0) {
		return;
	}

	this->printRatings = printRs;

	double referenceObjectWidth = std::abs(this->empiricalData[0].referenceObject->getLeftTop().x() - this->empiricalData[0].referenceObject->getRightTop().x());
	double referenceObjectHeight = std::abs(this->empiricalData[0].referenceObject->getHightop() - this->empiricalData[0].referenceObject->getCGALPolygon()->bottom_vertex()->y());

	double locatedObjectWidth = std::abs(this->empiricalData[0].locatedObject.getLeftBottom().x() - this->empiricalData[0].locatedObject.getRightBottom().x());
	double locatedObjectHeight = std::abs(this->empiricalData[0].locatedObject.getHighBottom() - this->empiricalData[0].locatedObject.getCGALPolygon()->top_vertex()->y());

	if (widthLimitHigh == std::numeric_limits<int>::max()) {
		this->width = (int) (this->empiricalData[0].referenceObject->getRightTop().x() + 0.5 * referenceObjectWidth);
		if (this->width < this->empiricalData[0].locatedObject.getRightBottom().x() + 2 * locatedObjectWidth) {
			this->width = (int) (this->empiricalData[0].locatedObject.getRightBottom().x() + 2 * locatedObjectWidth);
		}
	} else {
		this->width = widthLimitHigh;
	}

	if (heightLimitHigh == std::numeric_limits<int>::max()) {
		this->height = (int) (this->empiricalData[0].referenceObject->getHightop() + 0.5 * referenceObjectHeight);
		if (this->height < this->empiricalData[0].locatedObject.getCGALPolygon()->top_vertex()->y() + 2 * locatedObjectHeight) {
			this->height = (int) (this->empiricalData[0].locatedObject.getCGALPolygon()->top_vertex()->y() + 2 * locatedObjectHeight);
		}
	} else {
		this->height = heightLimitHigh;
	}

	if (widthLimitLow == std::numeric_limits<int>::min()) {
		this->xOrigin = (int) (this->empiricalData[0].referenceObject->getLeftTop().x() - 0.5 * referenceObjectWidth);
		if (this->xOrigin > this->empiricalData[0].locatedObject.getLeftBottom().x() - 2 * locatedObjectWidth) {
			this->xOrigin = (int) (this->empiricalData[0].locatedObject.getLeftBottom().x() - 2 * locatedObjectWidth);
		}
	} else {
		this->xOrigin = widthLimitLow;
	}

	if (heightLimitLow == std::numeric_limits<int>::min()) {
		this->yOrigin = (int) (this->empiricalData[0].referenceObject->getCGALPolygon()->bottom_vertex()->y() - 0.5 * referenceObjectHeight);
		if (this->yOrigin > this->empiricalData[0].locatedObject.getCGALPolygon()->bottom_vertex()->y() - 2 * locatedObjectHeight) {
			this->yOrigin = (int) (this->empiricalData[0].locatedObject.getCGALPolygon()->bottom_vertex()->y() - 2 * locatedObjectHeight);
		}
	} else {
		this->yOrigin = heightLimitLow;
	}

	// check the limits; especially needed for more than one LO
	#if VERBOSITY >= 1
	std::cout << "setLimits():\t display-size: x: " << this->xOrigin << " -- " << this->width << ", y: " << this->yOrigin << " -- " << this->height << std::endl;
	#endif
	this->checkLimits(this->empiricalData, 10);

	initializeArrays();
}

void Model::checkLimits(std::vector<RatingDataPoint> LOs, double margin) {
	#if VERBOSITY >=1
	bool changedLimits = false;
	#endif

	// use one safety-pixel to avoid rounding errors from scaling resulting in segfaults ...

	if (margin < 1.0) {
		margin = 1.0;
	}

	for (size_t t = 0; t < LOs.size(); t++) {
		if (LOs[t].locatedObject.getCGALPolygon()->left_vertex()->x() < (this->xOrigin + margin)) {
			this->xOrigin = (int) (std::floor(LOs[t].locatedObject.getCGALPolygon()->left_vertex()->x()) - margin);
	#if VERBOSITY >= 1
			changedLimits = true;
	#endif
		}
		if (LOs[t].locatedObject.getCGALPolygon()->right_vertex()->x() > (this->width - margin)) {
			this->width = (int) (ceil(LOs[t].locatedObject.getCGALPolygon()->right_vertex()->x()) + margin);
	#if VERBOSITY >= 1
			changedLimits = true;
	#endif
		}
		if (LOs[t].locatedObject.getCGALPolygon()->bottom_vertex()->y() < (this->yOrigin + margin)) {
			this->yOrigin = (int) (std::floor(LOs[t].locatedObject.getCGALPolygon()->bottom_vertex()->y()) - margin);
	#if VERBOSITY >= 1
			changedLimits = true;
	#endif
		}
		if (LOs[t].locatedObject.getCGALPolygon()->top_vertex()->y() > (this->height - margin)) {
			this->height = (int) (ceil(LOs[t].locatedObject.getCGALPolygon()->top_vertex()->y()) + margin);
	#if VERBOSITY >= 1
			changedLimits = true;
	#endif
		}
	}

	#if VERBOSITY >= 1
	if (changedLimits) {
		std::cout << "checkLimits():\tDisplay limits were changed, since some LOs don't fit in the display." << std::endl;
		std::cout << "checkLimits():\tnew display: x: " << this->xOrigin << " -- " << this->width << ", y: " << this->yOrigin << " -- " << this->height << std::endl;
	}
	#endif
}

void Model::initializeArrays() {

	this->display.clear();

	#if VERBOSITY >= 1
	std::cout << "reserving display array. width: " << this->width << ", xOrigin: " << this->xOrigin << ", i.e. size: " << (this->width - this->xOrigin) << std::endl;
	#endif

	assert(this->width >= this->xOrigin);
	assert(this->height >= this->yOrigin);
	this->display.reserve( (size_t) (this->width - this->xOrigin));

	#if VERBOSITY >= 1
	std::cout << "display reserved" << std::endl;
	#endif

	// one type of focus for each functional part + hard-coded amount of foci
	// different foci may occur due to focus on LO at center-of-mass or edge or ...
	this->amountOfFoci = (unsigned int) this->functionalFileNames.size() + 1;
	for (unsigned int idx = 0; idx < this->functionalFileNames.size(); ++idx) {
		this->funcFileToFocusIdx[functionalFileNames[idx]] = idx;
	}

	for (unsigned int mi = 0; mi < this->models.size(); ++mi){
		for (unsigned int r = 0; r < this->nrOfROs; ++r){
			for (unsigned int fi = 0; fi < this->amountOfFoci; ++fi) {
				for (unsigned int idx = 0; idx < this->empiricalData.size(); ++idx) {
					this->fociLO[this->models[mi]][r][fi][idx] = Point(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());
					this->proximalPoints[idx] = Point(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());
				}
				for (int x = this->xOrigin; x < this->width; ++x) {
					this->display.push_back(std::vector<double>( (size_t) (this->height - this->yOrigin), 0.0));
					// NaN means: focus needs to be computed
					assert((x- this->xOrigin) >= 0);
					this->fociAbove[this->models[mi]][r][fi][(unsigned int) (x - this->xOrigin)] = Point(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());
					this->fociBelow[this->models[mi]][r][fi][(unsigned int) (x - this->xOrigin)] = Point(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());
					this->fociFunction[this->models[mi]][r][fi][(unsigned int) (x - this->xOrigin)] = Point(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());
				}
			}
		}
	}
}

void Model::setEmpiricalData(std::vector<RatingDataPoint> empirical, unsigned int noROs, std::pair<unsigned int, unsigned int> ratingScale) {
	this->empiricalData = empirical;
	this->nrOfROs = noROs;
	this->minRating = ratingScale.first;
	this->maxRating = ratingScale.second;
}

std::string Model::getResultsPE() {
	return this->resultsPE;
}

ModelParameter Model::getParam(std::string p){
	if(p == "lambda"){
		return this->lambda;
	} else if (p == "slope"){
		return this->slope;
	} else if (p == "intercept"){
		return this->intercept;
	} else if (p == "highgain"){
		return this->highgain;
	} else if (p == "alpha"){
		return this->alpha;
	} else if (p == "phi"){
		return this->phi;
	} else if (p == "lrexp"){
		return this->lrexp;
	} else if (p == "lrgain"){
		return this->lrgain;
	} else if (p == "sigmaOrd"){
		return this->sigmaOrd;
	} else if ((p.substr(0, 1) == "t") && (stoi(p.substr(1)) >= 0) && (stoi(p.substr(1)) < (int) this->thresholds.size())){
		unsigned int idx = (unsigned int) std::stoi(p.substr(1));
		return this->thresholds[idx];
	} else {
		std::cout << "getParam():\tparameter " << p << " does not exist!" << std::endl;
		this->error("Non existing model parameter.", "getParam()", this->empiricalData[0], true);
		return this->lambda; // is not reached because this->error() throws an exception first.
		//~ return std::numeric_limits<double>::quiet_NaN();
	}
}

std::vector<double> Model::getOrdinalParams() {

	double meanMeanRating = 0.0; // mean over all mean ratings

	for (size_t idx = 0; idx < this->empiricalData.size(); ++idx) {
		meanMeanRating = meanMeanRating + rate(&(this->empiricalData[idx]), 0, false, 0.0);
	}

	meanMeanRating = meanMeanRating / (double) this->empiricalData.size();

	std::cout << "mean of mean ratings: " << std::setprecision(6) << meanMeanRating << std::endl;

	std::vector<double> ordinalParams;
	ordinalParams.push_back(meanMeanRating);
	ordinalParams.push_back(this->sigmaOrd.value);

	for (unsigned int t = 0; t < this->thresholds.size(); ++t) {
		ordinalParams.push_back(this->thresholds[t].value);
	}

	return ordinalParams;
}

unsigned int Model::getMinRating() {
	return this->minRating;
}

unsigned int Model::getMaxRating() {
	return this->maxRating;
}

std::vector<std::pair<Point, Vector> > Model::getVectorsToVisualize() {
	return this->vectorsToVisualize;
}

Point Model::getFocusToVisualize() {
	return this->focusToVisualize;
}

std::string Model::getMethod() {
	return enumToString(this->method);
}

bool Model::getOrdinal() {
	return this->ordinal;
}

bool Model::getPosterior() {
	return this->posterior;
}

bool Model::getPriorOnly() {
	return this->priorOnly;
}

bool Model::getLikelihoodOnly() {
	return this->likelihoodOnly;
}

bool Model::getNoChangeOrdinal() {
	return this->noChangeOrdinal;
}

std::string Model::getInformativePriors() {
	return this->informativePriors;
}

std::vector<std::vector<double> > Model::getDisplay() {
	return this->display;
}

std::pair<std::vector<double>, std::vector<double> > Model::getRatingDist() {
	return this->ratingDist;
}

std::vector<std::map<std::string, float> > Model::getVisitedParams() {
	return this->visitedParams;
}

std::vector<double> Model::getRatings() {
	return this->computedRatings;
}

int Model::getHeight() {
	return this->height;
}

int Model::getWidth() {
	return this->width;
}

int Model::getXOrigin() {
	return this->xOrigin;
}

int Model::getYOrigin() {
	return this->yOrigin;
}

void Model::error(std::string errorMsg, std::string nameOfFunction, RatingDataPoint currentInput, bool critical){

	std::cerr << std::endl << "error():\tError in function " << nameOfFunction << ": " << errorMsg << std::endl;
	std::cerr << "error():\tLogging current state of the model to the file sl-model-errors-" << getpid() << ".txt. Please have a look there." << std::endl;

	std::chrono::system_clock::time_point startTime = std::chrono::system_clock::now();
	time_t startTime_C = std::chrono::system_clock::to_time_t(startTime);

	std::ofstream errorfile;
	std::stringstream filename;
	filename << "sl-model-errors-" << getpid() << ".txt";
	errorfile.open(filename.str(), std::ios::app);
	errorfile << std::endl << "----------------------------------------" << std::endl;
	errorfile << "Time: " << std::put_time(localtime(&startTime_C), "%b %d, %T") << std::endl;
	errorfile << "In function " << nameOfFunction << ", there was the error: " << errorMsg << std::endl;
	errorfile << "This was the input: " << std::endl;
	errorfile << this->printInformation(false);
	errorfile << "LO: " << *(currentInput.locatedObject.getCenterOfMass()) << std::endl;
	errorfile << "RO: " << *(currentInput.referenceObject->getCGALPolygon()) << std::endl;
	errorfile << "preposition: " << prepToString(currentInput.prep) << std::endl;
	errorfile << "emp. rating: " << currentInput.meanRating << std::endl;

	errorfile.close();

	if (critical){
		std::cerr << "error():\tError is fatal. Stopping the program." << std::endl;
		throw std::runtime_error(errorMsg);
	}
}
