/*
 * referenceObject.h
 *
 * This file is part of sl-model:
 * https://www.gitlab.com/tkluth/sl-model
 *
 * Copyright(c) 2014-2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 *
 * From October 2014 to September 2018, the
 * development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 *
 */

#ifndef REFERENCEOBJECT_H
#define REFERENCEOBJECT_H

#include "polygonObject.h"

/**
 * This class represents a referenceObject with its functional part as CGAL-polygon.
 */

class ReferenceObject: public PolygonObject {
	public:

		/**
		 * empty constructor
		 * */
		 ReferenceObject();

		/**
		 * constructs an object of type \ref ReferenceObject, which extends \ref PolygonObject
		 * @param polyCGAL a 2D CGAL-Polygon
		 * @param functionalFileNames path(s) to text-files containing the description of the functional part(s)
		 * @param functionalCGALPolygons 2D CGAL-Polygons of the funnctional parts as described in functionalFileNames
		 * @param revertedYAxis if true, Y-coordinates are treated reverted, i.e., the higher a point, the lower the Y-coordinate
		 * @param magnifier a factor with which the RO is multiplied in order to zoom/shrink
		 * @param idx a (hopefully) unique index
		 * */
		ReferenceObject(Polygon polyCGAL, std::vector<std::string> functionalFileNames, std::vector<Polygon> functionalCGALPolygons, bool revertedYAxis, double magnifier, unsigned int idx);

		// functional part:
		/**
		 * returns whether a point is inside the current functional part of the RO
		 * @param p the point that should be tested
		 * @returns true, if p is inside the current functional part, false otherwise
		 * */
		bool isInsideFunction(Point* p);

		/**
		 * sets one of several possible functional parts to be the current.
		 * A RO can have several functional parts, but only _one_ functional part can be 'active' at the same time
		 * @param functionalFileName the path to the functional part that should be activated.
		 * */
		void setFunctionalPolygon(std::string functionalFileName);

		/**
		 * @returns the 2D CGAL-Polygon of the current functional part
		 * */
		Polygon getFunctionalPolygon();

		/**
		 * @returns the Y-coordinate of the highest point on top of the current functional part
		 * */
		double getHightopFunction();

		/**
		 * @returns the Y-coordinate of the lowest point on top of the current functional part
		 * */
		double getLowtopFunction();

		/**
		 * @returns the leftmost point on top of the current functional part
		 * */
		Point getLeftTopFunction();

		/**
		 * @returns the rightmost point on top of the current functional part
		 * */
		Point getRightTopFunction();

		/**
		 * @returns the edges that make up the top of the current functional part
		 * */
		std::vector<Segment> getTopEdgesFunction();

		/**
		 * @returns the Y-coordinate of the lowest point on the bottom of the current functional part
		 * */
		double getLowBottomFunction();

		/**
		 * @returns the Y-coordinate of the highest point on the bottom of the current functional part
		 * */
		double getHighBottomFunction();

		/**
		 * @returns the leftmost point on the bottom of the current functional part
		 * */
		Point getLeftBottomFunction();

		/**
		 * @returns the rightmost point on the bottom of the current functional part
		 * */
		Point getRightBottomFunction();

		/**
		 * @returns the edges that make up the bottom of the current functional part
		 * */
		std::vector<Segment> getBottomEdgesFunction();

	private:

		// functional part variables:

		/** whether the RO has at least one functional part */
		bool hasFunctionalPart;

		/** a data structure to access the CGAL-Polygons of the functional parts based on their path */
		std::map<std::string, Polygon> functionalParts;

		/** the path to the current 'activated' functional part. Among other things, it is used to access its CGAL-polygon in \ref functionalParts. */
		std::string currentFunctionalFileName;

		/** y-coordinate of the highest point on top of the functional part of the object */
		double hightopFunction;

		/** y-coordinate of the lowest point on top of the functional part of the object */
		double lowtopFunction;

		/** CGAL 2D-point of the leftmost point on top of the functional part of the object */
		Point leftTopFunction;

		/** CGAL 2D-point of the rightmost point on top of the functional part of the object */
		Point rightTopFunction;

		/** the edges that make up the top of the functional part of the object */
		std::vector<Segment> topEdgesFunction;

		/** y-coordinate of the highest point on the bottom of the functional part of the object */
		double highBottomFunction;

		/** y-coordinate of the highest point on the bottom of the functional part of the object */
		double lowBottomFunction;

			/** CGAL 2D-point of the leftmost point on the bottom of the functional part of the object */
		Point leftBottomFunction;

			/** CGAL 2D-point of the rightmost point on the bottom of the functional part of the object */
		Point rightBottomFunction;

		/** the edges that make up the bottom of the functional part */
		std::vector<Segment> bottomEdgesFunction;

		/** a vector of points that are inside a functional part specified with a std::string containing its path */
		std::unordered_map<std::string, std::vector<Point> > insideFunctional;

		// methods:

		/**
		 * calculates the top of the current functional part, i.e., the variables \ref hightopFunction, \ref lowtopFunction, \ref leftTopFunction, and \ref rightTopFunction
		 * */
		void calculateTopFunction();

		/**
		 * Warning: This function is not tested!
		 * calculates the bottom of the current functional part, i.e., the variables \ref highBottomFunction, \ref lowBottomFunction, \ref leftBottomFunction, and \ref rightBottomFunction.
		 * */
		void calculateBottomFunction();

		/**
		 * calculates all points inside the current functional part, i.e., fills the corresponding vector of the variable \ref insideFunctional.
		 * */
		void calculateInnerFunctionPoints();
};

#endif /* REFERENCEOBJECT_H */
