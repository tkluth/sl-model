% Copyright(c) 2016-2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see http://www.gnu.org/licenses/.
%
% From October 2014 to September 2018, the
% development of this software was supported by the
% Excellence Cluster EXC 277 Cognitive Interaction Technology.
% The Excellence Cluster EXC 277 is a grant of the Deutsche
% Forschungsgemeinschaft (DFG) in the context of the German
% Excellence Initiative.

function [difference] = rAVS_CoO_compare_journal(parameters)

	%parameters are the following:
	% for rAVS: [alpha, slope, intercept, highgain]

	% L, thin vs tall:
	dataset = "../../data/predictions/L_subset_com.data,../../data/predictions/thin_rectangle.data,../../data/predictions/tall_rectangle.data";

	command = ["../../build/sl-model -d ", dataset, " -o ratings_rAVS_CoO_compare_journal.txt --quiet -m 11 --alpha ", num2str(parameters(1)), " --slope ", num2str(parameters(2)), " --intercept ", num2str(parameters(3)), " --highgain ", num2str(parameters(4))];

	system(command);

	ratings = textread('ratings_rAVS_CoO_compare_journal.txt')';
	% mean difference LOs left of CoM - LOs right of CoM, L
	difference(1) = mean(ratings(1:6) - ratings(7:12));
	% mean difference LOs above thin - LOs above tall
	difference(2) = mean(ratings(13:40) - ratings(41:68));

	equalityThreshold = 0.5;

	for i=1:length(difference)
		if (isnan(difference(i)))
			difference(:,:) = NaN
			break
		endif
		if (abs(difference(i)) < equalityThreshold)
			difference(i) = 0;
		else
			if (difference(i) < 0.0)
				difference(i) = -1;
			else
				difference(i) = 1;
			endif
		endif
	end
