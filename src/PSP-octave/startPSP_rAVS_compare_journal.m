% Copyright(c) 2016-2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see http://www.gnu.org/licenses/.
%
% From October 2014 to September 2018, the
% development of this software was supported by the
% Excellence Cluster EXC 277 Cognitive Interaction Technology.
% The Excellence Cluster EXC 277 is a grant of the Deutsche
% Forschungsgemeinschaft (DFG) in the context of the German
% Excellence Initiative.

startparams = [[0.321687, -0.004450464, 0.9663647, 1.108344]', [1.164967, -0.0048133956, 0.9668951, 4.695772]', [1.0, -0.006, 1.007, 0.131]'];

constraints = [[0.001, 5.0]', [-1.0/45.0, 0.0]', [0.7, 1.3]', [0.0, 10.0]']';

more off

pspOptions = optsetpsp('VolEst', 'HitMiss', 'SmpSize', ceil([150 300]*1.2^4));

[Y,XMCV] = psp(@rAVS_compare_journal, startparams, constraints, pspOptions)

more on
