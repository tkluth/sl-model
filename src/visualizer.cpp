/*
 * visualizer.cpp
 *
 * This file is part of sl-model:
 * https://www.gitlab.com/tkluth/sl-model
 *
 * Copyright(c) 2014-2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 *
 * From October 2014 to September 2018, the
 * development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 *
 */

/**
 * the higher this value, the more output
 * if set to 0 only important messages will be printed
 * this debug switch is placed in every file and only valid for one file!
 */
#define VERBOSITY 0

#include "visualizer.h"

#include <fstream>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
// measure time:
#include <chrono>

void Visualizer::visualizeTemplate() {
	std::ofstream templateFile = openFile(this->fileOut + "_template");

	std::vector<std::vector<double> > result = this->model.getDisplay();
	int xOrigin = model.getXOrigin();
	int yOrigin = model.getYOrigin();
	int width = model.getWidth();
	int height = model.getHeight();

	// use ints for the loops as xOrigin etc. might be negative
	for (int x = 0; x < (int) result.size(); ++x) {
		for (int y = 0; y < (int) result[0].size(); ++y) {
			//x y z value
			if(std::isnan(result[(unsigned int) x][(unsigned int) y])) {
				templateFile << (xOrigin + x) << " " << (yOrigin + y) << " -1" << std::endl;
			} else {
				templateFile << (xOrigin + x) << " " << (yOrigin + y) << " " << result[(unsigned int) x][(unsigned int) y] << std::endl;
			}
		}

		templateFile << std::endl;
	}

	templateFile.close();

	std::vector<std::pair<Point, Vector> > visualizeVectors = this->model.getVectorsToVisualize();
	double minVectorX = xOrigin;
	double minVectorY = yOrigin;
	double maxVectorX = width;
	double maxVectorY = height;

	if (visualizeVectors.size() > 0) {
		std::ofstream vectorFile = openFile(this->fileOut + ".vectors");

		for (size_t i = 0; i < visualizeVectors.size(); i++) {
			Point referenceObjectpoint = visualizeVectors[i].first;
			Vector vectorDestination = visualizeVectors[i].second;
			if (visualizeVectors.size() == 3 && i == 2) {
				vectorFile << referenceObjectpoint.x() << " " << referenceObjectpoint.y() << " " << vectorDestination.x() << " " << vectorDestination.y() << " 0x000000" << std::endl;
			} else {
				vectorFile << referenceObjectpoint.x() << " " << referenceObjectpoint.y() << " " << vectorDestination.x() << " " << vectorDestination.y() << " 0xBFBFBF" << std::endl;
			}
		}

		vectorFile.close();
	}

	// to save the file "plot.command" in the same folder as the results file,
	// remove the filename from this->fileOut and then add "plot.command" to the path
	char tmp[this->fileOut.size() + 1];
	strcpy(tmp, this->fileOut.c_str());
	char* last_slash = strrchr(tmp, '/');
	std::string gnuplotFilePath;

	if (last_slash) {
		*(last_slash + 1) = '\0';
		gnuplotFilePath = std::string(tmp) + "plot.command";
	} else {
		// current directory
		gnuplotFilePath = "plot.command";
	}

	std::ofstream gnuplotFile = openFile(gnuplotFilePath.c_str(), false);

	std::string latexFileOut;
	if (this->latex) {
		std::string mkimgdir = "mkdir -p img/";

		if (system(mkimgdir.c_str())) {
			latexFileOut = latexFigureName + ".tex";
		} else {
			latexFileOut = "img/" + latexFigureName + ".tex";
		}

		gnuplotFile << "set terminal epslatex input clip";

		if (this->color) {
			gnuplotFile << " color" << std::endl;
		}
		gnuplotFile << std::endl;

		gnuplotFile << "set size (" << (width - xOrigin) << "/(5*2.54))/25,(" << (height - yOrigin) << "/(3.5*2.54))/25" << std::endl;
		gnuplotFile << "set size ratio " << (double) (height - yOrigin) / (width - xOrigin) << std::endl;

		gnuplotFile << "unset key; unset tics; unset border; unset colorbox" << std::endl;

		gnuplotFile << "set output '" << latexFileOut << "'" << std::endl;

	} else {
		gnuplotFile << "set terminal wxt " << std::endl;
		gnuplotFile << "set size ratio " << (double) (height - yOrigin) / (width - xOrigin) << std::endl;
	}

	if (!this->color) {
		gnuplotFile << "set palette grey" << std::endl;
	} else {
		gnuplotFile << "# uni bielefeld green: rgb '#007354'" << std::endl;
		gnuplotFile << "# uni bielefeld citec-orange: rgb '#00FA9433'" << std::endl;
		gnuplotFile << "# uni bielefeld green / CITEC orange:" << std::endl;
		gnuplotFile << "# set palette defined (0 0.00 0.45 0.33, 1 0.98 0.58 0.2)" << std::endl;
		gnuplotFile << "# set palette defined (0 'red', 1 'green')" << std::endl;
		gnuplotFile << "# set palette defined (0 'dark-blue', 1 'orange')" << std::endl;
	}

	gnuplotFile << "set yrange[" << minVectorY << ":" << maxVectorY << "]" << std::endl;
	gnuplotFile << "set xrange[" << minVectorX << ":" << maxVectorX << "]" << std::endl;

	for (size_t t = 0; t < this->empiricalData.size(); t++) {
		gnuplotFile << "# LO #" << t << ":" << std::endl;
		// visualize located object as gnuplot polygon
		Polygon::Vertex_const_iterator loVertices = this->empiricalData[t].locatedObject.getCGALPolygon()->vertices_begin();

		if (loVertices + 1 == this->empiricalData[t].locatedObject.getCGALPolygon()->vertices_end()) {
			// single point LO, visualize as circle:
			gnuplotFile << "set object circle at ";
			gnuplotFile << this->empiricalData[t].locatedObject.getCenterOfMass()->x() << ", " << this->empiricalData[t].locatedObject.getCenterOfMass()->y() << " front" << std::endl;
		} else {
			// multi-point LO, visualize as polygon
			gnuplotFile << "; set object polygon from ";

			while (loVertices != this->empiricalData[t].locatedObject.getCGALPolygon()->vertices_end()) {
				gnuplotFile << loVertices->x() << ", " << loVertices->y();
				loVertices++;
				gnuplotFile << " to ";
				if (loVertices == this->empiricalData[t].locatedObject.getCGALPolygon()->vertices_end()) {
					// add first vertex again, to close polygon
					gnuplotFile << this->empiricalData[t].locatedObject.getCGALPolygon()->vertices_begin()->x() << ", " << this->empiricalData[t].locatedObject.getCGALPolygon()->vertices_begin()->y();
				}
			}

			gnuplotFile << " front fillstyle pattern 4 linewidth 3" << std::endl;
		}
	}

	// visualize reference object(s) as gnuplot rectangle

#if VERBOSITY >= 1
	std::cout << "visualize(): \t" << "vertices of the RO: ";
#endif

	for (size_t r = 0; r < this->referenceObjects.size(); r++) {
		gnuplotFile << "# RO #" << r << std::endl;
		gnuplotFile << "set object polygon from ";

		Polygon::Vertex_const_iterator vertices = this->referenceObjects[r]->getCGALPolygon()->vertices_begin();
		while (vertices != this->referenceObjects[r]->getCGALPolygon()->vertices_end()) {
			gnuplotFile << vertices->x() << ", " << vertices->y();
#if VERBOSITY >= 1
			std::cout << vertices->x() << ", " << vertices->y() << " to ";
#endif
			vertices++;
			gnuplotFile << " to ";
			if (vertices == this->referenceObjects[r]->getCGALPolygon()->vertices_end()) {
				gnuplotFile << this->referenceObjects[r]->getCGALPolygon()->vertices_begin()->x() << ", " << referenceObjects[r]->getCGALPolygon()->vertices_begin()->y();
			}
		}

#if VERBOSITY >= 1
		std::cout << std::endl;
#endif

		gnuplotFile << " front fillstyle pattern 5 linewidth 3" << std::endl;
	}

	if (this->latex) {
		gnuplotFile << "plot '" << latexFigureName + "_results.txt_template" << "' with image notitle";
	} else {
		if(this->plot3D){
			gnuplotFile << "splot '" << this->fileOut << "_template' with pm3d notitle";
			gnuplotFile << std::endl << "#plot '" << this->fileOut << "' with image notitle";
		} else {
			gnuplotFile << "#splot '" << this->fileOut << "_template' with pm3d notitle";
			gnuplotFile << std::endl << "plot '" << this->fileOut << "_template' with image notitle";
		}
	}

	if (visualizeVectors.size() > 0) {
		if (visualizeVectors.size() == 3) {
			// visualize attentionalSwitch - plot all 3 vectors
			gnuplotFile << "; plot '" << this->fileOut << ".vectors' using 1:2:3:4:5 with vectors linewidth 3 linecolor rgb variable title 'vectors'" << std::endl;
		} else {
			double vectorDisplayRatio = 1.0;
			if(this->latex){
				gnuplotFile << ", '" << latexFigureName << "_results.txt.vectors' every " << vectorDisplayRatio << " with vectors linecolor rgb 'green' notitle" << std::endl;
			} else {
				gnuplotFile << ", '" << this->fileOut << ".vectors' every " << vectorDisplayRatio << " with vectors linecolor rgb 'green' notitle" << std::endl;
			}
		}
	} else if (this->latex) {
		if (!(almostEqual(this->empiricalData[0].locatedObject.getRightBottom().x(), -1))) {
			gnuplotFile << "; set label '$t$' at " << this->empiricalData[0].locatedObject.getRightBottom().x() << "," << this->empiricalData[0].locatedObject.getRightBottom().y() << " front offset 3,-3" << std::endl;
		}
		if (!(almostEqual(model.getFocusToVisualize().x(), -1))) {
			gnuplotFile << "; set label '$f$' at " << model.getFocusToVisualize().x() << "," << model.getFocusToVisualize().y() << " front offset 2,3" << std::endl;
		}
	}

	gnuplotFile << std::endl;

	if (this->latex) {
		gnuplotFile << "set output" << std::endl;
	} else {
		gnuplotFile << "pause -1" << std::endl;
	}

	gnuplotFile.close();

	std::string startGnuplot = "";

	if (this->latex) {
		// move results files and plot command to img folder to be able to quickly reproduce the latex figures
		startGnuplot = "cp plot.command " + latexFigureName + "_plot.command" + " && cp " + this->fileOut + "_template " + latexFigureName + "_results.txt_template";
		if (visualizeVectors.size() > 0) {
			startGnuplot = startGnuplot + " && cp " + this->fileOut + ".vectors " + latexFigureName + "_results.txt.vectors";
		}
	}

	startGnuplot = startGnuplot + "\n gnuplot " + gnuplotFilePath;

	if (this->latex) {
		std::ofstream fixBB = openFile("fixGnuplotEPSBoundingBox.sh", true);
		fixBB << this->fixGnuplotBoundingBoxScript();
		fixBB.close();
		startGnuplot = startGnuplot + "\n chmod +x fixGnuplotEPSBoundingBox.sh \n ./fixGnuplotEPSBoundingBox.sh img/" + latexFigureName + ".eps";
	}

	if (this->plot & system(startGnuplot.c_str())) {
		std::cout << "gnuplot failed to start" << std::endl;
	}
}

void Visualizer::visualizeRatingDist() {
	std::ofstream distEmpFile = openFile(this->fileOut + "_distEmp");
	std::ofstream distModelFile = openFile(this->fileOut + "_distModel");

	std::pair<std::vector<double>, std::vector<double> > result = this->model.getRatingDist();

	assert(result.first.size() == result.second.size());

	distEmpFile << "# empirical distributions (aggregated proportions)" << std::endl;
	distEmpFile << "# rating proportion" << std::endl;
	distModelFile << "# model distributions (best fitting, estimated probabilitites)" << std::endl;
	distModelFile << "# rating probability" << std::endl;

	for (size_t i = 0; i < result.first.size(); i++){
		distEmpFile << this->model.getMinRating() + i << " " << result.first[i] << " 0.4" << std::endl;;
		distModelFile << this->model.getMinRating() + i << " " << result.second[i] << std::endl;;
	}

	distEmpFile.close();
	distModelFile.close();

	// to save the file "plot.command" in the same folder as the results file,
	// remove the filename from this->fileOut and then add "plot.command" to the path
	char tmp[this->fileOut.size() + 1];
	strcpy(tmp, this->fileOut.c_str());
	char* last_slash = strrchr(tmp, '/');
	std::string gnuplotFilePath;

	if (last_slash) {
		*(last_slash + 1) = '\0';
		gnuplotFilePath = std::string(tmp) + "plot.command";
	} else {
		// current directory
		gnuplotFilePath = "plot.command";
	}
	std::ofstream gnuplotFile = openFile(gnuplotFilePath.c_str(), false);

	std::string latexFileOut;
	if (this->latex) {
		std::string mkimgdir = "mkdir -p img/";
		if (system(mkimgdir.c_str())) {
			latexFileOut = latexFigureName + ".tex";
		} else {
			latexFileOut = "img/" + latexFigureName + ".tex";
		}

		gnuplotFile << "set terminal cairolatex";

		if (this->color) {
			gnuplotFile << " color";
		}
		gnuplotFile << std::endl;

		//~ gnuplotFile << "set size (" << (width - xOrigin) << "/(5*2.54))/5,(" << (height - yOrigin) << "/(3*2.54))/5" << std::endl;
		//~ gnuplotFile << "set size ratio " << (double) (height - yOrigin) / (width - xOrigin) << std::endl;

		//~ gnuplotFile << "unset key; unset tics; unset border; unset colorbox" << std::endl;

		gnuplotFile << "set output '" << latexFileOut << "'" << std::endl;

	} else {
		gnuplotFile << "set terminal wxt " << std::endl;
	}

	if (!this->color) {
		gnuplotFile << "set palette grey negative" << std::endl;
		gnuplotFile << "set style fill pattern" << std::endl;
		gnuplotFile << "set monochrome" << std::endl;
	} else {
		gnuplotFile << "set style fill transparent solid 0.35" << std::endl;
	}

	gnuplotFile << "set format y '%.2f'" << std::endl;
	gnuplotFile << "set xrange [0:10]" << std::endl;
	gnuplotFile << "# color" << std::endl;

	gnuplotFile << "set ylabel 'proportion / probability'" << std::endl;
	gnuplotFile << "set grid ytics front" << std::endl;
	gnuplotFile << "set xlabel 'rating'" << std::endl;
	gnuplotFile << "set xtics (1,2,3,4,5,6,7,8,9) out nomirror" << std::endl;
	gnuplotFile << "set key left Left reverse" << std::endl;

	gnuplotFile << "Gaussian(x, mu, sd) = (1/(sd*sqrt(2*pi)))*exp(-(x-mu)**2/(2*sd**2))" << std::endl;

	//~ gnuplotFile << "stats '" << this->fileOut << "_distModel' nooutput" << std::endl; // gives e.g. STATS_max_y

	std::vector<double> ordinalParameters = this->model.getOrdinalParams();
	double mu = ordinalParameters[0];
	double sigmaOrd = ordinalParameters[1];
	for (size_t t = 2; t < ordinalParameters.size(); ++t) {
		// draw a dashed line for each threshold, as high as the maximum on the Gaussian distribution
		gnuplotFile << "set arrow from " << ordinalParameters[t] << ",0.0 to " << ordinalParameters[t] << ", Gaussian(" << mu << ", " << mu << ", " << sigmaOrd << ") + 0.1 nohead dt 5" << std::endl;
	}

	if (this->latex) {
		gnuplotFile << "plot '" << latexFigureName << "_results.txt_distEmp' u 1:2:3 w boxes title 'empirical data' linecolor 2, '" << latexFigureName << "_results.txt_distModel' w points title 'model output' pointtype 2, [0:10] Gaussian(x, " << mu << ", " << sigmaOrd << ") title 'latent distribution' dt 2, NaN dt 5 title 'thresholds'" << std::endl;
	} else {
		gnuplotFile << "plot '" << this->fileOut << "_distEmp' u 1:2:3 w boxes title 'empirical data', '" << this->fileOut << "_distModel' w points title 'model output' pointtype 2 linecolor 7, [0:10] Gaussian(x, " << mu << ", " << sigmaOrd << ") title 'latent distribution' dt 2, NaN dt 5 title 'thresholds'" << std::endl;
	}

	gnuplotFile << "# error bars: 'confidenceMedian.data' u 1:2:3:4 w errorbars notitle pointsize 0 linewidth 7.5 linetype 1" << std::endl;

	if (this->latex) {
		gnuplotFile << "set output" << std::endl;
	} else {
		gnuplotFile << "pause -1" << std::endl;
	}

	gnuplotFile.close();

	std::string startGnuplot = "";

	if (this->latex) {
		// move results files and plot command to img folder to be able to quickly reproduce the latex figures
		startGnuplot = "cp plot.command " + latexFigureName + "_plot.command" +
		" && cp " + this->fileOut + "_distEmp " + latexFigureName + "_results.txt_distEmp" + " && cp " + this->fileOut + "_distModel " + latexFigureName + "_results.txt_distModel";
	}

	startGnuplot = startGnuplot + "\n gnuplot " + gnuplotFilePath;

	if (this->plot & system(startGnuplot.c_str())) {
		std::cout << "gnuplot failed to start" << std::endl;
	}
}

void Visualizer::visualizePosterior(unsigned int noOfChains) {

	std::ofstream RplotFile = openFile(this->fileOut + "-plot.R");

	RplotFile << "# This should be commented when run from the same directory as this file (manually, afterwards)" << std::endl;
	RplotFile << "setwd(dirname('" << this->fileOut << "'))" << std::endl;
	RplotFile << "# load necessary libraries" << std::endl;
	RplotFile << "library(ggmcmc)" << std::endl;
	RplotFile << "library(coda)" << std::endl;
	RplotFile << "# create img folder for plots" << std::endl;
	RplotFile << "if (! dir.exists('img')) { dir.create('img') }" << std::endl;
	RplotFile << std::endl;

	for (unsigned int chain = 0; chain < noOfChains; ++chain) {
		RplotFile << "# load data" << std::endl;
		RplotFile << "posteriors" << chain << " = read.csv(basename('" << this->fileOut << "_posterior" << chain << "'), comment.char = '#')" << std::endl;
		RplotFile << "chain" << chain << " = mcmc(subset(posteriors" << chain << ", select = -step))" << std::endl;
	}

	RplotFile << std::endl;
	RplotFile << "chains = mcmc.list(";
	for (unsigned int chain = 0; chain < noOfChains; ++chain) {
		if (chain != noOfChains - 1) {
			RplotFile << "chain" << chain << ", ";
		} else {
			// don't add a comma after the last
			RplotFile << "chain" << chain;
		}
	}
	RplotFile << ")" << std::endl;
	RplotFile << std::endl;

	RplotFile << "# produce diagnostic plots" << std::endl;
	RplotFile << "ggmcmc(ggs(chains), file = paste0('img/', basename('" << this->fileOut << "-ggmcmc.pdf')))" << std::endl;
	RplotFile << "print(paste0('Plotted results to ', dirname('" << this->fileOut << "'), '/img/', basename('" << this->fileOut << "-ggmcmc.pdf')))" << std::endl;

	RplotFile.close();

	std::string startplotR = "R --slave -f " + this->fileOut + "-plot.R";

	if (system(startplotR.c_str())) {
		std::cout << "R failed to plot. You can retry with the command 'R --slave -f " << this->fileOut << "-plot.R' (You might want to disable the setwd() command in that file first.)" << std::endl;
	} else {
		std::cout << "R has plotted, see above for the path to the image file." << std::endl;
	}
}

void Visualizer::saveResultsPE() {
	std::ofstream resultFile = openFile(this->fileOut);
	resultFile << this->model.getResultsPE();
	resultFile.close();

	std::vector<std::map<std::string, float> > paramValues = this->model.getVisitedParams();

	std::stringstream filename;
	filename << this->fileOut << "_params";

	std::ofstream distributionFile = openFile(filename.str());

	// file header
	distributionFile << "step";

	for (std::map<std::string, float>::iterator param = paramValues[0].begin(); param != paramValues[0].end(); ++param) {
		distributionFile << " " << param->first;
	}

	distributionFile << std::endl;

	for (size_t step = 0; step < paramValues.size(); ++step) {

		distributionFile << step;

		for (std::map<std::string, float>::iterator param = paramValues[step].begin(); param != paramValues[step].end(); ++param) {
			distributionFile << " " << param->second;
		}

		distributionFile << std::endl;
	}

	distributionFile.close();

	if (this->plot) {
		if (!this->latex) {
		this->plotErrorFunction();
		}
		if (this->model.getOrdinal()) {
			this->visualizeRatingDist();
		} else {
			this->visualizeTemplate();
		}
	}
}

void Visualizer::plotErrorFunction() {
	std::stringstream filename;
	filename << this->fileOut << "_params";

	std::string startGnuplot = "gnuplot -e \"plot '" + filename.str();
	startGnuplot = startGnuplot + "' u (column('step')):(column('value')) w lines; pause -1\"";
	if (system(startGnuplot.c_str())) {
		std::cout << "failed to start gnuplot for a visualization of the model error" << std::endl;
	}
}

void Visualizer::saveResultsSHO(unsigned int noOfThreads) {
	std::ofstream resultFile = openFile(this->fileOut);

	if (this->model.getOrdinal()) {
	resultFile << "GOF (Kullback-Leibler):" << std::endl;
	} else {
		resultFile << "GOF (nRMSE):" << std::endl;
	}
	resultFile << this->comparison.getGOF();

	resultFile.close();

	std::ofstream RscriptFile = openFile(this->fileOut + "-bootstrap.R");
	std::ofstream RplotFile = openFile(this->fileOut + "-plot.R");

	RscriptFile << "library(boot)" << std::endl;
	RscriptFile << "options(boot.parallel = 'multicore', boot.ncpus = " << noOfThreads << ")" << std::endl;

	std::vector<std::pair<std::string, std::string> > resultsSHO = this->comparison.getResultsSHO();

	std::stringstream plotBuffer;
	plotBuffer << "parameters = data.frame(";

	for (size_t rIdx = 0; rIdx < resultsSHO.size(); ++rIdx) {
		std::string rname = resultsSHO[rIdx].first;

		// plot parameter distributions:
		plotBuffer << rname;
		if (rIdx < resultsSHO.size() - 1) {
			plotBuffer << ", ";
		} else {
			plotBuffer << ")" << std::endl;
		}

		RscriptFile << rname << " = c(";
		RscriptFile << resultsSHO[rIdx].second << ")" << std::endl;

		RplotFile << rname << " = c(";
		RplotFile << resultsSHO[rIdx].second << ")" << std::endl;

		// mean, median, SD, bootstrap CIs:
		RscriptFile << "bootstraps_median_" << rname << " = boot(" << rname << ", function(u,i) median(u[i]), R = 100000)" << std::endl;
		RscriptFile << "med_" << rname << " = median(" << rname << ")" << std::endl; //\nmed_" << rname << std::endl;
		RscriptFile << "median_" << rname << "_CIs = boot.ci(bootstraps_median_" << rname << ", type='bca')$bca[4:5]" << std::endl;
		RscriptFile << "bootstraps_mean_" << rname << " = boot(" << rname << ", function(u,i) mean(u[i]), R = 100000)" << std::endl;
		RscriptFile << "m_" << rname << " = mean(" << rname << ")" << std::endl; //\nm_" << rname << std::endl;
		RscriptFile << "sd_" << rname << " = sd(" << rname << ")" << std::endl; //\nsd_" << rname << std::endl;
		RscriptFile << "mean_" << rname << "_CIs = boot.ci(bootstraps_mean_" << rname << ", type='bca')$bca[4:5]" << std::endl;

		// write results to file
		RscriptFile << "write('median (" << rname << ") with bootstrap BCa CIs: ', '" << this->fileOut.c_str() << "', append=TRUE)" << std::endl;
		RscriptFile << "write(c(med_" << rname << ", median_" << rname << "_CIs), '" << this->fileOut.c_str() << "', append=TRUE)" << std::endl;
		RscriptFile << "write('mean (" << rname << ") with bootstrap BCa CIs and standard deviation: ', '" << this->fileOut.c_str() << "', append=TRUE)" << std::endl;
		RscriptFile << "write(c(m_" << rname << ", mean_" << rname << "_CIs, sd_" << rname << "), '" << this->fileOut.c_str() << "', append=TRUE)" << std::endl;

		RscriptFile << "'progress (in %):'" << std::endl;
		RscriptFile << rIdx+1 << "/" << resultsSHO.size() << std::endl;
	}

	RscriptFile.close();

	RplotFile << plotBuffer.str() << std::endl;
	RplotFile << "# you can set createPlots to TRUE and run the commands below / source this file" << std::endl;
	RplotFile << "createPlots = " << this->plot << std::endl;
	RplotFile << "# canWePlot is FALSE if one of the packages fails to load" << std::endl;
	RplotFile << "canWePlot = require(GGally)" << std::endl;
	RplotFile << "canWePlot = canWePlot & require(ggplot2)" << std::endl;
	RplotFile << "if(canWePlot[1] == FALSE){" << std::endl;
	RplotFile << "print ('Failed to load plotting packages. Are the packages ggplot2 and GGally installed? No parameter plot will be produced.')" << std::endl;
	RplotFile << "} else {" << std::endl;

	RplotFile << "ordinalParameters = subset(parameters, select = grep('t[0123456789]', colnames(parameters)))" << std::endl;
	RplotFile << "modelParameters = subset(parameters, select = grep('t[0123456789]', colnames(parameters), invert = TRUE))" << std::endl;

	// if in no-ordinal model, subsetting fails
	RplotFile << "if(!is.null(parameters$sigmaOrd)){" << std::endl;
	RplotFile << "\tordinalParameters = data.frame(ordinalParameters, subset(parameters, select = 'sigmaOrd'))" << std::endl;
	RplotFile << "\tmodelParameters = subset(modelParameters, select = -sigmaOrd)" << std::endl;
	RplotFile << "if (createPlots) {" << std::endl;
	RplotFile << "\tp = ggpairs(ordinalParameters, #columns=1:5, " << std::endl;
	RplotFile << "\t\t\ttitle = 'parameter sets for SHO iterations (best fits only)')" << std::endl;
	RplotFile << "\t\tggsave(p, file='" << this->fileOut << "-SHO-ordinal-parameters.eps')" << std::endl;
	RplotFile << "\t}" << std::endl;
	RplotFile << "}" << std::endl;
	RplotFile << "}" << std::endl;

	RplotFile << "if (createPlots) {" << std::endl;
	RplotFile << "p = ggpairs(modelParameters, #columns=1:5, " << std::endl;
	RplotFile << "\t\ttitle = 'parameter sets for SHO iterations')" << std::endl;
	RplotFile << "\tggsave(p, file='" << this->fileOut << "-SHO-parameters.eps')" << std::endl;
	RplotFile << "}" << std::endl;

	RplotFile.close();

	std::cout << "Using R now to compute bootstrap confidence intervals. This could take some minutes." << std::endl;

	std::string startR = "R --slave -f " + this->fileOut + "-bootstrap.R";
	std::string startplotR = "R --slave -f " + this->fileOut + "-plot.R";

	if (system(startR.c_str())) {
		std::cout << "R failed to start, is the 'boot' package installed? You can retry with the command 'R --slave -f " << this->fileOut << "-bootstrap.R'" << std::endl;
	} else {
		std::cout << "R is done, for confidence intervals " << this->fileOut.c_str() << std::endl;
	}

	if (system(startplotR.c_str())) {
		std::cout << "R failed to plot. You can retry with the command 'R --slave -f " << this->fileOut << "-plot.R'" << std::endl;
	} else {
		if (this->plot) {
			std::cout << "R has plotted, see " << this->fileOut << "-SHO-parameters.eps (and potentially also " << this->fileOut << "-SHO-ordinal-parameters.eps)" << std::endl;
		} else {
			std::cout << "If you want to see pairs plots of the best fitting parameters, open the file " << this->fileOut << "-plot.R, change the createPlots variable to TRUE and run 'R --slave -f "<< this->fileOut << "-plot.R'" << std::endl;
		}
	}
}

void Visualizer::saveResultsMFA() {
	std::ofstream resultFile = openFile(this->fileOut);

	resultFile << this->comparison.getResultsMFA();

	resultFile.close();
}

std::ofstream Visualizer::openFile(std::string fileToOpen, bool quiet){
	std::ofstream file;

	// the loop that tries to open the file more than once
	// is a workaround for a buggy system at Bielefeld University that sometimes failed
	// to save files (it turned out to actually be a "feature" of Kerberos authentication
	// that restricted access after some time without user input. If this is the same for
	// your system you might want to try the program krenew).
	// Since some computations take a long time, the attempt to save is repeated
	// instead of just trying once and failing.
	int maxTries = 8; // actual number of tries is +2
	int nTry = 0;

	do {

		file.open(fileToOpen);

		if(file.fail()){
			std::chrono::system_clock::time_point errorTime = std::chrono::system_clock::now();
			time_t errorTime_C = std::chrono::system_clock::to_time_t(errorTime);
			std::cerr << std::put_time(localtime(&errorTime_C), "%b %d, %T") << ": Failed to open " << fileToOpen << std::endl;
			std::cerr << "debug info(): failbit (Logical error on i/o operation): " << (file.fail() && !file.bad()) << ", badbit (Read/writing error on i/o operation): " << file.bad() << ", eofbit (end-of-file reached): " << file.eof() << std::endl;
			if(nTry > maxTries){
				throw std::runtime_error("Couldn't open file. Check if all folders are existing and you didn't do a typo ... (If your system uses Kerberos authentication, try to use 'krenew' together with 'sl-model'.)");
			} else {
				std::cerr << "Couldn't open file. You have to manually create directories. (If your system uses Kerberos authentication, try to use 'krenew' together with 'sl-model'.) Waiting for 5 minutes and trying for " << (maxTries - nTry)+1 << " more times." << std::endl;
				sleep(300);
			}
		} else { // success in opening file
			break; // the loop
		}
		nTry++;
	} while(true); // the loop either ends with a std::runtime_error or with a successful opening

	if(!quiet) {
		// don't print general info if file is used as input for octave (PSP computation)
		file << this->printGeneralInfo();
	}

	return file;
}

std::string Visualizer::printGeneralInfo() {
	std::stringstream buffer;

	buffer << "# sl-model results" << std::endl;
	buffer << "# referenceObject-file: ";
	for (size_t r = 0; r < this->roFiles.size(); r++) {
		buffer << this->roFiles[r] << " ";
	}
	buffer << std::endl;
	buffer << "# functional-file(s): ";
	for (size_t t = 0; t < this->functionfiles.size(); t++) {
		buffer << this->functionfiles[t] << " ";
	}
	buffer << std::endl;
	buffer << "# zoom/scale-factor: " << std::setprecision(2) << this->empiricalData[0].referenceObject->getScale() << std::endl;
	buffer << "# LO file(s): ";
	for (size_t t = 0; t < this->loFiles.size(); t++) {
		buffer << this->loFiles[t] << " ";
	}
	buffer << std::endl;
	buffer << "# datafile: " << datafile << std::endl;
	buffer << "# minrating: " << std::setprecision(0) << this->model.getMinRating() << std::endl;
	buffer << "# maxrating: " << std::setprecision(0) << this->model.getMaxRating() << std::endl;
	buffer << "# parameter ranges: " << std::endl;
	buffer << "# lambda:\t[" << std::setprecision(5) << this->model.getParam("lambda").min << ", " << this->model.getParam("lambda").max << "]" << std::endl;
	buffer << "# alpha:\t[" << std::setprecision(5) << this->model.getParam("alpha").min << ", " << this->model.getParam("alpha").max << "]" << std::endl;
	buffer << "# slope:\t[" << std::setprecision(5) << this->model.getParam("slope").min << ", " << this->model.getParam("slope").max << "]" << std::endl;
	buffer << "# intercept:\t[" << std::setprecision(5) << this->model.getParam("intercept").min << ", " << this->model.getParam("intercept").max << "]" << std::endl;
	buffer << "# highgain:\t[" << std::setprecision(5) << this->model.getParam("highgain").min << ", " << this->model.getParam("highgain").max << "]" << std::endl;
	if (this->model.getOrdinal()) {
		buffer << "# sigmaOrd:\t[" << std::setprecision(5) << this->model.getParam("sigmaOrd").min << ", " << this->model.getParam("sigmaOrd").max << "]" << std::endl;
		for (size_t t = 0; t < (this->model.getMaxRating() - this->model.getMinRating()); ++t) {
			std::stringstream tmp;
			tmp << "t" << t;
			buffer << "# t" << t << ":\t[" << std::setprecision(5) << this->model.getParam(tmp.str()).min << ", " << this->model.getParam(tmp.str()).max << "]" << std::endl;
		}
	}

	buffer << "# model used: " << this->model.getMethod() << ", ordinal mode: " << this->model.getOrdinal() << std::endl;

	return buffer.str();
}

std::string Visualizer::fixGnuplotBoundingBoxScript() {
	std::stringstream script;

	script << "#!/bin/bash" << std::endl;
	script << std::endl;
	script << "# This is fixbb version 0.31.  Copyright 2000 by Jeff Spirko." << std::endl;
	script << "# Redistributable under the GNU General Public License." << std::endl;
	script << "# See http://topquark.dhs.org/~spirko/fixbb/" << std::endl;
	script << std::endl;
	script << "# Changes by Petr Mikulik:" << std::endl;
	script << "#   -  8.  9. 2000: working file in the $TMP directory; remove backup file" << std::endl;
	script << "#   - 18. 10. 2000: AWKPARSEBB to support multiple-page (non-EPS) documents" << std::endl;
	script << "#   - 26. 10. 2000: error if bounding box not determined (GS < 5.50 or empty" << std::endl;
	script << "#     pages); page status info added" << std::endl;
	script << std::endl;
	script << "# Changes by Thomas Kluth: " << std::endl;
	script << "#   - removed old, unused code" << std::endl;
	script << "#   - create this script automatically" << std::endl;
	script << std::endl;
	script << "# Options for ghostscript to print the BoundingBox without other junk." << std::endl;
	script << "GSOPTS=\"-dQUIET -dBATCH -dNOPAUSE -sDEVICE=bbox\"" << std::endl;
	script << "export GSOPTS" << std::endl;
	script << std::endl;
	script << "AWKPROG='" << std::endl;
	script << "\t(found) { print; next }" << std::endl;
	script << "\t$1 == \"%%BoundingBox:\" { print $1, ENVIRON[\"bbox\"]; found=1; next }" << std::endl;
	script << "\t{ print }" << std::endl;
	script << "\t'" << std::endl;
	script << std::endl;
	script << "# Finds min and max of the 4 numbers after the \"%%BoundingBox:\" label" << std::endl;
	script << "AWKPARSEBB='" << std::endl;
	script << "\tBEGIN { x1=99999; y1=99999; x2=0; y2=0; page=0 }" << std::endl;
	script << "\t$1!= \"%%BoundingBox:\" { next }" << std::endl;
	script << "\t$2<0 || $3<0 { next }" << std::endl;
	script << "\tx1>$2 { x1=$2 }" << std::endl;
	script << "\ty1>$3 { y1=$3 }" << std::endl;
	script << "\tx2<$4 { x2=$4 }" << std::endl;
	script << "\ty2<$5 { y2=$5 }" << std::endl;
	script << "\t{ page++; printf \"Page \" page \": \" $0 \"  max = \" x1 \" \" y1 \" \" x2 \" \" y2  \"  \\t\\r\" >\"/dev/stderr\" }" << std::endl;
	script << "\tEND { printf \"\\t\\t\\t\\t\\t\\t\\t\\r\" >\"/dev/stderr\"" << std::endl;
	script << "\t      print x1 \" \" y1 \" \" x2 \" \" y2 }" << std::endl;
	script << "\t'" << std::endl;
	script << std::endl;
	script << "export bbox" << std::endl;
	script << std::endl;
	script << "# Each command-line argument is assumed to be a file to process" << std::endl;
	script << "for FILE in $* ; do" << std::endl;
	script << std::endl;
	script << "  BAK='fixbb$$.ps'" << std::endl;
	script << std::endl;
	script << "  # Get the correct BoundingBox from ghostscript." << std::endl;
	script << "  bbox=`gs ${GSOPTS} ${FILE} 2>&1 | awk \"$AWKPARSEBB\"`" << std::endl;
	script << std::endl;
	script << "  case $bbox in" << std::endl;
	script << "    \"99999 99999 0 0\")" << std::endl;
	script << "\techo \"Bounding box was NOT determined (empty pages or Ghostscript >= 5.50 required)\"" << std::endl;
	script << "\texit ;;" << std::endl;
	script << "  esac" << std::endl;
	script << "" << std::endl;
	script << "  # Save the old version of the file" << std::endl;
	script << "  mv $FILE $BAK" << std::endl;
	script << std::endl;
	script << "  # Create the new version, replacing the old BoundingBox" << std::endl;
	script << "  awk \"$AWKPROG\" $BAK > $FILE" << std::endl;
	script << std::endl;
	script << "  case `uname` in " << std::endl;
	script << "    \"OS/2\") dos2unix $FILE ;;" << std::endl;
	script << "  esac" << std::endl;
	script << std::endl;
	script << "  # delete the backup file" << std::endl;
	script << "  rm -f $BAK" << std::endl;
	script << std::endl;
	script << "done" << std::endl;

	return script.str();
}

void Visualizer::saveModelCrossfitting() {

	Landscape landscape = this->comparison.getLandscape();
	DeltaGOFsHistograms deltaGOFsHistograms = this->comparison.getDeltaGOFsHistograms();

	size_t sep = this->fileOut.find_last_of("\\/");
	std::string filenameOnly;
	if (sep != std::string::npos) {
		filenameOnly = this->fileOut.substr(sep + 1, this->fileOut.size() - sep - 1);
	} else {
		filenameOnly = this->fileOut;
	}

	std::string data1Path = this->fileOut + "_data_" + landscape.model1.getMethod() + ".csv";
	std::ofstream data1File = openFile(data1Path.c_str(), true);

	std::string data2Path = this->fileOut + "_data_" + landscape.model2.getMethod() + ".csv";
	std::ofstream data2File = openFile(data2Path.c_str(), true);

	assert(landscape.model1Data.size() == landscape.model2Data.size());
	// csv header
	for (size_t h = 0; h < landscape.model1Data[0].size(); h++) {
		data1File << "rating" << h <<  ", ";
		data2File << "rating" << h <<  ", ";
	}
	data1File << landscape.model1.getMethod() << "_fit, " << landscape.model2.getMethod() << "_fit" << std::endl;
	data2File << landscape.model1.getMethod() << "_fit, " << landscape.model2.getMethod() << "_fit" << std::endl;

	// csv content
	assert(landscape.model1Data.size() == landscape.model1GOF_model1Data.size());
	assert(landscape.model1Data.size() == landscape.model1GOF_model2Data.size());
	assert(landscape.model1Data.size() == landscape.model2GOF_model1Data.size());
	assert(landscape.model1Data.size() == landscape.model2GOF_model2Data.size());
	for (size_t set = 0; set < landscape.model1Data.size(); set++) {
		assert(landscape.model1Data[set].size() == landscape.model2Data[set].size());
		for (size_t point = 0; point < landscape.model1Data[set].size(); point++) {
			data1File << landscape.model1Data[set][point] << ", ";
			data2File << landscape.model2Data[set][point] << ", ";
		}
		data1File << landscape.model1GOF_model1Data[set] << ", " << landscape.model2GOF_model1Data[set] << std::endl;
		data2File << landscape.model1GOF_model2Data[set] << ", " << landscape.model2GOF_model2Data[set] << std::endl;
	}

	data1File.close();
	data2File.close();

	std::string hist1Path = this->fileOut + "_hist_" + landscape.model1.getMethod();
	std::string hist1filenameOnly = filenameOnly + "_hist_" + landscape.model1.getMethod();

	std::string hist2Path = this->fileOut + "_hist_" + landscape.model2.getMethod();
	std::string hist2filenameOnly = filenameOnly + "_hist_" + landscape.model2.getMethod();

	std::ofstream gnuplotHistogramsFile = openFile(this->fileOut + "_plotHistograms");
	gnuplotHistogramsFile << "set terminal cairolatex" << std::endl;
	gnuplotHistogramsFile << "set monochrome" << std::endl;
	gnuplotHistogramsFile << "set output 'img/" << filenameOnly << "_histograms.tex'" << std::endl;
	gnuplotHistogramsFile << "set xlabel '$\\Delta$ GOFs = GOF " << deltaGOFsHistograms.model1.getMethod() << " - GOF " << deltaGOFsHistograms.model2.getMethod() << "' offset 0,-4" << std::endl;
	gnuplotHistogramsFile << "set ylabel 'Density' offset -2,0" << std::endl;
	gnuplotHistogramsFile << "set xtics offset 0,-0.5 rotate" << std::endl;
	gnuplotHistogramsFile << "#set xrange[:]" << std::endl;
	gnuplotHistogramsFile << "#set yrange[:]" << std::endl;
	gnuplotHistogramsFile << "#set title 'Histogram of Differences in GOFs'" << std::endl;
	gnuplotHistogramsFile << "set style fill transparent solid 0.6" << std::endl;
	gnuplotHistogramsFile << "set key left Left reverse spacing 3.0 title 'data generator' at graph 0.11,0.925" << std::endl;
	gnuplotHistogramsFile << "set grid" << std::endl;
	gnuplotHistogramsFile << "plot '" << hist1filenameOnly << "' w boxes title '" << deltaGOFsHistograms.model1.getMethod() << "' fillstyle pattern 3 linetype 1, '" << hist2filenameOnly << "' w boxes title '" << deltaGOFsHistograms.model2.getMethod() << "' fillstyle pattern 0 linetype 1" << std::endl;
	gnuplotHistogramsFile << "set output" << std::endl;
	gnuplotHistogramsFile.close();

	std::string landscapeModel1path = this->fileOut + "_landscape_" + landscape.model1.getMethod();
	std::string landscapeModel1filenameOnly = filenameOnly + "_landscape_" + landscape.model1.getMethod();
	this->model = landscape.model1; // for proper generalInfo output in the file
	std::ofstream landscapeModel1File = openFile(landscapeModel1path.c_str());

	std::string landscapeModel2path = this->fileOut + "_landscape_" + landscape.model2.getMethod();
	std::string landscapeModel2filenameOnly = filenameOnly + "_landscape_" + landscape.model2.getMethod();
	this->model = landscape.model2; // for proper generalInfo output in the file
	std::ofstream landscapeModel2File = openFile(landscapeModel2path.c_str());

	landscapeModel1File << "# " << landscape.model1.getMethod() << " generated data" << std::endl;
	landscapeModel1File << "# "<< landscape.model1.getMethod() << " fit, "<< landscape.model2.getMethod() << " fit" << std::endl;

	landscapeModel2File << "# "<< landscape.model2.getMethod() << " generated data" << std::endl;
	landscapeModel2File << "# "<< landscape.model1.getMethod() << " fit, "<< landscape.model2.getMethod() << " fit" << std::endl;

	assert (landscape.model1GOF_model1Data.size() == landscape.model1GOF_model2Data.size());
	assert (landscape.model1GOF_model2Data.size() == landscape.model2GOF_model1Data.size());
	assert (landscape.model2GOF_model1Data.size() == landscape.model2GOF_model2Data.size());

	for (size_t x = 0; x < landscape.model1GOF_model1Data.size(); x++) {
		landscapeModel1File << landscape.model1GOF_model1Data[x] << ", " << landscape.model2GOF_model1Data[x] << std::endl;
		landscapeModel2File << landscape.model1GOF_model2Data[x] << ", " << landscape.model2GOF_model2Data[x] << std::endl;
	}

	landscapeModel1File.close();
	landscapeModel2File.close();

	this->model = landscape.model1; // for proper generalInfo output in the file
	std::ofstream hist1File = openFile(hist1Path);
	hist1File << std::endl;
	hist1File << "# Histogram 1 of Delta GOFs" << std::endl;
	hist1File << "# data generated by " << deltaGOFsHistograms.model1.getMethod() << std::endl;
	hist1File << "# GOF " << deltaGOFsHistograms.model1.getMethod() << " - GOF " << deltaGOFsHistograms.model2.getMethod() << "; # of GOFs in this range" << std::endl;
	hist1File << deltaGOFsHistograms.histogram_model1Data << std::endl;
	hist1File.close();

	this->model = landscape.model2; // for proper generalInfo output in the file
	std::ofstream hist2File = openFile(hist2Path);
	hist2File << std::endl;
	hist2File << "# Histogram 2 of Delta GOFs" << std::endl;
	hist2File << "# data generated by " << deltaGOFsHistograms.model2.getMethod() << std::endl;
	hist2File << "# GOF " << deltaGOFsHistograms.model1.getMethod() << " - GOF " << deltaGOFsHistograms.model2.getMethod() << "; # of GOFs in this range" << std::endl;
	hist2File << deltaGOFsHistograms.histogram_model2Data << std::endl;
	hist2File.close();

	this->model = landscape.model1; // for proper generalInfo output in the file
	std::ofstream gnuplotLandscapeFileModel1 = openFile(this->fileOut + "_plotLandscape_" + landscape.model1.getMethod());
	gnuplotLandscapeFileModel1 << "set terminal cairolatex" << std::endl;
	gnuplotLandscapeFileModel1 << "set monochrome"<< std::endl;
	gnuplotLandscapeFileModel1 << "set output 'img/" << filenameOnly << "_landscape_" << landscape.model1.getMethod() << ".tex' "<< std::endl;
	gnuplotLandscapeFileModel1 << "set xlabel '"<< landscape.model1.getMethod() << "-fit (nRMSE)' offset 0,-0.5" << std::endl;
	gnuplotLandscapeFileModel1 << "set ylabel '"<< landscape.model2.getMethod() << "-fit (nRMSE)' offset -5.0,0" << std::endl;
	gnuplotLandscapeFileModel1 << "set style fill transparent solid 0.1 noborder" << std::endl;
	gnuplotLandscapeFileModel1 << "#deactivate x-tics at the bottom" << std::endl;
	gnuplotLandscapeFileModel1 << "set format x ''" << std::endl;
	gnuplotLandscapeFileModel1 << "set format x2 '%.2f'" << std::endl;
	gnuplotLandscapeFileModel1 << "set format y '%.2f'" << std::endl;
	gnuplotLandscapeFileModel1 << "set x2tics rotate" << std::endl;
	gnuplotLandscapeFileModel1 << "#set ytics 0.06, 0.04, 0.22" << std::endl;
	gnuplotLandscapeFileModel1 << "set grid" << std::endl;
	gnuplotLandscapeFileModel1 << "#set title '"<< landscape.model1.getMethod() << " generated data'" << std::endl;
	gnuplotLandscapeFileModel1 << "unset key" << std::endl;
	gnuplotLandscapeFileModel1 << "#set xrange[:]" << std::endl;
	gnuplotLandscapeFileModel1 << "#set x2range[:]" << std::endl;
	gnuplotLandscapeFileModel1 << "#set yrange[:]" << std::endl;
	gnuplotLandscapeFileModel1 << "#comment the following lines when changing the ranges!" << std::endl;
	gnuplotLandscapeFileModel1 << "stats '"<< landscapeModel1filenameOnly << "' using 1:2 name 'm1'" << std::endl;
	gnuplotLandscapeFileModel1 << "min = (m1_min_y < m1_min_x ? m1_min_y : m1_min_x)" << std::endl;
	gnuplotLandscapeFileModel1 << "max = (m1_max_y > m1_max_x ? m1_max_y : m1_max_x)" << std::endl;
	gnuplotLandscapeFileModel1 << "sc = 1.01" << std::endl;
	gnuplotLandscapeFileModel1 << "set xrange[sc*min:sc*max]" << std::endl;
	gnuplotLandscapeFileModel1 << "set yrange[sc*min:sc*max]" << std::endl;
	gnuplotLandscapeFileModel1 << "set size square" << std::endl;
	gnuplotLandscapeFileModel1 << "plot '" << landscapeModel1filenameOnly << "' w circles, x, 'GOFs_" << landscape.model1.getMethod() << "_vs_" << landscape.model2.getMethod() << ".data' pointtype 3 pointsize 2" << std::endl;
	gnuplotLandscapeFileModel1 << "set output" << std::endl;
	gnuplotLandscapeFileModel1 << "#pause -1" << std::endl;
	gnuplotLandscapeFileModel1.close();

	this->model = landscape.model2; // for proper generalInfo output in the file
	std::ofstream  gnuplotLandscapeFileModel2 = openFile(this->fileOut + "_plotLandscape_" + landscape.model2.getMethod());
	gnuplotLandscapeFileModel2 << "set terminal cairolatex" << std::endl;
	gnuplotLandscapeFileModel2 << "set monochrome" << std::endl;
	gnuplotLandscapeFileModel2 << "set output 'img/" << filenameOnly << "_landscape_" << landscape.model2.getMethod() << ".tex' "<< std::endl;
	gnuplotLandscapeFileModel2 << "set xlabel '"<< landscape.model1.getMethod() << "-fit (nRMSE)' offset 0,-0.5" << std::endl;
	gnuplotLandscapeFileModel2 << "set ylabel '"<< landscape.model2.getMethod() << "-fit (nRMSE)' offset -5.0,0" << std::endl;
	gnuplotLandscapeFileModel2 << "#deactivate x-tics at the bottom" << std::endl;
	gnuplotLandscapeFileModel2 << "set format x ''" << std::endl;
	gnuplotLandscapeFileModel2 << "set format x2 '%.2f'" << std::endl;
	gnuplotLandscapeFileModel2 << "set format y '%.2f'" << std::endl;
	gnuplotLandscapeFileModel2 << "set x2tics rotate" << std::endl;
	gnuplotLandscapeFileModel2 << "set style fill transparent solid 0.1 noborder" << std::endl;
	gnuplotLandscapeFileModel2 << "#set ytics 0.06, 0.04, 0.22" << std::endl;
	gnuplotLandscapeFileModel2 << "set grid" << std::endl;
	gnuplotLandscapeFileModel2 << "#set title '"<< landscape.model2.getMethod() << " generated data'" << std::endl;
	gnuplotLandscapeFileModel2 << "unset key" << std::endl;
	gnuplotLandscapeFileModel2 << "#set xrange[:]" << std::endl;
	gnuplotLandscapeFileModel2 << "#set x2range[:]" << std::endl;
	gnuplotLandscapeFileModel2 << "#set yrange[:]" << std::endl;
	gnuplotLandscapeFileModel2 << "#comment the following lines when changing ranges!" << std::endl;
	gnuplotLandscapeFileModel2 << "stats '"<< landscapeModel2filenameOnly << "' using 1:2 name 'm2'" << std::endl;
	gnuplotLandscapeFileModel2 << "min = (m2_min_y < m2_min_x ? m2_min_y : m2_min_x)" << std::endl;
	gnuplotLandscapeFileModel2 << "max = (m2_max_y > m2_max_x ? m2_max_y : m2_max_x)" << std::endl;
	gnuplotLandscapeFileModel2 << "sc = 1.01" << std::endl;
	gnuplotLandscapeFileModel2 << "set xrange[sc*min:sc*max]" << std::endl;
	gnuplotLandscapeFileModel2 << "set yrange[sc*min:sc*max]" << std::endl;
	gnuplotLandscapeFileModel2 << "set size square" << std::endl;
	gnuplotLandscapeFileModel2 << "plot '" << landscapeModel2filenameOnly << "' w circles, x, 'GOFs_" << landscape.model1.getMethod() << "_vs_" << landscape.model2.getMethod() << ".data' pointtype 3 pointsize 2" << std::endl;
	gnuplotLandscapeFileModel2 << "set output" << std::endl;
	gnuplotLandscapeFileModel2 << "#pause -1" << std::endl;
	gnuplotLandscapeFileModel2.close();
}

void Visualizer::saveOctaveVector(){
	std::ofstream resultFile = openFile(this->fileOut, true);

	std::vector<double> ratings = this->model.getRatings();

	for(size_t i = 0; i < ratings.size() - 1 ; i++){
		resultFile << ratings[i] << " ";
	}

	resultFile << ratings[ratings.size() - 1] << std::endl;

	resultFile.close();
}
