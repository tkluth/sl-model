/*
 * visualizer.h
 *
 * This file is part of sl-model:
 * https://www.gitlab.com/tkluth/sl-model
 *
 * Copyright(c) 2014-2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 *
 * From October 2014 to September 2018, the
 * development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 *
 */

#ifndef VISUALIZER_H
#define VISUALIZER_H

#include "referenceObject.h"
#include "model.h"
#include "comparison.h"

#include <iostream>

/**
 * This class visualizes the results obtained by \ref Model. Both by plotting them via gnuplot and saving them to various files.
 */

class Visualizer {

	public:
		/**
		 * Constructor of the Visualizer class.
		 * @param modelresult an object of the type Model that contains all information that should be visualized
		 * @param comp an instance of a Comparison class that contains information about model evaluation that should be visualized
		 * @param empData a vector that contains the LO(s) and RO(s) used to compute the ratings.
		 * @param referenceObjs the same as locatedObjects but for the RO(s) used.
		 * @param fileO a std::string containing a filename that gets created and where the output is stored. Furthermore, more files are created with suffixes, such as "<fileOut>".gnuplotData, depending on the visualization function used.
		 * @param fileD a std::string containing the path of the data file used for computations. Used to print general information about the current run of the program.
		 * @param filesRO a vector of strings with the path(s) of files were the RO(s) are stored in. Used to print general information about the current run of the program
		 * @param filesLO a vector of strings with the path(s) of files were the LO(s) are stored in. Used to print general information about the current run of the program.
		 * @param filesFunction a vector of strings with the path(s) of files were the functional parts of the RO(s) are stored in. Used to print general information about the current run of the program.
		 * @param p whether to plot at all (used only in some methods)
		 * @param c whether the plot should be in colors (true) or in greyscale (false)
		 * @param p3D whether the plotz should be in 3D (true) or in 2D (false
		 * @param ltx whether the plot should be displayed (false) or be saved in a .tex and a .eps file (true)
		 * @param ltxFigureName the name of the .tex and .eps file that gets saved in the folder img. Only gets considered if latex is set to true)
		 * */
		Visualizer(Model modelresult, Comparison comp, std::vector<RatingDataPoint> empData, std::vector<std::shared_ptr<ReferenceObject> > referenceObjs, std::string fileO, std::string fileD, std::vector<std::string> filesRO, std::vector<std::string> filesLO, std::vector<std::string> filesFunction,
				bool p, bool c, bool p3D, bool ltx, std::string ltxFigureName) :
				model(modelresult),
				comparison(comp),
				referenceObjects(referenceObjs),
				empiricalData(empData),
				fileOut(fileO),
				datafile(fileD),
				roFiles(filesRO),
				loFiles(filesLO),
				functionfiles(filesFunction),
				plot(p),
				color(c),
				plot3D(p3D),
				latex(ltx),
				latexFigureName(ltxFigureName)
				{
					// empty constructor
				}

		/**
		 * This method saves the results for non-ordinal models in the file \ref fileOut. Also, a plot.command file is saved.
		 * If the parameter plot is set to true, the command `gnuplot plot.command` is run which then visualizes
		 * either a spatial template or a single vectorDestination (with or without attentional distribution and/or vectors).
		 * You may also run gnuplot `plot.command` on your own.
		 */
		void visualizeTemplate();

		/**
		 * This method saves the results for ordinal models in the file \ref fileOut. Also, a plot.command file is saved.
		 * If the parameter plot is set to true, the command `gnuplot plot.command` is run which then visualizes
		 * the empirical proportions as well as the estimated probabilites.
		 * You may also run gnuplot `plot.command` on your own.
		 * */
		void visualizeRatingDist();

		/**
		 * writes and calls an R script that visualizes the outcome of the
		 * posterior estimation using several diagnostic plots with the
		 * help of the R package `ggmcmc`.
		 * */
		void visualizePosterior(unsigned int noOfChains);

		/**
		 * saves parameter estimation results to the file fileOut
		 */
		void saveResultsPE();

		/**
		 * uses gnuplot to visualize a plot of the error of the model
		 * as a function of iteration after parameter estimation
		 * */
		void plotErrorFunction();

		/**
		 * saves the results of the MFA in human-readable format
		 */
		 void saveResultsMFA();

		/**
		 * This method saves the results of a model comparison with the PBCM or landscaping method (see \ref Comparison)
		 * Therefore, it creates four data files:
		 * - Two data files contain histograms of the differences of GOFs (ending: _hist1 and _hist2)
		 * - Two data files contain the landscapes of the GOFs; i.e., the GOF of each model for one generated rating (endings: _landscape1 and _landscape2)
		 * Furthermore, two files with plottings commands for gnuplot are created (endings _plotHistograms, _plotLandscape1, and _plotLandscape2)
		 * All files start with \ref fileOut
		 */
		void saveModelCrossfitting();

		/**
		 * saves simple hold-out results to the file fileOut.
		 * To this end, the results are obtained by calling the corresponding getters from Comparison.
		 * Furthermore, this method generates an R script and calls R with this script (name: fileOut+".Rcommand")
		 * The script computes the medians and means and the corresponding bootstrap confidence intervals with the help of the R package boot.
		 * @param noOfThreads the number of cores that are used by the boot package
		 */
		void saveResultsSHO(unsigned int noOfThreads);

		/**
		 * saves a vector of ratings in the file fileOut to be read by GNU Octave or Matlab
		 */
		void saveOctaveVector();

	private:

		/** Model object that contains generated ratings, computed spatial templates, vectors to visualize, etc. */
		Model model;
		/** Comparison object that contains the results for landscaping, PBCM and SHO */
		Comparison comparison;
		/** all used ROs */
		std::vector<std::shared_ptr<ReferenceObject> > referenceObjects;
		/** the empirical data that was used */
		std::vector<RatingDataPoint> empiricalData;

		/** path to the file results are saved to */
		std::string fileOut;
		/** path to the file that contains the used empirical data */
		std::string datafile;
		/** paths to the files that contain the used RO(s) */
		std::vector<std::string> roFiles;
		/** paths to the files that contain the used LO positions */
		std::vector<std::string> loFiles;
		/** paths to the files that contain the used functional parts */
		std::vector<std::string> functionfiles;

		/** if true, plots are produced (only used in some methods) */
		bool plot;
		/** if true, plots are in color (otherwise in greyscale) */
		bool color;
		/** if true plots are in 3D (otherwise in 2D) */
		bool plot3D;
		/** if true, plots are saved to a .tex and a .eps file to be used in a LaTeX document */
		bool latex;
		/** the name of the .tex and .eps files */
		std::string latexFigureName;

		/**
		 * a function that opens a file, and writes general information to it.
		 * this function throws an exeption if something went wrong with the file opening.
		 * is used by all save* function.
		 * @param fileToOpen the file to open. It is recommended to use \ref fileOut here, because this is set to what the user wants
		 * @param quiet if set to true, the default general information will not be printed
		 * @returns an std::ofstream of the opened file including already the general information
		 * */
		std::ofstream openFile(std::string fileToOpen, bool quiet=false);

		/**
		 * @returns a std::string that contains all relevant information about the current run of the program (such as: which data file was used, which model variant was used, which LO files were used etc.)
		 * */
		std::string printGeneralInfo();

		/**
		 * @returns source code of a bash script to fix the bounding box of gnuplot generated eps files. Source is Copyright Jeff Spirko / Petr Mikulik and licensed under the GPL.
		 * */
		 std::string fixGnuplotBoundingBoxScript();
};

#endif /* VISUALIZER_H */
