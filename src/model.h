/*
 * model.h
 *
 * This file is part of sl-model:
 * https://www.gitlab.com/tkluth/sl-model
 *
 * Copyright(c) 2014-2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 *
 * From October 2014 to September 2018, the
 * development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 *
 */

#ifndef MODEL_H
#define MODEL_H
#include "referenceObject.h"
#include "locatedObject.h"
#include "utilities.h"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Boolean_set_operations_2.h>
#include <CGAL/Polygon_2.h>

// GNU Scientific Library
#include <gsl/gsl_matrix.h>
// Random Numbers
#include <gsl/gsl_rng.h>

#include <armadillo>

#include <memory>
#include <iostream>
#include <sstream>
#include <time.h>

#pragma GCC diagnostic push
// turn off warnings for RInside
#pragma GCC diagnostic ignored "-Wfloat-equal"
#pragma GCC diagnostic ignored "-Wshadow"
#pragma GCC diagnostic ignored "-Wdeprecated"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wfloat-conversion"
#pragma GCC diagnostic ignored "-Wswitch-enum"
#pragma GCC diagnostic ignored "-Wswitch-default"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wcast-function-type"
#if defined(__clang__) || __GNUC__ >= 7
#pragma GCC diagnostic ignored "-Wimplicit-fallthrough"
#endif
// for the embedded R via RInside
#include <RInside.h>
#pragma GCC diagnostic pop

typedef CGAL::Exact_predicates_inexact_constructions_kernel CGALKernel;
typedef CGAL::Point_2<CGALKernel> Point;
typedef CGAL::Segment_2<CGALKernel> Segment;
typedef CGALKernel::Vector_2 Vector;
typedef CGALKernel::Direction_2 Direction;
typedef CGAL::Polygon_2<CGALKernel> Polygon;
typedef CGAL::Polygon_with_holes_2<CGALKernel> Polygon_with_holes;

/**
 * This class contains the main AVS-model formulas and (and their modifications).
 * It handles all necessary computations and data structures.
 */
class Model {

public:

	/**
	 * This is an empty constructor that calls the parameterized constructor with values that make up an invalid Model instance.
	 * */
	Model();

	/**
	 * Use this method to construct a Model() object. The model parameters have default values, if not set otherwise.
	 * @param empData the empirical data, see \ref RatingDataPoint
	 * @param noROs the number of ROs in the empirical data
	 * @param ratingScale the lowest and highest possible rating
	 * @param widthLimitLow the lowest x coordinate for the \ref display. It is computed, if this parameter is set to std::numeric_limits<int>::min()
	 * @param widthLimitHigh the highest x coordinate for the \ref display. It is computed, if this parameter is set to std::numeric_limits<int>::max()
	 * @param heightLimitLow the lowest y coordinate for the \ref display. It is computed, if this parameter is set to std::numeric_limits<int>::min()
	 * @param heightLimitHigh the highest y coordnnate for the \ref display. It is computed, if this parameter is set to std::numeric_limits<int>::max()
	 * @param printRs whether the ratings should be printed
	 * @param m the model that should be used for computation. See \ref AVSmethod for possible options.
	 * @param ord if true and proportions of empirical ratings are given (via ratings.data_prop, see \ref Parser.cpp), the model will compute probabilities of each rating (aka ordinal regression)
	 * @param posteriorEstimation if true, posterior distribution is estimated (instead of estimating best fitting parameters)
	 * @param informativePriors sets \ref informativePriors, see there for more details
	 * @param pOnly only considered, if estimating the posterior distribution: if true, only the prior distribution is computed (likelihood is ignored)
	 * @param lOnly only considered, if estimating the posterior distribution: if true, only the likelihood is computed (prior is ignored)
	 * @param noChangeOrd only considered, if in ordinal mode: if true, ordinal parameters are not changed during parameter estimation
	 * @param threads number of threads to be spawned. Currently not in use.
	 * @param vLambda value for the attentional width parameter
	 * @param vSlope value for slope of the linear deviation function \ref g(), measured in change _per radian_, not _per degree_
	 * @param vIntercept value for intercept of the linear deviation function \ref g()
	 * @param vHighgain value for gain on the sigmoid function used to compute the height component (see \ref height())
	 * @param vPhi value for parameter for the importance of the functional part. Is only used by model variants that consider functionality.
	 * @param vAlpha value for parameter that controls for the importance of either center-of-mass orientation or proximal orientation in the rAVS-w-comb model
	 * @param vLrgain value for gain on left-right sigmoids in the BB model
	 * @param vLrexp value for exponent for the left-right sigmoids in the BB model
	 * @param vSigmaOrd only used in ordinal model: value for the standard deviation of the latent Gaussian distribution
	 * @param vThresholds only used in ordinal model: values of the thresholds for the latent Gaussian distribution. The vector should have length K - 1 - 2, where K is the number of possible ratings. This is only checked in debug mode.
	 * */
	 Model(std::vector<RatingDataPoint> empData, unsigned int noROs, std::pair<unsigned int, unsigned int> ratingScale,
	 int widthLimitLow, int widthLimitHigh, int heightLimitLow, int heightLimitHigh, bool printRs,
	 AVSmethod m, bool ord, bool posteriorEstimation, std::string informativePriors, bool pOnly, bool lOnly, bool noChangeOrd, unsigned int threads,
	 double vLambda = 0.512, double vSlope = -0.4010705, double vIntercept = 1.224, double vHighgain = 0.002, double vPhi = 0.6804, double vAlpha = 0.322, double vLrgain = 0.065, double vLrexp = 0.22, double vSigmaOrd = 2.72304, std::vector<double> vThresholds = {3.07496, 3.95202, 4.47031, 5.10299, 5.72637, 6.80488});

	// model evaluation methods:

	/**
	 * This method either:
	 * - finds best fitting model parameters with the simulated annealing method (by minimizing the nRMSE or, if in ordinal mode, the Kullback-Leiber divergence) or
	 * - estimates the posterior estimation using the Metropolis algorithm (enhanced by the adaptations proposed by \cite Garthwaite2016) by multiplying the likelihood with the prior.
	 *
	 * The global variable \ref posterior controls which of the two estimation algorithms is used.
	 * @param data the (empirical) data that the model should fit.
	 * @param print controls wether best parameters and correlation info should be printed to std::out
	 * @returns either the lowest error (nRMSE or Kullback-Leibler, if in ordinal mode) or the highest value of the posterior distribution
	 * */
	double parameterEstimation(std::vector<RatingDataPoint> data, bool print);

	/**
	 * Computes a value denoting how well the model-generated data fit to the empirical data.
	 * Depending on the mode the Model is run in, this means different things.
	 * If in error minimizing mode, this method returns a GOF; either the normalized Root Mean Square Error (nRMSE) or the Kullback-Leibler divergence (if in ordinal mode) via \ref compareResults() or \ref compareDistResults().
	 * If in posterior estimation mode, this method returns the current posterior value by using the cross-match test \cite Rosenbaum2005
	 * as likelihood function (see \ref crossmatch()).
	 * To do so, this method computes ratings for the given positions by calling \ref rate(), \ref rateProbs() (ordinal model) or \ref generateRatings() (posterior estimation). The ratings are stored in the variable \ref computedRatings.
	 * If current model parameters are not in the parameter ranges (as checked by \ref areParamsInBoundaries()), this method returns the worst value:
	 * Either infinity if error is minimized or 0.0 if posterior is estimated.
	 * @param data (empirical) data for the same LOs as the to-be-computed ratings, used to compute the GOF by calling \ref compareResults().
	 * @param printCorrelation if true (and \ref VERBOSITY has an appropriate value), the correlation between computed results and data is printed on std::out.
	 * @returns a GOF (nRMSE or KL-divergence) or a likelihood value (depending on the mode; see above)
	 * */
	double computeResults(std::vector<RatingDataPoint>* data, bool printCorrelation);

	/**
	 * computes only the ratings by calling \ref computeResults(). Prints nothing to std::out.
	 * RO(s) and LO(s) must be set beforehands (see \ref setEmpiricalData())!
	 * */
	void computeOnlyRatings();

/**
	 * This method generates ratings with the current model parameters.
	 * Used as part of the landscaping method \cite Navarro2004 and the posterior estimation.
	 * If in ordinal mode, individual ratings are sampled from the current probability distribution over all ratings.
	 * If in not-ordinal mode, mean ratings are computed.
	 * @param noOfIndividuals used only in ordinal mode: how many individual ratings should be sampled for each LO
	 * @param noOfSamples used only in ordinal mode: now many times to sample for a single individual rating; the rounded mean is then the generated rating
	 * @returns the generated ratings in the same format as \ref empiricalData
	 * */
	std::vector<RatingDataPoint> generateRatings(unsigned int noOfIndividuals=0, unsigned int noOfSamples=0);

	// visualization methods:

	/**
	 * Prepares \ref display so that the given RO-LO configuration(s) can be visualized appropriately by \ref Visualizer.
	 * @param visualizeVectors if this is true, vectors rooted at the RO and weighted by attention will be calculated
	 * @param longVectorScaling if this is bigger than zero, the vector sum (i.e., the final direction) will be visualized
	 * @param visualizeAttention if this is true the attentional distribution assumed by the actual model will be computed
	 * @param LOtoVisualize the index of the LO that should be used for computing the visualization
	 */
	void visualize(bool visualizeVectors, double longVectorScaling, bool visualizeAttention, unsigned int LOtoVisualize);

	/**
	 * prints information about the current settings of the model simulation to std::cout.
	 * @param print if set to false, only the std::string is returned and not printed to std:std::cout
	 * @returns a human readable std::string with current settings
	 * */
	std::string printInformation(bool print);

	// setter:

	/**
	 * sets the empirical data (with its corresponding rating scale) the model is compared to.
	 * @param empirical the data.
	 * @param noROs the number of ROs in use
	 * @param ratingScale the rating scale used for the data. First number is the minimal, second number the maximal rating.
	 * */
	void setEmpiricalData(std::vector<RatingDataPoint> empirical, unsigned int noROs, std::pair<unsigned int, unsigned int> ratingScale);

	/**
	 * Sets the method / model to be used for computation, as well as the values for the model parameters.
	 * If model parameters are not specified, default values are used (mostly from Regier & Carlson, 2001, fits).
	 * Default parameter ranges are hard-coded in this function as follows:
	 * \f[ 0.001 \leq \lambda / alpha \leq 5 \f]
	 * \f[-1/45 \leq slope \leq 0 \f]
	 * \f[0.7 \leq intercept \leq 1.3 \f]
	 * \f[0 \leq highgain \leq 10 \f]
	 * \f[0 \leq phi \leq 2 \f]
	 * \f[0 \leq lrgain \leq 5 \f]
	 * \f[0 \leq lrexp \leq 5 \f]
	 * The slope parameter is internally handled in changes _per radian_, not _per degree_, so the parameter range should actually read like:
	 * \f[ \frac{1}{45 * \pi / 180} = -1.27324 \leq slope \leq 0 \f]
	 * Model parameters might be overwritten by some methods (e.g., obviously \ref parameterEstimation()).
	 * @param m the model that should be used for computation. See \ref AVSmethod for possible options.
	 * @param vLambda value for the attentional width parameter
	 * @param vSlope value for slope of the linear deviation function \ref g(), measured in change _per radian_, not _per degree_
	 * @param vIntercept value for intercept of the linear deviation function \ref g()
	 * @param vHighgain value for gain on the sigmoid function used to compute the height component (see \ref height())
	 * @param vPhi value for parameter for the importance of the functional part. Is only used by model variants that consider functionality.
	 * @param vAlpha value for parameter that controls for the importance of either center-of-mass orientation or proximal orientation in the rAVS-w-comb model
	 * @param vLrgain value for gain on left-right sigmoids in the BB model
	 * @param vLrexp value for exponent for the left-right sigmoids in the BB model
	 * @param vSigmaOrd only used in ordinal model: value for the standard deviation of the latent Gaussian distribution
	 * @param vThresholds only used in ordinal model: values of the thresholds for the latent Gaussian distribution. The vector should have length K - 1 - 2, where K is the number of possible ratings. This is only checked in debug mode.
	 * */
	void setMethod(AVSmethod m, double vLambda = 0.512, double vSlope = -0.4010705, double vIntercept = 1.224, double vHighgain = 0.002, double vPhi = 0.6804, double vAlpha = 0.322, double vLrgain = 0.065, double vLrexp = 0.22, double vSigmaOrd = 2.72304, std::vector<double> vThresholds = {3.07496, 3.95202, 4.47031, 5.10299, 5.72637, 6.80488});

	// getter:

	/**
	 * @returns the height of the \ref display
	 * */
	int getHeight();

	/**
	 * @returns the width of the \ref display
	 * */
	int getWidth();

	/**
	 * @returns the lowest y coordinate of the \ref display
	 * */
	int getYOrigin();

	/**
	 * @returns the lowest x coordinate of the \ref display
	 * */
	int getXOrigin();

	/**
	 * @returns the model variation in use as a std::string with the help of enumToString()
	 * */
	std::string getMethod();

	/**
	 * @returns whether the model is running in ordinal mode
	 * */
	bool getOrdinal();

	/**
	 * @returns whether the model is running in posterior estimation mode
	 * */
	bool getPosterior();

	/**
	 * @returns whether the model is using informative priors
	 * */
	std::string getInformativePriors();

	/**
	 * @returns whether the model is only estimating the prior
	 * */
	bool getPriorOnly();

	/**
	 * @returns whether the model is only estimating the likelihood
	 * */
	bool getLikelihoodOnly();

	/**
	 * @returns whether the ordinal parameters are not allowed to be changed during parameter estimation
	 * */
	bool getNoChangeOrdinal();

	/**
	 * @returns the results of the parameter estimation (i.e., the best parameters found) in a human readable form
	 * */
	std::string getResultsPE();

	/**
	 * @param p one of the following: "lambda", "slope", "intercept", "highgain", "alpha", "phi", "lrexp", "lrgain".
	 * @returns the \ref ModelParameter struct of the parameter specified by the argument. You can access the current value with returnedParam.value. Not every parameter is used by every model, the returned value is arbitrary for not-used parameters.
	 * */
	ModelParameter getParam(std::string p);

	/** @returns a vector containing the mean of the mean ratings and the values of the ordinal params (sigmaOrd and thresholds) */
	std::vector<double> getOrdinalParams();

	/**
	 * @returns the minimum of the rating scale
	 * */
	unsigned int getMinRating();

	/**
	 * @returns the meximum of the rating scale
	 * */
	unsigned int getMaxRating();

	/**
	 * @returns a vector of all computed ratings
	 * */
	std::vector<double> getRatings();

	/**
	 * @returns a vector with parameter names (string) and values (float) visited during the parameter estimation.
	 * Used for instance to save the posterior distribution.
	 * */
	 std::vector<std::map<std::string, float> > getVisitedParams();

	/**
	 * @returns a 2-dimensional vector of doubles -- the \ref display.
	 * In this \ref display either the attentional distribution or the spatial template (i.e., computed ratings) are stored --
	 * depending on the visualization option of the user.
	 * */
	std::vector<std::vector<double> > getDisplay();

	/**
	 * @returns \ref ratingDist, to be plotted by \ref Visualizer::visualizeRatingDist().
	 * */
	std::pair<std::vector<double>, std::vector<double> > getRatingDist();

	/**
	 * @returns a vector of attentional vectors that should be visualized.
	 * Each attentional vector has a starting point (2D CGAL point; first part of the pair)
	 * and a 2D CGAL Vector that defines the length and direction of the vector.
	 * If the user did not specify any of the vector visualization options, this function probably returns an empty vector.
	 * */
	std::vector<std::pair<Point, Vector> > getVectorsToVisualize();

	/**
	 * @returns the attentional focus as a 2D CGAL point
	 * */
	Point getFocusToVisualize();

private:

	// core AVS methods:

	/**
	 * Main method for the computation of the rating.
	 * Depending on the used model variation, this method:
	 * - computes the focus and attentional distribution by calling the appropriate function
	 * (\ref getFocus(), \ref getFocusOnLocatedObject(), \ref computeProximalPoint(), or \ref getFocusAtFunction())
	 * - calls the fitting \ref vectorSum() or \ref vectorSumLocatedObject() method
	 * - computes the deviation from \ref upward or \ref downward
	 * - multiplies the result of \ref height() with \ref g()
	 * @returns the rating
	 * @param input a \ref RatingDataPoint (i.e., a LO and a RO) for which a rating should be computed.
	 * @param focusIndex if several functional parts are used this is needed to obtain the correct focus
	 * @param visualizeAllVectors used for the call of \ref vectorSum() or \ref vectorSumLocatedObject().
	 * @param longVectorScaling used for the call of \ref vectorSum() or \ref vectorSumLocatedObject().
	 * */
	double rate(RatingDataPoint* input, unsigned int focusIndex, bool visualizeAllVectors, double longVectorScaling);

	/**
	 * This method calls \ref rate() and uses the output as the mean of a latent normal distribution
	 * to compute probabilites for each single rating (ordinal component).
	 * To do so, it uses the additional model parameters \ref sigmaOrd and the \ref thresholds.
	 * The input is the same as for \ref rate().
	 * @param input see \ref rate()
	 * @param focusIndex see \ref rate()
	 * @param visualizeAllVectors see \ref rate()
	 * @param longVectorScaling see \ref rate()
	 * @returns a vector with the size of the number of levels of the rating scale (maxRating - minRating + 1).
	 * Each item _i_ in this vector correspond to the estimated probability of rating _i_.
	 * */
	std::vector<double> rateProbs(RatingDataPoint* input, unsigned int focusIndex, bool visualizeAllVectors, double longVectorScaling);

	/**
	 * This is a wrapper function that calls either getFocusAbove() or getFocusBelow() depending on the preposition in use.
	 * @param lo the LO for which a focus should be computed
	 * @param ro the RO that should be used
	 * @param prep the preposition in use. depending on this argument either getFocusAbove() or getFocusBelow() is called
	 * @param focusIndex if several functional parts are used this is needed to obtain the correct focus
	 * @returns the attentional focus (a 2D CGAL point)
	 * */
	Point getFocus(LocatedObject* lo, std::shared_ptr<ReferenceObject> ro, preposition* prep, unsigned int focusIndex);

	/**
	 * Computes the focus for superior prepositions (see \ref getFocus()).
	 * */
	Point getFocusAbove(LocatedObject* lo, std::shared_ptr<ReferenceObject> ro, unsigned int focusIndex);

	/**
	 * Computes the focus for inferior prepositions (see \ref getFocus()).
	 * */
	Point getFocusBelow(LocatedObject* lo, std::shared_ptr<ReferenceObject> ro, unsigned int focusIndex);

	/**
	 * Computes the focus on the functional part by letting fall a perpendicular from the LO onto the functional part of the RO.
	 * @param lo the LO for which a focus should be computed
	 * @param ro the RO that should be used
	 * @param focusIndex if several functional parts are used this is needed to obtain the correct focus
	 * @returns the attentional focus (a 2D CGAL point)
	 * */
	Point getFocusAtFunction(LocatedObject* lo, std::shared_ptr<ReferenceObject> ro, unsigned int focusIndex);

	/**
	 * Computes the focus on the LO by letting fall a perpendicular from the RO to the LO.
	 * This method is used by the rAVS-variations to have an attentional focus on the LO
	 * (although the attentional distribution is of no importance for the rAVS-variations, as long as the amount of attention at the LO is > 0)
	 * @param lo the LO for which a focus should be computed
	 * @param ro the RO that should be used
	 * @param focusIndex if several functional parts are used this is needed to obtain the correct focus
	 * @returns the attentional focus (a 2D CGAL point)
	 * */
	Point getFocusOnLocatedObject(LocatedObject* lo, std::shared_ptr<ReferenceObject> ro, unsigned int focusIndex);

	/**
	 * Computes the proximal point on the RO (specified by roIndex) given the LO (specified by lo).
	 * @param lo the LO for which the proximal point is searched for
	 * @param ro the RO that should be used
	 * @returns the proximal (2D CGAL) point
	 * */
	Point computeProximalPoint(LocatedObject* lo, std::shared_ptr<ReferenceObject> ro);

	/**
	 * This function computes the height component of the AVS model and its variations.
	 * More precisely it computes this formula:
	 * \f[ height(y_{LO}) = \frac{sig(y - hightop, highgain) + sig(y - lowtop, 1)}{2} \f]
	 * @param y the y-coordinate of the LO
	 * @param ro the RO that should be used
	 * @param p the preposition in use
	 * @returns the value of the height component
	 * */
	double heightFunction(double y, std::shared_ptr<ReferenceObject> ro, preposition* p);

	/**
	 * Helper function for \ref heightFunction. Computes:
	 * \f[ sig(x, gain) = \frac{1}{1 + \exp(-x \cdot gain)} \f]
	 * @param x the x variable of the formula
	 * @param gain the gain parameter of the formula
	 * @returns sig(x, gain)
	 * */
	double sig(double x, double gain);

	/**
	 * The linear function that maps angular deviation to rating.
	 * \f[ g(angle) = intercept + slope \cdot angle \f]
	 * @param x the angular deviaton in radian
	 * @returns g(angle)
	 * */
	double g(double x);

	/**
	 * Alignment function of the PC model described in \cite Regier2001 : linear + sigmoid
	 * maps angular deviation onto  rating with this formula:
	 * \f[ f(angle) = (slope \cdot angle + intercept) \cdot sig(90 - angle, highgain) \f]
	 * This function is only used for the PC model but it might prove useful
	 * in the future as replacement of \ref g() and \ref heightFunction() for other models as well.
	 * @param x the angular deviation
	 * @returns f(angle)
	 * */
	double f(double x);

	/**
	 * Center function of the BB model.
	 * @param x position of the LO
	 * @param ro the RO that should be used
	 * @returns \f[ center(x) = \textrm{sig}(x - left, lrgain)^{lrexp} \cdot \textrm{sig}(right - x, lrgain)^{lrexp} \f]
	 * */
	double centerBB(int x, std::shared_ptr<ReferenceObject> ro);

	/**
	 * Computes and returns the center-of-mass orientation, i.e., the deviation of the vector connecting both c-o-m from upright vertical.
	 * @param lo the LO for which the center-of-mass orientation should be computed
	 * @param ro the RO that should be used
	 * @returns \f[ angle = acos\left( \frac{direction \cdot upward}{|direction|} \right) \cdot 180 / \pi \f]
	 * with direction being a vector that connects c-o-m of the LO with the c-o-m of the RO
	 * */
	double centerOfMassOrientation(LocatedObject* lo, std::shared_ptr<ReferenceObject> ro);

	/**
	 * Computes and returns the proximal orientation, i.e., the deviation of the vector connecting the c-o-m of the LO with the proximal point of the RO from upright vertical.
	 * @param lo the LO for which the proximal orientation should be computed
	 * @param ro the RO that should be used
	 * @returns \f[ angle = acos\left( \frac{direction \cdot upward}{|direction|} \right) \cdot 180 / \pi \f]
	 * with direction being a vector that connects c-o-m of the LO with the proximal point on the RO
	 * */
	double proximalOrientation(LocatedObject* lo, std::shared_ptr<ReferenceObject> ro);

	/**
	 * Computes the vector sum from the RO to the LO.
	 * @param attentionalFocus the focus of the underlying attentional distribution
	 * @param vectorDestination the point of the LO, where the vector should point to
	 * @param sigma the sigma parameter of the attentional distribution (distance LO - focus)
	 * @param visualizeAll if true, all vectors are pushed to \ref vectorsToVisualize and later on used by the \ref Visualizer to show all vectors
	 * @param longVectorScaling if bigger than zero, the sum of all vectors is pushed to \ref vectorsToVisualize, scaled with this parameter and later on used by the \ref Visualizer to show the final vector
	 * @param ro the RO that should be used
	 * @returns the sum of all weighted vectors from the RO to the LO
	 * */
	Vector vectorSum(Point attentionalFocus, Point* vectorDestination, double sigma, bool visualizeAll, double longVectorScaling, std::shared_ptr<ReferenceObject> ro);

	/**
	 * Computes a vector sum from the LO to the RO.
	 * This function does exactly the same as \ref vectorSum(), it only reverses the direction of each vector and puts the starting point on the LO.
	 * It resembles an "attentional spread" with this; multiple vectors are spreading from the LO to the RO.
	 * This function is only used by the rAVS-multiple model variation, which gives exactly the same rating as the AVS model
	 * (because it is only a conceptual difference between "attentional spreading" and "attentional focusing" but not a mathematical).
	 * @param attentionalFocus the focus of the underlying attentional distribution
	 * @param vectorDestination the point of the LO, where the vector should point to
	 * @param sigma the sigma parameter of the attentional distribution (distance LO - focus)
	 * @param visualizeAll if true, all vectors are pushed to \ref vectorsToVisualize and later on used by the \ref Visualizer to show all vectors
	 * @param longVectorScaling if bigger than zero, the sum of all vectors is pushed to \ref vectorsToVisualize, scaled with this parameter and later on used by the \ref Visualizer to show the final vector
	 * @param ro the RO that should be used
	 * @returns the sum of all weighted vectors from the RO to the LO
	 * */
	Vector vectorSumReversed(Point attentionalFocus, Point* vectorDestination, double sigma, bool visualizeAll, double longVectorScaling, std::shared_ptr<ReferenceObject> ro);

	/**
	 * Does the same as \ref vectorSum(), but reversed. Thus, this function computes the vector sum of the LO to one point on the RO.
	 * For rAVS-variations there is no vector _sum_, but only a single vector.
	 * @param attentionalFocus the focus of the underlying attentional distribution
	 * @param vectorDestination the point of the RO, where the vector should point to
	 * @param lo the LO for which the vector sum should be computed
	 * @param sigma the sigma parameter of the attentional distribution (distance LO - RO)
	 * @param visualizeAll if true, all vectors are pushed to \ref vectorsToVisualize and later on used by the \ref Visualizer to show all vectors
	 * @param longVectorScaling if bigger than zero, the sum of all vectors is pushed to \ref vectorsToVisualize, scaled with this parameter and later on used by the \ref Visualizer to show the final vector
	 * @returns the sum of all weighted vectors from the LO to the RO
	 * */
	Vector vectorSumLocatedObject(Point attentionalFocus, Point* vectorDestination, LocatedObject* lo, double sigma, bool visualizeAll, double longVectorScaling);

	// model evaluation methods:

	/**
	 * This method compares computed ratings with given (empirical) ratings and returns the normalized Root Mean Square Error (nRMSE),
	 * a measure of Goodness of Fit (GOF), of the two:
	 * \f[RMSE = \sqrt{\frac{1}{n}\sum_i^n(data_i - modelOutput_i)^2}\f]
	 * \f[nRMSE = \frac{RMSE}{rating_{max} - rating_{min}}\f]
	 * It is called from \ref computeResults() after ratings are computed.
	 * @returns the nRMSE of computed ratings and given (empirical) ratings
	 * @param modelResults computed ratings.
	 * @param empiricalResults (empirical) data for the same LOs as the computed ratings.
	 * @param printCorrelation if true (and \ref VERBOSITY has an appropriate value), the correlation between computed results and data is printed on std::out via \ref computeCorrelation()
	 * */
	double compareResults(std::vector<double>* modelResults, std::vector<RatingDataPoint>* empiricalResults, bool printCorrelation);

	/**
	 * Computes, returns and eventually prints (depends on the value \ref VERBOSITY) the correlation between two vectors of doubles.
	 * @param vector1 the first vector
	 * @param vector2 the second vector. This data structure is more complex, because this is the slot for the empirical data. In this method, vector2 is treated as a vector of doubles (i.e., a vector of doubles is extracted).
	 * @returns correlation between vector1 and vector2
	 * */
	double computeCorrelation(std::vector<double>* vector1, std::vector<RatingDataPoint>* vector2);

	/**
	 * Computes the Kullback-Leibler divergence between model generated ratings and empirical data, used in ordinal mode only.
	 * It is called from \ref computeResults() after ratings are computed.
	 * \f[ KL(modelData, empData) = \sum_{i,r} empData_{i,r} * \log\left(\frac{empData_{i,r}}{modelData_{i,r}}\right) \f]
	 * _i_ denotes the LO index (the number of LOs), _r_ denotes the rating index (running from minRating till maxRating).
	 * If \f$empData_{i,r}  = 0.0\f$, KL equals _0_ (first factor) and is not computed (because \f$\log(0)\f$ is not defined).
	 * \f$empData_{i,r} < 0.0\f$, should never happen (negative proportions in empirical data?).
	 * However, this is only checked by an assertion and thus will only stop the program when compiled in DEBUG mode.
	 * @param modelResults same as in \ref compareResults()
	 * @param empiricalResults same as in \ref compareResults()
	 * @param print prints human-readable output to std::out
	 * @returns the Kullback-Leibler divergence
	 * */
	double compareDistResults(std::vector<std::vector<double> >* modelResults, std::vector<RatingDataPoint>* empiricalResults, bool print);

	// methods for posterior estimation

	/**
	 * This function computes the covariance of matrix m.
	 * Part of the implementation of the adaptive MCMC algorithm proposed by \cite Garthwaite2016 (used for posterior estimation).
	 *
	 * The code is a modified version of sample code from the gsl mailing list:
	 * https://www.mail-archive.com/help-gsl@gnu.org/msg03071.html
	 * @param m matrix for which to compute the covariance matrix
	 * @param c the covariance matrix
	 * */
	void cov(const gsl_matrix* m, gsl_matrix* c);

	/**
	 * Updates the covariance matrix during MCMC.
	 * Based on R code from Yanan Fan, available from http://web.maths.unsw.edu.au/~yanan/RM.html
	 *
	 * Part of the implementation of the adaptive MCMC algorithm proposed by \cite Garthwaite2016 (used for posterior estimation),
	 * which in turn implements eq. (3) from \cite Haario2001.
	 * */
	void updateCov(gsl_matrix* thetaMean, gsl_matrix* temperatureMatrix, const gsl_matrix* currentParams, unsigned int iteration);

	/**
	 * Helper function preparing column means for updateCov().
	 *
	 * Part of the implementation of the adaptive MCMC algorithm proposed by \cite Garthwaite2016 (used for posterior estimation).
	 *
	 * @param m matrix for which to compute column means
	 * @param meansMatrix matrix in which column means are stored
	 * */
	void colMeans(const gsl_matrix* m, gsl_matrix* meansMatrix);

	/**
	 * This function updates the scaling factor of the "temperature" for the MCMC algorithm,
	 * i.e., the width of the proposal function.
	 *
	 * Part of the implementation of the adaptive MCMC algorithm proposed by \cite Garthwaite2016 (used for posterior estimation).
	 * Based on R code from Yanan Fan, available from http://web.maths.unsw.edu.au/~yanan/RM.html .
	 *
	 * @param acceptanceProbability the current acceptance probability
	 * @param pstar the optimal acceptance probability that the adapative algorithm should consider
	 * @param iteration the iteration of the MCMC algorithm
	 * @param c a constant that depends on the data, see \cite Garthwaite2016
	 * @returns a new value for tempChange
	 * */
	double updateSigma(double acceptanceProbability, double pstar, unsigned int iteration, double c);

	/**
	 * initializes the GSL copy of the temperatureMatrix with sensible values.
	 *
	 * Part of the implementation of the adaptive MCMC algorithm proposed by \cite Garthwaite2016 (used for posterior estimation).
	 *
	 * @param tm the temperature matrix; changed by this function
	 * */
	void initializeTemperatureMatrix(gsl_matrix* tm);

	/**
	 * Used in posterior estimation.
	 * @returns the hardcoded prior for the current parameter values depending on the value of \ref informativePriors
	 * */
	double computePrior();

	// cross-match as likelihood

	/**
	 * Computes the cross-match between empirical data and model-generated ratings.
	 * The cross-match test computes the probability of whether multivariate responses of two differently treated subject groups come from the same distribution.
	 * In our case, the first group are empirical individual data and the second group are model-generated individual data.
	 * Part of using the cross-match test \cite Rosenbaum2005 as likelihood function (for posterior estimation).
	 * Calls mahalanobis() and afterwards a condensed version from the R package crossmatch \cite Rcrossmatch (see file `crossmatchtest_a1.R`).
	 * @param modelResults model-generated ratings
	 * @param empiricalData empirical ratings
	 * @returns the probability of the cross-match test
	* */
	double crossmatch(std::vector<RatingDataPoint>* modelResults, std::vector<RatingDataPoint>* empiricalData);

	/**
	 * This method computes the Mahalanobis distance.
	 * It reimplements R's mahalanobis() function, while being more performant.
	 * See R's documentation for further information.
	 *
	 * Part of using the cross-match test \cite Rosenbaum2005 as likelihood function (for posterior estimation).
	 *
	 * The source code is based on https://stackoverflow.com/questions/17617618/dmvnorm-mvn-density-rcpparmadillo-implementation-slower-than-r-package-includi
	 * @param x the mat
	 * @param center
	 * @param icov inverse of the covariance matrix
	 * @return a vector of Mahalanobis distances
	 * */
	arma::vec mahalanobis(const arma::mat* x, const arma::rowvec* center, const arma::mat* icov);

	// misc methods:

	/**
	 * Checks whether all parameters are within their boundaries. See \ref setMethod() for parameter ranges.
	 * @param throwException if true, the method throws a std::runtime_error() when a parameter is violating its boundary
	 * @returns true when all parameters are in their boundaries, false otherwise
	 * */
	 bool areParamsInBoundaries(bool throwException);

	/**
	 * Sets the limits of the \ref display as specified by the user.
	 * If the limits were not specified, appropriate limits are computed based on the RO(s) and LO(s).
	 * Therefore, LO(s) and RO(s) must be set first!
	 * This method also sets the variable \ref printRatings.
	 * @param widthLimitLow the lowest x coordinate for the \ref display. It is computed, if this parameter is set to std::numeric_limits<int>::min()
	 * @param widthLimitHigh the highest x coordinate for the \ref display. It is computed, if this parameter is set to std::numeric_limits<int>::max()
	 * @param heightLimitLow the lowest y coordinate for the \ref display. It is computed, if this parameter is set to std::numeric_limits<int>::min()
	 * @param heightLimitHigh the highest y coordnnate for the \ref display. It is computed, if this parameter is set to std::numeric_limits<int>::max()
	 * @param printRs whether the ratings should be printed
	 * */
	void setLimits(int widthLimitLow, int widthLimitHigh, int heightLimitLow, int heightLimitHigh, bool printRs);

	/**
	 * checks (and eventually changes) the limits of the \ref display given some LOs
	 * @param LOs a vector of RatingDataPoints with LOs that should fit in the display.
	 * @param margin a fixed amount of "pixels" that is added to every side of the \ref display to avoid rounding issues.
	 * */
	void checkLimits(std::vector<RatingDataPoint> LOs, double margin);

	/**
	 * initializes all internal arrays/vectors with their needed size
	 * */
	void initializeArrays();

	/**
	 * This function prints the current state of the model (parameters, used ROs / LOs) to the file sl-model-errors.txt.
	 * @param errorMsg a message to be included to the file and printed out on std::cerr
	 * @param nameOfFunction the function in which the error occured. Will be printed in the file and to std::cerr.
	 * @param currentInput a RatingDataPoint with which the error occured.
	 * @param critical if true, a std::runtime_error() with the errorMsg is thrown stopping the program
	 * */
	void error(std::string errorMsg, std::string nameOfFunction, RatingDataPoint currentInput, bool critical);

	// variables

	// core AVS variables:

	/** canonical upright. Vector sum is compared with this, if model is AVS and preposition is above.*/
	Direction upward = Direction(0.0, 1.0);

	/** canonical downward. In the rAVS-variations, the vector is compared with this. */
	Direction downward = Direction(0.0, -1.0);

	/** this is either upwards or downwards, depending on the model in use and the preposition.*/
	Direction canonicalOrientation;

	/** the number of ROs that are used. Used in \ref initializeArrays() */
	unsigned int nrOfROs;

	/** the method / model that is in use.  */
	AVSmethod method;

	/** a vector of all available models */
	std::vector<AVSmethod> models {BB, PC, PC_BB, originalAVS,
		fAVS, moveFocus, focusOnlyAtFunction, attentionalSwitch,
		rAVS_comb, rAVS_prox, rAVS_COM, rAVS_w_comb, rAVS_multiple, rAVS_abs_middle, rAVS_CoO,
		AVS_BB, AVSr};

	// rating variables

	/** minimum of the rating scale */
	unsigned int minRating;

	/** maximum of the rating scale */
	unsigned int maxRating;

	/**
	 * This variable contains the empirical rating data for ROs and LOs in the form of a \ref RatingDataPoint.
	 * See \ref RatingDataPoint for detailed information.
	 * */
	std::vector<RatingDataPoint> empiricalData;

	/**
	 * Used to store probabilities of ratings as computed by \ref rateProbs()
	 * The size of this vector depends on the number of possible ratings (i.e., on the rating scale).
	 * */
	std::vector<double> ratingsProbs;

	/** In this variable the computed mean ratings are stored. */
	std::vector<double> computedRatings;

	/** In this variable generated data points incl. all information are stored. */
	std::vector<RatingDataPoint> generatedRatings;

	// lookup-tables to cache intermediate computation of points on the RO / LO

	/** LUT of proximal points to speed-up computation */
	std::map<unsigned int, Point> proximalPoints;

	/** LUT of attentional foci on the top of the RO (i.e., for superior prepositions) to speed-up computation */
	std::map<AVSmethod, std::map<unsigned int, std::map<unsigned int, std::map<unsigned int, Point> > > > fociAbove;

	/** LUT of attentional foci on the bottom of the RO (i.e., for inferior prepositions) to speed-up computation */
	std::map<AVSmethod, std::map<unsigned int, std::map<unsigned int, std::map<unsigned int, Point> > > > fociBelow;

	/** LUT of attentional foci on the LO to speed-up computation */
	std::map<AVSmethod, std::map<unsigned int, std::map<unsigned int, std::map<unsigned int, Point> > > > fociLO;

	/** LUT of attentional foci on the functional part to speed-up computation */
	std::map<AVSmethod, std::map<unsigned int, std::map<unsigned int, std::map<unsigned int, Point> > > > fociFunction;

	/** a vector of strings that contain file names / paths to functional parts. Only used to compute the \ref amountOfFoci */
	std::vector<std::string> functionalFileNames;

	/** if one RO with multiple functional parts is used (e.g., a piggy bank with different positions of the slot), this value is bigger than one */
	unsigned int amountOfFoci;

	/** this map maps the path of a functional file to the corresponding focus-index */
	std::map<std::string, unsigned int> funcFileToFocusIdx;

	// model parameter variables:

	/** In this variable, the results of the parameter estimation (i.e., the best parameters found) in a human readable form are stored. */
	std::string resultsPE;

	/** A counter of how many times parameter values that are out of the boundaries are sampled. */
	unsigned int outOfBounds;

	/** A vector of all currently used parameters. */
	std::vector<ModelParameter*> paramList;

	/**
	 * Attentional width parameter \f$ \lambda \f$.
	 * Minimal value of \ref lambda used e.g. by \ref parameterEstimation() currently hardcoded as 0.001 in \ref setMethod().
	 * Maximal value of \ref lambda used e.g. by \ref parameterEstimation() currently hardcoded as 5.0 in \ref setMethod().
	 */
	ModelParameter lambda;

	/**
	 * slope parameter for \ref g().
	 * Codes changes per radian, not per degree (different from \cite Regier2001)!
	 * Minimal value of \ref slope used e.g. by \ref parameterEstimation() currently hardcoded as \f$- 1.0 / (45.0  * \frac{\pi}{180.0})\f$ in \ref setMethod().
	 * Maximal value of \ref slope used e.g. by \ref parameterEstimation() currently hardcoded as 0.0 in \ref setMethod().
	 * */
	ModelParameter slope;

	/**
	 * intercept parameter for \ref g().
	 * Minimal value of \ref intercept used e.g. by \ref parameterEstimation() currently hardcoded as 0.7 in \ref setMethod().
	 * Maximal value of \ref intercept used e.g. by \ref parameterEstimation() currently hardcoded as 1.3 in \ref setMethod().
	 * */
	ModelParameter intercept;

	/**
	 * highgain parameter for \ref heightFunction().
	 * Minimal value of \ref highgain used e.g. by \ref parameterEstimation() currently hardcoded as 0.0 in \ref setMethod().
	 * Maximal value of \ref highgain used e.g. by \ref parameterEstimation() currently hardcoded as 10.0 in \ref setMethod().
	 * */
	ModelParameter highgain;

	/**
	 * \f$\alpha\f$, weights the importance of center-of-mass orientation vs proximal orientation. Only used by rAVS-w-comb and the PC models.
	 * Minimal value of \ref alpha used e.g. by \ref parameterEstimation() currently hardcoded as 0.001 in \ref setMethod().
	 * Maximal value of \ref alpha used e.g. by \ref parameterEstimation() currently hardcoded as 5.0 in \ref setMethod().
	 * */
	ModelParameter alpha;

	/**
	 * Functional strength parameter \f$\varphi\f$. Only used by model variations that consider functional ROs.
	 * Minimal value of \ref phi used e.g. by \ref parameterEstimation() currently hardcoded as 0.0 in \ref setMethod().
	 * Maximal value of \ref phi used e.g. by \ref parameterEstimation() currently hardcoded as 2.0 in \ref setMethod().
	 * */
	ModelParameter phi;

	/**
	 * One of the two parameters of the BB model.
	 * Minimal value of \ref lrgain used e.g. by \ref parameterEstimation() currently hardcoded as 0.0 in \ref setMethod().
	 * maximal value of \ref lrgain used e.g. by \ref parameterEstimation() currently hardcoded as 5.0 in \ref setMethod().
	 * */
	ModelParameter lrgain;

	/**
	 * One of the two parameters of the BB model.
	 * Minimal value of \ref lrexp used e.g. by \ref parameterEstimation() currently hardcoded as 0.0 in \ref setMethod().
	 * Maximal value of \ref lrexp used e.g. by \ref parameterEstimation() currently hardcoded as 5.0 in \ref setMethod().
	 * */
	ModelParameter lrexp;

	/**
	 * Thresholds for ordinal regression component.
	 * The size of this vector depends on the number of possible ratings (i.e., on the rating scale).
	 * */
	std::vector<ModelParameter> thresholds;

	/**
	 * sigma of latent normal distribution for the ordinal regression component.
	 * */
	ModelParameter sigmaOrd;

	// variables for ordinal / posterior estimation

	/** if true, probabilities of each rating are computed (in contrast to a single `mean` rating only) */
	bool ordinal;

	/** If true, the ordinal parameters (sigmaOrd, thresholds) are not changed during parameter estimation. */
	bool noChangeOrdinal;

	/** if true, parameterEstimation() estimates the posterior distribution instead of finding best fitting parameters. */
	bool posterior;

	/**
	 * This variable has one of these three values
	 * - "uninf"
	 * - "regier2001"
	 * - "PSP"
	 * Based on these values, corresponding informative priors are used,
	 * see source code of computePrior() for hardcoded distributions.
	 * */
	std::string informativePriors;

	/** If true, only the prior distribution is computed and the likelihood computation is ignored. */
	bool priorOnly;

	/** If true, only the likelihood is computed and the prior distribution is ignored. */
	bool likelihoodOnly;

	// variables for parameter / posterior estimation

	/**
	 * In this variable, parameterEstimation() saves the values (and names) of all parameters visited during the estimation.
	 * In particular valuable when estimating the posterior distribution (which is in this variable).
	 * */
	std::vector<std::map<std::string, float> > visitedParams;

	/**
	 * Table that contains empirical and model-generated data. Used as input to compute the cross-match between the two.
	 *
	 * Table looks like this:
	 *
	 *      type  LO1 LO2 LO3 LO4 LO5 ...
	 *      emp   8   8   5   2   2   ...
	 *      emp   7   8   4   3   2   ...
	 *      emp   9   8   7   1   1   ...
	 *      model 7   8   5   2   1   ...
	 *      model 7   8   5   2   1   ...
	 *      model 7   8   5   2   1   ...
	 * */
	std::vector<std::vector<double> > dataMatrixForCrossmatch;

	/** Lookup-table that stores the probabilities (double) for given number of cross-matches (int). */
	std::map<int, double> crossmatchTable;

	/**
	 * This variable controls the adaption of the MCMC algorithm in parameterEstimation().
	 * Depending on its value, the "temperature" of the proposal function is adapted.
	 * */
	double tempChange;

	#if VERBOSITY >= 0
	/** A counter that counts how many likelihoods were computed (for computing a mean processing speed per LH computation). */
	int iterLH = 0;
	/** Counts the number of seconds needed for likelihood computations. */
	double allSeconds = 0.0;
	/** Holds the running mean of the standard error of the likelihood estimation. */
	double meanSE = 0.0;
	#endif

	// visualization variables:

	/**
	 * This variable contains the information that will be plotted using \ref Visualizer::visualizeRatingDist().
	 * The first vector contains the empirical proportions, the second vector contains the estimated probabilities from the model
	 * **/
	std::pair<std::vector<double>, std::vector<double> > ratingDist;

	/** If this variable is true, all computed ratings are printed */
	bool printRatings;

	/** Used to store spatial templates (i.e., ratings) or attentional distribution. Used by the \ref Visualizer. */
	std::vector<std::vector<double> > display;

	/** Lowest x-coordinate of the \ref display. */
	int xOrigin;

	/** Lowest y-coordinate of the \ref display. */
	int yOrigin;

	/** Width of the \ref display. */
	int width;

	/** Height of the \ref display. */
	int height;

	/** In this vector, vectors (starting point, direction) that should be visualized by the \ref Visualizer are stored. */
	std::vector<std::pair<Point, Vector> > vectorsToVisualize;

	/** the attentional focus to be visualized with the \ref Visualizer */
	Point focusToVisualize;

	/** a flag to surpress output during spatial template computation */
	bool beVerbose = true;

	// misc variables

	/**
	 * A private instance of a GSL random generator.
	 * Used everywhere, where random numbers are needed.
	 * Only a single instance to avoid sampling equal numbers. */
	std::shared_ptr<gsl_rng> randomGenerator;

	/**
	 * Number of threads to be spawned within model.cpp.
	 * This variable is currently not in use!
	 * */
	unsigned int noOfThreads;
};

#endif /* MODEL_H */

