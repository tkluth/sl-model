/*
 * main.cpp
 *
 * This file is part of sl-model:
 * https://www.gitlab.com/tkluth/sl-model
 *
 * Copyright(c) 2014-2018 Thomas Kluth <t DOT kluth AT posteo DOT de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 *
 * From October 2014 to September 2018, the
 * development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 *
 */

/** \file main.cpp
 * This file contains the `main()`-function.
 * Parsing of commandline-options is done here and an instance of Model is created with the given options.
 * For a summary of available options see \ref options.
 */

/**
 * the higher this value, the more output
 * if set to 0 only important messages will be printed
 * this debug switch is placed in every file and only valid for one file!
 */
#define VERBOSITY 1

#include "model.h"
#include "visualizer.h"
#include "parser.h"
#include "comparison.h"
#include "utilities.h"

#include <iostream>
/* For PATH_MAX (the maximum length of a binaryPath). */
#include <sys/param.h>
#include <getopt.h>
#include <stdio.h>
#include <cstring>
#include <stdlib.h>
//measure time:
#include <chrono>

/**
 * The main()-function. Here, the command line options are parsed, appropriate LO(s) and RO(s) are created and the chosen model simulation is called.
 * */
int main(int argc, char **argv) {

	try{
		std::stringstream license;

		license << "sl-model 4.0" << std::endl<< "Copyright © 2014-2018 Thomas Kluth" << std::endl << "This is free software; see the source or the file COPYING for copying conditions. There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. From October 2014 to September 2018, the development of this software was supported by the Excellence Cluster EXC 277 Cognitive Interaction Technology. The Excellence Cluster EXC 277 is a grant of the Deutsche Forschungsgemeinschaft (DFG) in the context of the German Excellence Initiative." << std::endl;

		// create the one embedded R instance, used automagically in Model::R via RInside::instance()
		RInside R(argc, argv);

		// set precision for std::cout to a fixed number of digits
		std::cout.setf(std::ios::fixed, std::ios::floatfield);

		char pathC[PATH_MAX];

		if (realpath(argv[0], pathC) == 0) {
			std::cerr << "realpath failed. " << std::endl;
		}

		char* last_slash = strrchr(pathC, '/');

		if (last_slash) {
			*(last_slash + 1) = '\0';
		}

		std::string binaryPath = std::string(pathC);

		//set the options to default values:

		std::vector<Polygon> cgalLOs;
		std::vector<std::string> referenceObjectFiles;
		std::vector<std::string> locatedObjectFiles;
		std::vector<std::string> functionalfiles;
		std::string singleLO = "";
		std::string datafile = binaryPath;
		std::string resultsfile = binaryPath + "results.txt";

		std::pair<unsigned int, unsigned int> ratingScale = std::pair<unsigned int, unsigned int>(0, 9);
		std::string runtimeRatingScale = "";
		AVSmethod method = rAVS_w_comb;

		bool reverseY = false;
		bool estimateParameters = false;
		bool plotResults = false;
		unsigned int simpleHoldOut = 0;
		unsigned int landscaping = 0;
		unsigned int pbcmInformed = 0;
		unsigned int pbcmUninformed = 0;
		std::pair<AVSmethod, AVSmethod> crossfittingModels = std::pair<AVSmethod, AVSmethod> (INVALID_MODEL, INVALID_MODEL);
		bool visualizeVectors = false;
		double visualizeVectorSum = 0.0;
		bool visualizeAttention = false;
		unsigned int LOindexToVisualize = 0;
		bool flagDused = false;
		bool flagFused = false;
		bool colorplot = true;
		bool printRatings = false;
		bool plot3D = false;
		bool latexplot = false;
		std::string latexFigureName = "figure";
		bool quiet = false;
		unsigned int parameterSpaceSplitMFA = 0;
		bool aggregratePerRO_MFA = false;
		unsigned int noOfThreads = 2;
		bool ordinal = false;
		bool posterior = false;
		std::string informativePriors = "uninf";
		bool priorOnly = false;
		bool likelihoodOnly = false;
		bool noChangeOrdinal = false;
		unsigned int noOfChains = 1;

		bool compareModelsFlagSet = false;
		bool flagMSet = false;

		double zoom = 1.0;

		// AVS fit experiment 7 Regier & Carlson, 2001:
		double lambda = 0.512;
		double slope = -0.007 * (180.0 / M_PI); // convert fit in slope _per degree_ (1 / degree -> -0.007) to fit in slope _per radian_ (1 / (degree * (pi / 180)) = slope * (180 / pi) )
		double intercept = 1.224;
		double highgain = 0.002;
		double phi = 1.0;

		double lrgain = 0.109;
		double lrexp = 0.062;

		// parameter for linear weighting function in the rAVS-w-comb model
		double alpha = 0.5;

		// for ordinal mode (individual data), fit is from 101 SHO iterations on all data from Kluth et al. (2018)
		double sigmaOrd = 2.72304;
		// two thresholds less than needed (first and last threshold are fixed) -> K - 1 - 2
		std::vector<double> thresholds = {3.07496, 3.95202, 4.47031, 5.10299, 5.72637, 6.80488};

		bool userProvidedParams = false;
		bool slopePerRadian = false;

		std::string heightLimits = "";
		std::string widthLimits = "";

		std::stringstream helpmessage;

		helpmessage << "Available options: \n" << std::endl << "\t I/O-options (example files can be found in the data-folder):\n"
			<< std::endl
			<< "--located-object, -l x,y \t\t A single, one-point LO position.\n\t\t\t\t\t This option cannot be used with --data, -d.\n"
			<< "--reference-object, -r object.file \t Pass a file containing a CGAL polygon. Will be used as RO.\n\t\t\t\t\t This option cannot be used with --data, -d.\n"
			<< "--functional, -f functional.file \t Pass a file containing a CGAL polygon. Will be used as functional part of the RO.\n\t\t\t\t\t This option cannot be used with --data, -d. \n"
			<< "--data, -d data.file \t\t\t Pass a file (or several files separated by a ,) containing rating results. These\n\t\t\t\t\t results will be used with the LO placements found in 'data.file_LOplacements' and\n\t\t\t\t\t the preposition found in 'data.file_prep'. The RO will be read from 'data.file_RO'.\n\t\t\t\t\t The rating scale will be read from 'data.file_scale'. The functional part of\n\t\t\t\t\t the reference object will be searched in 'data.file_functionalPart'.\n\t\t\t\t\t There exist four shortcuts to the following datasets:\n\t\t\t\t\t -d regier2001 uses all rating data from Regier & Carlson (2001),\n\t\t\t\t\t -d kluth2018_above uses all 'above' ratings from Kluth, Burigo, Schultheis, and Knoeferle (2018),\n\t\t\t\t\t -d kluth2018_below uses all below ratings from Kluth et al. (2018),\n\t\t\t\t\t -d kluth2018_asym uses all ratings from the asymmetrical ROs from Kluth et al. (2018) and finally,\n\t\t\t\t\t -d kluth2018_rect uses all ratings from the rectangular ROs used in Kluth et al. (2018).\n\t\t\t\t\t These shortcuts only work if you start the program from a build directory and\n\t\t\t\t\t the data folder is at '..' (and no files were deleted). See example files in the data-folder or\n\t\t\t\t\t the documentation for Class Parser for the syntax of these files. The -r, -l, and -f flags are ignored. \n"
			<< "--model, -m one int from -3 to 13 \t Choose a model out of these: {BB = -3, PC = -2, PC-BB = -1, AVS = 0, fAVS = 1,\n\t\t\t\t\t moveFocus = 2, focusOnlyAtFunction = 3, attentionalSwitch = 4, rAVS-comb = 5,\n\t\t\t\t\t rAVS-prox = 6, rAVS-c-o-m = 7, rAVS-w-comb = 8, rAVS-multiple (experimental) = 9,\n\t\t\t\t\t rAVS-abs-CoO (experimental) = 10, rAVS-CoO = 11, AVS-BB = 12, AVSr (experimental) = 13}.\n\t\t\t\t\t Default is rAVS-w-comb (8), this is the rAVS model proposed in Kluth, Burigo, and Knoeferle (2017).\n\t\t\t\t\t Models 1-4 take the functionality of objects into account, see Kluth and Schultheis (2014).\n"
			<< "--ordinal\t\t\t\t If this flag is set, the model will compute a probability distribution across\n\t\t\t\t\t all ratings (in contrast to a single floating point value). You need to provide\n\t\t\t\t\t empirical rating distributions and the individual ratings (see `datafile.data_dist` and\n\t\t\t\t\t `datafile.data_ratings` files in the data folder or the Parser documentation). This computation is based on\n\t\t\t\t\t an ordinal regression model and introduces new model parameters: sigmaOrd (SD of the latent\n\t\t\t\t\t normal distribution) and r-1 thresholds, where r is the number of possible ratings in the rating scale.\n\t\t\t\t\t For parameter estimation, the Kullback-Leibler divergence is used as difference\n\t\t\t\t\t function to compare model output (probability distribution over all ratings)\n\t\t\t\t\t with empirical distribution (relative frequencies over all ratings).\n"
			<< "--posterior[=noOfChains]\t\t If this flag is set, the posterior distribution will be estimated with the provided number of\n\t\t\t\t\t MCMC chains (default: 2). This works only in the 'ordinal' mode. The parameter estimation method\n\t\t\t\t\t is adapted to reflect the default Metropolis algorithm with the posterior\n\t\t\t\t\t distribution (i.e., prior distribution multiplied with likelihood function)\n\t\t\t\t\t as objective function. For priors, see --informative-priors. As likelihood function,\n\t\t\t\t\t the cross-match test between the individual ratings and model generated ratings\n\t\t\t\t\t (for each LO, as many ratings as there were participants are generated)\n\t\t\t\t\t is used (Rosenbaum, 2005). Only even numbers of participants are allowed (for performance reasons).\n\t\t\t\t\t Use --threads to control the number of threads R is using (the R package 'crossmatch' is called;\n\t\t\t\t\t thread control might not work on every system)." << std::endl
			<< "--informative-priors\t\t\t If this option is set, the prior distributions for the posterior estimation will be changed from the default \n\t\t\t\t\t 'uninformative' priors (uniform distribution over the range of each parameter) to 'informative' priors:\n\t\t\t\t\t Gaussian distributions with mean and SD which are either based on 101 simple hold-out iterations\n\t\t\t\t\t on the data from Regier & Carlson (2001, --informative-priors=regier2001) or on the \n\t\t\t\t\t closest to empirical patterns as estimated via PSP, threshold t_e = 0.5 (--informative-priors=PSP).\n\t\t\t\t\t Parameter ranges are still in place, though, making these\n\t\t\t\t\t 'informative' priors improper (they do not sum to 1.0)." << std::endl
			<< "--prior-only \t\t\t\t If this flag is set, only the prior distribution is estimated (i.e., the likelihood\n\t\t\t\t\t is ignored). Only considered with --posterior." << std::endl
			<< "--likelihood-only \t\t\t If this flag is set, only the likelihood is estimated (i.e., the prior distribution\n\t\t\t\t\t is ignored). Only considered with --posterior." << std::endl
			<< "--output, -o output.txt \t\t Specify the file, where the results should be stored. Depending on the computations,\n\t\t\t\t\t several files will be saved with names appended to the argument of this option. If you use\n\t\t\t\t\t subfolders, be sure that those exist. Otherwise, the program will fail\n\t\t\t\t\t to save results only after all computations are done; it does neither create subfolders nor\n\t\t\t\t\t does it check for their existence in the beginning! Defaults to `results.txt`.\n"
			<< "--reverse-y, -R \t\t\t If this flag is set, the y-axis will be grow from top to bottom (like in computer graphics).\n\t\t\t\t\t Otherwise the y-axis will grow from bottom to top.\n"
			<< "--scale min:max \t\t\t Set the minimum and maximum of the rating scale. Default is 0:9 (if the options -r, -l, -f are\n\t\t\t\t\t used) or read from the data files (`data.file_scale`).\n"
			<< "--print-ratings \t\t\t Print the ratings of all LOs to the command line.\n" <<

			std::endl << "\t Model parameter options: " << std::endl
			<< std::endl
			<< "--lambda double-value \t\t\t Set parameter lambda. Default depends on the used model, see documentation.\n"
			<< "--slope double-value \t\t\t Set parameter slope. Default depends on the used model, see documentation.\n"
			<< "--slope-per-radian \t\t\t If this flag is set, the provided slope value will be interpreted as 'change per radian' not\n\t\t\t\t\t per degree as in the literature. To convert from 'change per degree' to 'change per radian' multiply\n\t\t\t\t\t with 180/pi. To convert back from 'change per radian' to 'change per degree', multiply with pi/180.\n"
			<< "--intercept double-value \t\t Set parameter intercept. Default depends on the used model, see documentation.\n"
			<< "--highgain double-value \t\t Set parameter highgain. Default depends on the used model, see documentation.\n"
			<< "--phi double-value \t\t\t Set parameter phi. Default depends on the used model, see documentation.\n"
			<< "--alpha double-value \t\t\t Set parameter alpha. Default depends on the used model, see documentation.\n"
			<< "--sigma-ord double-value \t\t Set parameter sigma for the ordinal regression extension (Kluth & Schultheis,\n\t\t\t\t\t 2018). Only considered if --ordinal is set, too. Default: 2.72304.\n"
			<< "--thresholds comma-separated doubles \t Set threshold parameters for the ordinal regression extension (Kluth & Schultheis, 2018). The first and\n\t\t\t\t\t last thresholds are fixed, so you need to provide K-1-2 thresholds (where K is the number of ratings).\n\t\t\t\t\t Only considered if --ordinal is set, too. Default: 3.07496,3.95202,4.47031,5.10299,5.72637,6.80488.\n"
			<< "--lrgain \t\t\t\t Set the lrgain parameter (for the BB model from Regier & Carlson,\n\t\t\t\t\t 2001). Default depends on the used model, see documentation.\n"
			<< "--lrexp \t\t\t\t Set the lrexp parameter (for the BB model from Regier & Carlson,\n\t\t\t\t\t 2001). Default depends on the used model, see documentation.\n"
			<< std::endl
			<< "--parameter-estimation, -p \t\t Estimate parameters with the metropolis algorithm. RO(s), LO(s) and data must be given.\n"
			<< "--simple-hold-out[=iterations] \t\t Use the simple hold out (SHO) method to compare models (see Schultheis et al.\n\t\t\t\t\t 2013). Optionally, you can provide the number of iterations.\n"
			<< "--compare-models int-value,int-value\t Sets the two models to be compared with landscaping, or PBCM (data informed and uniformed versions).\n\t\t\t\t\t Integer values are interpreted as model variations, see -m flag for a list.\n"
			<< "--landscaping[=integer] \t\t Produces a landscape of two models (need to be set via the flag --compare-models) with the given amount of\n\t\t\t\t\t data points (default: 100 data points). GOF (nRMSE) is used for fitting the generated data; \n\t\t\t\t\t see Navarro et al. (2003, 2004) for details on this method. This method\n\t\t\t\t\t is the same as the data uninformed parametric bootstrap crossfitting method\n\t\t\t\t\t but adds noise to the generated ratings, at the moment this is hardcoded Gausssian noise with SD 0.3).\n"
			<< "--pbcm-uninformed[=integer] \t\t Applies the data uninformed version of the parametric bootstrap crossfitting method (PBCM) for two models\n\t\t\t\t\t (need to be set via the flag --compare-models) with the given amount of\n\t\t\t\t\t data points (default: 100 data points; see Wagenmakers et al. 2004). This is\n\t\t\t\t\t basically the same as --landscaping, but without adding noise to the generated ratings.\n"
			<< "--pbcm-informed[=integer] \t\t Applies the data informed version of the parametric bootstrap crossfitting method (PBCM) for two models\n\t\t\t\t\t (need to be set via the flag --compare-models) with the given amount of\n\t\t\t\t\t data points (default: 100 data points; see Wagenmakers et al. 2004).\n"
			<< "--mfa[=integer-value] \t\t\t Starts a grid search with the specified number of intervals (default: 50) for each parameter, i.e., model\n\t\t\t\t\t output for the whole parameter space are computed. Thereafter, this output used to compute the phi value\n\t\t\t\t\t for the Model Flexibility Analysis (MFA) proposed by Veksler et al. (2015). Be careful, this needs a lot of\n\t\t\t\t\t memory (depending on the number of intervals, obviously)!\n"
			<< "--mfa-mean \t\t\t\t This option is only considered for the MFA Computation. If this flag is set,\n\t\t\t\t\t the mean of all ratings for LOs above/below the same RO is computed to reduce the dimensionality of\n\t\t\t\t\t the data space. If this flag is not set, all single LO ratings are considered.\n "
			<< "--threads integer-value \t\t This option controls the number of threads. This works only, if the\n\t\t\t\t\t program is compiled with OpenMP support. Default is 2.\n" <<

			std::endl << "\t Visualization options: " << std::endl
			<< std::endl
			<< "--plot \t\t\t\t\t Plot results (the spatial template or, if ordinal is set, plot of empirical and estimated\n\t\t\t\t\t proportions/probabilities of rating distribution). If not set, you can get the plot by manually running the\n\t\t\t\t\t command `gnuplot plot.command`. Defaults to false if a model evaluation of any kind is started.\n"
			<< "--visualize-vectors, -v \t\t Visualize almost all weighted vectors.\n"
			<< "--visualize-vector-sum factor, -V factor If this is set to a value bigger than zero, the vector sum is visualized (i.e., the final direction).\n\t\t\t\t\t Length of the direction vector is scaled with this parameter and\n\t\t\t\t\t the height of the display (but the direction does not change, only the length of the vector).\n"
			<< "--visualize-attention, -a \t\t Visualize attentional distribution.\n"
			<< "--LO-Index, -i integer-value >= 0 \t If more than one LO is given (via the -d option), this option specifies which LO to use for visualizing.\n\t\t\t\t\t All other LOs are also shown but the visible computation (attentional distribution, vector(s),\n\t\t\t\t\t spatial template) is only done with one LO. Default is 0 (i.e., the first LO).\n"
			<< "--grey \t\t\t\t\t Visualize in greyscale/disable color.\n"
			<< "-3 \t\t\t\t\t Visualize the spatial template in 3D.\n"
			<< "--latex[=filename] \t\t\t Use gnuplots epslatex terminal. Beware of the = sign after the option! Output will be saved in\n\t\t\t\t\t img/<filename>.tex and img/<filename>.eps. Furthermore, the gnuplot commands and result files\n\t\t\t\t\t are saved as <filename>_plot.command and <filename>_results.txt[.vectors]. Thus, you should be able to\n\t\t\t\t\t get the same plot with 'gnuplot <filename>_plot.command' again. Moreover, you may easily adopt\n\t\t\t\t\t the plot by editing this plot.command file. Default <filename> is 'figure'.\n"
			<< "--zoom, -z scalefactor \t\t\t The referenceObject and the functional-part are scaled with this factor. Default: 1.0.\n"
			<< "--width, -x lowestX:highestX \t\t Specify the width limits of the display.\n"
			<< "--height, -y lowestY:highestY \t\t Specify the height limits of the display.\n"
			<< "--quiet \t\t\t\t Suppresses all text output, only saving a simple text file with ratings (the one\n\t\t\t\t\t specified with the --output flag; default: results.txt) and not starting gnuplot or R.\n\t\t\t\t\t Useful if this program is called by another program (e.g., GNU octave)."

			<< std::endl << "--help, -h \t\t\t\t Print this help message and exit.\n";

		int c;
		opterr = 0;

		while (1) {
			static struct option longOptions[] = {
				{"reference-object", required_argument, 0, 'r' },
				{"functional", required_argument, 0, 'f' },
				{"data", required_argument, 0, 'd' },
				{"reverse-y", no_argument, 0, 'R' },
				{"model", required_argument, 0, 'm' },
				{"output", required_argument, 0, 'o' },
				{"parameter-estimation", no_argument, 0, 'p' },
				{"located-object", required_argument, 0, 'l' },
				{"visualize-attention", no_argument, 0, 'a' },
				{"visualize-vectors", no_argument, 0, 'v' },
				{"visualize-vector-sum", required_argument, 0, 'V' },
				{"zoom", required_argument, 0, 'z' },
				{"width", required_argument, 0, 'x' },
				{"height", required_argument, 0, 'y' },
				{"LO-index", required_argument, 0, 'i' },
				{"help", no_argument, 0, 'h' },
				{"lambda", required_argument, 0, 1000 },
				{"slope", required_argument, 0, 1001 },
				{"intercept", required_argument, 0, 1002 },
				{"highgain", required_argument, 0, 1003 },
				{"phi", required_argument, 0, 1004 },
				{"alpha", required_argument, 0, 1005},
				{"lrgain", required_argument, 0, 1006},
				{"lrexp", required_argument, 0, 1007},
				{"sigma-ord", required_argument, 0, 1008},
				{"thresholds", required_argument, 0, 1009},
				{"latex", optional_argument, 0, 1010},
				{"scale", required_argument, 0, 1011},
				{"landscaping", optional_argument, 0, 1012},
				{"quiet", no_argument, 0, 1013},
				{"grey", no_argument, 0, 1014},
				{"print-ratings", no_argument, 0, 1015},
				{"compare-models", required_argument, 0, 1016},
				{"pbcm-informed", optional_argument, 0, 1017},
				{"pbcm-uninformed", optional_argument, 0, 1018},
				{"simple-hold-out", optional_argument, 0, 1019},
				{"mfa", optional_argument, 0, 1020},
				{"mfa-mean", no_argument, 0, 1021},
				{"threads", required_argument, 0, 1022},
				{"ordinal", no_argument, 0, 1023},
				{"posterior", optional_argument, 0, 1024},
				{"informative-priors", required_argument, 0, 1025},
				{"prior-only", no_argument, 0, 1026},
				{"likelihood-only", no_argument, 0, 1027},
				{"no-change-ordinal", no_argument, 0, 1028},
				{"plot", no_argument, 0, 1029},
				{"slope-per-radian", no_argument, 0, 1030},
				{ 0, 0, 0, 0 }
			};

		/* getopt_long stores the option index here. */
		int optionIndex = 0;
		c = getopt_long(argc, argv, "r:f:d:Rl:vV:am:z:x:y:i:ho:p3", longOptions, &optionIndex);

		if (c == -1) {
				break;
		}

		switch (c) {
			case 'r':
				referenceObjectFiles.push_back(std::string(optarg));
				break;

			case '3':
				plot3D = true;
				break;

			case 'f':
				functionalfiles.push_back(std::string(optarg));
				flagFused = true;
				break;

			case 'p':
				estimateParameters = true;
				break;

			case 'o':
				resultsfile = std::string(optarg);
				break;

			case 'd':
				datafile = std::string(optarg);
				flagDused = true;
				break;

			case 'R':
				reverseY = true;
				break;

			case 'l':
				singleLO = optarg;
				break;

			case 'v':
				visualizeVectors = true;
				break;

			case 'V':
				visualizeVectorSum = atof(optarg);
				break;

			case 'a':
				visualizeAttention = true;
				break;

			case 'm':
				method = intToMethod(atoi(optarg));
				crossfittingModels.second = intToMethod(atoi(optarg));
				flagMSet = true;
				break;

			case 'z':
				zoom = atof(optarg);
				break;

			case 'x':
				widthLimits = optarg;
				break;

			case 'y':
				heightLimits = optarg;
				break;

			case 'i':
				LOindexToVisualize = (unsigned int) atoi(optarg);
				break;

			case 'h':
				std::cout << license.str() << std::endl;
				std::cout << helpmessage.str() << std::endl;
				return 0;
				break;

			case 1000:
				lambda = atof(optarg);
				userProvidedParams = true;
				break;

			case 1001:
				// default to parse slope as change per radian; thus convert it here
				// if user sets flag --slope-per-radian, this conversion is reversed
				slope = atof(optarg) * (180.0 / M_PI);
				userProvidedParams = true;
				break;

			case 1002:
				intercept = atof(optarg);
				userProvidedParams = true;
				break;

			case 1003:
				highgain = atof(optarg);
				userProvidedParams = true;
				break;

			case 1004:
				phi = atof(optarg);
				userProvidedParams = true;
				break;

			case 1005:
				alpha = atof(optarg);
				userProvidedParams = true;
				break;

			case 1006:
				lrgain = atof(optarg);
				userProvidedParams = true;
				break;

			case 1007:
				lrexp = atof(optarg);
				userProvidedParams = true;
				break;

			case 1008:
				sigmaOrd = atof(optarg);
				userProvidedParams = true;
				break;

			case 1009: {
				thresholds.clear();
				std::stringstream given;
				given << optarg;
				std::string delimiter = ",";
				size_t currentPos = 0;
				#pragma GCC diagnostic push
				#pragma GCC diagnostic ignored "-Wsign-conversion"
				size_t nextPos = -1;
				#pragma GCC diagnostic pop
				do {
					currentPos = nextPos + 1;
					nextPos = given.str().find_first_of(delimiter, currentPos);
					thresholds.push_back(atof(given.str().substr(currentPos, nextPos - currentPos).c_str()));
				} while (nextPos != std::string::npos);
				userProvidedParams = true;
				break;
			}

			case 1010:
				latexplot = true;
				if (optarg != 0) { // a figure name was given
					latexFigureName = optarg;
				}
				break;

			case 1011:
				runtimeRatingScale = optarg;
				break;

			case 1012:
				landscaping = 100;
				if(optarg != 0){
					landscaping = (unsigned int) atoi(optarg);
				}
				break;

			case 1013:
				quiet = true;
				break;

			case 1014:
				colorplot = false;
				break;

			case 1015:
				printRatings = true;
				break;

			case 1016: {
				std::string modelString1;
				std::string modelString2;
				std::stringstream argument;
				argument << optarg;
				getline(argument, modelString1, ',');
				getline(argument, modelString2, ',');
				int modelNo1 = stoi(modelString1);
				int modelNo2 = stoi(modelString2);
				crossfittingModels.first = intToMethod(modelNo1);
				crossfittingModels.second = intToMethod(modelNo2);
				compareModelsFlagSet = true;
				break;
			}

			case 1017:
				pbcmInformed = 100;
				if(optarg != 0){
					pbcmInformed = (unsigned int) atoi(optarg);
				}
				break;

			case 1018:
				pbcmUninformed = 100;
				if(optarg != 0){
					pbcmUninformed = (unsigned int) atoi(optarg);
				}
				break;

			case 1019:
				simpleHoldOut = 101;
				if(optarg != 0){
					simpleHoldOut = (unsigned int) atoi(optarg);
				}
				break;

			case 1020:
				parameterSpaceSplitMFA = 50;
				if (optarg != 0){
					parameterSpaceSplitMFA = (unsigned int) atoi(optarg);
				}
				break;

			case 1021:
				aggregratePerRO_MFA = true;
				break;

			case 1022:
				if (optarg != 0) {
					noOfThreads = (unsigned int) atoi(optarg);
				}
				break;

			case 1023:
				ordinal = true;
				break;

			case 1024:
				posterior = true;
				if (optarg != 0) {
					noOfChains = (unsigned int) atoi(optarg);
				} else {
					noOfChains = 2;
				}
				break;

			case 1025:
				informativePriors = std::string(optarg);
				break;

			case 1026:
				priorOnly = true;
				break;

			case 1027:
				likelihoodOnly = true;
				break;

			case 1028:
				noChangeOrdinal = true;
				break;

			case 1029:
				plotResults = true;
				break;

			case 1030:
				slopePerRadian = true;
				break;

			case '?':
				if (optopt == 'l' || optopt == 'r' || optopt == 'f' || optopt == 'd' || optopt == 'm' || optopt == 'o' || optopt == 'z' || optopt == 'x' || optopt == 'y' || optopt == 'i' || optopt == 'V' || optopt == 1000 || optopt == 1001 || optopt == 1002 || optopt == 1003 || optopt == 1004 || optopt == 1005 || optopt == 1006 || optopt == 1007 || optopt == 1009) {
					if (isprint(optopt)){
						fprintf(stderr, "Option -%c requires an argument. Try --help for more information.\n", optopt);
					} else {
						// long options without short options are not printable (or only the internal number is printed, which is not useful)
						std::cout << "Model parameter options and --scale require an argument. Try --help for more information" << std::endl;
					}
				} else {
					std::cerr << "Unknown option '" << argv[optind - 1] << "'. Try --help for more information.\n";
				}
				return 1;

			default:
				std::cout << optionIndex << " " << c << std::endl;
				printf("%c", c);
				abort();
			}
		}

		// non option parameters
		if (optind < argc) {
			std::cout << "main():\t " << "Found non-option command-line parameters (there should be none): " << std::endl;

			while (optind < argc) {
				printf("%s ", argv[optind++]);
			}

			std::cout << std::endl << "main():\t " << "Quitting because of bad input. Try --help for more information.\n" << std::endl;
			return 0;
		}

		if(!quiet){
			std::cout << license.str() << std::endl;
		}

		if (flagDused && flagFused) {
			throw std::runtime_error("You can't use --functional and --data at the same time. --data sets the functional part according to 'data.file_functionalPart'.");
		}

		if (flagMSet && compareModelsFlagSet) {
			throw std::runtime_error("You can't use the -m flag and --compare-models.");
		}

		if (posterior) {
			if (!ordinal) {
				throw std::runtime_error("Estimation of posterior distribution only works in ordinal mode (see --ordinal).");
			}
		}

		#pragma GCC diagnostic push
		// turn off warnings for exact float comparison for these two cases, where I want exactly this
		// (check against exact default value, to detect whether we have user-provided input)
		#pragma GCC diagnostic ignored "-Wfloat-equal"
		if (thresholds[0] != 3.07496 && !ordinal) {
			throw std::runtime_error("Using --thresholds option makes no sense in non-ordinal mode. Did you forget to set --ordinal?");
		}

		if (sigmaOrd != 2.72304 && !ordinal) {
			throw std::runtime_error("Setting --sigma-ord makes no sense in non-ordinal mode. Did you forget to set --ordinal?");
		}
		#pragma GCC diagnostic pop

		if (informativePriors != "uninf" && ! posterior) {
			std::cout << "WARNING: You have set the informative priors option but not the posterior estimation mode. The informative prior option is not used." << std::endl;
			informativePriors = "uninf";
		}

		if (informativePriors != "uninf" && informativePriors != "regier2001" && informativePriors != "PSP") {
			throw std::runtime_error("--informative-priors takes only one of these three value: --informative-priors=regier2001, --informative-priors=PSP, --informative-priors=uninf (default).");
		}

		if ((priorOnly || likelihoodOnly) && !posterior) {
			throw std::runtime_error("--prior-only or --likelihood-only only make sense with --posterior (although it's not a true posterior distribution then anylonger ...)");
		}

		if (slopePerRadian) {
			// revert conversion from per degree to per radian because user says the slope value was already converted
			slope = slope * (M_PI / 180);
		}

		// set default parameters according to the used model
		// default parameters reflect best fit of models to exp. 7 of Regier & Carlson (2001)
		// as reported in Regier & Carlson(2001); see default values above
		// or as manually fitted with this program
		// for functional models, the phi parameters is from fitting relevant datasets
		// parameters not reset here coincide with the default parameters from above

		if (!userProvidedParams) {
			switch (method) {

				case BB:
					highgain = 0.373;
					break;

				case PC:
					alpha = 0.174;
					intercept = 0.929;
					slope = -0.006 * (180.0 / M_PI);
					highgain = 3.265;
					break;

				case PC_BB:
					alpha = 0.115;
					intercept = 0.909;
					slope = -0.005 * (180.0 / M_PI);
					highgain = 6.114;
					break;

				case originalAVS:
					// everything set above
					break;

				case fAVS:
					phi = 0.6804;
					break;

				case moveFocus:
					phi = 0.6804;
					break;

				case focusOnlyAtFunction:
					phi = 0.6804;
					break;

				case attentionalSwitch:
					phi = 0.6804;
					break;

				case rAVS_comb:
					slope = -0.40107;
					break;

				case rAVS_prox:
					intercept = 1.16886;
					slope = -0.337504;
					highgain = 0.0042564;
					break;

				case rAVS_COM:
					intercept = 0.9735;
					slope = -0.199308;
					highgain = 2.58177;
					break;

				case rAVS_w_comb:
					alpha = 0.284187;
					intercept = 0.97288;
					slope = -0.353709;
					highgain = 2.39325;
					break;

				case rAVS_multiple:
					// everything set above
					break;

				case rAVS_abs_middle:
					// not fully developed yet
					alpha = 5.0;
					intercept = 0.980312;
					slope = -0.216995;
					highgain = 8.59439;
					break;

				case rAVS_CoO:
					alpha = 0.28417;
					intercept = 0.972863;
					slope = -0.353695;
					highgain = 5.32818;
					break;

				case AVS_BB:
					// everything set above
					break;

				case AVSr:
					// not yet fully developed
					break;

				case CROSS:
					break;

				case INVALID_MODEL:
					break;

				default:
					break;
			}
		}

		// read in ROs / LOs and empirical data

		Parser parser = Parser(zoom, reverseY);

		std::vector<RatingDataPoint> empirical(0);

		// simplified located object (i.e., one-point LO) given?
		char delimiter = ',';
		double x = 0.0;
		double y = 0.0;
		std::stringstream singleLOStream(singleLO);
		std::string xs, ys;

		if (getline(singleLOStream, xs, delimiter)) {
			std::stringstream(xs) >> x;
			if (getline(singleLOStream, ys, delimiter)) {
				std::stringstream(ys) >> y;
				if(reverseY){
					y = -y;
				}
				Point vectorDestination = Point(x * zoom, y * zoom);
				std::cout << "main():\t " << "single LO mode: " << vectorDestination << std::endl;

				// create a single RatingDataPoint
				RatingDataPoint fake;
				fake.meanRating = 0.0;
				fake.functionalPart = "fake_functionalPart";
				empirical.push_back(fake);

				if (estimateParameters) {
					std::cout << "main():\t " << "Parameter estimation not possible with a given single LO. Not doing any estimation." << std::endl;
				}

				// writing simple LO to file:
				std::ofstream LOFile;
				std::stringstream buffer;
				LOFile.open("generatedSinglePointLO.poly");
				buffer << "1" << std::endl;
				buffer << x << " " << y << std::endl;
				LOFile << buffer.str();
				LOFile.close();

				locatedObjectFiles.clear();
				locatedObjectFiles.push_back("generatedSinglePointLO.poly");
			}
		}

		if (locatedObjectFiles.size() > 0) {
			// set LO according to file given by -l oder generated by -L
			cgalLOs = parser.parseCGALPolygons(locatedObjectFiles[0], false);
		}

		// a std::list of ReferenceObjects; just used for visualization purposed by Visualizer
		// the ROs are stored with every data point
		std::vector<std::shared_ptr<ReferenceObject> > referenceObjects(0);

		std::vector<Polygon> functionalCGALPolygons;

		for(size_t f = 0; f < functionalfiles.size(); f++){
			functionalCGALPolygons.push_back(parser.parseCGALPolygons(functionalfiles[f], false).front());
		}

		unsigned int nrGivenDatafiles = 0;

		if (flagDused) {

			// shortcuts:
			std::stringstream datafiless;
			if (datafile == "regier2001") {
				std::cout << "main():\tshortcut to datafiles detected. Using all data from Regier & Carlson (2001)." << std::endl;
				datafiless << binaryPath << "../data/regiercarlson2001/exp1_tall_above.data," << binaryPath << "../data/regiercarlson2001/exp1_wide_above.data," << binaryPath << "../data/regiercarlson2001/exp2_tall_above.data," << binaryPath << "../data/regiercarlson2001/exp2_wide_above.data," << binaryPath << "../data/regiercarlson2001/exp3_tall.data," << binaryPath << "../data/regiercarlson2001/exp3_wide.data," << binaryPath << "../data/regiercarlson2001/exp4_up.data," << binaryPath << "../data/regiercarlson2001/exp4_inv.data," << binaryPath << "../data/regiercarlson2001/exp5.data," << binaryPath << "../data/regiercarlson2001/exp6.data," << binaryPath << "../data/regiercarlson2001/exp7.data";
				datafile = datafiless.str();
			} else if(datafile == "kluth2018_all") {
				std::cout << "main():\tshortcut to datafiles detected. Using all data (über and unter, all ROs) from Kluth, Burigo, Schultheis, and Knoeferle (2018)." << std::endl;
				datafiless << binaryPath << "../data/predictions/thin_rectangle.data," << binaryPath << "../data/predictions/thick_rectangle.data," << binaryPath << "../data/predictions/square.data," << binaryPath << "../data/predictions/tall_rectangle.data," << binaryPath << "../data/predictions/C.data," << binaryPath << "../data/predictions/L.data," << binaryPath << "../data/predictions/mC.data," << binaryPath << "../data/predictions/mL.data," << binaryPath << "../data/predictions/thin_rectangle_below.data," << binaryPath << "../data/predictions/thick_rectangle_below.data," << binaryPath << "../data/predictions/square_below.data," << binaryPath << "../data/predictions/tall_rectangle_below.data," << binaryPath << "../data/predictions/C_below.data," << binaryPath << "../data/predictions/L_below.data," << binaryPath << "../data/predictions/mC_below.data," << binaryPath << "../data/predictions/mL_below.data";
				datafile = datafiless.str();
			} else if(datafile == "kluth2018_above") {
				std::cout << "main():\tshortcut to datafiles detected. Using all above (über, all ROs) data from Kluth, Burigo, Schultheis, and Knoeferle (2018)." << std::endl;
				datafiless << binaryPath << "../data/predictions/thin_rectangle.data," << binaryPath << "../data/predictions/thick_rectangle.data," << binaryPath << "../data/predictions/square.data," << binaryPath << "../data/predictions/tall_rectangle.data," << binaryPath << "../data/predictions/C.data," << binaryPath << "../data/predictions/L.data," << binaryPath << "../data/predictions/mC.data," << binaryPath << "../data/predictions/mL.data";
				datafile = datafiless.str();
			} else if(datafile == "kluth2018_below") {
				std::cout << "main():\tshortcut to datafiles detected. Using all below (unter, all ROs) data from Kluth, Burigo, Schultheis, and Knoeferle (2018)." << std::endl;
				datafiless << binaryPath << "../data/predictions/thin_rectangle_below.data," << binaryPath << "../data/predictions/thick_rectangle_below.data," << binaryPath << "../data/predictions/square_below.data," << binaryPath << "../data/predictions/tall_rectangle_below.data," << binaryPath << "../data/predictions/C_below.data," << binaryPath << "../data/predictions/L_below.data," << binaryPath << "../data/predictions/mC_below.data," << binaryPath << "../data/predictions/mL_below.data";
				datafile = datafiless.str();
			} else if (datafile == "kluth2018_asym") {
				std::cout << "main():\tshortcut to datafiles detected. Using all data for asymmetrical ROs (über and unter) from Kluth, Burigo, Schultheis, and Knoeferle (2018)." << std::endl;
				datafiless << binaryPath << "../data/predictions/C.data," << binaryPath << "../data/predictions/L.data," << binaryPath << "../data/predictions/mC.data," << binaryPath << "../data/predictions/mL.data," << binaryPath << "../data/predictions/C_below.data," << binaryPath << "../data/predictions/L_below.data," << binaryPath << "../data/predictions/mC_below.data," << binaryPath << "../data/predictions/mL_below.data";
				datafile = datafiless.str();
			} else if (datafile == "kluth2018_rect") {
				std::cout << "main():\tshortcut to datafiles detected. Using all data for rectangular ROs (über and unter) from Kluth, Burigo, Schultheis, and Knoeferle (2018)." << std::endl;
				datafiless << binaryPath << "../data/predictions/thin_rectangle.data," << binaryPath << "../data/predictions/thick_rectangle.data," << binaryPath << "../data/predictions/square.data," << binaryPath << "../data/predictions/tall_rectangle.data," << binaryPath << "../data/predictions/thin_rectangle_below.data," << binaryPath << "../data/predictions/thick_rectangle_below.data," << binaryPath << "../data/predictions/square_below.data," << binaryPath << "../data/predictions/tall_rectangle_below.data";
				datafile = datafiless.str();
			}

			std::string oneDataFile;
			std::stringstream dataFiles(datafile);

		#if VERBOSITY >= 1
			if(!quiet && referenceObjectFiles.size() > 0){
				std::cout << "main():\t ignoring -r flag and using " << oneDataFile << "_RO as reference object file" << std::endl;
			}
		#endif

			// many LO placements, ignore previously set single LO:
			cgalLOs.clear();
			functionalCGALPolygons.clear();
			locatedObjectFiles.clear();
			referenceObjectFiles.clear();

			// more than one datafile might be given
			while (getline(dataFiles, oneDataFile, ',')) {
				referenceObjectFiles.push_back(oneDataFile + "_RO");

				// use any data-file's functional part as first functional part
				// (will be checked by simpleholdout/parameter estimation)
				std::string functionFile = oneDataFile + "_functionalPart";
				// try to open functional part file:
				std::ifstream testFunctionFile(functionFile.c_str());

				if (testFunctionFile) {
					functionalfiles.push_back(oneDataFile + "_functionalPart");
					functionalCGALPolygons.push_back(parser.parseCGALPolygons(oneDataFile + "_functionalPart", false).front());
				}

				testFunctionFile.close();

				locatedObjectFiles.push_back(oneDataFile + "_LOplacements");

				//push_back new LO placements. push_back() because LOs from previous data-files should not be overwritten
				std::vector<Polygon> tmpPolys = parser.parseCGALPolygons(oneDataFile + "_LOplacements", true);
				cgalLOs.insert(cgalLOs.end(), tmpPolys.begin(), tmpPolys.end());

				ratingScale = parser.parseRatingScale(oneDataFile + "_scale");
				preposition prep = parser.parsePreposition(oneDataFile + "_prep");

				std::vector<double> meanRatings = parser.parseEmpiricalMeanData(oneDataFile);
				std::vector<std::vector<unsigned int> > individualRatings = parser.parseEmpiricalIndividualData(oneDataFile + "_ratings", meanRatings.size());

				if(meanRatings.size() != tmpPolys.size()){
					std::cerr << "main():\tThe number of given LOs is not the same as the number of given ratings. Please check your data files. Stopping the program." << std::endl;
					throw std::runtime_error("Number of LOs and ratings must be equal.");
				}

				std::pair<unsigned char, std::vector<std::vector<unsigned char> > > ratingDists =
						parser.parseEmpiricalRatingDist(oneDataFile + "_dist",
						(unsigned int) meanRatings.size());
				unsigned char noOfParticipants = ratingDists.first;
				std::vector<std::vector<unsigned char> > noOfRatings = ratingDists.second;

				if (posterior && (individualRatings[0].size() != noOfParticipants)) {
					std::cout << individualRatings[0].size() << " != " <<  noOfParticipants << std::endl;
					throw std::runtime_error("There are less individual ratings than the number of participants. Cannot compute posterior distribution.");
				}

				if (posterior && individualRatings.size() != noOfRatings.size()) {
					throw std::runtime_error("Not every LO has individual ratings or too much LOs have individual ratings. Cannot compute posterior distribution");
				}

				if (meanRatings.size() != noOfRatings.size()) {
					throw std::runtime_error("There is a different number of mean ratings and rating distributions.");
				}

				std::vector<std::vector<double> > ratingProportions;
				for (size_t i = 0; i < noOfRatings.size(); ++i) {
					if(ordinal && (noOfRatings[i].size() != ratingScale.second - ratingScale.first + 1)) {
						throw std::runtime_error("You requested the ordinal mode but the number of levels in the rating distribution does not match the rating scale (data not available?).");
					}

					if (std::accumulate(noOfRatings[i].begin(), noOfRatings[i].end(), (unsigned char) 0) != noOfParticipants) {
						std::stringstream errormsg;
						errormsg << "The number of ratings for LO " << i << " in the rating distribution (" << (int) std::accumulate(noOfRatings[i].begin(), noOfRatings[i].end(), (unsigned char) 0) << ") does not match the number of participants ("<< (int) noOfParticipants << ").";
						throw std::runtime_error(errormsg.str());
					}

					double meanR = 0.0;
					if (ordinal) {
						unsigned int discreteRating = ratingScale.first;
						for (size_t r = 0; r < noOfRatings[i].size(); ++r) {
							meanR = meanR + ((double) noOfRatings[i][r] * (double) discreteRating);
							++discreteRating;
						}
						assert(discreteRating == ratingScale.second + 1);
						meanR = meanR / noOfParticipants;
						if (! almostEqual(meanR, meanRatings[i], 1e-6, 1e-6)) {
							std::stringstream errormsg;
							errormsg << "For LO #" << i << ", you provided a different mean rating (" << std::scientific << meanRatings[i] << ") than the mean of the provided rating distribution (" << meanR << "). Quitting." << std::endl;
							throw std::runtime_error(errormsg.str());
						}
					}

					if (posterior) {
						meanR = 0.0;
						for (size_t indiv = 0; indiv < individualRatings[i].size(); ++indiv) {
							meanR = meanR + (double) individualRatings[i][indiv];
						}
						meanR = meanR / (double) individualRatings[i].size();
						if (! almostEqual(meanR, meanRatings[i], 1e-6, 1e-6)) {
							std::stringstream errormsg;
							errormsg << "For LO #" << i << ", you provided a different mean rating (" << std::scientific << meanRatings[i] << ") than the mean of the individual ratings (" << meanR << "). Quitting." << std::endl;
							throw std::runtime_error(errormsg.str());
						}
					}

					std::vector<double> propsLO;
					if (ordinal) {
						for (size_t r = 0; r < noOfRatings[i].size(); ++r) {
							propsLO.push_back((double) noOfRatings[i][r] / (double) noOfParticipants);
						}
					} else {
						// invalid data, vector is too small
						propsLO.push_back(0.0);
					}
					ratingProportions.push_back(propsLO);
				}

				// keep a std::list of only the ROs: just for visualization purposes
				Polygon cgalRO = parser.parseCGALPolygons(oneDataFile + "_RO", false).front();
				referenceObjects.push_back(std::shared_ptr<ReferenceObject>(new ReferenceObject(cgalRO, functionalfiles, functionalCGALPolygons, reverseY, zoom, (unsigned int) nrGivenDatafiles)));

				for (size_t i = 0; i < meanRatings.size(); i++) {
					// nrGivenDatafiles == roIndex
					RatingDataPoint p;
					p.meanRating = meanRatings[i];
					p.individualRatings = individualRatings[i];
					p.noOfParticipants = noOfParticipants;
					p.absoluteNoOfRatings = noOfRatings[i];
					p.proportions = ratingProportions[i];
					p.prep = prep;
					p.functionalPart = oneDataFile + "_functionalPart";
					p.referenceObject = std::shared_ptr<ReferenceObject>(new ReferenceObject(cgalRO, functionalfiles, functionalCGALPolygons, reverseY, zoom, (unsigned int) nrGivenDatafiles));
					empirical.push_back(p);
				}
				++nrGivenDatafiles;
			}
		}

		assert(empirical.size() == cgalLOs.size());

		// put single located object or all LOs into empirical data structure
		for (size_t i = 0; i < empirical.size(); i++) {
			empirical[i].locatedObject = LocatedObject(cgalLOs[i], reverseY, zoom, (unsigned int) i);
			if(!flagDused){
				// single LO mode
				if (referenceObjectFiles.size() > 0) {
					Polygon cgalRO = parser.parseCGALPolygons(referenceObjectFiles[0], false).front();
					empirical[i].referenceObject = std::shared_ptr<ReferenceObject>(new ReferenceObject(cgalRO, functionalfiles, functionalCGALPolygons, reverseY, zoom, 0));
					if (referenceObjects.size() == 0) {
						referenceObjects.push_back(std::shared_ptr<ReferenceObject>(new ReferenceObject(cgalRO, functionalfiles, functionalCGALPolygons, reverseY, zoom, 0)));
					}
					if (functionalfiles. size() == 0) {
						empirical[i].functionalPart = "fake_functionalPart";
					} else {
						empirical[i].functionalPart = functionalfiles[0];
					}
					empirical[i].prep = parser.parsePreposition(referenceObjectFiles[0] + "_prep");

					nrGivenDatafiles = 1;
				} else {
					throw std::runtime_error("You must specify at least one RO. See -r or -d options.");
				}
			}
		}

		std::stringstream widthLimitStream(widthLimits);
		std::string widthLimitLowString, widthLimitHighString;
		int widthLimitLow = std::numeric_limits<int>::min();
		int widthLimitHigh = std::numeric_limits<int>::max();
		std::stringstream heightLimitStream(heightLimits);
		std::string heightLimitLowString, heightLimitHighString;
		int heightLimitLow = std::numeric_limits<int>::min();
		int heightLimitHigh = std::numeric_limits<int>::max();

		if (getline(widthLimitStream, widthLimitLowString, ':')) {
			std::stringstream(widthLimitLowString) >> widthLimitLow;
			//~ widthLimitLow = widthLimitLow; * zoom;

			if (getline(widthLimitStream, widthLimitHighString, ':')) {
				std::stringstream(widthLimitHighString) >> widthLimitHigh;
				//~ widthLimitHigh = widthLimitHigh; * zoom;
			}
		}

		if (getline(heightLimitStream, heightLimitLowString, ':')) {
			std::stringstream(heightLimitLowString) >> heightLimitLow;
			//~ heightLimitLow = heightLimitLow; * zoom;

			if (getline(heightLimitStream, heightLimitHighString, ':')) {
				std::stringstream(heightLimitHighString) >> heightLimitHigh;
				//~ heightLimitHigh = heightLimitHigh; * zoom;
			}
		}

		// set user defined rating scale
		std::stringstream ratingScaleStream(runtimeRatingScale);
		std::string minRating, maxRating;

		if (getline(ratingScaleStream, minRating, ':')) {
			std::stringstream(minRating) >> ratingScale.first;
			if (getline(ratingScaleStream, maxRating, ':')) {
				std::stringstream(maxRating) >> ratingScale.second;
			}
		}

		if (empirical.size() <= 0) {
			throw std::runtime_error("Please set a located object or provide a set of data files.");
		}

		if (ratingScale.first == ratingScale.second) {
			throw std::runtime_error("minRating == maxRating. This does not make sense.");
		}

		if (ordinal && thresholds.size() > 0 && thresholds.size() != ((ratingScale.second - ratingScale.first + 1) - 1 - 2)) {
			throw std::runtime_error("If you provide thresholds, you must provide only K - 1 - 2 thresholds (the first and the last are fixed).");
		}

		if (ordinal) {
			RInside::instance().parseEvalQ("library(crossmatch)");

			std::stringstream rThreads;
			rThreads << "if (library(RhpcBLASctl, logical = TRUE)) { blas_set_num_threads(" << noOfThreads << "); } else { cat('\n\nYou need to install the R package RhpcBLASctl to control the number of threads for the posterior estimation.\n\n') }\n";
			RInside::instance().parseEvalQ(rThreads.str());

			std::stringstream crossmatchwrapper;
			crossmatchwrapper << "source('" << binaryPath << "../src/crossmatchtest_a1.R')";
			RInside::instance().parseEvalQ(crossmatchwrapper.str());
		}

		Model model = Model(empirical, nrGivenDatafiles, ratingScale, widthLimitLow, widthLimitHigh, heightLimitLow, heightLimitHigh, printRatings, method, ordinal, posterior, informativePriors, priorOnly, likelihoodOnly, noChangeOrdinal, noOfThreads, lambda, slope, intercept, highgain, phi, alpha, lrgain, lrexp, sigmaOrd, thresholds);

		// the Comparison class handles landscaping / PBCM, simple hold-out, and MFA
		Comparison comparison = Comparison(model, ratingScale, empirical, nrGivenDatafiles, crossfittingModels.first, crossfittingModels.second, reverseY, printRatings, widthLimitLow, widthLimitHigh, heightLimitLow, heightLimitHigh, noOfThreads);

		if (landscaping == 0 && pbcmInformed == 0 && pbcmUninformed == 0) {
			// no crossfitting method is applied - ignoring any models set by --compare-models
			crossfittingModels.second = method;

		#if VERBOSITY >= 0
			if (!quiet) {
				std::cout << "main():\t " << "functionalfile(s): ";
				for (size_t f = 0; f < functionalfiles.size(); f++) {
					std::cout << functionalfiles[f] << " ";
				}
				std::cout << std::endl;
				std::cout << "main():\t " << "located object file(s): ";
				for (size_t t = 0; t < locatedObjectFiles.size(); t++) {
					std::cout << locatedObjectFiles[t] << " ";
				}
				std::cout << std::endl;
				std::cout << "main():\t " << "Using " << empirical.size() << " data point(s) / located object(s) in total." << std::endl;
				std::cout << "main():\t " << "Using " << nrGivenDatafiles << " data file(s) / RO(s) in total." << std::endl;
				std::cout << "main():\t " << "datafile(s): " << datafile << std::endl;
				std::cout << "main():\t " << "reverse-y?: " << reverseY << std::endl;
				std::cout << "main():\t " << std::setprecision(2) << "zoom: " << zoom << std::endl;
				std::cout << "main():\t " << std::setprecision(0) << "minrating: " << ratingScale.first << std::endl;
				std::cout << "main():\t " << std::setprecision(0) << "maxrating: " << ratingScale.second << std::endl;

				std::cout << "main():\t " << "model: " << enumToString(method) << std::endl;
				if (ordinal) {
					std::cout << "main():\t " << "ordinal regression mode" << std::endl;
				}
			}
		#endif

		} else {
			// crossfitting method
			std::cout << "Comparing models " << enumToString(crossfittingModels.first) << " and " << enumToString(crossfittingModels.second) << " via crossfitting (landscaping / PBCM). Ignoring the -m flag." << std::endl;
			method = crossfittingModels.second;
			comparison = Comparison(model, ratingScale, empirical, nrGivenDatafiles, crossfittingModels.first, crossfittingModels.second, reverseY, printRatings, widthLimitLow, widthLimitHigh, heightLimitLow, heightLimitHigh, noOfThreads);
			// for proper output in the files set method to CROSS
			model.setMethod(CROSS, lambda, slope, intercept, highgain, phi, alpha, lrgain, lrexp, sigmaOrd, thresholds);
		}

		auto start = std::chrono::system_clock::now();

		// TODO (for a future version) this is a just a quick hack to implement both directionalities.
		// if this seems to work: do better
		//~ if{method == AVSr){
			//~ Model model2 = Model(empirical, nrGivenDatafiles, ratingScale, widthLimitLow, widthLimitHigh, heightLimitLow, heightLimitHigh, zoom, printRatings, AVS, ordinal, lambda, slope, intercept, highgain, phi, alpha, lrgain, lrexp);
		//~ }

		if (simpleHoldOut > 0) {
			std::cout << "main():\t " << "computing simple hold out with " << simpleHoldOut << " iterations. Be patient ..." << std::endl;
			comparison.simpleHoldOut(simpleHoldOut);
			std::cout << "main():\t " << "datafile used: " << datafile << std::endl;
		} else if (posterior) {
			std::cout << "main():\t " << "estimating posterior distribution with " << noOfChains << " chains. Be patient ..." << std::endl;
			comparison.posteriorEstimation(empirical, noOfChains, resultsfile);
		} else if (estimateParameters) {
			std::cout << "main():\t " << "parameter estimation" << std::endl;
			model.parameterEstimation(empirical, true);
			if (plotResults) {
				std::cout << "main():\t " << "visualization mode after PE" << std::endl;
				model.visualize(visualizeVectors, visualizeVectorSum, visualizeAttention, LOindexToVisualize);
			}
		} else if (quiet) {
			model.computeOnlyRatings();
		} else if (parameterSpaceSplitMFA > 0){
			comparison.mfa(parameterSpaceSplitMFA, aggregratePerRO_MFA);
		} else if (landscaping > 0) {
			comparison.landscaping(landscaping);
		}	else if (pbcmInformed > 0) {
			comparison.pbcmInformed(pbcmInformed);
		}	else if(pbcmUninformed > 0) {
			comparison.pbcmUninformed(pbcmUninformed);
		} else {
			std::cout << "main():\t " << "visualization mode" << std::endl;
			model.visualize(visualizeVectors, visualizeVectorSum, visualizeAttention, LOindexToVisualize);
		}

		auto end = std::chrono::system_clock::now();
		auto time_diff = end - start;

		if(!quiet){
			std::cout << "main():\t time needed: " << std::setprecision(2) << std::chrono::duration<double>(time_diff).count() << " seconds. (That are " << std::chrono::duration<double, std::ratio<60> >(time_diff).count() << " minutes or " << std::chrono::duration<double, std::ratio<3600> >(time_diff).count() << " hours or " << std::chrono::duration<double, std::ratio<86400> >(time_diff).count() << " days.)" << std::endl << std::endl;
		}

		Visualizer visualizer = Visualizer(model, comparison, empirical, referenceObjects, resultsfile, datafile, referenceObjectFiles, locatedObjectFiles, functionalfiles, plotResults, colorplot, plot3D, latexplot, latexFigureName);
		if (simpleHoldOut > 0) {
			visualizer.saveResultsSHO(noOfThreads);
		} else if (estimateParameters) {
			visualizer.saveResultsPE();
		} else if (posterior) {
			std::cout << "main():\t " << "plotting chains." << std::endl;
			visualizer.visualizePosterior(noOfChains);
		} else if (landscaping > 0 || pbcmInformed > 0 || pbcmUninformed > 0) {
			visualizer.saveModelCrossfitting();
		} else if (quiet) {
			visualizer.saveOctaveVector();
		}	else if (parameterSpaceSplitMFA > 0) {
			visualizer.saveResultsMFA();
		} else {
			if (ordinal) {
				visualizer.visualizeRatingDist();
			} else {
				visualizer.visualizeTemplate();
			}
		}
		if (!quiet) {
			std::stringstream errorfilename;
			errorfilename << "sl-model-errors-" << getpid() << ".txt";
			std::ifstream errorfile(errorfilename.str().c_str());
			if (errorfile.good()) {
				std::cout << "You might want to have a look in the error file " << errorfilename.str() << std::endl;
			} // else the errorfile does not exist
		}

		return EXIT_SUCCESS;
	}
	catch(const std::runtime_error& ex) {
		std::cerr << "ERROR: There was the following error that you can probably fix: " << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
	catch(const std::exception& ex) {
		std::cerr << "ERROR: There was an error that you probably can't fix: " << ex.what() << std::endl;
		std::cerr << "Please try to reproduce and then send a message to: tkluth@cit-ec.uni-bielefeld.de. Thanks!" << std::endl;
		return EXIT_FAILURE;
	}
}
