# Command to Recompute GOF, SHO, MFA, Landscaping

This file contains the commands to recompute all model simulations presented in Kluth, Burigo, Schultheis, and Knoeferle (accepted August 17, 2018, see https://pub.uni-bielefeld.de/record/2930489).
It should be rather self-explanatory. All commands print estimates on when they will be finished, you might need to wait some hours for these estimates to appear.
Most of the simulations should be ready in roughly 1 week (many only take 1 day). However, the landscaping simulations take longer, in particular the landscaping simulations where the AVS or the AVS-BB model is involved.
Generally, the AVS or AVS-BB model take longer than the rAVS or rAVS-CoO model, because of the higher number of vectors to be computed.
Also, the larger the data set, the longer the computation.
As of now, the commands will overwrite already existing results (see the `-o` option). If you change this, make sure that the folder exists. The program will otherwise fail only in the very end as it does not check for this in the beginning! Relatedly, you might want to make sure that your account has write-access after several days of computing. If your system uses a Kerberos authentication, this might not be the case anymore. If so, you will find the program `krenew` useful.
You might want to change the `--threads` option to a sensible value depending on your hardware.

## Simple Hold-Out Computations (Includes Goodness-of-Fit Computations)

```
./sl-model --simple-hold-out=101 --threads=6 -d kluth2018_all -o ../results/finalPaper/GOFSHO/SHO_AVS_all.txt -m 0
./sl-model --simple-hold-out=101 --threads=6 -d kluth2018_asym -o ../results/finalPaper/GOFSHO/SHO_AVS_asym.txt -m 0
./sl-model --simple-hold-out=101 --threads=6 -d kluth2018_rect -o ../results/finalPaper/GOFSHO/SHO_AVS_rect.txt -m 0
./sl-model --simple-hold-out=101 --threads=6 -d regier2001 -o ../results/finalPaper/GOFSHO/SHO_AVS_regier2001.txt -m 0

./sl-model --simple-hold-out=101 --threads=6 -d kluth2018_all -o ../results/finalPaper/GOFSHO/SHO_AVS-BB_all.txt -m 12
./sl-model --simple-hold-out=101 --threads=6 -d kluth2018_asym -o ../results/finalPaper/GOFSHO/SHO_AVS-BB_asym.txt -m 12
./sl-model --simple-hold-out=101 --threads=6 -d kluth2018_rect -o ../results/finalPaper/GOFSHO/SHO_AVS-BB_rect.txt -m 12
./sl-model --simple-hold-out=101 --threads=6 -d regier2001 -o ../results/finalPaper/GOFSHO/SHO_AVS-BB_regier2001.txt -m 12

./sl-model --simple-hold-out=101 --threads=3 -d kluth2018_all -o ../results/finalPaper/GOFSHO/SHO_rAVS_all.txt -m 8
./sl-model --simple-hold-out=101 --threads=3 -d kluth2018_asym -o ../results/finalPaper/GOFSHO/SHO_rAVS_asym.txt -m 8
./sl-model --simple-hold-out=101 --threads=3 -d kluth2018_rect -o ../results/finalPaper/GOFSHO/SHO_rAVS_rect.txt -m 8
./sl-model --simple-hold-out=101 --threads=3 -d regier2001 -o ../results/finalPaper/GOFSHO/SHO_rAVS_regier2001.txt -m 8

./sl-model --simple-hold-out=101 --threads=3 -d kluth2018_all -o ../results/finalPaper/GOFSHO/SHO_rAVS-CoO_all.txt -m 11
./sl-model --simple-hold-out=101 --threads=3 -d kluth2018_asym -o ../results/finalPaper/GOFSHO/SHO_rAVS-CoO_asym.txta -m 11
./sl-model --simple-hold-out=101 --threads=3 -d kluth2018_rect -o ../results/finalPaper/GOFSHO/SHO_rAVS-CoO_rect.txt -m 11
./sl-model --simple-hold-out=101 --threads=3 -d regier2001 -o ../results/finalPaper/GOFSHO/SHO_rAVS-CoO_regier2001.txt -m 11
```

## Model Flexibility Analysis Computations

```
./sl-model --mfa=50 --threads=5 -d kluth2018_all -o ../results/finalPaper/MFA/MFA_AVS_all.txt -m 0
./sl-model --mfa=50 --threads=5 -d kluth2018_asym -o ../results/finalPaper/MFA/MFA_AVS_asym.txt -m 0
./sl-model --mfa=50 --threads=5 -d kluth2018_rect -o ../results/finalPaper/MFA/MFA_AVS_rect.txt -m 0
./sl-model --mfa=50 --threads=5 -d regier2001 -o ../results/finalPaper/MFA/MFA_AVS_regier2001.txt -m 0

./sl-model --mfa=50 --threads=5 -d kluth2018_all -o ../results/finalPaper/MFA/MFA_AVS-BB_all.txt -m 12
./sl-model --mfa=50 --threads=5 -d kluth2018_asym -o ../results/finalPaper/MFA/MFA_AVS-BB_asym.txt -m 12
./sl-model --mfa=50 --threads=5 -d kluth2018_rect -o ../results/finalPaper/MFA/MFA_AVS-BB_rect.txt -m 12
./sl-model --mfa=50 --threads=5 -d regier2001 -o ../results/finalPaper/MFA/MFA_AVS-BB_regier2001.txt -m 12

./sl-model --mfa=50 --threads=3 -d kluth2018_all -o ../results/finalPaper/MFA/MFA_rAVS_all.txt -m 8
./sl-model --mfa=50 --threads=3 -d kluth2018_asym -o ../results/finalPaper/MFA/MFA_rAVS_asym.txt -m 8
./sl-model --mfa=50 --threads=3 -d kluth2018_rect -o ../results/finalPaper/MFA/MFA_rAVS_rect.txt -m 8
./sl-model --mfa=50 --threads=3 -d regier2001 -o ../results/finalPaper/MFA/MFA_rAVS_regier2001.txt -m 8

./sl-model --mfa=50 --threads=3 -d kluth2018_all -o ../results/finalPaper/MFA/MFA_rAVS-CoO_all.txt -m 11
./sl-model --mfa=50 --threads=3 -d kluth2018_asym -o ../results/finalPaper/MFA/MFA_rAVS-CoO_asym.txt -m 11
./sl-model --mfa=50 --threads=3 -d kluth2018_rect -o ../results/finalPaper/MFA/MFA_rAVS-CoO_rect.txt -m 11
./sl-model --mfa=50 --threads=3 -d regier2001 -o ../results/finalPaper/MFA/MFA_rAVS-CoO_regier2001.txt -m 11
```

## Landscaping Computations

```
./sl-model --landscaping=1000 --threads=20 -d kluth2018_asym --compare-models=11,12 -o ../results/finalPaper/landscaping/landscaping_rAVS-CoO_vs_AVS-BB_asym
./sl-model --landscaping=1000 --threads=20 -d regier2001 --compare-models=11,12 -o ../results/finalPaper/landscaping/landscaping_rAVS-CoO_vs_AVS-BB_regier2001

./sl-model --landscaping=1000 --threads=6 -d kluth2018_all --compare-models=8,11 -o ../results/finalPaper/landscaping/landscaping_rAVS_vs_rAVS-CoO_all
./sl-model --landscaping=1000 --threads=6 -d kluth2018_asym --compare-models=8,11 -o ../results/finalPaper/landscaping/landscaping_rAVS_vs_rAVS-CoO_asym

./sl-model --landscaping=1000 --threads=20 -d regier2001 --compare-models=8,0 -o ../results/finalPaper/landscaping/landscaping_rAVS_vs_AVS_regier2001
```

## Parameter Space Partitioning Computations

The scripts in the `src/PSP` folder (`startPSP_*`) assume that you successfully compiled the sl-model binary in `sl-model/build`. To run the PSP analysis, start octave-cli (not tested with MATLAB) and type the name of the corresponding start script. The provided start scripts call the psp() function provided in the `psp.m` file. The `psp.m` and `optsetpsp.m` files were downloaded from http://faculty.psy.ohio-state.edu/myung/personal/psp.html and only slightly adapted to be able to use them with GNU octave (instead of MATLAB).
