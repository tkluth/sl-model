# Commands to Recompute Posterior Distributions

These results were presented in the ACL workshop paper Kluth & Schultheis (2018, see https://pub.uni-bielefeld.de/record/2920199).
The commands below run 4 MCMC chains one after the other and produce output under the file specified with the `-o` option and plots under a newly created `img/` folder at the same location.
Each command takes roughly 12-15 days.
Accordingly, you might want to make sure that your account has write-access after several days of computing. If your system uses a Kerberos authentication, this might not be the case anymore. If so, you will find the program `krenew` useful.
We actually computed only one chain at the same time (`--posterior=1`) and combined 4 chains manually afterwards, because the chains are currently not run in parallel and we were running out of time.
For these simulations, the `--threads` option does only control the number of threads used by `R` (if possible; this depends on whether `R` is configured to use an optimized BLAS implementation).
We identified the option `threads=2` to result in the highest speed.
In addition to the presented results, there are also two commands that (for testing purposes) ignore the prior or the likelihood when estimating the posterior (`--likelihood-only` / `--prior-only`).
If you want to change the model for which you estimate the posterior distribution, change `-m` to another model (see documentation).
The number of samples per chain is hard-coded in `model.cpp`.

```
./sl-model --ordinal --posterior=4 -d kluth2018_asym -m 11 --threads=2 -o ../results/posteriors/aclfinal/asym_rAVS-CoO_no_ordinalParams_uninf.txt --informative-priors=uninf --no-change-ordinal
./sl-model --ordinal --posterior=4 -d kluth2018_asym -m 11 --threads=2 -o ../results/posteriors/aclfinal/asym_rAVS-CoO_no_ordinalParams_likelihood_only.txt --likelihood-only --no-change-ordinal
./sl-model --ordinal --posterior=4 -d kluth2018_asym -m 11 --threads=2 -o ../results/posteriors/aclfinal/asym_rAVS-CoO_no_ordinalParams_uninf_prior_only.txt --informative-priors=uninf --prior-only --no-change-ordinal
```

For comparison, these computations also change the ordinal parameters (which were fixed above and in the paper):

```
./sl-model --ordinal --posterior=4 -d kluth2018_asym -m 11 --threads=2 -o ../results/posteriors/aclfinal/asym_rAVS-CoO_w_ordinalParams_uninf.txt --informative-priors=uninf
./sl-model --ordinal --posterior=4 -d kluth2018_asym -m 11 --threads=2 -o ../results/posteriors/aclfinal/asym_rAVS-CoO_w_ordinalParams_likelihood_only.txt --likelihood-only
./sl-model --ordinal --posterior=4 -d kluth2018_asym -m 11 --threads=2 -o ../results/posteriors/aclfinal/asym_rAVS-CoO_w_ordinalParams_uninf_prior_only.txt --informative-priors=uninf --prior-only
```
